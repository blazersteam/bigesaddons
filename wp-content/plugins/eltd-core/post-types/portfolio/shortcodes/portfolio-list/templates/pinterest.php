<?php // This line is needed for mixItUp gutter ?>
	<article class="eltd-portfolio-item <?php echo esc_attr($categories) ?>">
		<div class="eltd-ptf-wrapper">

			<div class="eltd-ptf-item-image-holder">
				<a href="<?php echo esc_url($item_link); ?>" target="<?php echo esc_attr($item_target); ?>">
					<?php echo get_the_post_thumbnail(get_the_ID(), $thumb_size); ?>
				</a>
			</div>
			<div class="eltd-ptf-item-text-overlay">
				<div class="eltd-ptf-item-text-overlay-inner">
					<div class="eltd-ptf-item-text-holder">
						<<?php echo esc_attr($title_tag) ?> class="eltd-ptf-item-title">
						<a href="<?php echo esc_url($item_link); ?>" target="<?php echo esc_attr($item_target); ?>">
							<?php echo esc_html(get_the_title()); ?>
						</a>
					</<?php echo esc_attr($title_tag) ?>>

					<?php if(!empty($category_html)) : ?>
						<?php echo malmo_elated_get_module_part($category_html); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="eltd-ptf-item-overlay-bg"></div>
		</div>
		</div>
	</article>
<?php // This line is needed for mixItUp gutter ?>