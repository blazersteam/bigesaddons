<div class="eltd-portfolio-filter-holder <?php echo esc_attr($filter_class) ?>">
	<div class="eltd-portfolio-filter-holder-inner">
		<?php
		$rand_number = rand();
		if(is_array($filter_categories) && count($filter_categories)) { ?>
			<ul>
				<?php if($type == 'masonry' || $type == 'pinterest' || $type == 'justified-gallery') { ?>
					<li class="filter" data-filter="*"><span><?php print esc_html__('All', 'eltd_core') ?></span></li>
				<?php } else { ?>
					<li data-class="filter_<?php echo malmo_elated_get_module_part($rand_number); ?>" class="filter_<?php echo malmo_elated_get_module_part($rand_number); ?>" data-filter="all">
						<span><?php print esc_html__('All', 'eltd_core') ?></span></li>
				<?php } ?>
				<?php foreach($filter_categories as $cat) {
					if($type == 'masonry' || $type == 'pinterest' || $type == 'justified-gallery') {
						?>
						<li data-class="filter" class="filter" data-filter=".portfolio_category_<?php echo malmo_elated_get_module_part($cat->term_id); ?>">
						<span>
							<?php echo malmo_elated_get_module_part($cat->name); ?>
						</span>
						</li>
					<?php } else { ?>
						<li data-class="filter_<?php echo malmo_elated_get_module_part($rand_number); ?>" class="filter_<?php echo malmo_elated_get_module_part($rand_number); ?>" data-filter=".portfolio_category_<?php echo malmo_elated_get_module_part($cat->term_id); ?>">
					<span>
						<?php echo malmo_elated_get_module_part($cat->name); ?>
					</span>
						</li>
					<?php }
				} ?>
			</ul>
		<?php } ?>
	</div>
</div>