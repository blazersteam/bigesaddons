<article class="eltd-portfolio-item <?php echo esc_attr($article_masonry_size) ?> <?php echo esc_attr($categories) ?> <?php echo esc_attr($masonry_parallax_behavior) ?>">
	<div class="eltd-portfolio-masonry-item">
		<a class="eltd-portfolio-link" href="<?php echo esc_url($item_link); ?>" target="<?php echo esc_attr($item_target); ?>">
			<div class="eltd-item-image-holder" style="background-image: url('<?php the_post_thumbnail_url( $thumb_size ); ?>')"></div>
			<div class="eltd-ptf-item-text-overlay <?php echo esc_attr($hover_type); ?>">
				<div class="eltd-ptf-item-text-overlay-inner">
					<div class="eltd-ptf-item-text-holder">
						<<?php echo esc_attr($title_tag) ?> class="eltd-ptf-item-title">
						
						<?php echo esc_html(get_the_title()); ?>

						</<?php echo esc_attr($title_tag) ?>>

						<?php if(!empty($category_html)) : ?>
							<?php echo malmo_elated_get_module_part($category_html); ?>
						<?php endif; ?>
					</div>
				</div>
			<div class="eltd-ptf-item-overlay-bg"></div>
		</div>
		</a>
	</div>
</article>
