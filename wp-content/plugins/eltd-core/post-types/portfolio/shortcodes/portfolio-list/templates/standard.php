<?php // This line is needed for mixItUp gutter ?>
	<article class="eltd-portfolio-item mix <?php echo esc_attr($categories) ?>">
		<div class="eltd-ptf-item-image-holder">
			<a href="<?php echo esc_url($item_link); ?>" target="<?php echo esc_attr($item_target); ?>">
				<?php if($use_custom_image_size && (is_array($custom_image_sizes) && count($custom_image_sizes))) : ?>
					<?php echo malmo_elated_generate_thumbnail(get_post_thumbnail_id(get_the_ID()), null, $custom_image_sizes[0], $custom_image_sizes[1]); ?>
				<?php else: ?>
					<?php the_post_thumbnail($thumb_size); ?>
				<?php endif; ?>
			</a>
		</div>
		<div class="eltd-ptf-item-text-holder">
			<<?php echo esc_attr($title_tag) ?> class="eltd-ptf-item-title">
			<a href="<?php echo esc_url($item_link); ?>">
				<?php echo esc_attr(get_the_title()); ?>
			</a>
		</<?php echo esc_attr($title_tag) ?>>

		<?php if(!empty($category_html)) : ?>
			<div class="eltd-ptf-item-category">
				<?php if(!empty($category_html)) : ?>
					<?php echo malmo_elated_get_module_part($category_html); ?>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if($show_excerpt === 'yes') : ?>
			<div class="eltd-ptf-item-excerpt">
				<?php the_excerpt(); ?>
			</div>
		<?php endif; ?>
		</div>
	</article>
<?php // This line is needed for mixItUp gutter ?>