<div class="eltd-testimonial-content <?php echo esc_attr($testimonial_type); ?>">
	<div class="eltd-testimonial-content-inner">
		<div class="eltd-testimonial-text-holder">
			<div class="eltd-testimonial-text-inner <?php echo esc_attr($light_class); ?>">
				<p class="eltd-testimonial-text"><?php echo trim($text) ?></p>
				<?php if($show_author == "yes") { ?>
					<div class="eltd-testimonial-author">
						<?php if($show_author_image == "yes" && $author_image !== '') { ?>
							<div class="eltd-testimonial-image-holder <?php echo esc_attr($light_class); ?>"><?php echo wp_get_attachment_image($author_image); ?></div>
						<?php } ?>
						<h5 class="eltd-testimonial-author-text <?php echo esc_attr($light_class); ?>">
						<span class="eltd-testimonial-author-name <?php echo esc_attr($light_class); ?>"><?php echo esc_attr($author) ?></span>
						<?php if($show_position == "yes" && $job !== '') { ?>
							<span class="eltd-testimonials-job <?php echo esc_attr($light_class); ?>"><?php echo esc_attr($job) ?></span>
						<?php } ?>
						</h5>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
