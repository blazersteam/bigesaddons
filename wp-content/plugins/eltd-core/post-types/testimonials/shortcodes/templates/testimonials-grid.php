<div class="eltd-testimonials-holder-inner">
	<div class="eltd-testimonial-content <?php echo esc_attr($testimonial_type); ?><?php echo esc_attr($columns_number); ?>">
		<div class="eltd-testimonial-text-inner <?php echo esc_attr($light_class); ?>">
			<?php if(has_post_thumbnail($current_id)) { ?>
				<div class="eltd-testimonial-image-holder">
					<?php esc_html(the_post_thumbnail($current_id)) ?>
				</div>
			<?php } ?>
			
			<p class="eltd-testimonial-text <?php echo esc_attr($light_class); ?>"><?php echo trim($text) ?></p>

			<?php if($show_author == "yes") { ?>
				<div class="eltd-testimonial-author">
					<h5 class="eltd-testimonial-author-text <?php echo esc_attr($light_class); ?>">
					<span class="eltd-testimonial-author-name"><?php echo esc_attr($author) ?></span>
					<?php if($show_position == "yes" && $job !== '') { ?>
						<span class="eltd-testimonials-job <?php echo esc_attr($light_class); ?>"><?php echo esc_attr($job) ?></span>
					<?php } ?>
					</h5>
				</div>
			<?php } ?>
		</div>

	</div>
</div>
