<?php

namespace ElatedCore\CPT\Testimonials\Shortcodes;


use ElatedCore\Lib;

/**
 * Class Testimonials
 * @package ElatedCore\CPT\Testimonials\Shortcodes
 */
class Testimonials implements Lib\ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    public function __construct() {
        $this->base = 'eltd_testimonials';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer
     *
     * @see vc_map()
     */
    public function vcMap() {
        if(function_exists('vc_map')) {
            vc_map(array(
                'name'                      => esc_html__('Testimonials', 'eltd_core'),
                'base'                      => $this->base,
                'category'                  => esc_html__('by ELATED', 'eltd_core'),
                'icon'                      => 'icon-wpb-testimonials extended-custom-icon',
                'allowed_container_element' => 'vc_row',
                'params'                    => array(
                    array(
                        'type'        => 'dropdown',
                        'admin_label' => true,
                        'heading'     => esc_html__('Choose Testimonial Type', 'eltd_core'),
                        'param_name'  => 'testimonial_type',
                        'value'       => array(
                            esc_html__('Testimonials Grid', 'eltd_core')   => 'testimonials-grid',
                            esc_html__('Testimonials Slider', 'eltd_core') => 'testimonials-slider',
                            esc_html__('Testimonials Blocks', 'eltd_core') => 'testimonials-blocks'
                        ),
                        'save_always' => true
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Make Blocks Slide?', 'eltd_core'),
                        'param_name'  => 'sliding_blocks',
                        'value'       => array(
                            esc_html__('No', 'eltd_core')  => 'no',
                            esc_html__('Yes', 'eltd_core') => 'yes',
                        ),
                        'save_always' => true,
                        'dependency'  => array('element' => 'testimonial_type', 'value' => array('testimonials-blocks'))
                    ),
                    array(
                        'type'       => 'colorpicker',
                        'heading'    => esc_html__('Blocks Color', 'eltd_core'),
                        'param_name' => 'blocks_color',
                        'value'      => '',
                        'dependency' => array('element' => 'testimonial_type', 'value' => array('testimonials-blocks'))
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Set Dark/Light type', 'eltd_core'),
                        'param_name'  => 'dark_light_type',
                        'value'       => array(
                            esc_html__('Dark', 'eltd_core')  => 'dark',
                            esc_html__('Light', 'eltd_core') => 'light',
                        ),
                        'save_always' => true
                    ),
                    array(
                        'type'        => 'dropdown',
                        'holder'      => 'div',
                        'class'       => '',
                        'heading'     => esc_html__('Number of Columns', 'eltd_core'),
                        'param_name'  => 'number_of_columns',
                        'value'       => array(
                            esc_html__('Two', 'eltd_core')   => '2',
                            esc_html__('Three', 'eltd_core') => '3',
                            esc_html__('Four', 'eltd_core')  => '4'
                        ),
                        'description' => '',
                        'dependency'  => Array(
                            'element' => 'testimonial_type',
                            'value'   => array('testimonials-grid', 'testimonials-blocks')
                        ),
                        'save_always' => true
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Category', 'eltd_core'),
                        'param_name'  => 'category',
                        'value'       => '',
                        'description' => esc_html__('Category Slug (leave empty for all)', 'eltd_core')
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Number', 'eltd_core'),
                        'param_name'  => 'number',
                        'value'       => '',
                        'description' => esc_html__('Number of Testimonials', 'eltd_core')
                    ),
                    array(
                        'type'        => 'dropdown',
                        'admin_label' => true,
                        'heading'     => esc_html__('Show Title', 'eltd_core'),
                        'param_name'  => 'show_title',
                        'value'       => array(
                            esc_html__('Yes', 'eltd_core') => 'yes',
                            esc_html__('No', 'eltd_core')  => 'no'
                        ),
                        'save_always' => true,
                        'description' => ''
                    ),
                    array(
                        'type'        => 'dropdown',
                        'admin_label' => true,
                        'heading'     => esc_html__('Show Author', 'eltd_core'),
                        'param_name'  => 'show_author',
                        'value'       => array(
                            esc_html__('Yes', 'eltd_core') => 'yes',
                            esc_html__('No', 'eltd_core')  => 'no'
                        ),
                        'save_always' => true,
                        'description' => ''
                    ),
                    array(
                        'type'        => 'dropdown',
                        'admin_label' => true,
                        'heading'     => esc_html__('Show Author Image', 'eltd_core'),
                        'param_name'  => 'show_author_image',
                        'value'       => array(
                            esc_html__('Yes', 'eltd_core') => 'yes',
                            esc_html__('No', 'eltd_core')  => 'no'
                        ),
                        'save_always' => true,
                        'dependency'  => array('element' => 'show_author', 'value' => array('yes')),
                        'description' => ''
                    ),
                    array(
                        'type'        => 'dropdown',
                        'admin_label' => true,
                        'heading'     => esc_html__('Show Author Job Position', 'eltd_core'),
                        'param_name'  => 'show_position',
                        'value'       => array(
                            esc_html__('Yes', 'eltd_core') => 'yes',
                            esc_html__('No', 'eltd_core')  => 'no',
                        ),
                        'save_always' => true,
                        'dependency'  => array('element' => 'show_author', 'value' => array('yes')),
                        'description' => ''
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Animation speed', 'eltd_core'),
                        'param_name'  => 'animation_speed',
                        'value'       => '',
                        'description' => esc_html__('Speed of slide animation in miliseconds')
                    )
                )
            ));
        }
    }

    /**
     * Renders shortcodes HTML
     *
     * @param $atts array of shortcode params
     * @param $content string shortcode content
     *
     * @return string
     */
    public function render($atts, $content = null) {

        $args = array(
            'testimonial_type'  => 'testimonials-grid',
            'number'            => '-1',
            'category'          => '',
            'show_title'        => 'yes',
            'show_author'       => 'yes',
            'show_author_image' => 'yes',
            'show_position'     => 'yes',
            'animation_speed'   => '',
            'dark_light_type'   => '',
            'number_of_columns' => '',
            'sliding_blocks'    => '',
            'blocks_color'      => '',
        );

        $params = shortcode_atts($args, $atts);

        //Extract params for use in method
        extract($params);

        $data_attr  = $this->getDataParams($params);
        $query_args = $this->getQueryParams($params);

        $html = '';
        $html .= '<div class="eltd-testimonials-holder clearfix '.$dark_light_type.'">';
        $html .= '<div class="eltd-testimonials '.$testimonial_type.' '.($sliding_blocks == 'yes' ? 'sliding-blocks' : '').'" '.$data_attr.'>';

        query_posts($query_args);
        if(have_posts()) :
            while(have_posts()) : the_post();
                $author       = get_post_meta(get_the_ID(), 'eltd_testimonial_author', true);
                $text         = get_post_meta(get_the_ID(), 'eltd_testimonial_text', true);
                $title        = get_post_meta(get_the_ID(), 'eltd_testimonial_title', true);
                $job          = get_post_meta(get_the_ID(), 'eltd_testimonial_author_position', true);
                $author_image = get_post_thumbnail_id();

                $counter_classes = '';

                if($params['dark_light_type'] == 'light') {
                    $counter_classes .= 'light';
                }

                $params['light_class'] = $counter_classes;

                $params['columns_number'] = $this->getColumnNumberClass($params);

                $params['author']       = $author;
                $params['text']         = $text;
                $params['title']        = $title;
                $params['job']          = $job;
                $params['author_image'] = $author_image;
                $params['current_id']   = get_the_ID();

                $html .= eltd_core_get_shortcode_module_template_part('templates/'.$params['testimonial_type'], 'testimonials', '', $params);

            endwhile;
        else:
            $html .= esc_html__('Sorry, no posts matched your criteria.', 'eltd_core');
        endif;

        wp_reset_query();
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * Generates testimonial data attribute array
     *
     * @param $params
     *
     * @return string
     */
    private function getDataParams($params) {
        $data_attr = '';

        if(!empty($params['animation_speed'])) {
            $data_attr .= ' data-animation-speed ="'.$params['animation_speed'].'"';
        }

        if($params['testimonial_type'] == 'testimonials-blocks' && $params['sliding_blocks'] == 'yes') {
            $data_attr .= ' data-sliding-blocks-columns="'.$params['number_of_columns'].'"';
        }

        return $data_attr;
    }

    /**
     * Generates testimonials query attribute array
     *
     * @param $params
     *
     * @return array
     */
    private function getQueryParams($params) {

        $args = array(
            'post_type'      => 'testimonials',
            'orderby'        => 'date',
            'order'          => 'DESC',
            'posts_per_page' => $params['number']
        );

        if($params['category'] != '') {
            $args['testimonials_category'] = $params['category'];
        }

        return $args;
    }

    private function getColumnNumberClass($params) {

        $columnsNumber = '';
        $columns       = $params['number_of_columns'];

        if($params['testimonial_type'] != 'testimonials-blocks' || $params['testimonial_type'] == 'testimonials-blocks' && $params['sliding_blocks'] != 'yes') {
            switch($columns) {
                case 2:
                    $columnsNumber = ' eltd-two-columns';
                    break;
                case 3:
                    $columnsNumber = ' eltd-three-columns';
                    break;
                case 4:
                    $columnsNumber = ' eltd-four-columns';
                    break;
                default:
                    $columnsNumber = ' eltd-three-column';
                    break;
            }
        }

        return $columnsNumber;
    }

}