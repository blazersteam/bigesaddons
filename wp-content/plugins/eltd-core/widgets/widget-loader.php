<?php

if(!function_exists('malmo_elated_register_widgets')) {

	function malmo_elated_register_widgets() {

		$widgets = array(
			'MalmoElatedFullScreenMenuOpener',
			'MalmoElatedLatestPosts',
			'MalmoElatedSearchOpener',
			'MalmoElatedSideAreaOpener',
			'MalmoElatedStickySidebar',
			'MalmoElatedSocialIconWidget',
			'MalmoElatedSeparatorWidget',
			'MalmoElatedCallToActionButton',
			'MalmoElatedStickySidebar',
			'MalmoElatedHtmlWidget'
		);

		if (malmo_elated_is_woocommerce_installed()) {
			$widgets[]='MalmoElatedWoocommerceDropdownCart';

		}
		foreach($widgets as $widget) {
			register_widget($widget);
		}
	}
}

add_action('widgets_init', 'malmo_elated_register_widgets');