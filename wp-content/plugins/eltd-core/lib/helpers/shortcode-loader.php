<?php

namespace Malmo\Modules\Shortcodes\Lib;

use Malmo\Modules\CallToAction\CallToAction;
use Malmo\Modules\Counter\Countdown;
use Malmo\Modules\Counter\Counter;
use Malmo\Modules\ElementsHolder\ElementsHolder;
use Malmo\Modules\ElementsHolderItem\ElementsHolderItem;
use Malmo\Modules\GoogleMap\GoogleMap;
use Malmo\Modules\Separator\Separator;
use Malmo\Modules\PieCharts\PieChartBasic\PieChartBasic;
use Malmo\Modules\PieCharts\PieChartDoughnut\PieChartDoughnut;
use Malmo\Modules\PieCharts\PieChartDoughnut\PieChartPie;
use Malmo\Modules\PieCharts\PieChartWithIcon\PieChartWithIcon;
use Malmo\Modules\SeparatorWithIcon\SeparatorWithIcon;
use Malmo\Modules\Shortcodes\BlogSlider\BlogSlider;
use Malmo\Modules\Shortcodes\ComparisonPricingTables\ComparisonPricingTable;
use Malmo\Modules\Shortcodes\ComparisonPricingTables\ComparisonPricingTablesHolder;
use Malmo\Modules\Shortcodes\Icon\Icon;
use Malmo\Modules\Shortcodes\ImageGallery\ImageGallery;
use Malmo\Modules\Shortcodes\InfoBox\InfoBox;
use Malmo\Modules\Shortcodes\Process\ProcessHolder;
use Malmo\Modules\Shortcodes\Process\ProcessItem;
use Malmo\Modules\Shortcodes\ProductList\ProductList;
use Malmo\Modules\Shortcodes\SectionSubtitle\SectionSubtitle;
use Malmo\Modules\Shortcodes\SectionTitle\SectionTitle;
use Malmo\Modules\SocialShare\SocialShare;
use Malmo\Modules\TeamList\TeamList;
use Malmo\Modules\Team\Team;
use Malmo\Modules\OrderedList\OrderedList;
use Malmo\Modules\UnorderedList\UnorderedList;
use Malmo\Modules\Message\Message;
use Malmo\Modules\ProgressBar\ProgressBar;
use Malmo\Modules\IconListItem\IconListItem;
use Malmo\Modules\Tabs\Tabs;
use Malmo\Modules\Tab\Tab;
use Malmo\Modules\Clients\Clients;
use Malmo\Modules\Client\Client;
use Malmo\Modules\HidingImages\HidingImages;
use Malmo\Modules\HidingImage\HidingImage;
use Malmo\Modules\VariableWidthSlider\VariableWidthSlider;
use Malmo\Modules\VariableWidthSliderItem\VariableWidthSliderItem;
use Malmo\Modules\PricingTables\PricingTables;
use Malmo\Modules\PricingTable\PricingTable;
use Malmo\Modules\Accordion\Accordion;
use Malmo\Modules\AccordionTab\AccordionTab;
use Malmo\Modules\BlogList\BlogList;
use Malmo\Modules\Shortcodes\Button\Button;
use Malmo\Modules\Blockquote\Blockquote;
use Malmo\Modules\CustomFont\CustomFont;
use Malmo\Modules\Highlight\Highlight;
use Malmo\Modules\VideoButton\VideoButton;
use Malmo\Modules\HoverBanner\HoverBanner;
use Malmo\Modules\Dropcaps\Dropcaps;
use Malmo\Modules\Shortcodes\IconWithText\IconWithText;
use Malmo\Modules\Shortcodes\VerticalSplitSlider\VerticalSplitSlider;
use Malmo\Modules\Shortcodes\VerticalSplitSliderContentItem\VerticalSplitSliderContentItem;
use Malmo\Modules\Shortcodes\VerticalSplitSliderLeftPanel\VerticalSplitSliderLeftPanel;
use Malmo\Modules\Shortcodes\VerticalSplitSliderRightPanel\VerticalSplitSliderRightPanel;

/**
 * Class ShortcodeLoader
 */
class ShortcodeLoader {
	/**
	 * @var private instance of current class
	 */
	private static $instance;
	/**
	 * @var array
	 */
	private $loadedShortcodes = array();

	/**
	 * Private constuct because of Singletone
	 */
	private function __construct() {
	}

	/**
	 * Private sleep because of Singletone
	 */
	private function __wakeup() {
	}

	/**
	 * Private clone because of Singletone
	 */
	private function __clone() {
	}

	/**
	 * Returns current instance of class
	 * @return ShortcodeLoader
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			return new self;
		}

		return self::$instance;
	}

	/**
	 * Adds new shortcode. Object that it takes must implement ShortcodeInterface
	 *
	 * @param ShortcodeInterface $shortcode
	 */
	private function addShortcode(ShortcodeInterface $shortcode) {
		if(!array_key_exists($shortcode->getBase(), $this->loadedShortcodes)) {
			$this->loadedShortcodes[$shortcode->getBase()] = $shortcode;
		}
	}

	/**
	 * Adds all shortcodes.
	 *
	 * @see ShortcodeLoader::addShortcode()
	 */
	private function addShortcodes() {
		$this->addShortcode(new ElementsHolder());
		$this->addShortcode(new ElementsHolderItem());
		$this->addShortcode(new TeamList());
		$this->addShortcode(new Team());
		$this->addShortcode(new Icon());
		$this->addShortcode(new CallToAction());
		$this->addShortcode(new OrderedList());
		$this->addShortcode(new UnorderedList());
		$this->addShortcode(new Message());
		$this->addShortcode(new Counter());
		$this->addShortcode(new Countdown());
		$this->addShortcode(new ProgressBar());
		$this->addShortcode(new IconListItem());
		$this->addShortcode(new Tabs());
		$this->addShortcode(new Tab());
		$this->addShortcode(new Clients());
		$this->addShortcode(new Client());
		$this->addShortcode(new HidingImages());
		$this->addShortcode(new HidingImage());
		$this->addShortcode(new VariableWidthSlider());
		$this->addShortcode(new VariableWidthSliderItem());
		$this->addShortcode(new PricingTables());
		$this->addShortcode(new PricingTable());
		$this->addShortcode(new PieChartBasic());
		$this->addShortcode(new PieChartPie());
		$this->addShortcode(new PieChartDoughnut());
		$this->addShortcode(new PieChartWithIcon());
		$this->addShortcode(new Accordion());
		$this->addShortcode(new AccordionTab());
		$this->addShortcode(new BlogList());
		$this->addShortcode(new Button());
		$this->addShortcode(new Blockquote());
		$this->addShortcode(new CustomFont());
		$this->addShortcode(new Highlight());
		$this->addShortcode(new ImageGallery());
		$this->addShortcode(new GoogleMap());
		$this->addShortcode(new Separator());
		$this->addShortcode(new VideoButton());
		$this->addShortcode(new Dropcaps());
		$this->addShortcode(new IconWithText());
		$this->addShortcode(new SocialShare());
		$this->addShortcode(new SectionTitle());
		$this->addShortcode(new SectionSubtitle());
        $this->addShortcode(new VerticalSplitSlider());
        $this->addShortcode(new VerticalSplitSliderLeftPanel());
        $this->addShortcode(new VerticalSplitSliderRightPanel());
        $this->addShortcode(new VerticalSplitSliderContentItem());
		$this->addShortcode(new InfoBox());
		$this->addShortcode(new ProcessHolder());
		$this->addShortcode(new ProcessItem());
		$this->addShortcode(new ComparisonPricingTablesHolder());
		$this->addShortcode(new ComparisonPricingTable());
		$this->addShortcode(new BlogSlider());
		$this->addShortcode(new HoverBanner());
		$this->addShortcode(new SeparatorWithIcon());
        if(malmo_elated_is_woocommerce_installed()) {
            $this->addShortcode(new ProductList());
        }
	}

	/**
	 * Calls ShortcodeLoader::addShortcodes and than loops through added shortcodes and calls render method
	 * of each shortcode object
	 */
	public function load() {
		$this->addShortcodes();

		foreach($this->loadedShortcodes as $shortcode) {
			add_shortcode($shortcode->getBase(), array($shortcode, 'render'));
		}

	}
}

$shortcodeLoader = ShortcodeLoader::getInstance();
$shortcodeLoader->load();