<div class="eltd-comment-holder clearfix" id="comments">
	<div class="eltd-comment-number">
		<div class="eltd-comment-number-inner">
			<h5><?php comments_number( esc_html__('No Comments','malmo'), '1'.esc_html__(' Comment ','malmo'), '% '.esc_html__(' Comments ','malmo')); ?></h5>
		</div>
	</div>
<div class="eltd-comments">
<?php if ( post_password_required() ) : ?>
				<p class="eltd-no-password"><?php esc_html_e( 'This post is password protected. Enter the password to view any comments.', 'malmo' ); ?></p>
			</div></div>
<?php
		return;
	endif;
?>
<?php if ( have_comments() ) : ?>

	<ul class="eltd-comment-list">
		<?php wp_list_comments(array( 'callback' => 'malmo_elated_comment')); ?>
	</ul>


<?php // End Comments ?>

 <?php else : // this is displayed if there are no comments so far 

	if ( ! comments_open() ) :
?>
		<!-- If comments are open, but there are no comments. -->

	 
		<!-- If comments are closed. -->
		<p><?php esc_html_e('Sorry, the comment form is closed at this time.', 'malmo'); ?></p>

	<?php endif; ?>
<?php endif; ?>
</div></div>
<?php
$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );

$consent  = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';

$args = array(
	'id_form' => 'commentform',
	'id_submit' => 'submit_comment',
	'title_reply'=> esc_html__( 'Leave a Reply','malmo' ),
	'title_reply_to' => esc_html__( 'Leave a Reply to %s','malmo' ),
	'cancel_reply_link' => esc_html__( 'Cancel Reply','malmo' ),
	'label_submit' => esc_html__( 'Submit','malmo' ),
	'comment_field' => '<textarea id="comment" placeholder="'.esc_attr__( 'Comment','malmo' ).'" name="comment" cols="45" rows="8" aria-required="true"></textarea>',
	'comment_notes_before' => '',
	'comment_notes_after' => '',
	'fields' => apply_filters( 'comment_form_default_fields', array(
		'author' => '<div><input id="author" name="author" placeholder="'. esc_attr__( 'Your full name','malmo' ) .'" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '"' . $aria_req . ' />',
		'url' => '<input id="email" name="email" placeholder="'. esc_attr__( 'E-mail address','malmo' ) .'" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '"' . $aria_req . ' />',
		'email' => '<input id="url" name="url" type="text" placeholder="'. esc_attr__( 'Website','malmo' ) .'" value="' . esc_attr( $commenter['comment_author_url'] ) . '" /></div>',
		'cookies' => '<p class="comment-form-cookies-consent"><input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes"' . $consent . ' />' .
			'<label for="wp-comment-cookies-consent">' . esc_html__( 'Save my name, email, and website in this browser for the next time I comment.', 'malmo' ) . '</label></p>',
		 )));
 ?>
<?php if(get_comment_pages_count() > 1){
	?>
	<div class="eltd-comment-pager">
		<p><?php paginate_comments_links(); ?></p>
	</div>
<?php } ?>
 <div class="eltd-comment-form">
	<?php comment_form($args); ?>
</div>
								
							


