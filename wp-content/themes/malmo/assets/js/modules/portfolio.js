(function($) {
	'use strict';

	var portfolio = {};
	eltd.modules.portfolio = portfolio;

	portfolio.eltdOnDocumentReady = eltdOnDocumentReady;
	portfolio.eltdOnWindowLoad = eltdOnWindowLoad;
	portfolio.eltdOnWindowResize = eltdOnWindowResize;
	portfolio.eltdOnWindowScroll = eltdOnWindowScroll;

	$(document).ready(eltdOnDocumentReady);
	$(window).load(eltdOnWindowLoad);
	$(window).resize(eltdOnWindowResize);
	$(window).scroll(eltdOnWindowScroll);

	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function eltdOnDocumentReady() {
		portfolio.eltdPortfolioSlider();
	}

	/*
	 All functions to be called on $(window).load() should be in this function
	 */
	function eltdOnWindowLoad() {
		eltdPortfolioSingleFollow().init();
	}

	/*
	 All functions to be called on $(window).resize() should be in this function
	 */
	function eltdOnWindowResize() {

	}

	/*
	 All functions to be called on $(window).scroll() should be in this function
	 */
	function eltdOnWindowScroll() {

	}

	portfolio.eltdPortfolioSlider = function() {
		var sliders = $('.eltd-portfolio-slider-holder');

		var setPortfolioResponsiveOptions = function(slider, options) {
			var columns = slider.data('columns');
			if(typeof columns === 'number') {
				switch(columns) {
					case 1:
						options.items = 1;
						break;
					default:
						options.items = columns;
						options.itemsDesktop = false;
						break;
				}
			}
		};

		if(sliders.length) {
			sliders.each(function() {
				var sliderHolder = $(this).find('.eltd-portfolio-slider-list');
				var options = {};

				options.pagination = typeof sliderHolder.data('enable-pagination') !== 'undefined' && sliderHolder.data('enable-pagination') === 'yes';

				setPortfolioResponsiveOptions(sliderHolder, options);

				sliderHolder.owlCarousel(options);
			});

		}
	};


	var eltdPortfolioSingleFollow = function() {

		var info = $('.eltd-follow-portfolio-info .small-images.eltd-portfolio-single-holder .eltd-portfolio-info-holder, ' +
			'.eltd-follow-portfolio-info .small-slider.eltd-portfolio-single-holder .eltd-portfolio-info-holder');

		if(info.length) {
			var infoHolder = info.parent(),
				infoHolderOffset = infoHolder.offset().top,
				infoHolderHeight = infoHolder.height(),
				mediaHolder = $('.eltd-portfolio-media'),
				mediaHolderHeight = mediaHolder.height(),
				header = $('.header-appear, .eltd-fixed-wrapper'),
				headerHeight = (header.length) ? header.height() : 0,
				col1 = $('.eltd-portfolio-single-holder .eltd-column1'),
				col2 = $('.eltd-portfolio-single-holder .eltd-column2');
		}

		var infoHolderPosition = function() {

			if(info.length) {

				if(mediaHolderHeight > infoHolderHeight) {
					if(eltd.scroll > infoHolderOffset) {
						info.animate({
							marginTop: (eltd.scroll - (infoHolderOffset) + eltdGlobalVars.vars.eltdAddForAdminBar + headerHeight + 20) //20 px is for styling, spacing between header and info holder
						});
					}
				}

			}
		};

		var recalculateInfoHolderPosition = function() {

			if(info.length) {
				if (col1.offset().top == col2.offset().top) {
					if(mediaHolderHeight > infoHolderHeight) {
						if(eltd.scroll > infoHolderOffset) {

							if(eltd.scroll + headerHeight + eltdGlobalVars.vars.eltdAddForAdminBar + infoHolderHeight + 20 < infoHolderOffset + mediaHolderHeight) {    //20 px is for styling, spacing between header and info holder

								//Calculate header height if header appears
								if($('.header-appear, .eltd-fixed-wrapper').length) {
									headerHeight = $('.header-appear, .eltd-fixed-wrapper').height();
								}
								info.stop().animate({
									marginTop: (eltd.scroll - (infoHolderOffset) + eltdGlobalVars.vars.eltdAddForAdminBar + headerHeight + 20) //20 px is for styling, spacing between header and info holder
								});
								//Reset header height
								headerHeight = 0;
							}
							else {
								info.stop().animate({
									marginTop: mediaHolderHeight - infoHolderHeight
								});
							}
						} else {
							info.stop().animate({
								marginTop: 0
							});
						}
					}
				}
				else {
					info.css('margin-top','');
				}
			}
		};

		return {

			init: function() {
				if(!eltd.modules.common.eltdIsTouchDevice()) {
					infoHolderPosition();
					$(window).scroll(function() {
						recalculateInfoHolderPosition();
					});
				}
			}

		};

	};

})(jQuery);