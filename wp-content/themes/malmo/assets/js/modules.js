(function($) {
    "use strict";

    window.eltd = {};
    eltd.modules = {};

    eltd.scroll = 0;
    eltd.window = $(window);
    eltd.document = $(document);
    eltd.windowWidth = $(window).width();
    eltd.windowHeight = $(window).height();
    eltd.body = $('body');
    eltd.html = $('html, body');
    eltd.htmlEl = $('html');
    eltd.menuDropdownHeightSet = false;
    eltd.defaultHeaderStyle = '';
    eltd.minVideoWidth = 1500;
    eltd.videoWidthOriginal = 1280;
    eltd.videoHeightOriginal = 720;
    eltd.videoRatio = 1280/720;

    eltd.eltdOnDocumentReady = eltdOnDocumentReady;
    eltd.eltdOnWindowLoad = eltdOnWindowLoad;
    eltd.eltdOnWindowResize = eltdOnWindowResize;
    eltd.eltdOnWindowScroll = eltdOnWindowScroll;

    $(document).ready(eltdOnDocumentReady);
    $(window).load(eltdOnWindowLoad);
    $(window).resize(eltdOnWindowResize);
    $(window).scroll(eltdOnWindowScroll);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function eltdOnDocumentReady() {
        eltd.scroll = $(window).scrollTop();

        //set global variable for header style which we will use in various functions
        if(eltd.body.hasClass('eltd-dark-header')){ eltd.defaultHeaderStyle = 'eltd-dark-header';}
        if(eltd.body.hasClass('eltd-light-header')){ eltd.defaultHeaderStyle = 'eltd-light-header';}

        eltdHandle404();

    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function eltdOnWindowLoad() {

    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function eltdOnWindowResize() {
        eltd.windowWidth = $(window).width();
        eltd.windowHeight = $(window).height();
        eltdHandle404();
    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function eltdOnWindowScroll() {
        eltd.scroll = $(window).scrollTop();
    }



    // Code for 404 page

    function eltdHandle404() {
        if(eltd.body.hasClass('error404')) {
            var cont = $('.eltd-container-inner.eltd-404-page');
            var notFound = cont.children('.eltd-page-not-found');
            cont.outerHeight(Math.max(eltd.windowHeight - cont.offset().top, notFound.outerHeight()+60));
        }
    }



    //set boxed layout width variable for various calculations

    switch(true){
        case eltd.body.hasClass('eltd-grid-1300'):
            eltd.boxedLayoutWidth = 1350;
            break;
        case eltd.body.hasClass('eltd-grid-1200'):
            eltd.boxedLayoutWidth = 1250;
            break;
        case eltd.body.hasClass('eltd-grid-1000'):
            eltd.boxedLayoutWidth = 1050;
            break;
        case eltd.body.hasClass('eltd-grid-800'):
            eltd.boxedLayoutWidth = 850;
            break;
        default :
            eltd.boxedLayoutWidth = 1150;
            break;
    }

})(jQuery);
(function($) {
	"use strict";

    var common = {};
    eltd.modules.common = common;

	common.eltdIsTouchDevice = eltdIsTouchDevice;
	common.eltdDisableSmoothScrollForMac = eltdDisableSmoothScrollForMac;
    common.eltdFluidVideo = eltdFluidVideo;
    common.eltdPreloadBackgrounds = eltdPreloadBackgrounds;
    common.eltdPrettyPhoto = eltdPrettyPhoto;
    common.eltdCheckHeaderStyleOnScroll = eltdCheckHeaderStyleOnScroll;
    common.eltdInitParallax = eltdInitParallax;
    //common.eltdSmoothScroll = eltdSmoothScroll;
    common.eltdEnableScroll = eltdEnableScroll;
    common.eltdDisableScroll = eltdDisableScroll;
    common.eltdWheel = eltdWheel;
    common.eltdKeydown = eltdKeydown;
    common.eltdPreventDefaultValue = eltdPreventDefaultValue;
    common.eltdOwlSlider = eltdOwlSlider;
    common.eltdInitSelfHostedVideoPlayer = eltdInitSelfHostedVideoPlayer;
    common.eltdSelfHostedVideoSize = eltdSelfHostedVideoSize;
    common.eltdInitBackToTop = eltdInitBackToTop;
    common.eltdBackButtonShowHide = eltdBackButtonShowHide;
    common.eltdSmoothTransition = eltdSmoothTransition;
	common.eltdInitCustomMenuDropdown = eltdInitCustomMenuDropdown;

    common.eltdOnDocumentReady = eltdOnDocumentReady;
    common.eltdOnWindowLoad = eltdOnWindowLoad;
    common.eltdOnWindowResize = eltdOnWindowResize;
    common.eltdOnWindowScroll = eltdOnWindowScroll;
    common.eltdIsTouchDevice = eltdIsTouchDevice;

    $(document).ready(eltdOnDocumentReady);
    $(window).load(eltdOnWindowLoad);
    $(window).resize(eltdOnWindowResize);
    $(window).scroll(eltdOnWindowScroll);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function eltdOnDocumentReady() {
        eltdTouchDeviceBodyClass();
		eltdDisableSmoothScrollForMac();
        eltdFluidVideo();
        eltdPreloadBackgrounds();
        eltdPrettyPhoto();
        eltdInitAnchor().init();
        eltdInitVideoBackground();
        eltdInitVideoBackgroundSize();
        eltdSetContentBottomMargin();
        //eltdSmoothScroll();
        eltdOwlSlider();
        eltdInitSelfHostedVideoPlayer();
        eltdSelfHostedVideoSize();
        eltdInitBackToTop();
        eltdBackButtonShowHide();
		eltdInitCustomMenuDropdown();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function eltdOnWindowLoad() {
        eltdInitElementsAnimations();
        eltdCheckHeaderStyleOnScroll(); //called on load since all content needs to be loaded in order to calculate row's position right
        eltdInitParallax();
        eltdSmoothTransition();
    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function eltdOnWindowResize() {
        eltdInitVideoBackgroundSize();
        eltdSelfHostedVideoSize();
        eltdInitParallax();
    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function eltdOnWindowScroll() {
        
    }

	/*
	 ** Disable shortcodes animation on appear for touch devices
	 */
    function eltdTouchDeviceBodyClass() {
        if(eltdIsTouchDevice()) {
            eltd.body.addClass('eltd-no-animations-on-touch');
        }
    }

    function eltdIsTouchDevice() {
        return Modernizr.touch && !eltd.body.hasClass('eltd-no-animations-on-touch');
    }
	/*
	 ** Disable smooth scroll for mac if smooth scroll is enabled
	 */
	function eltdDisableSmoothScrollForMac() {
		var os = navigator.appVersion.toLowerCase();

		if (os.indexOf('mac') > -1 && eltd.body.hasClass('eltd-smooth-scroll')) {
			eltd.body.removeClass('eltd-smooth-scroll');
		}
	}

	function eltdFluidVideo() {
        fluidvids.init({
			selector: ['iframe'],
			players: ['www.youtube.com', 'player.vimeo.com']
		});
	}

    /**
     * Init Owl Carousel
     */
    function eltdOwlSlider() {

        var sliders = $('.eltd-owl-slider');

        if (sliders.length) {
            sliders.each(function(){

                var slider = $(this);
                slider.owlCarousel({
                    items: 1,
                    loop: true,
                    nav: true,
                    autoHeight: true,
                    pagination: false,
                    animateOut: 'fadeOutRight',
                    animateIn: 'fadeInLeft',
                    navText: [
                        '<span class="eltd-prev-icon"><i class="lnr lnr-chevron-left"></i></span>',
                        '<span class="eltd-next-icon"><i class="lnr lnr-chevron-right"></i></span>'
                    ]
                });

            });
        }

    }


    /*
     *	Preload background images for elements that have 'eltd-preload-background' class
     */
    function eltdPreloadBackgrounds(){

        $(".eltd-preload-background").each(function() {
            var preloadBackground = $(this);
            if(preloadBackground.css("background-image") !== "" && preloadBackground.css("background-image") != "none") {

                var bgUrl = preloadBackground.attr('style');

                bgUrl = bgUrl.match(/url\(["']?([^'")]+)['"]?\)/);
                bgUrl = bgUrl ? bgUrl[1] : "";

                if (bgUrl) {
                    var backImg = new Image();
                    backImg.src = bgUrl;
                    $(backImg).load(function(){
                        preloadBackground.removeClass('eltd-preload-background');
                    });
                }
            }else{
                $(window).load(function(){ preloadBackground.removeClass('eltd-preload-background'); }); //make sure that eltd-preload-background class is removed from elements with forced background none in css
            }
        });
    }

	/* This prettyPhoto script is added because of our video banner shortcode*/
    function eltdPrettyPhoto() {
        /*jshint multistr: true */
        var markupWhole = '<div class="pp_pic_holder"> \
                        <div class="ppt">&nbsp;</div> \
                        <div class="pp_top"> \
                            <div class="pp_left"></div> \
                            <div class="pp_middle"></div> \
                            <div class="pp_right"></div> \
                        </div> \
                        <div class="pp_content_container"> \
                            <div class="pp_left"> \
                            <div class="pp_right"> \
                                <div class="pp_content"> \
                                    <div class="pp_loaderIcon"></div> \
                                    <div class="pp_fade"> \
                                        <a href="#" class="pp_expand" title="Expand the image">Expand</a> \
                                        <div class="pp_hoverContainer"> \
                                            <a class="pp_next" href="#"><span class="lnr lnr-chevron-right"></span></a> \
                                            <a class="pp_previous" href="#"><span class="lnr lnr-chevron-left"></span></a> \
                                        </div> \
                                        <div id="pp_full_res"></div> \
                                        <div class="pp_details"> \
                                            <div class="pp_nav"> \
                                                <a href="#" class="pp_arrow_previous">Previous</a> \
                                                <p class="currentTextHolder">0/0</p> \
                                                <a href="#" class="pp_arrow_next">Next</a> \
                                            </div> \
                                            <p class="pp_description"></p> \
                                            {pp_social} \
                                            <a class="pp_close" href="#">Close</a> \
                                        </div> \
                                    </div> \
                                </div> \
                            </div> \
                            </div> \
                        </div> \
                        <div class="pp_bottom"> \
                            <div class="pp_left"></div> \
                            <div class="pp_middle"></div> \
                            <div class="pp_right"></div> \
                        </div> \
                    </div> \
                    <div class="pp_overlay"></div>';

        $("a[data-rel^='prettyPhoto']").prettyPhoto({
            hook: 'data-rel',
            animation_speed: 'normal', /* fast/slow/normal */
            slideshow: false, /* false OR interval time in ms */
            autoplay_slideshow: false, /* true/false */
            opacity: 0.80, /* Value between 0 and 1 */
            show_title: true, /* true/false */
            allow_resize: true, /* Resize the photos bigger than viewport. true/false */
            horizontal_padding: 0,
            default_width: 960,
            default_height: 540,
            counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
            theme: 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
            hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
            wmode: 'opaque', /* Set the flash wmode attribute */
            autoplay: true, /* Automatically start videos: True/False */
            modal: false, /* If set to true, only the close button will close the window */
            overlay_gallery: false, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
            keyboard_shortcuts: true, /* Set to false if you open forms inside prettyPhoto */
            deeplinking: false,
            custom_markup: '',
            social_tools: false,
            markup: markupWhole
        });
    }

    /*
     *	Check header style on scroll, depending on row settings
     */
    function eltdCheckHeaderStyleOnScroll(){

        if($('[data-eltd_header_style]').length > 0 && eltd.body.hasClass('eltd-header-style-on-scroll')) {

            var waypointSelectors = $('.wpb_row.eltd-section');
            var changeStyle = function(element){
                (element.data("eltd_header_style") !== undefined) ? eltd.body.removeClass('eltd-dark-header eltd-light-header').addClass(element.data("eltd_header_style")) : eltd.body.removeClass('eltd-dark-header eltd-light-header').addClass(''+eltd.defaultHeaderStyle);
            };

            waypointSelectors.waypoint( function(direction) {
                if(direction === 'down') { changeStyle($(this.element)); }
            }, { offset: 0});

            waypointSelectors.waypoint( function(direction) {
                if(direction === 'up') { changeStyle($(this.element)); }
            }, { offset: function(){
                return -$(this.element).outerHeight();
            } });
        }
    }

    /*
     *	Start animations on elements
     */
    function eltdInitElementsAnimations(){

        var touchClass = $('.eltd-no-animations-on-touch'),
            noAnimationsOnTouch = true,
            elements = $('.eltd-grow-in, .eltd-fade-in-down, .eltd-fade-in-up, .eltd-element-from-fade, .eltd-element-from-left, .eltd-element-from-right, .eltd-element-from-top, .eltd-element-from-bottom, .eltd-flip-in, .eltd-x-rotate, .eltd-z-rotate, .eltd-y-translate, .eltd-fade-in, .eltd-fade-in-left-x-rotate'),
            animationClass,
            animationData;

        if (touchClass.length) {
            noAnimationsOnTouch = false;
        }

        if(elements.length > 0 && noAnimationsOnTouch){
            elements.each(function(){
				$(this).appear(function() {
					animationData = $(this).data('animation');
					if(typeof animationData !== 'undefined' && animationData !== '') {
						animationClass = animationData;
						$(this).addClass(animationClass+'-on');
					}
                },{accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});
            });
        }

    }


/*
 **	Sections with parallax background image
 */
function eltdInitParallax(){

    if($('.eltd-parallax-section-holder').length){
        $('.eltd-parallax-section-holder').each(function() {

            var parallaxElement = $(this);
            if(parallaxElement.hasClass('eltd-full-screen-height-parallax')){
                parallaxElement.height(eltd.windowHeight);
                parallaxElement.find('.eltd-parallax-content-outer').css('padding',0);
            }
            var speed = parallaxElement.data('eltd-parallax-speed')*0.4;
            parallaxElement.parallax("50%", speed);
        });
    }
}

/*
 **	Anchor functionality
 */
var eltdInitAnchor = eltd.modules.common.eltdInitAnchor = function() {

    /**
     * Set active state on clicked anchor
     * @param anchor, clicked anchor
     */
    var setActiveState = function(anchor){

        $('.eltd-main-menu .eltd-active-item, .eltd-mobile-nav .eltd-active-item, .eltd-vertical-menu .eltd-active-item, .eltd-fullscreen-menu .eltd-active-item').removeClass('eltd-active-item');
        anchor.parent().addClass('eltd-active-item');

        $('.eltd-main-menu a, .eltd-mobile-nav a, .eltd-vertical-menu a, .eltd-fullscreen-menu a').removeClass('current');
        anchor.addClass('current');
    };

    /**
     * Check anchor active state on scroll
     */
    var checkActiveStateOnScroll = function(){

        $('[data-eltd-anchor]').waypoint( function(direction) {
            if(direction === 'down') {
                setActiveState($("a[href='"+window.location.href.split('#')[0]+"#"+$(this.element).data("eltd-anchor")+"']"));
            }
        }, { offset: '50%' });

        $('[data-eltd-anchor]').waypoint( function(direction) {
            if(direction === 'up') {
                setActiveState($("a[href='"+window.location.href.split('#')[0]+"#"+$(this.element).data("eltd-anchor")+"']"));
            }
        }, { offset: function(){
            return -($(this.element).outerHeight() - 150);
        } });

    };

    /**
     * Check anchor active state on load
     */
    var checkActiveStateOnLoad = function(){
        var hash = window.location.hash.split('#')[1];

        if(hash !== "" && $('[data-eltd-anchor="'+hash+'"]').length > 0){
            //triggers click which is handled in 'anchorClick' function
            var linkURL = window.location.href.split('#')[0]+"#"+hash;
            if($("a[href='"+linkURL+"']").length){ //if there is a link on page with such href
                $("a[href='"+linkURL+"']").trigger( "click" );
            }else{ //than create a fake link and click it
                var link = $('<a/>').attr({'href':linkURL,'class':'eltd-anchor'}).appendTo('body');
                link.trigger('click');
            }
        }
    };

    /**
     * Calculate header height to be substract from scroll amount
     * @param anchoredElementOffset, anchorded element offest
     */
    var headerHeihtToSubtract = function(anchoredElementOffset){

        if(eltd.modules.header.behaviour == 'eltd-sticky-header-on-scroll-down-up') {
            (anchoredElementOffset > eltd.modules.header.stickyAppearAmount) ? eltd.modules.header.isStickyVisible = true : eltd.modules.header.isStickyVisible = false;
        }

        if(eltd.modules.header.behaviour == 'eltd-sticky-header-on-scroll-up') {
            (anchoredElementOffset > eltd.scroll) ? eltd.modules.header.isStickyVisible = false : '';
        }

        var headerHeight = eltd.modules.header.isStickyVisible ? eltdGlobalVars.vars.eltdStickyHeaderTransparencyHeight : eltdPerPageVars.vars.eltdHeaderTransparencyHeight;

        return headerHeight;
    };

    /**
     * Handle anchor click
     */
    var anchorClick = function() {
        eltd.document.on("click", ".eltd-main-menu a, .eltd-vertical-menu a, .eltd-fullscreen-menu a, .eltd-btn, .eltd-anchor, .eltd-mobile-nav a", function() {
            var scrollAmount;
            var anchor = $(this);
            var hash = anchor.prop("hash").split('#')[1];

            if(hash !== "" && $('[data-eltd-anchor="' + hash + '"]').length > 0 /*&& anchor.attr('href').split('#')[0] == window.location.href.split('#')[0]*/) {

                var anchoredElementOffset = $('[data-eltd-anchor="' + hash + '"]').offset().top;
                scrollAmount = $('[data-eltd-anchor="' + hash + '"]').offset().top - headerHeihtToSubtract(anchoredElementOffset);

                setActiveState(anchor);

                eltd.html.stop().animate({
                    scrollTop: Math.round(scrollAmount)
                }, 1000, function() {
                    //change hash tag in url
                    if(history.pushState) { history.pushState(null, null, '#'+hash); }
                });
                return false;
            }
        });
    };

    return {
        init: function() {
            if($('[data-eltd-anchor]').length) {
                anchorClick();
                checkActiveStateOnScroll();
                $(window).load(function() { checkActiveStateOnLoad(); });
            }
        }
    };

};

/*
 **	Video background initialization
 */
function eltdInitVideoBackground(){

    $('.eltd-section .eltd-video-wrap .eltd-video').mediaelementplayer({
        enableKeyboard: false,
        iPadUseNativeControls: false,
        pauseOtherPlayers: false,
        // force iPhone's native controls
        iPhoneUseNativeControls: false,
        // force Android's native controls
        AndroidUseNativeControls: false
    });

    //mobile check
    if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/)){
        eltdInitVideoBackgroundSize();
        $('.eltd-section .eltd-mobile-video-image').show();
        $('.eltd-section .eltd-video-wrap').remove();
    }
}

    /*
     **	Calculate video background size
     */
    function eltdInitVideoBackgroundSize(){

        $('.eltd-section .eltd-video-wrap').each(function(){

            var element = $(this);
            var sectionWidth = element.closest('.eltd-section').outerWidth();
            element.width(sectionWidth);

            var sectionHeight = element.closest('.eltd-section').outerHeight();
            eltd.minVideoWidth = eltd.videoRatio * (sectionHeight+20);
            element.height(sectionHeight);

            var scaleH = sectionWidth / eltd.videoWidthOriginal;
            var scaleV = sectionHeight / eltd.videoHeightOriginal;
            var scale =  scaleV;
            if (scaleH > scaleV)
                scale =  scaleH;
            if (scale * eltd.videoWidthOriginal < eltd.minVideoWidth) {scale = eltd.minVideoWidth / eltd.videoWidthOriginal;}

            element.find('video, .mejs-overlay, .mejs-poster').width(Math.ceil(scale * eltd.videoWidthOriginal +2));
            element.find('video, .mejs-overlay, .mejs-poster').height(Math.ceil(scale * eltd.videoHeightOriginal +2));
            element.scrollLeft((element.find('video').width() - sectionWidth) / 2);
            element.find('.mejs-overlay, .mejs-poster').scrollTop((element.find('video').height() - (sectionHeight)) / 2);
            element.scrollTop((element.find('video').height() - sectionHeight) / 2);
        });

    }

    /*
     **	Set content bottom margin because of the uncovering footer
     */
    function eltdSetContentBottomMargin(){
        var uncoverFooter = $('.eltd-footer-uncover');

        if(uncoverFooter.length){
            $('.eltd-content').css('margin-bottom', $('.eltd-footer-inner').height());
        }
    }

	/*
	** Initiate Smooth Scroll
	*/
	//function eltdSmoothScroll(){
	//
	//	if(eltd.body.hasClass('eltd-smooth-scroll')){
	//
	//		var scrollTime = 0.4;			//Scroll time
	//		var scrollDistance = 300;		//Distance. Use smaller value for shorter scroll and greater value for longer scroll
	//
	//		var mobile_ie = -1 !== navigator.userAgent.indexOf("IEMobile");
	//
	//		var smoothScrollListener = function(event){
	//			event.preventDefault();
	//
	//			var delta = event.wheelDelta / 120 || -event.detail / 3;
	//			var scrollTop = eltd.window.scrollTop();
	//			var finalScroll = scrollTop - parseInt(delta * scrollDistance);
	//
	//			TweenLite.to(eltd.window, scrollTime, {
	//				scrollTo: {
	//					y: finalScroll, autoKill: !0
	//				},
	//				ease: Power1.easeOut,
	//				autoKill: !0,
	//				overwrite: 5
	//			});
	//		};
	//
	//		if (!$('html').hasClass('touch') && !mobile_ie) {
	//			if (window.addEventListener) {
	//				window.addEventListener('mousewheel', smoothScrollListener, false);
	//				window.addEventListener('DOMMouseScroll', smoothScrollListener, false);
	//			}
	//		}
	//	}
	//}

    function eltdDisableScroll() {

        if (window.addEventListener) {
            window.addEventListener('DOMMouseScroll', eltdWheel, false);
        }
        window.onmousewheel = document.onmousewheel = eltdWheel;
        document.onkeydown = eltdKeydown;

        if(eltd.body.hasClass('eltd-smooth-scroll')){
            window.removeEventListener('mousewheel', smoothScrollListener, false);
            window.removeEventListener('DOMMouseScroll', smoothScrollListener, false);
        }
    }

    function eltdEnableScroll() {
        if (window.removeEventListener) {
            window.removeEventListener('DOMMouseScroll', eltdWheel, false);
        }
        window.onmousewheel = document.onmousewheel = document.onkeydown = null;

        if(eltd.body.hasClass('eltd-smooth-scroll')){
            window.addEventListener('mousewheel', smoothScrollListener, false);
            window.addEventListener('DOMMouseScroll', smoothScrollListener, false);
        }
    }

    function eltdWheel(e) {
        eltdPreventDefaultValue(e);
    }

    function eltdKeydown(e) {
        var keys = [37, 38, 39, 40];

        for (var i = keys.length; i--;) {
            if (e.keyCode === keys[i]) {
                eltdPreventDefaultValue(e);
                return;
            }
        }
    }

    function eltdPreventDefaultValue(e) {
        e = e || window.event;
        if (e.preventDefault) {
            e.preventDefault();
        }
        e.returnValue = false;
    }

    function eltdInitSelfHostedVideoPlayer() {

        var players = $('.eltd-self-hosted-video');
            players.mediaelementplayer({
                audioWidth: '100%'
            });
    }

	function eltdSelfHostedVideoSize(){

		$('.eltd-self-hosted-video-holder .eltd-video-wrap').each(function(){
			var thisVideo = $(this);

			var videoWidth = thisVideo.closest('.eltd-self-hosted-video-holder').outerWidth();
			var videoHeight = videoWidth / eltd.videoRatio;

			if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/)){
				thisVideo.parent().width(videoWidth);
				thisVideo.parent().height(videoHeight);
			}

			thisVideo.width(videoWidth);
			thisVideo.height(videoHeight);

			thisVideo.find('video, .mejs-overlay, .mejs-poster').width(videoWidth);
			thisVideo.find('video, .mejs-overlay, .mejs-poster').height(videoHeight);
		});
	}

    function eltdToTopButton(a) {

        var b = $("#eltd-back-to-top");
        b.removeClass('off on');
        if (a === 'on') { b.addClass('on'); } else { b.addClass('off'); }
    }

    function eltdBackButtonShowHide(){
        eltd.window.scroll(function () {
            var b = $(this).scrollTop();
            var c = $(this).height();
            var d;
            if (b > 0) { d = b + c / 2; } else { d = 1; }
            if (d < 1e3) { eltdToTopButton('off'); } else { eltdToTopButton('on'); }
        });
    }

    function eltdInitBackToTop(){
        var backToTopButton = $('#eltd-back-to-top');
        backToTopButton.on('click',function(e){
            e.preventDefault();
            eltd.html.animate({scrollTop: 0}, eltd.window.scrollTop()/3, 'linear');
        });
    }

    function eltdSmoothTransition() {
		var loader = $('body > .eltd-smooth-transition-loader.eltd-mimic-ajax');
        if (loader.length) {
            loader.fadeOut(500);
			$(window).on('pageshow', function(event) {
				if (event.originalEvent.persisted) {
					loader.fadeOut(500);
				}
			});

            $('a').on('click', function(e) {
                var a = $(this);
                if (
                    e.which == 1 && // check if the left mouse button has been pressed
                    a.attr('href').indexOf(window.location.host) >= 0 && // check if the link is to the same domain
					(typeof a.data('rel') === 'undefined') && //Not pretty photo link
                    (typeof a.attr('rel') === 'undefined') && //Not VC pretty photo link
                    (typeof a.attr('target') === 'undefined' || a.attr('target') === '_self') && // check if the link opens in the same window
                    (a.attr('href').split('#')[0] !== window.location.href.split('#')[0]) // check if it is an anchor aiming for a different page
                ) {
                    e.preventDefault();
                    loader.addClass('eltd-hide-spinner');
                    loader.fadeIn(500, function() {
                        window.location = a.attr('href');
                    });
                }
            });
        }
    }

	function eltdInitCustomMenuDropdown() {
		var menus = $('.eltd-sidebar .widget_nav_menu .menu');

		var dropdownOpeners,
			currentMenu;


		if(menus.length) {
			menus.each(function() {
				currentMenu = $(this);

				dropdownOpeners = currentMenu.find('li.menu-item-has-children > a');

				if(dropdownOpeners.length) {
					dropdownOpeners.each(function() {
						var currentDropdownOpener = $(this);

						currentDropdownOpener.on('click', function(e) {
							e.preventDefault();

							var dropdownToOpen = currentDropdownOpener.parent().children('.sub-menu');

							if(dropdownToOpen.is(':visible')) {
								dropdownToOpen.slideUp();
								currentDropdownOpener.removeClass('eltd-custom-menu-active');
							} else {
								dropdownToOpen.slideDown();
								currentDropdownOpener.addClass('eltd-custom-menu-active');
							}
						});
					});
				}
			});
		}
	}

})(jQuery);
(function($) {
    'use strict';

    var ajax = {};

    eltd.modules.ajax = ajax;

    var animation = {};
    ajax.animation = animation;

    ajax.eltdFetchPage = eltdFetchPage;
    ajax.eltdInitAjax = eltdInitAjax;
    ajax.eltdHandleLinkClick = eltdHandleLinkClick;
    ajax.eltdInsertFetchedContent = eltdInsertFetchedContent;
    ajax.eltdInitBackBehavior = eltdInitBackBehavior;
    ajax.eltdSetActiveState = eltdSetActiveState;
    ajax.eltdReinitiateAll = eltdReinitiateAll;
    ajax.eltdHandleMeta = eltdHandleMeta;

    ajax.eltdOnDocumentReady = eltdOnDocumentReady;
    ajax.eltdOnWindowLoad = eltdOnWindowLoad;
    ajax.eltdOnWindowResize = eltdOnWindowResize;
    ajax.eltdOnWindowScroll = eltdOnWindowScroll;

    $(document).ready(eltdOnDocumentReady);
    $(window).load(eltdOnWindowLoad);
    $(window).resize(eltdOnWindowResize);
    $(window).scroll(eltdOnWindowScroll);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function eltdOnDocumentReady() {
        eltdInitAjax();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function eltdOnWindowLoad() {
    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function eltdOnWindowResize() {
    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function eltdOnWindowScroll() {
    }


    var loadedPageFlag = true; // Indicates whether the page is loaded
    var firstLoad = true; // Indicates whether this is the first loaded page, for back button functionality
    animation.type = null;
    animation.time = 500; // Duration of animation for the content to be changed
    animation.simultaneous = true; // False indicates that the new content should wait for the old content to disappear, true means that it appears at the same time as the old content disappears
    animation.loader = null;
    animation.loaderTime = 500;

    /**
     * Fetching the targeted page
     */
    function eltdFetchPage(params, destinationSelector, targetSelector) {

        function setDefaultParam(key,value) {
            params[key] = typeof params[key] !== 'undefined' ? params[key] : value;
        }

        destinationSelector = typeof destinationSelector !== 'undefined' ? destinationSelector : '.eltd-content';
        targetSelector = typeof targetSelector !== 'undefined' ? targetSelector : '.eltd-content';
        
        // setting default ajax parameters
        params = typeof params !== 'undefined' ? params : {};

        setDefaultParam('url', window.location.href);
        setDefaultParam('type', 'POST');
        setDefaultParam('success', function(response) {
            var jResponse = $(response);

            var meta = jResponse.find('.eltd-meta');
            if (meta.length) { eltdHandleMeta(meta); }

            var new_content = jResponse.find(targetSelector);
            if (!new_content.length) {
                loadedPageFlag = true;
                return false;
            }
            else {
                eltdInsertFetchedContent(params.url, new_content, destinationSelector);
            }
        });

        // setting data parameters
        setDefaultParam('ajaxReq', 'yes');
        //setDefaultParam('hasHeader', eltd.body.find('header').length ? true : false);
        //setDefaultParam('hasFooter', eltd.body.find('footer').length ? true : false);

        $.ajax({
            url: params.url,
            type: params.type,
            data: {
                ajaxReq: params.ajaxReq
                //hasHeader: params.hasHeader,
                //hasFooter: params.hasFooter
            },
            success: params.success
        });
    }

    function eltdInitAjax() {
        eltd.body.removeClass('page-not-loaded'); // Might be necessary for ajax calls
		animation.loader = $('body > .eltd-smooth-transition-loader.eltd-ajax');
        if (animation.loader.length) {
            animation.loader.fadeOut(animation.loaderTime);
            $(window).focus(function() {
                animation.loader.fadeOut(animation.loaderTime);
            });

            if(eltd.body.hasClass('woocommerce') || eltd.body.hasClass('woocommerce-page')) {
                return false;
            }
            else {
                eltdInitBackBehavior();
                $(document).on('click', 'a[target!="_blank"]:not(.no-ajax):not(.no-link)', function(click) {
                    var link = $(this);

                    if(click.ctrlKey == 1) { // Check if CTRL key is held with the click
                        window.open(link.attr('href'), '_blank');
                        return false;
                    }

                    if(link.parents('.eltd-ptf-load-more').length){ return false; } // Don't initiate ajax for portfolio load more link

                    if(link.parents('.eltd-blog-load-more-button').length){ return false; } // Don't initiate ajax for blog load more link

                    if(link.parents('eltd-post-info-comments').length){ // If it's a comment link, don't load any content, just scroll to the comments section
                        var hash = link.attr('href').split("#")[1];  
                        $('html, body').scrollTop( $("#"+hash).offset().top );  
                        return false;  
                    }

                    if(window.location.href.split('#')[0] == link.attr('href').split('#')[0]){ return false; } //  If the link leads to the same page, don't use ajax

                    if(link.closest('.eltd-no-animation').length === 0){ // If no parents are set to no-animation...

                        if(document.location.href.indexOf("?s=") >= 0){ // Don't use ajax if currently on search page
                            return true;
                        }
                        if(link.attr('href').indexOf("wp-admin") >= 0){ // Don't use ajax for wp-admin
                            return true;
                        }
                        if(link.attr('href').indexOf("wp-content") >= 0){ // Don't use ajax for wp-content
                            return true;
                        }

                        if(jQuery.inArray(link.attr('href').split('#')[0], eltdGlobalVars.vars.no_ajax_pages) !== -1){ // If the target page is a no-ajax page, don't use ajax
                            document.location.href = link.attr('href');
                            return false;
                        }

                        if((link.attr('href') !== "http://#") && (link.attr('href') !== "#")){ // Don't use ajax if the link is empty
                            //disableHashChange = true;

                            var url = link.attr('href');
                            var start = url.indexOf(window.location.protocol + '//' + window.location.host); // Check if the link leads to the same domain
                            if(start === 0){
                                if(!loadedPageFlag){ return false; } //if page is not loaded don't load next one
                                click.preventDefault();
                                click.stopImmediatePropagation();
                                click.stopPropagation();
                                if (!link.is('.current')) {
                                    eltdHandleLinkClick(link);
                                }
                            }

                        }else{
                            return false;
                        }
                    }
                });
            }
        }
    }

    function eltdInitBackBehavior() {
        if (window.history.pushState) {
            /* the below code is to override back button to get the ajax content without reload*/
            $(window).on('popstate', function() {
                "use strict";

                var url = location.href;
                if (!firstLoad && loadedPageFlag) {
                    loadedPageFlag = false;
                    eltdFetchPage({
                        url: url
                    });
                }
            });
        }
    }

    function eltdHandleLinkClick(link) {
        loadedPageFlag = false;
        animation.loader.fadeIn(animation.loaderTime);
        eltdFetchPage({
            url: link.attr('href')
        });
    }

    function eltdSetActiveState(url) {
        var me = $("nav a[href='"+url+"'], .widget_nav_menu a[href='"+url+"']");

		$('.eltd-main-menu a, .eltd-mobile-nav a, .eltd-mobile-nav h4, .eltd-vertical-menu a, .popup_menu a, .widget_nav_menu a').removeClass('current').parent().removeClass('eltd-active-item');
		//$('.main_menu a, .mobile_menu a, .mobile_menu h4, .vertical_menu a, .popup_menu a').parent().removeClass('active');
		$('.widget_nav_menu ul.menu > li').removeClass('current-menu-item');

        me.each(function() {
            var me = $(this);

            if(me.closest('.second').length === 0){
                me.parent().addClass('eltd-active-item');
            }else{
                me.closest('.second').parent().addClass('eltd-active-item');
            }

            if(me.closest('.eltd-mobile-nav').length > 0){
                me.closest('.level0').addClass('eltd-active-item');
                me.closest('.level1').addClass('eltd-active-item');
                me.closest('.level2').addClass('eltd-active-item');
            }

            if(me.closest('.widget_nav_menu').length > 0){
                me.closest('.widget_nav_menu').find('.menu-item').addClass('current-menu-item');
            }


            //$('.eltd-main-menu a, .eltd-mobile-nav a, .eltd-vertical-menu a, .popup_menu a').removeClass('current');
            me.addClass('current');
        });
    }

    function eltdReinitiateAll() {
        $(document).off(); // Remove all event handlers before reinitialization
		$(window).off();
        eltd.body.off().find('*').off(); // Remove all event handlers before reinitialization

        eltd.eltdOnDocumentReady();
        eltd.eltdOnWindowLoad();
        eltd.eltdOnWindowResize();
        eltd.eltdOnWindowScroll();

        var modules = ['common', 'ajax', 'header', 'title', 'shortcodes', 'woocommerce', 'portfolio', 'blog', 'like'];
        for (var i=0; i<modules.length; i++) {
            if (1 || typeof eltd.modules[modules[i]] !== 'undefined') {
                eltd.modules[modules[i]].eltdOnDocumentReady();
                eltd.modules[modules[i]].eltdOnWindowLoad();
                eltd.modules[modules[i]].eltdOnWindowResize();
                eltd.modules[modules[i]].eltdOnWindowScroll();
            }
        }
    }

    function eltdHandleMeta(meta_data) {
        // set up title, meta description and meta keywords
        var newTitle = meta_data.find(".eltd-seo-title").text();
        var pageTransition = meta_data.find(".eltd-page-transition").text();
        var newDescription = meta_data.find(".eltd-seo-description").text();
        var newKeywords = meta_data.find(".eltd-seo-keywords").text();
        if (typeof pageTransition !== 'undefined') {
            animation.type = pageTransition;
        } 
        if ($('head meta[name="description"]').length) {
            $('head meta[name="description"]').attr('content', newDescription);
        } else if (typeof newDescription !== 'undefined') {
            $('<meta name="description" content="'+newDescription+'">').prependTo($('head'));
        } 
        if ($('head meta[name="keywords"]').length) {
            $('head meta[name="keywords"]').attr('content', newKeywords);
        } else if (typeof newKeywords !== 'undefined') {
            $('<meta name="keywords" content="'+newKeywords+'">').prependTo($('head'));
        } 
        document.title = newTitle;

        var newBodyClasses = meta_data.find(".eltd-body-classes").text();
        var myArray = newBodyClasses.split(',');
        eltd.body.removeClass();
        for(var i=0;i<myArray.length;i++){
            if (myArray[i] !== "eltd-page-not-loaded"){
                eltd.body.addClass(myArray[i]);
            }
        }

        if($("#wp-admin-bar-edit").length > 0){
            // set up edit link when wp toolbar is enabled
            var pageId = meta_data.find("#eltd-page-id").text();
            var old_link = $('#wp-admin-bar-edit a').attr("href");
            var new_link = old_link.replace(/(post=).*?(&)/,'$1' + pageId + '$2');
            $('#wp-admin-bar-edit a').attr("href", new_link);
        }
    }

    function eltdInsertFetchedContent(url, new_content, destinationSelector) {
        destinationSelector = typeof destinationSelector !== 'undefined' ? destinationSelector : '.eltd-content';
        var destination = eltd.body.find(destinationSelector);
        
        new_content.height(destination.height()).css({'position': 'absolute', 'opacity': 0, 'overflow': 'hidden'}).insertBefore(destination);
       
        new_content.waitForImages(function() {
            if (url.indexOf('#') !== -1) {
                $('<a class="eltd-temp-anchor eltd-anchor" href="'+url+'" style="display: none"></a>').appendTo('body');
            }
            eltdReinitiateAll();

            if (animation.type == "fade") {
                destination.stop().fadeTo(animation.time, 0, function() {
                    destination.remove();
                    if (window.history.pushState) {
                        if(url!==window.location.href){
                            window.history.pushState({path:url},'',url);
                        }

                        //does Google Analytics code exists on page?
                        if(typeof _gaq !== 'undefined') {
                            //add new url to Google Analytics so it can be tracked
                            _gaq.push(['_trackPageview', url]);
                        }
                    } else {
                        document.location.href = window.location.protocol + '//' + window.location.host + '#' + url.split(window.location.protocol + '//' + window.location.host)[1];
                    }
                    eltdSetActiveState(url);
                    eltd.body.animate({scrollTop: 0}, animation.time, 'swing');
                });
                setTimeout(function() {
                    new_content.css('position','relative').height('').stop().fadeTo(animation.time, 1, function() {
                        loadedPageFlag = true;
                        firstLoad = false;
                        animation.loader.fadeOut(animation.loaderTime, function() {
                            var anch = $('.eltd-temp-anchor');
                            if (anch.length) {
                                anch.trigger('click').remove();
                            }
                        });
                    });
                }, !animation.simultaneous * animation.time);
            }
        });
    }


})(jQuery);
(function($) {
    "use strict";

    var header = {};
    eltd.modules.header = header;

    header.isStickyVisible = false;
    header.stickyAppearAmount = 0;
    header.behaviour;
    header.eltdSideArea = eltdSideArea;
    header.eltdSideAreaScroll = eltdSideAreaScroll;
    header.eltdFullscreenMenu = eltdFullscreenMenu;
    header.eltdInitMobileNavigation = eltdInitMobileNavigation;
    header.eltdMobileHeaderBehavior = eltdMobileHeaderBehavior;
    header.eltdSetDropDownMenuPosition = eltdSetDropDownMenuPosition;
    header.eltdDropDownMenu = eltdDropDownMenu;
    header.eltdSearch = eltdSearch;

    header.eltdOnDocumentReady = eltdOnDocumentReady;
    header.eltdOnWindowLoad = eltdOnWindowLoad;
    header.eltdOnWindowResize = eltdOnWindowResize;
    header.eltdOnWindowScroll = eltdOnWindowScroll;

    $(document).ready(eltdOnDocumentReady);
    $(window).load(eltdOnWindowLoad);
    $(window).resize(eltdOnWindowResize);
    $(window).scroll(eltdOnWindowScroll);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function eltdOnDocumentReady() {
        eltdHeaderBehaviour();
        eltdSideArea();
        eltdSideAreaScroll();
        eltdFullscreenMenu();
        eltdInitMobileNavigation();
        eltdMobileHeaderBehavior();
        eltdSetDropDownMenuPosition();
        eltdDropDownMenu();
        eltdSearch();
        eltdVerticalMenu().init();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function eltdOnWindowLoad() {
        eltdSetDropDownMenuPosition();
    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function eltdOnWindowResize() {
        eltdDropDownMenu();
    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function eltdOnWindowScroll() {
        
    }



    /*
     **	Show/Hide sticky header on window scroll
     */
    function eltdHeaderBehaviour() {

        var header = $('.eltd-page-header');
        var stickyHeader = $('.eltd-sticky-header');
        var fixedHeaderWrapper = $('.eltd-fixed-wrapper');

        var headerMenuAreaOffset = $('.eltd-page-header').find('.eltd-fixed-wrapper').length ? $('.eltd-page-header').find('.eltd-fixed-wrapper').offset().top : null;

        var stickyAppearAmount;


        switch(true) {
            // sticky header that will be shown when user scrolls up
            case eltd.body.hasClass('eltd-sticky-header-on-scroll-up'):
                eltd.modules.header.behaviour = 'eltd-sticky-header-on-scroll-up';
                var docYScroll1 = $(document).scrollTop();
                stickyAppearAmount = eltdGlobalVars.vars.eltdTopBarHeight + eltdGlobalVars.vars.eltdLogoAreaHeight + eltdGlobalVars.vars.eltdMenuAreaHeight + eltdGlobalVars.vars.eltdStickyHeaderHeight;

                var headerAppear = function(){
                    var docYScroll2 = $(document).scrollTop();

                    if((docYScroll2 > docYScroll1 && docYScroll2 > stickyAppearAmount) || (docYScroll2 < stickyAppearAmount)) {
                        eltd.modules.header.isStickyVisible= false;
                        stickyHeader.removeClass('header-appear').find('.eltd-main-menu .second').removeClass('eltd-drop-down-start');
                    }else {
                        eltd.modules.header.isStickyVisible = true;
                        stickyHeader.addClass('header-appear');
                    }

                    docYScroll1 = $(document).scrollTop();
                };
                headerAppear();

                $(window).scroll(function() {
                    headerAppear();
                });

                break;

            // sticky header that will be shown when user scrolls both up and down
            case eltd.body.hasClass('eltd-sticky-header-on-scroll-down-up'):
                var setStickyScrollAmount = function() {
                    var amount;

                    if(isStickyAmountFullScreen()) {
                        amount = eltd.window.height();
                    } else {
                        if(eltdPerPageVars.vars.eltdStickyScrollAmount !== 0) {
                            amount = eltdPerPageVars.vars.eltdStickyScrollAmount;
                        } else {
                            amount = eltdGlobalVars.vars.eltdTopBarHeight + eltdGlobalVars.vars.eltdLogoAreaHeight + eltdGlobalVars.vars.eltdMenuAreaHeight;
                        }
                    }

                    stickyAppearAmount = amount;
                };

                var isStickyAmountFullScreen = function() {
                    var fullScreenStickyAmount = eltdPerPageVars.vars.eltdStickyScrollAmountFullScreen;

                    return typeof fullScreenStickyAmount !== 'undefined' && fullScreenStickyAmount === true;
                };
                
                eltd.modules.header.behaviour = 'eltd-sticky-header-on-scroll-down-up';
                setStickyScrollAmount();
                eltd.modules.header.stickyAppearAmount = stickyAppearAmount; //used in anchor logic
                
                var headerAppear = function(){
                    if(eltd.scroll < stickyAppearAmount) {
                        eltd.modules.header.isStickyVisible = false;
                        stickyHeader.removeClass('header-appear').find('.eltd-main-menu .second').removeClass('eltd-drop-down-start');
                    }else{
                        eltd.modules.header.isStickyVisible = true;
                        stickyHeader.addClass('header-appear');
                    }
                };

                headerAppear();

                $(window).scroll(function() {
                    headerAppear();
                });

                break;

            // on scroll down, part of header will be sticky
            case eltd.body.hasClass('eltd-fixed-on-scroll'):
                eltd.modules.header.behaviour = 'eltd-fixed-on-scroll';
                var headerFixed = function(){
                    if(eltd.scroll < headerMenuAreaOffset){
                        fixedHeaderWrapper.removeClass('fixed');
                        header.css('margin-bottom',0);}
                    else{
                        fixedHeaderWrapper.addClass('fixed');
                        header.css('margin-bottom',fixedHeaderWrapper.height());
                    }
                };

                headerFixed();

                $(window).scroll(function() {
                    headerFixed();
                });

                break;
        }
    }

    /**
     * Show/hide side area
     */
    function eltdSideArea() {

        var wrapper = $('.eltd-wrapper'),
            sideMenu = $('.eltd-side-menu'),
            sideMenuButtonOpen = $('a.eltd-side-menu-button-opener'),
            cssClass,
        //Flags
            slideFromRight = false,
            slideWithContent = false,
            slideUncovered = false;

        if (eltd.body.hasClass('eltd-side-menu-slide-from-right')) {

            cssClass = 'eltd-right-side-menu-opened';
            wrapper.prepend('<div class="eltd-cover"/>');
            slideFromRight = true;

        } else if (eltd.body.hasClass('eltd-side-menu-slide-with-content')) {

            cssClass = 'eltd-side-menu-open';
            slideWithContent = true;

        } else if (eltd.body.hasClass('eltd-side-area-uncovered-from-content')) {

            cssClass = 'eltd-right-side-menu-opened';
            slideUncovered = true;

        }

        $('a.eltd-side-menu-button-opener, a.eltd-close-side-menu').on('click', function(e) {
            e.preventDefault();

            if(!sideMenuButtonOpen.hasClass('opened')) {

                sideMenuButtonOpen.addClass('opened');
                eltd.body.addClass(cssClass);

                if (slideFromRight) {
                    $('.eltd-wrapper .eltd-cover').on('click',function() {
                        eltd.body.removeClass('eltd-right-side-menu-opened');
                        sideMenuButtonOpen.removeClass('opened');
                    });
                }

                if (slideUncovered) {
                    sideMenu.css({
                        'visibility' : 'visible'
                    });
                }

                var currentScroll = $(window).scrollTop();
                $(window).scroll(function() {
                    if(Math.abs(eltd.scroll - currentScroll) > 400){
                        eltd.body.removeClass(cssClass);
                        sideMenuButtonOpen.removeClass('opened');
                        if (slideUncovered) {
                            var hideSideMenu = setTimeout(function(){
                                sideMenu.css({'visibility':'hidden'});
                                clearTimeout(hideSideMenu);
                            },400);
                        }
                    }
                });

            } else {

                sideMenuButtonOpen.removeClass('opened');
                eltd.body.removeClass(cssClass);
                if (slideUncovered) {
                    var hideSideMenu = setTimeout(function(){
                        sideMenu.css({'visibility':'hidden'});
                        clearTimeout(hideSideMenu);
                    },400);
                }

            }

            if (slideWithContent) {

                e.stopPropagation();
                wrapper.on('click',function() {
                    e.preventDefault();
                    sideMenuButtonOpen.removeClass('opened');
                    eltd.body.removeClass('eltd-side-menu-open');
                });

            }

        });

    }

    /*
    **  Smooth scroll functionality for Side Area
    */
    function eltdSideAreaScroll(){

        var sideMenu = $('.eltd-side-menu');

        if(sideMenu.length){    
            sideMenu.niceScroll({ 
                scrollspeed: 60,
                mousescrollstep: 40,
                cursorwidth: 0, 
                cursorborder: 0,
                cursorborderradius: 0,
                cursorcolor: "transparent",
                autohidemode: false, 
                horizrailenabled: false 
            });
        }
    }

    /**
     * Init Fullscreen Menu
     */
    function eltdFullscreenMenu() {

        if ($('a.eltd-fullscreen-menu-opener').length){

            var popupMenuOpener = $( 'a.eltd-fullscreen-menu-opener'),
                popupMenuHolderOuter = $(".eltd-fullscreen-menu-holder-outer"),
                cssClass,
            //Flags for type of animation
                fadeRight = false,
                fadeTop = false,
            //Widgets
                widgetAboveNav = $('.eltd-fullscreen-above-menu-widget-holder'),
                widgetBelowNav = $('.eltd-fullscreen-below-menu-widget-holder'),
            //Menu
                menuItems = $('.eltd-fullscreen-menu-holder-outer nav > ul > li > a'),
                menuItemWithChild =  $('.eltd-fullscreen-menu > ul li.has_sub > a'),
                menuItemWithoutChild = $('.eltd-fullscreen-menu ul li:not(.has_sub) a');

            //set height of popup holder and initialize nicescroll
            popupMenuHolderOuter.height(eltd.windowHeight).niceScroll({
                scrollspeed: 30,
                mousescrollstep: 20,
                cursorwidth: 0,
                cursorborder: 0,
                cursorborderradius: 0,
                cursorcolor: "transparent",
                autohidemode: false,
                horizrailenabled: false
            }); //200 is top and bottom padding of holder

            //set height of popup holder on resize
            $(window).resize(function() {
                popupMenuHolderOuter.height(eltd.windowHeight);
            });

            if (eltd.body.hasClass('eltd-fade-push-text-right')) {
                cssClass = 'eltd-push-nav-right';
                fadeRight = true;
            } else if (eltd.body.hasClass('eltd-fade-push-text-top')) {
                cssClass = 'eltd-push-text-top';
                fadeTop = true;
            }

            //Appearing animation
            if (fadeRight || fadeTop) {
                if (widgetAboveNav.length) {
                    widgetAboveNav.children().css({
                        '-webkit-animation-delay' : 0 + 'ms',
                        '-moz-animation-delay' : 0 + 'ms',
                        'animation-delay' : 0 + 'ms'
                    });
                }
                menuItems.each(function(i) {
                    $(this).css({
                        '-webkit-animation-delay': (i+1) * 70 + 'ms',
                        '-moz-animation-delay': (i+1) * 70 + 'ms',
                        'animation-delay': (i+1) * 70 + 'ms'
                    });
                });
                if (widgetBelowNav.length) {
                    widgetBelowNav.children().css({
                        '-webkit-animation-delay' : (menuItems.length + 1)*70 + 'ms',
                        '-moz-animation-delay' : (menuItems.length + 1)*70 + 'ms',
                        'animation-delay' : (menuItems.length + 1)*70 + 'ms'
                    });
                }
            }

            // Open popup menu
            popupMenuOpener.on('click',function(e){
                e.preventDefault();

                if (!popupMenuOpener.hasClass('opened')) {
                    popupMenuOpener.addClass('opened');
                    eltd.body.addClass('eltd-fullscreen-menu-opened');
                    eltd.body.removeClass('eltd-fullscreen-fade-out').addClass('eltd-fullscreen-fade-in');
                    eltd.body.removeClass(cssClass);
                    if(!eltd.body.hasClass('page-template-full_screen-php')){
                        eltd.modules.common.eltdDisableScroll();
                    }
                    $(document).keyup(function(e){
                        if (e.keyCode == 27 ) {
                            popupMenuOpener.removeClass('opened');
                            eltd.body.removeClass('eltd-fullscreen-menu-opened');
                            eltd.body.removeClass('eltd-fullscreen-fade-in').addClass('eltd-fullscreen-fade-out');
                            eltd.body.addClass(cssClass);
                            if(!eltd.body.hasClass('page-template-full_screen-php')){
                                eltd.modules.common.eltdEnableScroll();
                            }
                            $("nav.eltd-fullscreen-menu ul.sub_menu").slideUp(200, function(){
                                $('nav.popup_menu').getNiceScroll().resize();
                            });
                        }
                    });
                } else {
                    popupMenuOpener.removeClass('opened');
                    eltd.body.removeClass('eltd-fullscreen-menu-opened');
                    eltd.body.removeClass('eltd-fullscreen-fade-in').addClass('eltd-fullscreen-fade-out');
                    eltd.body.addClass(cssClass);
                    if(!eltd.body.hasClass('page-template-full_screen-php')){
                        eltd.modules.common.eltdEnableScroll();
                    }
                    $("nav.eltd-fullscreen-menu ul.sub_menu").slideUp(200, function(){
                        $('nav.popup_menu').getNiceScroll().resize();
                    });
                }
            });

            //logic for open sub menus in popup menu
            menuItemWithChild.on('tap click', function(e) {
                e.preventDefault();

                if ($(this).parent().hasClass('has_sub')) {
                    var submenu = $(this).parent().find('> ul.sub_menu');
                    if (submenu.is(':visible')) {
                        submenu.slideUp(200, function() {
                            popupMenuHolderOuter.getNiceScroll().resize();
                        });
                        $(this).parent().removeClass('open_sub');
                    } else {
                        $(this).parent().addClass('open_sub');
                        submenu.slideDown(200, function() {
                            popupMenuHolderOuter.getNiceScroll().resize();
                        });
                    }
                }
                return false;
            });

            //if link has no submenu and if it's not dead, than open that link
            menuItemWithoutChild.on('click',function (e) {

                if(($(this).attr('href') !== "http://#") && ($(this).attr('href') !== "#")){

                    if (e.which == 1) {
                        popupMenuOpener.removeClass('opened');
                        eltd.body.removeClass('eltd-fullscreen-menu-opened');
                        eltd.body.removeClass('eltd-fullscreen-fade-in').addClass('eltd-fullscreen-fade-out');
                        eltd.body.addClass(cssClass);
                        $("nav.eltd-fullscreen-menu ul.sub_menu").slideUp(200, function(){
                            $('nav.popup_menu').getNiceScroll().resize();
                        });
                        eltd.modules.common.eltdEnableScroll();
                    }
                }else{
                    return false;
                }

            });

        }

    }

    function eltdInitMobileNavigation() {
        var navigationOpener = $('.eltd-mobile-header .eltd-mobile-menu-opener');
        var navigationHolder = $('.eltd-mobile-header .eltd-mobile-nav');
        var dropdownOpener = $('.eltd-mobile-nav .mobile_arrow, .eltd-mobile-nav h4, .eltd-mobile-nav a[href*="#"]');
        var animationSpeed = 200;

        //whole mobile menu opening / closing
        if(navigationOpener.length && navigationHolder.length) {
            navigationOpener.on('tap click', function(e) {
                e.stopPropagation();
                e.preventDefault();

                if(navigationHolder.is(':visible')) {
                    navigationHolder.slideUp(animationSpeed);
                } else {
                    navigationHolder.slideDown(animationSpeed);
                }
            });
        }

        //dropdown opening / closing
        if(dropdownOpener.length) {
            dropdownOpener.each(function() {
                $(this).on('tap click', function(e) {
                    var dropdownToOpen = $(this).nextAll('ul').first();

                    if(dropdownToOpen.length) {
                        e.preventDefault();
                        e.stopPropagation();

                        var openerParent = $(this).parent('li');
                        if(dropdownToOpen.is(':visible')) {
                            dropdownToOpen.slideUp(animationSpeed);
                            openerParent.removeClass('eltd-opened');
                        } else {
                            dropdownToOpen.slideDown(animationSpeed);
                            openerParent.addClass('eltd-opened');
                        }
                    }

                });
            });
        }

        $('.eltd-mobile-nav a, .eltd-mobile-logo-wrapper a').on('click tap', function(e) {
            if($(this).attr('href') !== 'http://#' && $(this).attr('href') !== '#') {
                navigationHolder.slideUp(animationSpeed);
            }
        });
    }

    function eltdMobileHeaderBehavior() {
        if(eltd.body.hasClass('eltd-sticky-up-mobile-header')) {
            var stickyAppearAmount;
            var mobileHeader = $('.eltd-mobile-header');
            var adminBar     = $('#wpadminbar');
            var mobileHeaderHeight = mobileHeader.length ? mobileHeader.height() : 0;
            var adminBarHeight = adminBar.length ? adminBar.height() : 0;

            var docYScroll1 = $(document).scrollTop();
            stickyAppearAmount = mobileHeaderHeight + adminBarHeight;

            $(window).scroll(function() {
                var docYScroll2 = $(document).scrollTop();

                if(docYScroll2 > stickyAppearAmount) {
                    mobileHeader.addClass('eltd-animate-mobile-header');
                } else {
                    mobileHeader.removeClass('eltd-animate-mobile-header');
                }

                if((docYScroll2 > docYScroll1 && docYScroll2 > stickyAppearAmount) || (docYScroll2 < stickyAppearAmount)) {
                    mobileHeader.removeClass('mobile-header-appear');
                    mobileHeader.css('margin-bottom', 0);

                    if(adminBar.length) {
                        mobileHeader.find('.eltd-mobile-header-inner').css('top', 0);
                    }
                } else {
                    mobileHeader.addClass('mobile-header-appear');
                    mobileHeader.css('margin-bottom', stickyAppearAmount);

                    //if(adminBar.length) {
                    //    mobileHeader.find('.eltd-mobile-header-inner').css('top', adminBarHeight);
                    //}
                }

                docYScroll1 = $(document).scrollTop();
            });
        }

    }


    /**
     * Set dropdown position
     */
    function eltdSetDropDownMenuPosition(){

        var menuItems = $(".eltd-drop-down > ul > li.narrow");
        menuItems.each( function(i) {

            var browserWidth = eltd.windowWidth-16; // 16 is width of scroll bar
            var menuItemPosition = $(this).offset().left;
            var dropdownMenuWidth = $(this).find('.second .inner ul').width();

            var menuItemFromLeft = 0;
            if(eltd.body.hasClass('boxed')){
                menuItemFromLeft = eltd.boxedLayoutWidth  - (menuItemPosition - (browserWidth - eltd.boxedLayoutWidth )/2);
            } else {
                menuItemFromLeft = browserWidth - menuItemPosition;
            }

            var dropDownMenuFromLeft; //has to stay undefined beacuse 'dropDownMenuFromLeft < dropdownMenuWidth' condition will be true

            if($(this).find('li.sub').length > 0){
                dropDownMenuFromLeft = menuItemFromLeft - dropdownMenuWidth;
            }

            if(menuItemFromLeft < dropdownMenuWidth || dropDownMenuFromLeft < dropdownMenuWidth){
                $(this).find('.second').addClass('right');
                $(this).find('.second .inner ul').addClass('right');
            }
        });

    }


    function eltdDropDownMenu() {

        var menu_items = $('.eltd-drop-down > ul > li');

        menu_items.each(function(i) {
            if($(menu_items[i]).find('.second').length > 0) {

                var dropDownSecondDiv = $(menu_items[i]).find('.second');

                if($(menu_items[i]).hasClass('wide')) {

                    var dropdown = $(this).find('.inner > ul');
                    var dropdownPadding = parseInt(dropdown.css('padding-left').slice(0, -2)) + parseInt(dropdown.css('padding-right').slice(0, -2));
                    var dropdownWidth = dropdown.outerWidth();

                    if(!$(this).hasClass('left_position') && !$(this).hasClass('right_position')) {
                        dropDownSecondDiv.css('left', 0);
                    }

                    //set columns to be same height - start
                    var tallest = 0;
                    $(this).find('.second > .inner > ul > li').each(function() {
                        var thisHeight = $(this).height();
                        if(thisHeight > tallest) {
                            tallest = thisHeight;
                        }
                    });
                    $(this).find('.second > .inner > ul > li').css("height", ""); // delete old inline css - via resize
                    $(this).find('.second > .inner > ul > li').height(tallest);
                    //set columns to be same height - end

                    if(!eltd.body.hasClass('eltd-full-width-wide-menu')) {
                        if(!$(this).hasClass('left_position') && !$(this).hasClass('right_position')) {
                            var left_position = (eltd.windowWidth - 2 * (eltd.windowWidth - dropdown.offset().left)) / 2 + (dropdownWidth + dropdownPadding) / 2;
                            dropDownSecondDiv.css('left', -left_position);
                        }
                    } else {
                        if(!$(this).hasClass('left_position') && !$(this).hasClass('right_position')) {
                            var left_position = dropdown.offset().left;

                            dropDownSecondDiv.css('left', -left_position);
                            dropDownSecondDiv.css('width', eltd.windowWidth);

                        }
                    }
                }

                if(!eltd.menuDropdownHeightSet) {
                    $(menu_items[i]).data('original_height', dropDownSecondDiv.height() + 'px');
                    dropDownSecondDiv.height(0);
                }

                if(navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
                    $(menu_items[i]).on("touchstart mouseenter", function() {
                        dropDownSecondDiv.css({
                            'height': $(menu_items[i]).data('original_height'),
                            'overflow': 'visible',
                            'visibility': 'visible',
                            'opacity': '1'
                        });
                    }).on("mouseleave", function() {
                        dropDownSecondDiv.css({
                            'height': '0px',
                            'overflow': 'hidden',
                            'visibility': 'hidden',
                            'opacity': '0'
                        });
                    });

                } else {
                    if(eltd.body.hasClass('eltd-dropdown-animate-height')) {
                        $(menu_items[i]).mouseenter(function() {
                            dropDownSecondDiv.css({
                                'visibility': 'visible',
                                'height': '0px',
                                'opacity': '0'
                            });
                            dropDownSecondDiv.stop().animate({
                                'height': $(menu_items[i]).data('original_height'),
                                opacity: 1
                            }, 200, function() {
                                dropDownSecondDiv.css('overflow', 'visible');
                            });
                        }).mouseleave(function() {
                            dropDownSecondDiv.stop().animate({
                                'height': '0px'
                            }, 0, function() {
                                dropDownSecondDiv.css({
                                    'overflow': 'hidden',
                                    'visibility': 'hidden'
                                });
                            });
                        });
                    } else {
                        var config = {
                            interval: 0,
                            over: function() {
                                setTimeout(function() {
                                    dropDownSecondDiv.addClass('eltd-drop-down-start');
                                    dropDownSecondDiv.stop().css({'height': $(menu_items[i]).data('original_height')});
                                }, 150);
                            },
                            timeout: 150,
                            out: function() {
                                dropDownSecondDiv.stop().css({'height': '0px'});
                                dropDownSecondDiv.removeClass('eltd-drop-down-start');
                            }
                        };
                        $(menu_items[i]).hoverIntent(config);
                    }
                }
            }
        });
         $('.eltd-drop-down ul li.wide ul li a').on('click', function(e) {
            if (e.which == 1){
                var $this = $(this);
                setTimeout(function() {
                    $this.mouseleave();
                }, 500);
            }
        });

        eltd.menuDropdownHeightSet = true;
    }

    /**
     * Init Search Types
     */
    function eltdSearch() {

        var searchOpener = $('a.eltd-search-opener'),
            searchClose,
            searchForm,
            touch = false;

        if ( $('html').hasClass( 'touch' ) ) {
            touch = true;
        }

        if ( searchOpener.length > 0 ) {
            //Check for type of search
            if ( eltd.body.hasClass( 'eltd-fullscreen-search' ) ) {

                var fullscreenSearchFade = false,
                    fullscreenSearchFromCircle = false;

                searchClose = $( '.eltd-fullscreen-search-close' );

                if (eltd.body.hasClass('eltd-search-fade')) {
                    fullscreenSearchFade = true;
                } else if (eltd.body.hasClass('eltd-search-from-circle')) {
                    fullscreenSearchFromCircle = true;
                }
                eltdFullscreenSearch( fullscreenSearchFade, fullscreenSearchFromCircle );

            } else if ( eltd.body.hasClass( 'eltd-search-slides-from-window-top' ) ) {

                searchForm = $('.eltd-search-slide-window-top');
                searchClose = $('.eltd-search-close');
                eltdSearchWindowTop();

            } else if ( eltd.body.hasClass( 'eltd-search-slides-from-header-bottom' ) ) {

                eltdSearchHeaderBottom();

            } else if ( eltd.body.hasClass( 'eltd-search-covers-header' ) ) {

                eltdSearchCoversHeader();

            }

            //Check for hover color of search
            if(typeof searchOpener.data('hover-color') !== 'undefined') {
                var changeSearchColor = function(event) {
                    event.data.searchOpener.css('color', event.data.color);
                };

                var originalColor = searchOpener.css('color');
                var hoverColor = searchOpener.data('hover-color');

                searchOpener.on('mouseenter', { searchOpener: searchOpener, color: hoverColor }, changeSearchColor);
                searchOpener.on('mouseleave', { searchOpener: searchOpener, color: originalColor }, changeSearchColor);
            }

        }

        /**
         * Search slides from window top type of search
         */
        function eltdSearchWindowTop() {

            searchOpener.on('click', function(e) {
                e.preventDefault();

                var yPos = 0;
                if($('.title').hasClass('has_parallax_background')){
                    yPos = parseInt($('.title.has_parallax_background').css('backgroundPosition').split(" ")[1]);
                }

                if ( searchForm.height() === 0) {
                    $('.eltd-search-slide-window-top input[type="text"]').focus();
                    //Push header bottom
                    eltd.body.addClass('eltd-search-open');
                    $('.title.has_parallax_background').animate({
                        'background-position-y': (yPos + 50)+'px'
                    }, 150);
                } else {
                    eltd.body.removeClass('eltd-search-open');
                    $('.title.has_parallax_background').animate({
                        'background-position-y': (yPos - 50)+'px'
                    }, 150);
                }

                $(window).scroll(function() {
                    if ( searchForm.height() !== 0 && eltd.scroll > 50 ) {
                        eltd.body.removeClass('eltd-search-open');
                        $('.title.has_parallax_background').css('backgroundPosition', 'center '+(yPos)+'px');
                    }
                });

                searchClose.on('click',function(e){
                    e.preventDefault();
                    eltd.body.removeClass('eltd-search-open');
                    $('.title.has_parallax_background').animate({
                        'background-position-y': (yPos)+'px'
                    }, 150);
                });

            });
        }

        /**
         * Search slides from header bottom type of search
         */
        function eltdSearchHeaderBottom() {

            var searchInput = $('.eltd-search-slide-header-bottom input[type="submit"]');

            searchOpener.on('click', function(e) {
                e.preventDefault();

                //If there is form openers in multiple widgets, only one search form should be opened
                if ( $(this).closest('.eltd-mobile-header').length > 0 ) {
                    //    Open form in mobile header
                    searchForm = $(this).closest('.eltd-mobile-header').children().children().first();
                } else if ( $(this).closest('.eltd-sticky-header').length > 0 ) {
                    //    Open form in sticky header
                    searchForm= $(this).closest('.eltd-sticky-header').children().first();
                } else {
                    //Open first form in header
                    searchForm = $('.eltd-search-slide-header-bottom').first();
                }

                if( searchForm.hasClass( 'eltd-animated' ) ) {
                    searchForm.removeClass('eltd-animated');
                } else {
                    searchForm.addClass('eltd-animated');
                }

                searchForm.addClass('eltd-disabled');
                searchInput.attr('disabled','eltd-disabled');
                if( ( $('.eltd-search-slide-header-bottom .eltd-search-field').val() !== '' ) && ( $('.eltd-search-slide-header-bottom .eltd-search-field').val() !== ' ' ) ) {
                    searchInput.removeAttr('eltd-disabled');
                    searchForm.removeClass('eltd-disabled');
                } else {
                    searchForm.addClass('eltd-disabled');
                    searchInput.attr('disabled','eltd-disabled');
                }

                $('.eltd-search-slide-header-bottom .eltd-search-field').keyup(function() {
                    if( ($(this).val() !== '' ) && ( $(this).val() != ' ') ) {
                        searchInput.removeAttr('eltd-disabled');
                        searchForm.removeClass('eltd-disabled');
                    }
                    else {
                        searchInput.attr('disabled','eltd-disabled');
                        searchForm.addClass('eltd-disabled');
                    }
                });

                $('.content, footer').on('click',function(e){
                    e.preventDefault();
                    searchForm.removeClass('eltd-animated');
                });

            });

            //Submit form
            if($('.eltd-search-submit').length) {
                $('.eltd-search-submit').on('click',function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    searchForm.submit();
                });
            }
        }

        /**
         * Search covers header type of search
         */
        function eltdSearchCoversHeader() {

            searchOpener.on('click', function(e) {
                e.preventDefault();
                var searchFormHeight,
                    searchFormHolder = $('.eltd-search-cover .eltd-form-holder-outer'),
                    searchForm,
                    searchFormLandmark; // there is one more div element if header is in grid

                if($(this).closest('.eltd-grid').length){
                    searchForm = $(this).closest('.eltd-grid').children().first();
                    searchFormLandmark = searchForm.parent();
                }
                else{
                    searchForm = $(this).closest('.eltd-menu-area').children().first();
                    searchFormLandmark = searchForm;
                }

                if ( $(this).closest('.eltd-sticky-header').length > 0 ) {
                    searchForm = $(this).closest('.eltd-sticky-header').children().first();
                }
                if ( $(this).closest('.eltd-mobile-header').length > 0 ) {
                    searchForm = $(this).closest('.eltd-mobile-header').children().children().first();
                }

                //Find search form position in header and height
                if ( searchFormLandmark.parent().hasClass('eltd-logo-area') ) {
                    searchFormHeight = eltdGlobalVars.vars.eltdLogoAreaHeight;
                } else if ( searchFormLandmark.parent().hasClass('eltd-top-bar') ) {
                    searchFormHeight = eltdGlobalVars.vars.eltdTopBarHeight;
                } else if ( searchFormLandmark.parent().hasClass('eltd-menu-area') ) {
                    searchFormHeight = eltdGlobalVars.vars.eltdMenuAreaHeight;
                } else if ( searchFormLandmark.hasClass('eltd-sticky-header') ) {
                    searchFormHeight = eltdGlobalVars.vars.eltdMenuAreaHeight;
                } else if ( searchFormLandmark.parent().hasClass('eltd-mobile-header') ) {
                    searchFormHeight = $('.eltd-mobile-header-inner').height();
                }

                searchFormHolder.height(searchFormHeight);
                searchForm.stop(true).fadeIn(600);
                $('.eltd-search-cover input[type="text"]').focus();
                $('.eltd-search-close, .content, footer').on('click',function(e){
                    e.preventDefault();
                    searchForm.stop(true).fadeOut(450);
                });
                searchForm.blur(function() {
                    searchForm.stop(true).fadeOut(450);
                });
            });

        }

        /**
         * Fullscreen search (two types: fade and from circle)
         */
        function eltdFullscreenSearch( fade, fromCircle ) {

            var searchHolder = $( '.eltd-fullscreen-search-holder'),
                searchOverlay = $( '.eltd-fullscreen-search-overlay' ),
                fieldHolder = searchHolder.find('.eltd-field-holder');

            searchOpener.on('click', function(e) {
                e.preventDefault();

                //Fullscreen search fade
                if ( searchHolder.hasClass( 'eltd-animate' ) ) {
                    searchClose();
                } else {
                    searchOpen();
                }

                //Close on click away
                $(document).mouseup(function (e) {
                    if (!fieldHolder.is(e.target) && fieldHolder.has(e.target).length === 0)  {
                        e.preventDefault();
                        searchClose();
                    }
                });
                //Close on escape
                $(document).keyup(function(e){
                    if (e.keyCode == 27 ) { //KeyCode for ESC button is 27
                        searchClose();
                    }
                });

                function searchClose() {
                    eltd.body.removeClass('eltd-fullscreen-search-opened');
                    searchHolder.removeClass('eltd-animate');
                    eltd.body.removeClass('eltd-search-fade-in');
                    eltd.body.addClass('eltd-search-fade-out');
                    if(!eltd.body.hasClass('page-template-full_screen-php')){
                        eltd.modules.common.eltdEnableScroll();
                    }
                    fieldHolder.find('.eltd-search-field').blur().val('');
                }

                function searchOpen() {
                    eltd.body.addClass('eltd-fullscreen-search-opened');
                    eltd.body.removeClass('eltd-search-fade-out');
                    eltd.body.addClass('eltd-search-fade-in');
                    searchHolder.addClass('eltd-animate');
                    if(!eltd.body.hasClass('page-template-full_screen-php')){
                        eltd.modules.common.eltdDisableScroll();
                    }
                    setTimeout(function(){
                        fieldHolder.find('.eltd-search-field').focus();
                    },400);
                }

            });

            //Text input focus change
            $('.eltd-fullscreen-search-holder .eltd-search-field').focus(function(){
                $('.eltd-fullscreen-search-holder .eltd-field-holder .eltd-line').css("width","100%");
            });

            $('.eltd-fullscreen-search-holder .eltd-search-field').blur(function(){
                $('.eltd-fullscreen-search-holder .eltd-field-holder .eltd-line').css("width","0");
            });

        }

    }

    /**
     * Function object that represents vertical menu area.
     * @returns {{init: Function}}
     */
    var eltdVerticalMenu = function() {
        /**
         * Main vertical area object that used through out function
         * @type {jQuery object}
         */
        var verticalMenuObject = $('.eltd-vertical-menu-area');

        /**
         * Resizes vertical area. Called whenever height of navigation area changes
         * It first check if vertical area is scrollable, and if it is resizes scrollable area
         */
        var resizeVerticalArea = function() {
           if(verticalAreaScrollable()) {
               verticalMenuObject.getNiceScroll().resize();
           }
        };

        /**
         * Checks if vertical area is scrollable (if it has eltd-with-scroll class)
         *
         * @returns {bool}
         */
        var verticalAreaScrollable = function() {
           return verticalMenuObject.hasClass('.eltd-with-scroll');
        };

        /**
         * Initialzes navigation functionality. It checks navigation type data attribute and calls proper functions
         */
        var initNavigation = function() {
            var verticalNavObject = verticalMenuObject.find('.eltd-vertical-menu');
            var navigationType = typeof verticalNavObject.data('navigation-type') !== 'undefined' ? verticalNavObject.data('navigation-type') : '';
            dropdownClickToggle();
            
            /**
             * Initializes hover toggle navigation type. It has separate functionalities for touch and no-touch devices
             */
            function dropdownHoverToggle() {
               var menuItems = verticalNavObject.find('ul li.menu-item-has-children');

               menuItems.each(function() {
                   var elementToExpand = $(this).find(' > .second, > ul');
                   var numberOfChildItems = elementToExpand.find(' > .inner > ul > li, > li').length;

                   var animSpeed = numberOfChildItems * 40;
                   var animFunc = 'easeInOutSine';
                   var that = this;

                   //touch devices functionality
                   if(Modernizr.touch) {
                       var dropdownOpener = $(this).find('> a');

                       dropdownOpener.on('click tap', function(e) {
                           e.preventDefault();
                           e.stopPropagation();

                           if(elementToExpand.is(':visible')) {
                               $(that).removeClass('open');
                               elementToExpand.slideUp(animSpeed, animFunc, function() {
                                   resizeVerticalArea();
                               });
                           } else {
                               $(that).addClass('open');
                               elementToExpand.slideDown(animSpeed, animFunc, function() {
                                   resizeVerticalArea();
                               });
                           }
                       });
                   } else {
                       $(this).on('mouseenter', function() {
                           $(that).addClass('open');
                           elementToExpand.slideDown(animSpeed, animFunc, function() {
                               resizeVerticalArea();
                           });
                       });

					   $(this).on('mouseleave', function() {
						   setTimeout(function() {
							   $(that).removeClass('open');
							   elementToExpand.slideUp(animSpeed, animFunc, function() {
								   resizeVerticalArea();
							   });
						   }, 100);
					   });
                   }
               });
            }

            /**
             * Initializes click toggle navigation type. Works the same for touch and no-touch devices
             */
            function dropdownClickToggle() {
                var menuItems = verticalNavObject.find('ul li.menu-item-has-children').not('.second li');
            
                menuItems.each(function() {
                    var elementToExpand = $(this).find(' > .second, > ul');
                    var menuItem = this;
                    var dropdownOpener = $(this).find('> a');
                    var slideUpSpeed = 'fast';
                    var slideDownSpeed = 'slow';
            
                    dropdownOpener.on('click tap', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
            
                        if(elementToExpand.is(':visible')) {
                            $(menuItem).removeClass('open');
                            elementToExpand.slideUp(slideUpSpeed, function() {
                                resizeVerticalArea();
                            });
                        } else {
                            if(!$(this).parents('li').hasClass('open')) {
                                menuItems.removeClass('open');
                                menuItems.find(' > .second, > ul').slideUp(slideUpSpeed);
                            }
            
                            $(menuItem).addClass('open');
                            elementToExpand.slideDown(slideDownSpeed, function() {
                                resizeVerticalArea();
                            });
                        }
                    });
                });
            }

            /**
             * Initializes floating navigation type (it comes from the side as a dropdown)
             */
            function dropdownFloat() {
                var menuItems = verticalNavObject.find('ul li.menu-item-has-children');
                var allDropdowns = menuItems.find(' > .second, > ul');

                menuItems.each(function() {
                    var elementToExpand = $(this).find(' > .second, > ul');
                    var menuItem = this;

                    if(Modernizr.touch) {
                        var dropdownOpener = $(this).find('> a');

                        dropdownOpener.on('click tap', function(e) {
                            e.preventDefault();
                            e.stopPropagation();

                            if(elementToExpand.hasClass('eltd-float-open')) {
                                elementToExpand.removeClass('eltd-float-open');
                                $(menuItem).removeClass('open');
                            } else {
                                if(!$(this).parents('li').hasClass('open')) {
                                    menuItems.removeClass('open');
                                    allDropdowns.removeClass('eltd-float-open');
                                }

                                elementToExpand.addClass('eltd-float-open');
                                $(menuItem).addClass('open');
                            }
                        });
                    } else {
                        //must use hoverIntent because basic hover effect doesn't catch dropdown
                        //it doesn't start from menu item's edge
                        $(this).hoverIntent({
                            over: function() {
                                elementToExpand.addClass('eltd-float-open');
                                $(menuItem).addClass('open');
                            },
                            out: function() {
                                elementToExpand.removeClass('eltd-float-open');
                                $(menuItem).removeClass('open');
                            },
                            timeout: 300
                        });
                    }
                });
            }

            /**
             * Initializes slide in navigation type (dropdowns are coming on top of parent element and cover whole navigation area)
             */
            function dropdownSlideIn() {
               var menuItems = verticalNavObject.find('ul li.menu-item-has-children');
               var menuItemsLinks = menuItems.find('> a');

               menuItemsLinks.each(function() {
                   var elementToExpand = $(this).next('.second, ul');
                   appendToExpandableElement(elementToExpand, this);

                   if($(this).parent('li').is('.current-menu-ancestor', '.current_page_parent', '.current-menu-parent ')) {
                       elementToExpand.addClass('eltd-vertical-slide-open');
                   }

                   $(this).on('click tap', function(e) {
                       e.preventDefault();
                       e.stopPropagation();

                       menuItems.removeClass('open');

                       $(this).parent('li').addClass('open');
                       elementToExpand.addClass('eltd-vertical-slide-open');
                   });
               });

               var previousLevelItems = menuItems.find('li.eltd-previous-level > a');

               previousLevelItems.on('click tap', function(e) {
                   e.preventDefault();
                   e.stopPropagation();

                   menuItems.removeClass('open');
                   $(this).parents('.eltd-vertical-slide-open').first().removeClass('eltd-vertical-slide-open');
               });

               /**
                * Appends 'li' element as first element in dropdown, which will close current dropdown when clicked
                * @param {jQuery object} elementToExpand current dropdown to append element to
                * @param currentMenuItem
                */
               function appendToExpandableElement(elementToExpand, currentMenuItem) {
                   var itemUrl = $(currentMenuItem).attr('href');
                   var itemText = $(currentMenuItem).text();

                   var liItem = $('<li />', {class: 'eltd-previous-level'});

                   $('<a />', {
                       'href': itemUrl,
                       'html': '<i class="eltd-vertical-slide-arrow fa fa-angle-left"></i>' + itemText
                   }).appendTo(liItem);

                   if(elementToExpand.hasClass('second')) {
                       elementToExpand.find('> div > ul').prepend(liItem);
                   } else {
                       elementToExpand.prepend(liItem);
                   }
               }
            }
        };

        /**
         * Initializes scrolling in vertical area. It checks if vertical area is scrollable before doing so
         */
        //var initVerticalAreaScroll = function() {
        //    if(verticalAreaScrollable()) {
        //        verticalMenuObject.niceScroll({
        //            scrollspeed: 60,
        //            mousescrollstep: 40,
        //            cursorwidth: 0,
        //            cursorborder: 0,
        //            cursorborderradius: 0,
        //            cursorcolor: "transparent",
        //            autohidemode: false,
        //            horizrailenabled: false
        //        });
        //    }
        //};

        //var initHiddenVerticalArea = function() {
        //    var verticalLogo = $('.eltd-vertical-area-bottom-logo');
        //    var verticalMenuOpener = verticalMenuObject.find('.eltd-vertical-menu-hidden-button');
        //    var scrollPosition = 0;
        //
        //    verticalMenuOpener.on('click tap', function() {
        //        if(isVerticalAreaOpen()) {
        //            closeVerticalArea();
        //        } else {
        //            openVerticalArea();
        //        }
        //    });
        //
        //    //take click outside vertical left/right area and close it
        //    $j(verticalMenuObject).outclick({
        //        callback: function() {
        //            closeVerticalArea();
        //        }
        //    });
        //
        //    $(window).scroll(function() {
        //        if(Math.abs($(window).scrollTop() - scrollPosition) > 400){
        //            closeVerticalArea();
        //        }
        //    });
        //
        //    /**
        //     * Closes vertical menu area by removing 'active' class on that element
        //     */
        //    function closeVerticalArea() {
        //        verticalMenuObject.removeClass('active');
        //
        //        if(verticalLogo.length) {
        //            verticalLogo.removeClass('active');
        //        }
        //    }
        //
        //    /**
        //     * Opens vertical menu area by adding 'active' class on that element
        //     */
        //    function openVerticalArea() {
        //        verticalMenuObject.addClass('active');
        //
        //        if(verticalLogo.length) {
        //            verticalLogo.addClass('active');
        //        }
        //
        //        scrollPosition = $(window).scrollTop();
        //    }
        //
        //    function isVerticalAreaOpen() {
        //        return verticalMenuObject.hasClass('active');
        //    }
        //};

        return {
            /**
             * Calls all necessary functionality for vertical menu area if vertical area object is valid
             */
            init: function() {
                if(verticalMenuObject.length) {
                    initNavigation();
                    //initVerticalAreaScroll();
                    //
                    //if(eltd.body.hasClass('eltd-vertical-header-hidden')) {
                    //    initHiddenVerticalArea();
                    //}
                }
            }
        };
    };

})(jQuery);
(function($) {
    "use strict";

    var title = {};
    eltd.modules.title = title;

    title.eltdParallaxTitle = eltdParallaxTitle;

    title.eltdOnDocumentReady = eltdOnDocumentReady;
    title.eltdOnWindowLoad = eltdOnWindowLoad;
    title.eltdOnWindowResize = eltdOnWindowResize;
    title.eltdOnWindowScroll = eltdOnWindowScroll;

    $(document).ready(eltdOnDocumentReady);
    $(window).load(eltdOnWindowLoad);
    $(window).resize(eltdOnWindowResize);
    $(window).scroll(eltdOnWindowScroll);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function eltdOnDocumentReady() {
        eltdParallaxTitle();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function eltdOnWindowLoad() {

    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function eltdOnWindowResize() {

    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function eltdOnWindowScroll() {

    }
    

    /*
     **	Title image with parallax effect
     */
    function eltdParallaxTitle(){
        if($('.eltd-title.eltd-has-parallax-background').length > 0 && $('.touch').length === 0){

            var parallaxBackground = $('.eltd-title.eltd-has-parallax-background');
            var parallaxBackgroundWithZoomOut = $('.eltd-title.eltd-has-parallax-background.eltd-zoom-out');

            var backgroundSizeWidth = parseInt(parallaxBackground.data('background-width').match(/\d+/));
            var titleHolderHeight = parallaxBackground.data('height');
            var titleRate = (titleHolderHeight / 10000) * 7;
            var titleYPos = -(eltd.scroll * titleRate);

            //set position of background on doc ready
            parallaxBackground.css({'background-position': 'center '+ (titleYPos+eltdGlobalVars.vars.eltdAddForAdminBar) +'px' });
            parallaxBackgroundWithZoomOut.css({'background-size': backgroundSizeWidth-eltd.scroll + 'px auto'});

            //set position of background on window scroll
            $(window).scroll(function() {
                titleYPos = -(eltd.scroll * titleRate);
                parallaxBackground.css({'background-position': 'center ' + (titleYPos+eltdGlobalVars.vars.eltdAddForAdminBar) + 'px' });
                parallaxBackgroundWithZoomOut.css({'background-size': backgroundSizeWidth-eltd.scroll + 'px auto'});
            });

        }
    }

})(jQuery);

(function($) {
	'use strict';

	var shortcodes = {};

	eltd.modules.shortcodes = shortcodes;

	shortcodes.eltdInitCounter = eltdInitCounter;
	shortcodes.eltdInitProgressBars = eltdInitProgressBars;
	shortcodes.eltdInitCountdown = eltdInitCountdown;
	shortcodes.eltdInitMessages = eltdInitMessages;
	shortcodes.eltdInitMessageHeight = eltdInitMessageHeight;
	shortcodes.eltdInitTestimonials = eltdInitTestimonials;
	shortcodes.eltdInitTestimonialsBlocks = eltdInitTestimonialsBlocks;
	shortcodes.eltdInitCarousels = eltdInitCarousels;
	shortcodes.eltdInitPieChart = eltdInitPieChart;
	shortcodes.eltdInitPieChartDoughnut = eltdInitPieChartDoughnut;
	shortcodes.eltdInitTabs = eltdInitTabs;
	shortcodes.eltdInitTabIcons = eltdInitTabIcons;
	shortcodes.eltdInitVariableWidthSlider = eltdInitVariableWidthSlider;
	shortcodes.eltdInitBlogListMasonry = eltdInitBlogListMasonry;
	shortcodes.eltdCustomFontResize = eltdCustomFontResize;
	shortcodes.eltdInitImageGallery = eltdInitImageGallery;
	shortcodes.eltdInitAccordions = eltdInitAccordions;
	shortcodes.eltdShowGoogleMap = eltdShowGoogleMap;
	shortcodes.eltdInitPortfolioListMasonry = eltdInitPortfolioListMasonry;
	shortcodes.eltdInitPortfolioListMasonryParallax = eltdInitPortfolioListMasonryParallax;
	shortcodes.eltdInitPortfolioListPinterest = eltdInitPortfolioListPinterest;
	shortcodes.eltdInitPortfolioListJustifiedGallery = eltdInitPortfolioListJustifiedGallery;
	shortcodes.eltdInitPortfolio = eltdInitPortfolio;
	shortcodes.eltdInitPortfolioMasonryFilter = eltdInitPortfolioMasonryFilter;
	shortcodes.eltdInitPortfolioLoadMore = eltdInitPortfolioLoadMore;
	shortcodes.eltdPortfolioTiledGallery = eltdPortfolioTiledGallery;
	shortcodes.eltdCheckSliderForHeaderStyle = eltdCheckSliderForHeaderStyle;
	shortcodes.eltdInfoBox = eltdInfoBox;
	shortcodes.eltdProcess = eltdProcess;
	shortcodes.eltdComparisonPricingTables = eltdComparisonPricingTables;
	shortcodes.eltdInitVerticalSplitSlider = eltdInitVerticalSplitSlider;
	shortcodes.eltdBlogSlider = eltdBlogSlider;
	shortcodes.eltdOnDocumentReady = eltdOnDocumentReady;
	shortcodes.eltdOnWindowLoad = eltdOnWindowLoad;
	shortcodes.eltdOnWindowResize = eltdOnWindowResize;
	shortcodes.eltdOnWindowScroll = eltdOnWindowScroll;
	shortcodes.emptySpaceResponsive = emptySpaceResponsive;
	shortcodes.eltdScrollDownIcon = eltdScrollDownIcon;
	shortcodes.eltdHidingImages = eltdHidingImages;

	$(document).ready(eltdOnDocumentReady);
	$(window).load(eltdOnWindowLoad);
	$(window).resize(eltdOnWindowResize);
	$(window).scroll(eltdOnWindowScroll);

	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function eltdOnDocumentReady() {
		eltdInitCounter();
		eltdInitProgressBars();
		eltdInitCountdown();
		eltdIcon().init();
		eltdInitMessages();
		eltdInitMessageHeight();
		eltdInitCarousels();
		eltdInitPieChart();
		eltdInitPieChartDoughnut();
		eltdInitTabs();
		eltdInitTabIcons();
		eltdButton().init();
		eltdInitBlogListMasonry();
		eltdCustomFontResize();
		eltdInitImageGallery();
		eltdBlogSlider();
		eltdInitAccordions();
		eltdShowGoogleMap();
		eltdInitPortfolioListMasonry();
		eltdInitPortfolioListPinterest();
		eltdInitPortfolio();
		eltdInitPortfolioMasonryFilter();
		eltdInitPortfolioLoadMore();
		eltdSlider().init();
		eltdSocialIconWidget().init();
		eltdInitVerticalSplitSlider();
        eltdInitTestimonials();
        eltdInitTestimonialsBlocks();
		eltdProcess().init();
		eltdComparisonPricingTables().init();
		emptySpaceResponsive().init();
		eltdScrollDownIcon();
		eltdInitVariableWidthSlider();
		eltdHidingImages();
	}

	/*
	 All functions to be called on $(window).load() should be in this function
	 */
	function eltdOnWindowLoad() {
		eltdInfoBox();
		eltdPortfolioTiledGallery();
		eltdInitPortfolioListJustifiedGallery();
	}

	/*
	 All functions to be called on $(window).resize() should be in this function
	 */
	function eltdOnWindowResize() {
		eltdInitBlogListMasonry();
		eltdCustomFontResize();
		eltdInitPortfolioListMasonry();
		eltdInitPortfolioListPinterest();
	}

	/*
	 All functions to be called on $(window).scroll() should be in this function
	 */
	function eltdOnWindowScroll() {

	}


	/**
	 * Counter Shortcode
	 */
	function eltdInitCounter() {

		var counters = $('.eltd-counter');


		if(counters.length) {
			counters.each(function() {
				var counter = $(this);
				counter.appear(function() {
					counter.parent().addClass('eltd-counter-holder-show');

					//Counter zero type
					if(counter.hasClass('zero')) {
						var max = parseFloat(counter.text());
						counter.countTo({
							from: 0,
							to: max,
							speed: 1500,
							refreshInterval: 100
						});
					} else {
						counter.absoluteCounter({
							speed: 2000,
							fadeInDelay: 1000
						});
					}

				}, {accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});
			});
		}

	}

	/*
	 **	Horizontal progress bars shortcode
	 */
	function eltdInitProgressBars() {

		var progressBar = $('.eltd-progress-bar');

		if(progressBar.length){

			progressBar.each(function(){

				var thisBar = $(this);

				function progressBarAnimation(){
					eltdInitToCounterProgressBar(thisBar);
					if(thisBar.find('.eltd-floating.eltd-floating-inside') !== 0) {
						var floatingInsideMargin = thisBar.find('.eltd-progress-content').height();
						floatingInsideMargin += parseFloat(thisBar.find('.eltd-progress-title-holder').css('padding-bottom'));
						floatingInsideMargin += parseFloat(thisBar.find('.eltd-progress-title-holder').css('margin-bottom'));
						thisBar.find('.eltd-floating-inside').css('margin-bottom', -(floatingInsideMargin) + 'px');
					}
					var percentage = thisBar.find('.eltd-progress-content').data('percentage'),
						progressContent = thisBar.find('.eltd-progress-content'),
						progressNumber = thisBar.find('.eltd-progress-number');

						progressContent.css('width', '0%');
						progressContent.animate({'width': percentage + '%'}, 1500);
						progressNumber.css('left', '0%');
						progressNumber.animate({'left': percentage + '%'}, 1500);
				}

				if(thisBar.parents(".eltd-vss-ms-section").length){
					// Do nothing unless section is active
					if(thisBar.parents(".eltd-vss-ms-section").hasClass("active") && !(thisBar.hasClass("activated"))){
						progressBarAnimation();
						thisBar.addClass("activated");
					}

				}
				else { 
					thisBar.appear(function(){
						progressBarAnimation();
					});
				}
			});
		}

	}

	/*
	 **	Counter for horizontal progress bars percent from zero to defined percent
	 */
	function eltdInitToCounterProgressBar(progressBar) {
		var percentage = parseFloat(progressBar.find('.eltd-progress-content').data('percentage'));
		var percent = progressBar.find('.eltd-progress-number .eltd-percent');
		if(percent.length) {
			percent.each(function() {
				var thisPercent = $(this);
				thisPercent.parents('.eltd-progress-number-wrapper').css('opacity', '1');
				thisPercent.countTo({
					from: 0,
					to: percentage,
					speed: 1500,
					refreshInterval: 50
				});
			});
		}
	}

	/*
	 **	Function to close message shortcode
	 */
	function eltdInitMessages() {
		var message = $('.eltd-message');
		if(message.length) {
			message.each(function() {
				var thisMessage = $(this);
				thisMessage.find('.eltd-close').on('click',function(e) {
					e.preventDefault();
					$(this).parent().parent().fadeOut(500);
				});
			});
		}
	}

	/*
	 **	Init message height
	 */
	function eltdInitMessageHeight() {
		var message = $('.eltd-message.eltd-with-icon');
		if(message.length) {
			message.each(function() {
				var thisMessage = $(this);
				var textHolderHeight = thisMessage.find('.eltd-message-text-holder').height();
				var iconHolderHeight = thisMessage.find('.eltd-message-icon-holder').height();

				if(textHolderHeight > iconHolderHeight) {
					thisMessage.find('.eltd-message-icon-holder').height(textHolderHeight);
				} else {
					thisMessage.find('.eltd-message-text-holder').height(iconHolderHeight);
				}
			});
		}
	}

	/**
	 * Countdown Shortcode
	 */
	function eltdInitCountdown() {

		var countdowns = $('.eltd-countdown'),
			year,
			month,
			day,
			hour,
			minute,
			timezone,
			monthLabel,
			dayLabel,
			hourLabel,
			minuteLabel,
			secondLabel;

		if(countdowns.length) {

			countdowns.each(function() {

				//Find countdown elements by id-s
				var countdownId = $(this).attr('id'),
					countdown = $('#' + countdownId),
					digitFontSize,
					digitColor,
					labelFontSize,
					labelColor;

				//Get data for countdown
				year = countdown.data('year');
				month = countdown.data('month');
				day = countdown.data('day');
				hour = countdown.data('hour');
				minute = countdown.data('minute');
				timezone = countdown.data('timezone');
				monthLabel = countdown.data('month-label');
				dayLabel = countdown.data('day-label');
				hourLabel = countdown.data('hour-label');
				minuteLabel = countdown.data('minute-label');
				secondLabel = countdown.data('second-label');
				digitFontSize = countdown.data('digit-size');
				digitColor = countdown.data('digit-color');
				labelFontSize = countdown.data('label-size');
				labelColor = countdown.data('label-color');

				//Initialize countdown
				countdown.countdown({
					until: new Date(year, month - 1, day, hour, minute, 44),
					labels: ['Years', monthLabel, 'Weeks', dayLabel, hourLabel, minuteLabel, secondLabel],
					format: 'yodHMS',
					timezone: timezone,
					padZeroes: true,
					onTick: setCountdownStyle
				});

				function setCountdownStyle() {
					countdown.find('.countdown-amount').css({
						'font-size': digitFontSize + 'px',
						'line-height': digitFontSize + 'px',
						'color': digitColor
					});
					countdown.find('.countdown-period').css({
						'font-size': labelFontSize + 'px',
						'color': labelColor
					});
				}

			});

		}

	}

	/**
	 * Object that represents icon shortcode
	 * @returns {{init: Function}} function that initializes icon's functionality
	 */
	var eltdIcon = eltd.modules.shortcodes.eltdIcon = function() {
		//get all icons on page
		var icons = $('.eltd-icon-shortcode');

		/**
		 * Function that triggers icon animation and icon animation delay
		 */
		var iconAnimation = function(icon) {
			if(icon.hasClass('eltd-icon-animation')) {
				icon.appear(function() {
					icon.parent('.eltd-icon-animation-holder').addClass('eltd-icon-animation-show');
				}, {accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});
			}
		};

		/**
		 * Function that triggers icon hover color functionality
		 */
		var iconHoverColor = function(icon) {
			if(typeof icon.data('hover-color') !== 'undefined') {
				var changeIconColor = function(event) {
					event.data.icon.css('color', event.data.color);
				};

				var iconElement = icon.find('.eltd-icon-element');
				var hoverColor = icon.data('hover-color');
				var originalColor = iconElement.css('color');

				if(hoverColor !== '') {
					icon.on('mouseenter', {icon: iconElement, color: hoverColor}, changeIconColor);
					icon.on('mouseleave', {icon: iconElement, color: originalColor}, changeIconColor);
				}
			}
		};

		/**
		 * Function that triggers icon holder background color hover functionality
		 */
		var iconHolderBackgroundHover = function(icon) {
			if(typeof icon.data('hover-background-color') !== 'undefined') {
				var changeIconBgColor = function(event) {
					event.data.icon.css('background-color', event.data.color);
				};

				var hoverBackgroundColor = icon.data('hover-background-color');
				var originalBackgroundColor = icon.css('background-color');

				if(hoverBackgroundColor !== '') {
					icon.on('mouseenter', {icon: icon, color: hoverBackgroundColor}, changeIconBgColor);
					icon.on('mouseleave', {icon: icon, color: originalBackgroundColor}, changeIconBgColor);
				}
			}
		};

		/**
		 * Function that initializes icon holder border hover functionality
		 */
		var iconHolderBorderHover = function(icon) {
			if(typeof icon.data('hover-border-color') !== 'undefined') {
				var changeIconBorder = function(event) {
					event.data.icon.css('border-color', event.data.color);
				};

				var hoverBorderColor = icon.data('hover-border-color');
				var originalBorderColor = icon.css('border-color');

				if(hoverBorderColor !== '') {
					icon.on('mouseenter', {icon: icon, color: hoverBorderColor}, changeIconBorder);
					icon.on('mouseleave', {icon: icon, color: originalBorderColor}, changeIconBorder);
				}
			}
		};

		return {
			init: function() {
				if(icons.length) {
					icons.each(function() {
						iconAnimation($(this));
						iconHoverColor($(this));
						iconHolderBackgroundHover($(this));
						iconHolderBorderHover($(this));
					});

				}
			}
		};
	};

	/**
	 * Object that represents social icon widget
	 * @returns {{init: Function}} function that initializes icon's functionality
	 */
	var eltdSocialIconWidget = eltd.modules.shortcodes.eltdSocialIconWidget = function() {
		//get all social icons on page
		var icons = $('.eltd-social-icon-widget-holder');

		/**
		 * Function that triggers icon hover color functionality
		 */
		var socialIconHoverColor = function(icon) {
			if(typeof icon.data('hover-color') !== 'undefined') {
				var changeIconColor = function(event) {
					event.data.icon.css('color', event.data.color);
				};

				var iconElement = icon;
				var hoverColor = icon.data('hover-color');
				var originalColor = iconElement.css('color');

				if(hoverColor !== '') {
					icon.on('mouseenter', {icon: iconElement, color: hoverColor}, changeIconColor);
					icon.on('mouseleave', {icon: iconElement, color: originalColor}, changeIconColor);
				}
			}
		};

		return {
			init: function() {
				if(icons.length) {
					icons.each(function() {
						socialIconHoverColor($(this));
					});

				}
			}
		};
	};

	/**
	 * Init testimonials shortcode
	 */
	function eltdInitTestimonials() {

		var testimonial = $('.eltd-testimonials.testimonials-slider');
		if(testimonial.length) {
			testimonial.each(function() {
				var thisTestimonial = $(this);

				thisTestimonial.appear(function() {
					thisTestimonial.css('visibility', 'visible');
				}, {accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});

				var interval = 5000;
				var controlNav = true;
				var directionNav = true;
				var animationSpeed = 600;
				if(typeof thisTestimonial.data('animation-speed') !== 'undefined' && thisTestimonial.data('animation-speed') !== false) {
					animationSpeed = thisTestimonial.data('animation-speed');
				}

				//var iconClasses = getIconClassesForNavigation(directionNavArrowsTestimonials); TODO

				thisTestimonial.owlCarousel({
					items: 1,
					loop: true,
					autoplayTimeout: interval,
					nav: controlNav,
					pagination: false,
					smartSpeed: animationSpeed,
					animateOut: 'fadeOutLeft',
                    animateIn: 'fadeInRight',
					navText: [
						'<span class="eltd-prev-icon"><i class="lnr lnr-chevron-left"></i></span>',
						'<span class="eltd-next-icon"><i class="lnr lnr-chevron-right"></i></span>'
					]
				});

			});

		}

	}

	function eltdInitTestimonialsBlocks() {

		var testimonials = $('.eltd-testimonials.testimonials-blocks.sliding-blocks');
		
		testimonials.each(function() {

			var thisTestimonial = $(this);

			var interval = 3000;
			var controlNav = true;
			var directionNav = true;
			var animationSpeed = 600;
			var columns = 3;
			if(typeof thisTestimonial.data('animation-speed') !== 'undefined' && thisTestimonial.data('animation-speed') !== false) {
				animationSpeed = thisTestimonial.data('animation-speed');
			}
			if(typeof thisTestimonial.data('sliding-blocks-columns') !== 'undefined' && thisTestimonial.data('sliding-blocks-columns') !== false) {
				columns = thisTestimonial.data('sliding-blocks-columns');
			}
			var items = [
				[0, 1],
				[481, 2],
				[1025, Math.min(3, columns)],
				[1441, Math.min(4, columns)]
			];

			thisTestimonial.owlCarousel({
				items: columns,
				itemsCustom: items,
				loop: true,
				autoplayTimeout: interval,
				nav: directionNav,
				autoHeight: true,
				pagination: controlNav,
				smartSpeed: animationSpeed,
				animateOut: 'fadeOutRight',
                animateIn: 'fadeInLeft',
                responsive: {
                    0: {
                        items: 1
                    },
                    480: {
                        items: 2
                    },
                    768: {
                        items: 3
                    },
                    1024: {
                        items: columns
                    }
                },
				navText: [
					'<span class="eltd-prev-icon"><i class="lnr lnr-chevron-left"></i></span>',
					'<span class="eltd-next-icon"><i class="lnr lnr-chevron-right"></i></span>'
				]
			});

		});
	}

	/**
	 * Init Carousel shortcode
	 */
	function eltdInitCarousels() { 

		var carouselHolders = $('.eltd-carousel-holder'),
			carousel,
			numberOfItems;

		if(carouselHolders.length) {
			carouselHolders.each(function() {
				carousel = $(this).children('.eltd-carousel');
				numberOfItems = carousel.data('items');

				//Responsive breakpoints
				var items = [
					[0, 1],
					[480, 2],
					[768, 3],
					[1024, numberOfItems]
				];

				var showNav = carousel.data('navigation');

				if(typeof showNav !== 'undefined') {
					showNav = showNav === 'yes';
				} else {
					showNav = false;
				}

				carousel.owlCarousel({
					autoplayTimeout: 3000,
					items: numberOfItems,
					//itemsCustom: items,
					dots: showNav,
					smartSpeed: 600
				}); 

			});
		}

	}

	/**
	 * Init Variable Width Slider
	 */
	function eltdInitVariableWidthSlider() {

		var sliders = $('.eltd-variable-width-slider');

		sliders.each(function() {
			var slider = $(this).find('.eltd-vws-inner');

			slider.slick({
				dots: false,
				arrows: true,
				autoplay: true,
				autoplaySpeed: 3000,
				prevArrow: '<span class="eltd-prev-icon"><i class="lnr lnr-chevron-left"></i></span>',
				nextArrow: '<span class="eltd-next-icon"><i class="lnr lnr-chevron-right"></i></span>',
				infinite: true,
				speed: 750,
				slidesToShow: 1,
				centerMode: true,
				variableWidth: true,
				cssEase: 'cubic-bezier(.19,1,.22,1)',
				swipeToSlide: true
			});
		});

	}

	/**
	 * Init Pie Chart and Pie Chart With Icon shortcode
	 */
	function eltdInitPieChart() {

		var pieCharts = $('.eltd-pie-chart-holder, .eltd-pie-chart-with-icon-holder');

		if(pieCharts.length) {

			pieCharts.each(function() {

				var pieChart = $(this),
					percentageHolder = pieChart.find('.eltd-percentage, .eltd-percentage-with-icon'),
					barColor,
					trackColor,
					circleColor = '',
					lineWidth,
					size = 165;

				if(typeof percentageHolder.data('size') !== 'undefined' && percentageHolder.data('size') !== '') {
					size = percentageHolder.data('size');
				}

				if(typeof pieChart.data('bar-color') !== 'undefined' && pieChart.data('bar-color') !== '') {
					barColor = pieChart.data('bar-color');
				}

				if(typeof pieChart.data('track-color') !== 'undefined' && pieChart.data('track-color') !== '') {
					trackColor = pieChart.data('track-color');
				}

				if(typeof pieChart.data('circle-color') !== 'undefined' && pieChart.data('circle-color') !== '') {
					circleColor = pieChart.data('circle-color');
				}

				percentageHolder.appear(function() {
					initToCounterPieChart(pieChart);
					percentageHolder.css('opacity', '1');

					percentageHolder.easyPieChart({
						barColor: barColor,
						trackColor: trackColor,
						scaleColor: false,
						lineCap: 'butt',
						lineWidth: 2,
						animate: 1500,
						size: size
					});
				}, {accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});

				percentageHolder.closest('.eltd-percentage-holder, .eltd-percentage-with-icon-holder').width(size).css('background-color', circleColor);

			});

		}

	}

	/*
	 **	Counter for pie chart number from zero to defined number
	 */
	function initToCounterPieChart(pieChart) {

		pieChart.css('opacity', '1');
		var counter = pieChart.find('.eltd-to-counter'),
			max = parseFloat(counter.text());
		counter.countTo({
			from: 0,
			to: max,
			speed: 1500,
			refreshInterval: 50
		});

	}

	/**
	 * Init Pie Chart shortcode
	 */
	function eltdInitPieChartDoughnut() {

		var pieCharts = $('.eltd-pie-chart-doughnut-holder, .eltd-pie-chart-pie-holder');

		pieCharts.each(function() {

			var pieChart = $(this),
				canvas = pieChart.find('canvas'),
				chartID = canvas.attr('id'),
				chart = document.getElementById(chartID).getContext('2d'),
				data = [],
				jqChart = $(chart.canvas); //Convert canvas to JQuery object and get data parameters

			for(var i = 1; i <= 10; i++) {

				var chartItem,
					value = jqChart.data('value-' + i),
					color = jqChart.data('color-' + i);

				if(typeof value !== 'undefined' && typeof color !== 'undefined') {
					chartItem = {
						value: value,
						color: color
					};
					data.push(chartItem);
				}

			}

			if(canvas.hasClass('eltd-pie')) {
				new Chart(chart).Pie(data,
					{segmentStrokeColor: 'transparent'}
				);
			} else {
				new Chart(chart).Doughnut(data,
					{segmentStrokeColor: 'transparent'}
				);
			}

		});

	}

	/*
	 **	Init tabs shortcode
	 */
	function eltdInitTabs() {

		var tabs = $('.eltd-tabs');
		if(tabs.length) {
			tabs.each(function() {
				var thisTabs = $(this);

				thisTabs.children('.eltd-tab-container').each(function(index) {
					index = index + 1;
					var that = $(this),
						link = that.attr('id'),
						navItem = that.parent().find('.eltd-tabs-nav li:nth-child(' + index + ') a'),
						navLink = navItem.attr('href');

					link = '#' + link;

					if(link.indexOf(navLink) > -1) {
						navItem.attr('href', link);
					}
				});

				if(thisTabs.hasClass('eltd-horizontal')) {
					thisTabs.tabs();
				}
				else if(thisTabs.hasClass('eltd-vertical')) {
					thisTabs.tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
					thisTabs.find('.eltd-tabs-nav > ul >li').removeClass('ui-corner-top').addClass('ui-corner-left');
				}
			});
		}
	}

	/*
	 **	Generate icons in tabs navigation
	 */
	function eltdInitTabIcons() {

		var tabContent = $('.eltd-tab-container');
		if(tabContent.length) {

			tabContent.each(function() {
				var thisTabContent = $(this);

				var id = thisTabContent.attr('id');
				var icon = '';
				if(typeof thisTabContent.data('icon-html') !== 'undefined' || thisTabContent.data('icon-html') !== 'false') {
					icon = thisTabContent.data('icon-html');
				}

				var tabNav = thisTabContent.parents('.eltd-tabs').find('.eltd-tabs-nav > li > a[href="#' + id + '"]');

				if(typeof(tabNav) !== 'undefined') {
					tabNav.children('.eltd-icon-frame').append(icon);
				}
			});
		}
	}

	/**
	 * Button object that initializes whole button functionality
	 * @type {Function}
	 */
	var eltdButton = eltd.modules.shortcodes.eltdButton = function() {
		//all buttons on the page
		var buttons = $('.eltd-btn');

		/**
		 * Initializes button hover color
		 * @param button current button
		 */
		var buttonHoverColor = function(button) {
			if(typeof button.data('hover-color') !== 'undefined') {
				var changeButtonColor = function(event) {
					event.data.button.css('color', event.data.color);
				};

				var originalColor = button.css('color');
				var hoverColor = button.data('hover-color');

				button.on('mouseenter', {button: button, color: hoverColor}, changeButtonColor);
				button.on('mouseleave', {button: button, color: originalColor}, changeButtonColor);
			}
		};


		/**
		 * Initializes button hover background color
		 * @param button current button
		 */
		var buttonHoverBgColor = function(button) {
			if(typeof button.data('hover-bg-color') !== 'undefined') {
				var changeButtonBg = function(event) {
					event.data.button.css('background-color', event.data.color);
				};

				var originalBgColor = button.css('background-color');
				var hoverBgColor = button.data('hover-bg-color');

				button.on('mouseenter', {button: button, color: hoverBgColor}, changeButtonBg);
				button.on('mouseleave', {button: button, color: originalBgColor}, changeButtonBg);
			}
		};

		/**
		 * Initializes button border color
		 * @param button
		 */
		var buttonHoverBorderColor = function(button) {
			if(typeof button.data('hover-border-color') !== 'undefined') {
				var changeBorderColor = function(event) {
					event.data.button.css('border-color', event.data.color);
				};

				var originalBorderColor = button.css('borderTopColor'); //take one of the four sides
				var hoverBorderColor = button.data('hover-border-color');

				button.on('mouseenter', {button: button, color: hoverBorderColor}, changeBorderColor);
				button.on('mouseleave', {button: button, color: originalBorderColor}, changeBorderColor);
			}
		};

		return {
			init: function() {
				if(buttons.length) {
					buttons.each(function() {
						buttonHoverColor($(this));
						buttonHoverBgColor($(this));
						buttonHoverBorderColor($(this));
					});
				}
			}
		};
	};

	/*
	 **	Init blog list masonry type
	 */
	function eltdInitBlogListMasonry() {
		var blogList = $('.eltd-blog-list-holder.eltd-masonry .eltd-blog-list');
		if(blogList.length) {
			blogList.each(function() {
				var thisBlogList = $(this);
				thisBlogList.animate({opacity: 1});
				thisBlogList.isotope({
					itemSelector: '.eltd-blog-list-masonry-item',
					masonry: {
						columnWidth: '.eltd-blog-list-masonry-grid-sizer',
						gutter: '.eltd-blog-list-masonry-grid-gutter'
					}
				});
			});

		}
	}

	/*
	 **	Custom Font resizing
	 */
	function eltdCustomFontResize() {
		var customFont = $('.eltd-custom-font-holder');
		if(customFont.length) {
			customFont.each(function() {
				var thisCustomFont = $(this);
				var fontSize;
				var lineHeight;
				var coef1 = 1;
				var coef2 = 1;

				if(eltd.windowWidth < 1200) {
					coef1 = 0.8;
				}

				if(eltd.windowWidth < 1000) {
					coef1 = 0.7;
				}

				if(eltd.windowWidth < 768) {
					coef1 = 0.6;
					coef2 = 0.7;
				}

				if(eltd.windowWidth < 600) {
					coef1 = 0.5;
					coef2 = 0.6;
				}

				if(eltd.windowWidth < 480) {
					coef1 = 0.4;
					coef2 = 0.5;
				}

				if(typeof thisCustomFont.data('font-size') !== 'undefined' && thisCustomFont.data('font-size') !== false) {
					fontSize = parseInt(thisCustomFont.data('font-size'));

					if(fontSize > 70) {
						fontSize = Math.round(fontSize * coef1);
					}
					else if(fontSize > 35) {
						fontSize = Math.round(fontSize * coef2);
					}

					thisCustomFont.css('font-size', fontSize + 'px');
				}

				if(typeof thisCustomFont.data('line-height') !== 'undefined' && thisCustomFont.data('line-height') !== false) {
					lineHeight = parseInt(thisCustomFont.data('line-height'));

					if(lineHeight > 70 && eltd.windowWidth < 1200) {
						lineHeight = '1.2em';
					}
					else if(lineHeight > 35 && eltd.windowWidth < 768) {
						lineHeight = '1.2em';
					}
					else {
						lineHeight += 'px';
					}

					thisCustomFont.css('line-height', lineHeight);
				}
			});
		}
	}

	/*
	 **	Show Google Map
	 */
	function eltdShowGoogleMap() {

		if($('.eltd-google-map').length) {
			$('.eltd-google-map').each(function() {

				var element = $(this);

				var customMapStyle;
				if(typeof element.data('custom-map-style') !== 'undefined') {
					customMapStyle = element.data('custom-map-style');
				}

				var colorOverlay;
				if(typeof element.data('color-overlay') !== 'undefined' && element.data('color-overlay') !== false) {
					colorOverlay = element.data('color-overlay');
				}

				var saturation;
				if(typeof element.data('saturation') !== 'undefined' && element.data('saturation') !== false) {
					saturation = element.data('saturation');
				}

				var lightness;
				if(typeof element.data('lightness') !== 'undefined' && element.data('lightness') !== false) {
					lightness = element.data('lightness');
				}

				var zoom;
				if(typeof element.data('zoom') !== 'undefined' && element.data('zoom') !== false) {
					zoom = element.data('zoom');
				}

				var pin;
				if(typeof element.data('pin') !== 'undefined' && element.data('pin') !== false) {
					pin = element.data('pin');
				}

				var mapHeight;
				if(typeof element.data('height') !== 'undefined' && element.data('height') !== false) {
					mapHeight = element.data('height');
				}

				var uniqueId;
				if(typeof element.data('unique-id') !== 'undefined' && element.data('unique-id') !== false) {
					uniqueId = element.data('unique-id');
				}

				var scrollWheel;
				if(typeof element.data('scroll-wheel') !== 'undefined') {
					scrollWheel = element.data('scroll-wheel');
				}
				var addresses;
				if(typeof element.data('addresses') !== 'undefined' && element.data('addresses') !== false) {
					addresses = element.data('addresses');
				}

				var map = "map_" + uniqueId;
				var geocoder = "geocoder_" + uniqueId;
				var holderId = "eltd-map-" + uniqueId;

				eltdInitializeGoogleMap(customMapStyle, colorOverlay, saturation, lightness, scrollWheel, zoom, holderId, mapHeight, pin, map, geocoder, addresses);
			});
		}

	}

	/*
	 **	Init Google Map
	 */
	function eltdInitializeGoogleMap(customMapStyle, color, saturation, lightness, wheel, zoom, holderId, height, pin, map, geocoder, data) {

		if(typeof google !== 'object') {
			return;
		}

		var mapStyles = [
			{
				stylers: [
					{hue: color},
					{saturation: saturation},
					{lightness: lightness},
					{gamma: 1}
				]
			}
		];

		var googleMapStyleId;

		if(customMapStyle) {
			googleMapStyleId = 'eltd-style';
		} else {
			googleMapStyleId = google.maps.MapTypeId.ROADMAP;
		}

		var qoogleMapType = new google.maps.StyledMapType(mapStyles,
			{name: "Elated Google Map"});

		geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(-34.397, 150.644);

		if(!isNaN(height)) {
			height = height + 'px';
		}

		var myOptions = {

			zoom: zoom,
			scrollwheel: wheel,
			center: latlng,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.RIGHT_CENTER
			},
			scaleControl: false,
			scaleControlOptions: {
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			streetViewControl: false,
			streetViewControlOptions: {
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			panControl: false,
			panControlOptions: {
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			mapTypeControl: false,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'eltd-style'],
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			mapTypeId: googleMapStyleId
		};

		map = new google.maps.Map(document.getElementById(holderId), myOptions);
		map.mapTypes.set('eltd-style', qoogleMapType);

		var index;

		for(index = 0; index < data.length; ++index) {
			eltdInitializeGoogleAddress(data[index], pin, map, geocoder);
		}

		var holderElement = document.getElementById(holderId);
		holderElement.style.height = height;
	}

	/*
	 **	Init Google Map Addresses
	 */
	function eltdInitializeGoogleAddress(data, pin, map, geocoder) {
		if(data === '')
			return;
		var contentString = '<div id="content">' +
			'<div id="siteNotice">' +
			'</div>' +
			'<div id="bodyContent">' +
			'<p>' + data + '</p>' +
			'</div>' +
			'</div>';
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
		geocoder.geocode({'address': data}, function(results, status) {
			if(status === google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				var marker = new google.maps.Marker({
					map: map,
					position: results[0].geometry.location,
					icon: pin,
					title: data['store_title']
				});
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map, marker);
				});

				google.maps.event.addDomListener(window, 'resize', function() {
					map.setCenter(results[0].geometry.location);
				});

			}
		});
	}

	function eltdInitAccordions() {
		var accordion = $('.eltd-accordion-holder');
		if(accordion.length) {
			accordion.each(function() {

				var thisAccordion = $(this);

				if(thisAccordion.hasClass('eltd-accordion')) {

					thisAccordion.accordion({
						animate: "swing",
						collapsible: false,
						active: 0,
						icons: "",
						heightStyle: "content"
					});
				}

				if(thisAccordion.hasClass('eltd-toggle')) {

					var toggleAccordion = $(this);
					var toggleAccordionTitle = toggleAccordion.find('.eltd-title-holder');
					var toggleAccordionContent = toggleAccordionTitle.next();

					toggleAccordion.addClass("accordion ui-accordion ui-accordion-icons ui-widget ui-helper-reset");
					toggleAccordionTitle.addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom");
					toggleAccordionContent.addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").hide();

					toggleAccordionTitle.each(function() {
						var thisTitle = $(this);
						thisTitle.on('mouseenter mouseleave', function() {
							thisTitle.toggleClass("ui-state-hover");
						});

						thisTitle.on('click', function() {
							thisTitle.toggleClass('ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom');
							thisTitle.next().toggleClass('ui-accordion-content-active').slideToggle(400);
						});
					});
				}
			});
		}
	}

	function eltdInitImageGallery() {

		var galleries = $('.eltd-image-gallery');

		if(galleries.length) {
			galleries.each(function() {
				var gallery = $(this).children('.eltd-image-gallery-slider'),
					autoplay = gallery.data('autoplay'),
					animation = (gallery.data('animation') == 'slide') ? false : gallery.data('animation'),
					navigation = (gallery.data('navigation') == 'yes'),
					pagination = (gallery.data('pagination') == 'yes');

				gallery.owlCarousel({
					singleItem: true,
					autoPlay: autoplay * 1000,
					navigation: navigation,
					transitionStyle: animation, //fade, fadeUp, backSlide, goDown
					autoHeight: true,
					pagination: pagination,
					slideSpeed: 600,
					navigationText: [
						'<span class="eltd-prev-icon"><i class="lnr lnr-chevron-left"></i></span>',
						'<span class="eltd-next-icon"><i class="lnr lnr-chevron-right"></i></span>'
					]
				});
			});
		}

	}

	/**
	 * Initializes portfolio list
	 */
	function eltdInitPortfolio() {
		var portList = $('.eltd-portfolio-list-holder-outer.eltd-ptf-standard, .eltd-portfolio-list-holder-outer.eltd-ptf-gallery');
		if(portList.length) {
			portList.each(function() {
				var thisPortList = $(this);
				thisPortList.waitForImages(function() {
					eltdInitPortMixItUp(thisPortList);
				});
			});
		}
	}

	/**
	 * Initializes mixItUp function for specific container
	 */
	function eltdInitPortMixItUp(container) {
		var filterClass = '';
		if(container.hasClass('eltd-ptf-has-filter')) {
			filterClass = container.find('.eltd-portfolio-filter-holder-inner ul li').data('class');
			filterClass = '.' + filterClass;
		}

		var holderInner = container.find('.eltd-portfolio-list-holder');
		holderInner.mixItUp({
			callbacks: {
				onMixLoad: function() {
					holderInner.find('article').css('visibility', 'visible');
				},
				onMixStart: function() {
					holderInner.find('article').css('visibility', 'visible');
				},
				onMixBusy: function() {
					holderInner.find('article').css('visibility', 'visible');
				}
			},
			selectors: {
				filter: filterClass
			},
			animation: {
				effects: 'fade',
				duration: 400
			}
		});

	}

	/*
	 **	Init portfolio list masonry type
	 */
	function eltdInitPortfolioListMasonry() {
		var portList = $('.eltd-portfolio-list-holder-outer.eltd-ptf-masonry');
		if(portList.length) {
			portList.each(function() {
				var thisPortList = $(this).children('.eltd-portfolio-list-holder');
				var size = thisPortList.find('.eltd-portfolio-list-masonry-grid-sizer').width();
				eltdResizeMasonry(size, thisPortList);

				eltdInitMasonry(thisPortList);
				$(window).resize(function() {
					eltdResizeMasonry(size, thisPortList);
					eltdInitMasonry(thisPortList);
				});
			});
		}
	}

	function eltdInitMasonry(container) {
		container.animate({opacity: 1});
		container.waitForImages(function() {
			container.isotope({
				itemSelector: '.eltd-portfolio-item',
				masonry: {
					columnWidth: '.eltd-portfolio-list-masonry-grid-sizer'
				}
			});
			setTimeout(function() {
				eltdInitPortfolioListMasonryParallax();
			},100);
		});
	}

	function eltdResizeMasonry(size, container) {

		var defaultMasonryItem = container.find('.eltd-default-masonry-item');
		var largeWidthMasonryItem = container.find('.eltd-large-width-masonry-item');
		var largeHeightMasonryItem = container.find('.eltd-large-height-masonry-item');
		var largeWidthHeightMasonryItem = container.find('.eltd-large-width-height-masonry-item');

		defaultMasonryItem.css('height', size);
		largeHeightMasonryItem.css('height', Math.round(2 * size));

		if(eltd.windowWidth > 600) {
			largeWidthHeightMasonryItem.css('height', Math.round(2 * size));
			largeWidthMasonryItem.css('height', size);
		} else {
			largeWidthHeightMasonryItem.css('height', size);
			largeWidthMasonryItem.css('height', Math.round(size / 2));
		}
	}

	/*
	 **	Init portfolio list masonry parallax
	 */
	function eltdInitPortfolioListMasonryParallax() {
		
		var floatingItems = $('.eltd-ptf-masonry-with-parallax article.eltd-masonry-parallax-floating-item');

		var positionItems = function() {
			var centerPoint = $(window).scrollTop() + eltd.windowHeight/2;
			floatingItems.each(function() {
				var item = $(this);
				var thisCenter = item.offset().top + item.outerHeight()/2;
				item.css('transform', 'translate3d(0, '+(thisCenter - centerPoint)*0.3+'px, 0)');
			});
		};

		$(window).on('scroll resize', positionItems); 
		setTimeout(function() {
			positionItems();
		}, 300);
	}

	/**
	 * Initializes portfolio pinterest
	 */
	function eltdInitPortfolioListPinterest() {

		var portList = $('.eltd-portfolio-list-holder-outer.eltd-ptf-pinterest');
		if(portList.length) {
			portList.each(function() {
				var thisPortList = $(this).children('.eltd-portfolio-list-holder');
				setTimeout(function() {
					eltdInitPinterest(thisPortList);
					$(window).resize(function() {
						eltdInitPinterest(thisPortList);
					});
				}, 100);
			});

		}
	}

	function eltdInitPinterest(container) {
		container.animate({opacity: 1});
		container.waitForImages(function() {
			container.isotope({
				itemSelector: '.eltd-portfolio-item',
				masonry: {
					columnWidth: '.eltd-portfolio-list-masonry-grid-sizer',
					gutter: '.eltd-portfolio-list-masonry-grid-gutter'
				}
			});
		});

	}

	/**
	 * Initializes portfolio justified gallery
	 */
	function eltdInitPortfolioListJustifiedGallery() {
		var filterHolder = $('.eltd-portfolio-filter-holder.eltd-justified-gallery-filter');

		filterHolder.find('.filter').first().addClass('current');

		filterHolder.find('.filter').on('click',function(){
			/*
			var txt = $(this).text();
			var dropLabels = filterHolder.find('.label span');
			dropLabels.each(function(){
				$(this).text(txt);
			});
			*/

			var selector = $(this).attr('data-filter');
			var gallery = $(this).closest('.eltd-justified-gallery-filter').siblings('.eltd-ptf-justified-gallery').children('.justified-gallery');
			var articles = gallery.find('article');
			var transitionDuration = 700;
			articles.css('transition','all '+transitionDuration+'ms ease-out');
			articles.not(selector).css({
				'transform': 'scale(0)'
			});
			setTimeout(function() {
				articles.filter(selector).css({
					'transform': ''
				});
				gallery.css('transition','height '+transitionDuration+'ms ease').justifiedGallery({selector: '>article'+(selector != '*' ? selector : '')});
			}, 1.1*transitionDuration);
			setTimeout(function() {
				articles.css('transition','');
				gallery.css('transition','');
			}, 2.2*transitionDuration);

			$(".filter").removeClass("current active");
			$(this).addClass("current active");

			return false;
		});

		var galleries = $('.eltd-ptf-justified-gallery');
		galleries.each(function() {
			var spacing = $(this).is('.eltd-ptf-jg-with-space') ? 10 : 0,
				rowHeight = typeof $(this).data('row-height') !== 'undefined' ? $(this).data('row-height') : 200,
				lastRow = typeof $(this).data('last-row') !== 'undefined' ? $(this).data('last-row') : 'nojustify',
				justifyThreshold = typeof $(this).data('justify-threshold') !== 'undefined' ? $(this).data('justify-threshold') : 0.75;
			var gallery = $(this).children('.eltd-portfolio-list-holder');

            setTimeout(function(){ //since spaces between items are not working, settimeout is needed here
                gallery.justifiedGallery({
                        captions: false,
                        rowHeight: rowHeight,
                        margins: spacing,
                        border: 0,
                        lastRow: lastRow,
                        justifyThreshold: justifyThreshold,
                        selector: '> article'
                }).on('jg.complete jg.rowflush', function() {
                    var gal = $(this);
                    var deducted = false;
                    gal.find('article').addClass('show').each(function() {
                        $(this).height(Math.round($(this).height()));
                        if (!deducted && $(this).width() == 0) {
                            gal.height(gal.height() - $(this).height() - spacing);
                            deducted = true;
                        }
                    });
                });
            },200);


		});
	}

	/**
	 * Initializes portfolio masonry filter
	 */
	function eltdInitPortfolioMasonryFilter() {

		var filterHolder = $('.eltd-portfolio-filter-holder.eltd-masonry-filter');

		if(filterHolder.length) {
			filterHolder.each(function() {

				var thisFilterHolder = $(this);
				var transitionDuration = 6000;
				var portfolioIsotopeAnimation = null;

				var filter = thisFilterHolder.find('ul li').data('class');

				thisFilterHolder.find('.filter:first').addClass('current');

				thisFilterHolder.parent().find('article').css('transition','all '+transitionDuration+'ms ease-out');


				thisFilterHolder.find('.filter').on('click',function() {

					var currentFilter = $(this);
					clearTimeout(portfolioIsotopeAnimation);

					$('.isotope, .isotope .isotope-item').css('transition-duration', '0.8s');

					portfolioIsotopeAnimation = setTimeout(function() {
						$('.isotope, .isotope .isotope-item').css('transition-duration', '6s');
					}, 2700);

					var selector = $(this).attr('data-filter');
					thisFilterHolder.siblings('.eltd-portfolio-list-holder-outer').find('.eltd-portfolio-list-holder').isotope({filter: selector});

					thisFilterHolder.find('.filter').removeClass('current');
					currentFilter.addClass('current');

					return false;

				});

			});
		}
	}

    function eltdPortfolioTiledGallery() {

        var portList = $('.eltd-portfolio-list-holder-outer.eltd-ptf-justified-gallery');
        if(portList.length){
            portList.each(function() {
                var thisPortList = $(this);
                eltdInitPortfolioTiledGallery(thisPortList).init();
            })
        }

    }
    function eltdInitPortfolioTiledGallery(portList){

        var initGallery = function() {
            var gallery = portList.find('.eltd-portfolio-list-holder');

            var rowHeight = typeof portList.data('row-height') !== 'undefined' ? portList.data('row-height') : 200,
                spacing = typeof portList.data('spacing') !== 'undefined' ? portList.data('spacing') : 0,
                lastRow = typeof portList.data('last-row') !== 'undefined' ? portList.data('last-row') : 'nojustify',
                threshold = typeof portList.data('threshold') !== 'undefined' ? portList.data('threshold') : 0.75;

            setTimeout(function () {
                gallery.justifiedGallery({
                    rowHeight: rowHeight,
                    margins: spacing,
                    lastRow: lastRow,
                    justifyThreshold: threshold,
                    selector: '> article'
                }).on('jg.complete jg.rowflush', function() {
                    gallery.find('article').addClass('show');
                });
            }, 100);  //Timeout because of ajax page transitions

        };
        var initGalleryFilter = function(){
            if(portList.hasClass('eltd-ptf-has-filter')){

                var thisPortList = portList;
                var gallery = thisPortList.find('.eltd-portfolio-list-holder');
                var filterHolder = thisPortList.find('.eltd-portfolio-filter-holder');
                var filterItems = filterHolder.find('li');

                var currentItem;
                if(filterItems.length){
                    filterItems.each(function(){
                        if($(this).hasClass('active')){
                            currentItem = $(this);
                        }
                    })
                }

                if(typeof (currentItem) !== 'undefined'){
                    //filter items after ajax pagination call
                    eltdFilterPortfolioTiledGallery(currentItem);
                }else{
                    //filter items initially
                    filterItems.first().addClass('active');
                }

                //filter articles on click event
                filterHolder.find('li').on('click',function(){
                    eltdFilterPortfolioTiledGallery($(this));
                });

            }
            function eltdFilterPortfolioTiledGallery(filterItem){

                var selector = filterItem.attr('data-filter');
                var articles = gallery.find('article');
                var transitionDuration = 200;

                articles.css('transition','all '+transitionDuration+'ms ease');
                articles.not(selector).css({
                    'transform': 'scale(0)'
                });
                if(selector === '.all'){
                    articles.addClass('all');
                }
                setTimeout(function() {

                    articles.filter(selector).css({
                        'transform': ''
                    });

                    gallery.css('transition','height '+transitionDuration+'ms ease').justifiedGallery({selector: '>article'+(selector ? selector : '')});
                },  1.1*transitionDuration);
                setTimeout(function() {
                    articles.css('transition','');
                    gallery.css('transition','');
                }, 2.2*transitionDuration);

                filterItems.removeClass("active");
                filterItem.addClass("active");

                return false;

            }
        };

        return {

            init : function() {

                initGallery();
                initGalleryFilter();

            }

        };

    }


	/**
	 * Initializes portfolio load more function
	 */
	function eltdInitPortfolioLoadMore() {
		var portList = $('.eltd-portfolio-list-holder-outer.eltd-ptf-load-more'),
			containerInner = portList.find('.eltd-portfolio-list-holder');
		if(portList.length) {
			portList.each(function() {

				var thisPortList = $(this),
					thisPortListInner = thisPortList.find('.eltd-portfolio-list-holder'),
					nextPage,
					maxNumPages,
					loadMoreButton = thisPortList.find('.eltd-ptf-list-load-more a'),
					loadMoreButtonText = loadMoreButton.text();

				if(typeof thisPortList.data('max-num-pages') !== 'undefined' && thisPortList.data('max-num-pages') !== false) {
					maxNumPages = thisPortList.data('max-num-pages');
				}

				loadMoreButton.on('click', function(e) {
					
					var loadMoreDatta = eltdGetPortfolioAjaxData(thisPortList),
						thisLoadMoreButton = $(this);
					nextPage = loadMoreDatta.nextPage;
					e.preventDefault();
					e.stopPropagation();
					thisLoadMoreButton.text(eltdGlobalVars.vars.eltdMessage);
					if(nextPage <= maxNumPages) {
						var ajaxData = eltdSetPortfolioAjaxData(loadMoreDatta);
						$.ajax({
							type: 'POST',
							data: ajaxData,
							url: eltdCoreAjaxUrl,
							success: function(data) {
								nextPage++;
								thisPortList.data('next-page', nextPage);
								var response = $.parseJSON(data);
								var responseHtml = eltdConvertHTML(response.html); //convert response html into jQuery collection that Mixitup can work with
								thisPortList.waitForImages(function() {
									setTimeout(function() {

										if(thisPortList.hasClass('eltd-ptf-masonry') || thisPortList.hasClass('eltd-ptf-pinterest')) {
											var size = thisPortList.find('.eltd-portfolio-list-masonry-grid-sizer').width();
											thisPortListInner.isotope().append(responseHtml).isotope('appended', responseHtml, eltdResizeMasonry(size, thisPortList)).isotope('reloadItems');

					                    }else if(thisPortList.hasClass('eltd-ptf-justified-gallery')){
				                        	containerInner.append(responseHtml);
				                        	eltdInitPortfolioTiledGallery(portList).init();
					                    } else {
											thisPortListInner.mixItUp('append', responseHtml);
										}
											thisLoadMoreButton.text(loadMoreButtonText);
									}, 400);
	
								});
							}
						});
					}
					if(nextPage === maxNumPages) {
						loadMoreButton.hide();
					}

					return false;
				});

			});
		}
	}

	function eltdConvertHTML(html) {
		var newHtml = $.trim(html),
			$html = $(newHtml),
			$empty = $();

		$html.each(function(index, value) {
			if(value.nodeType === 1) {
				$empty = $empty.add(this);
			}
		});

		return $empty;
	}

	/**
	 * Initializes portfolio load more data params
	 * @param portfolio list container with defined data params
	 * return array
	 */
	function eltdGetPortfolioAjaxData(container) {
		var returnValue = {};

		returnValue.type = '';
		returnValue.columns = '';
		returnValue.gridSize = '';
		returnValue.orderBy = '';
		returnValue.order = '';
		returnValue.number = '';
		returnValue.imageSize = '';
		returnValue.customImageDimensions = '';
		returnValue.filter = '';
		returnValue.filterOrderBy = '';
		returnValue.category = '';
		returnValue.selectedProjectes = '';
		returnValue.showLoadMore = '';
		returnValue.titleTag = '';
		returnValue.nextPage = '';
		returnValue.maxNumPages = '';
		returnValue.showExcerpt = '';

		if(typeof container.data('type') !== 'undefined' && container.data('type') !== false) {
			returnValue.type = container.data('type');
		}

		if(typeof container.data('grid-size') !== 'undefined' && container.data('grid-size') !== false) {
			returnValue.gridSize = container.data('grid-size');
		}

		if(typeof container.data('columns') !== 'undefined' && container.data('columns') !== false) {
			returnValue.columns = container.data('columns');
		}

		if(typeof container.data('order-by') !== 'undefined' && container.data('order-by') !== false) {
			returnValue.orderBy = container.data('order-by');
		}

		if(typeof container.data('order') !== 'undefined' && container.data('order') !== false) {
			returnValue.order = container.data('order');
		}

		if(typeof container.data('number') !== 'undefined' && container.data('number') !== false) {
			returnValue.number = container.data('number');
		}

		if(typeof container.data('image-size') !== 'undefined' && container.data('image-size') !== false) {
			returnValue.imageSize = container.data('image-size');
		}

		if(typeof container.data('custom-image-dimensions') !== 'undefined' && container.data('custom-image-dimensions') !== false) {
			returnValue.customImageDimensions = container.data('custom-image-dimensions');
		}

		if(typeof container.data('filter') !== 'undefined' && container.data('filter') !== false) {
			returnValue.filter = container.data('filter');
		}

		if(typeof container.data('filter-order-by') !== 'undefined' && container.data('filter-order-by') !== false) {
			returnValue.filterOrderBy = container.data('filter-order-by');
		}

		if(typeof container.data('category') !== 'undefined' && container.data('category') !== false) {
			returnValue.category = container.data('category');
		}

		if(typeof container.data('selected-projects') !== 'undefined' && container.data('selected-projects') !== false) {
			returnValue.selectedProjectes = container.data('selected-projects');
		}

		if(typeof container.data('show-load-more') !== 'undefined' && container.data('show-load-more') !== false) {
			returnValue.showLoadMore = container.data('show-load-more');
		}

		if(typeof container.data('title-tag') !== 'undefined' && container.data('title-tag') !== false) {
			returnValue.titleTag = container.data('title-tag');
		}

		if(typeof container.data('next-page') !== 'undefined' && container.data('next-page') !== false) {
			returnValue.nextPage = container.data('next-page');
		}

		if(typeof container.data('max-num-pages') !== 'undefined' && container.data('max-num-pages') !== false) {
			returnValue.maxNumPages = container.data('max-num-pages');
		}

		if(typeof container.data('show-excerpt') !== 'undefined' && container.data('show-excerpt') !== false) {
			returnValue.showExcerpt = container.data('show-excerpt');
		}

		return returnValue;
	}

	/**
	 * Sets portfolio load more data params for ajax function
	 * @param portfolio list container with defined data params
	 * return array
	 */
	function eltdSetPortfolioAjaxData(container) {
		var returnValue = {
			action: 'eltd_core_portfolio_ajax_load_more',
			type: container.type,
			columns: container.columns,
			gridSize: container.gridSize,
			orderBy: container.orderBy,
			order: container.order,
			number: container.number,
			imageSize: container.imageSize,
			customImageDimensions: container.customImageDimensions,
			filter: container.filter,
			filterOrderBy: container.filterOrderBy,
			category: container.category,
			selectedProjectes: container.selectedProjectes,
			showLoadMore: container.showLoadMore,
			titleTag: container.titleTag,
			nextPage: container.nextPage,
			showExcerpt: container.showExcerpt
		};
		return returnValue;
	}

	/**
	 * Slider object that initializes whole slider functionality
	 * @type {Function}
	 */
	var eltdSlider = eltd.modules.shortcodes.eltdSlider = function() {

		//all sliders on the page
		var sliders = $('.eltd-slider .carousel');
		//image regex used to extract img source
		var imageRegex = /url\(["']?([^'")]+)['"]?\)/;
		//default responsive breakpoints set
		var responsiveBreakpointSet = [1600, 1200, 900, 650, 500, 320];
		//var init for coefficiens array
		var coefficientsGraphicArray;
		var coefficientsTitleArray;
		var coefficientsSubtitleArray;
		var coefficientsTextArray;
		var coefficientsButtonArray;
		//var init for slider elements responsive coefficients
		var sliderGraphicCoefficient;
		var sliderTitleCoefficient;
		var sliderSubtitleCoefficient;
		var sliderTextCoefficient;
		var sliderButtonCoefficient;
		var sliderTitleCoefficientLetterSpacing;
		var sliderSubtitleCoefficientLetterSpacing;
		var sliderTextCoefficientLetterSpacing;

		/*** Functionality for translating image in slide - START ***/

		var matrixArray = {
			zoom_center: '1.2, 0, 0, 1.2, 0, 0',
			zoom_top_left: '1.2, 0, 0, 1.2, -150, -150',
			zoom_top_right: '1.2, 0, 0, 1.2, 150, -150',
			zoom_bottom_left: '1.2, 0, 0, 1.2, -150, 150',
			zoom_bottom_right: '1.2, 0, 0, 1.2, 150, 150'
		};

		// regular expression for parsing out the matrix components from the matrix string
		var matrixRE = /\([0-9epx\.\, \t\-]+/gi;

		// parses a matrix string of the form "matrix(n1,n2,n3,n4,n5,n6)" and
		// returns an array with the matrix components
		var parseMatrix = function(val) {
			return val.match(matrixRE)[0].substr(1).split(",").map(function(s) {
				return parseFloat(s);
			});
		};

		// transform css property names with vendor prefixes;
		// the plugin will check for values in the order the names are listed here and return as soon as there
		// is a value; so listing the W3 std name for the transform results in that being used if its available
		var transformPropNames = [
			"transform",
			"-webkit-transform"
		];

		var getTransformMatrix = function(el) {
			// iterate through the css3 identifiers till we hit one that yields a value
			var matrix = null;
			transformPropNames.some(function(prop) {
				matrix = el.css(prop);
				return (matrix !== null && matrix !== "");
			});

			// if "none" then we supplant it with an identity matrix so that our parsing code below doesn't break
			matrix = (!matrix || matrix === "none") ?
				"matrix(1,0,0,1,0,0)" : matrix;
			return parseMatrix(matrix);
		};

		// set the given matrix transform on the element; note that we apply the css transforms in reverse order of how its given
		// in "transformPropName" to ensure that the std compliant prop name shows up last
		var setTransformMatrix = function(el, matrix) {
			var m = "matrix(" + matrix.join(",") + ")";
			for(var i = transformPropNames.length - 1; i >= 0; --i) {
				el.css(transformPropNames[i], m + ' rotate(0.01deg)');
			}
		};

		// interpolates a value between a range given a percent
		var interpolate = function(from, to, percent) {
			return from + ((to - from) * (percent / 100));
		};

		$.fn.transformAnimate = function(opt) {
			// extend the options passed in by caller
			var options = {
				transform: "matrix(1,0,0,1,0,0)"
			};
			$.extend(options, opt);

			// initialize our custom property on the element to track animation progress
			this.css("percentAnim", 0);

			// supplant "options.step" if it exists with our own routine
			var sourceTransform = getTransformMatrix(this);
			var targetTransform = parseMatrix(options.transform);
			options.step = function(percentAnim, fx) {
				// compute the interpolated transform matrix for the current animation progress
				var $this = $(this);
				var matrix = sourceTransform.map(function(c, i) {
					return interpolate(c, targetTransform[i],
						percentAnim);
				});

				// apply the new matrix
				setTransformMatrix($this, matrix);

				// invoke caller's version of "step" if one was supplied;
				if(opt.step) {
					opt.step.apply(this, [matrix, fx]);
				}
			};

			// animate!
			return this.stop().animate({percentAnim: 100}, options);
		};

		/*** Functionality for translating image in slide - END ***/


		/**
		 * Calculate heights for slider holder and slide item, depending on window width, but only if slider is set to be responsive
		 * @param slider, current slider
		 * @param defaultHeight, default height of slider, set in shortcode
		 * @param responsive_breakpoint_set, breakpoints set for slider responsiveness
		 * @param reset, boolean for reseting heights
		 */
		var setSliderHeight = function(slider, defaultHeight, responsive_breakpoint_set, reset) {
			var sliderHeight = defaultHeight;
			if(!reset) {
				if(eltd.windowWidth > responsive_breakpoint_set[0]) {
					sliderHeight = defaultHeight;
				} else if(eltd.windowWidth > responsive_breakpoint_set[1]) {
					sliderHeight = defaultHeight * 0.75;
				} else if(eltd.windowWidth > responsive_breakpoint_set[2]) {
					sliderHeight = defaultHeight * 0.6;
				} else if(eltd.windowWidth > responsive_breakpoint_set[3]) {
					sliderHeight = defaultHeight * 0.55;
				} else if(eltd.windowWidth <= responsive_breakpoint_set[3]) {
					sliderHeight = defaultHeight * 0.45;
				}
			}

			slider.css({'height': (sliderHeight) + 'px'});
			slider.find('.eltd-slider-preloader').css({'height': (sliderHeight) + 'px'});
			slider.find('.eltd-slider-preloader .eltd-ajax-loader').css({'display': 'block'});
			slider.find('.item').css({'height': (sliderHeight) + 'px'});
		};

		/**
		 * Calculate heights for slider holder and slide item, depending on window size, but only if slider is set to be full height
		 * @param slider, current slider
		 */
		var setSliderFullHeight = function(slider) {
			var mobileHeaderHeight = eltd.windowWidth < 1000 ? eltdGlobalVars.vars.eltdMobileHeaderHeight + $('.eltd-top-bar').height() : 0;
			slider.css({'height': (eltd.windowHeight - mobileHeaderHeight) + 'px'});
			slider.find('.eltd-slider-preloader').css({'height': (eltd.windowHeight) + 'px'});
			slider.find('.eltd-slider-preloader .eltd-ajax-loader').css({'display': 'block'});
			slider.find('.item').css({'height': (eltd.windowHeight) + 'px'});
		};

		/**
		 * Set initial sizes for slider elements and put them in global variables
		 * @param slideItem, each slide
		 * @param index, index od slide item
		 */
		var setSizeGlobalVariablesForSlideElements = function(slideItem, index) {
			window["slider_graphic_width_" + index] = [];
			window["slider_graphic_height_" + index] = [];
			window["slider_title_" + index] = [];
			window["slider_subtitle_" + index] = [];
			window["slider_text_" + index] = [];
			window["slider_button1_" + index] = [];
			window["slider_button2_" + index] = [];

			//graphic size
			window["slider_graphic_width_" + index].push(parseFloat(slideItem.find('.eltd-thumb img').data("width")));
			window["slider_graphic_height_" + index].push(parseFloat(slideItem.find('.eltd-thumb img').data("height")));

			// font-size (0)
			window["slider_title_" + index].push(parseFloat(slideItem.find('.eltd-slide-title').css("font-size")));
			window["slider_subtitle_" + index].push(parseFloat(slideItem.find('.eltd-slide-subtitle').css("font-size")));
			window["slider_text_" + index].push(parseFloat(slideItem.find('.eltd-slide-text').css("font-size")));
			window["slider_button1_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(0)').css("font-size")));
			window["slider_button2_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(1)').css("font-size")));

			// line-height (1)
			window["slider_title_" + index].push(parseFloat(slideItem.find('.eltd-slide-title').css("line-height")));
			window["slider_subtitle_" + index].push(parseFloat(slideItem.find('.eltd-slide-subtitle').css("line-height")));
			window["slider_text_" + index].push(parseFloat(slideItem.find('.eltd-slide-text').css("line-height")));
			window["slider_button1_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(0)').css("line-height")));
			window["slider_button2_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(1)').css("line-height")));

			// letter-spacing (2)
			window["slider_title_" + index].push(parseFloat(slideItem.find('.eltd-slide-title').css("letter-spacing")));
			window["slider_subtitle_" + index].push(parseFloat(slideItem.find('.eltd-slide-subtitle').css("letter-spacing")));
			window["slider_text_" + index].push(parseFloat(slideItem.find('.eltd-slide-text').css("letter-spacing")));
			window["slider_button1_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(0)').css("letter-spacing")));
			window["slider_button2_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(1)').css("letter-spacing")));

			// margin-bottom (3)
			window["slider_title_" + index].push(parseFloat(slideItem.find('.eltd-slide-title').css("margin-bottom")));
			window["slider_subtitle_" + index].push(parseFloat(slideItem.find('.eltd-slide-subtitle').css("margin-bottom")));


			// slider_button padding top/bottom(3), padding left/right(4)
			window["slider_button1_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(0)').css("padding-top")));
			window["slider_button2_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(1)').css("padding-top")));

			window["slider_button1_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(0)').css("padding-left")));
			window["slider_button2_" + index].push(parseFloat(slideItem.find('.eltd-btn:eq(1)').css("padding-left")));
		};

		/**
		 * Set responsive coefficients for slider elements
		 * @param responsiveBreakpointSet, responsive breakpoints
		 * @param coefficientsGraphicArray, responsive coeaficcients for graphic
		 * @param coefficientsTitleArray, responsive coeaficcients for title
		 * @param coefficientsSubtitleArray, responsive coeaficcients for subtitle
		 * @param coefficientsTextArray, responsive coeaficcients for text
		 * @param coefficientsButtonArray, responsive coeaficcients for button
		 */
		var setSliderElementsResponsiveCoeffeicients = function(responsiveBreakpointSet, coefficientsGraphicArray, coefficientsTitleArray, coefficientsSubtitleArray, coefficientsTextArray, coefficientsButtonArray) {

			function coefficientsSetter(graphicArray, titleArray, subtitleArray, textArray, buttonArray) {
				sliderGraphicCoefficient = graphicArray;
				sliderTitleCoefficient = titleArray;
				sliderSubtitleCoefficient = subtitleArray;
				sliderTextCoefficient = textArray;
				sliderButtonCoefficient = buttonArray;
			}

			if(eltd.windowWidth > responsiveBreakpointSet[0]) {
				coefficientsSetter(coefficientsGraphicArray[0], coefficientsTitleArray[0], coefficientsSubtitleArray[0], coefficientsTextArray[0], coefficientsButtonArray[0]);
			} else if(eltd.windowWidth > responsiveBreakpointSet[1]) {
				coefficientsSetter(coefficientsGraphicArray[1], coefficientsTitleArray[1], coefficientsSubtitleArray[1], coefficientsTextArray[1], coefficientsButtonArray[1]);
			} else if(eltd.windowWidth > responsiveBreakpointSet[2]) {
				coefficientsSetter(coefficientsGraphicArray[2], coefficientsTitleArray[2], coefficientsSubtitleArray[2], coefficientsTextArray[2], coefficientsButtonArray[2]);
			} else if(eltd.windowWidth > responsiveBreakpointSet[3]) {
				coefficientsSetter(coefficientsGraphicArray[3], coefficientsTitleArray[3], coefficientsSubtitleArray[3], coefficientsTextArray[3], coefficientsButtonArray[3]);
			} else if(eltd.windowWidth > responsiveBreakpointSet[4]) {
				coefficientsSetter(coefficientsGraphicArray[4], coefficientsTitleArray[4], coefficientsSubtitleArray[4], coefficientsTextArray[4], coefficientsButtonArray[4]);
			} else if(eltd.windowWidth > responsiveBreakpointSet[5]) {
				coefficientsSetter(coefficientsGraphicArray[5], coefficientsTitleArray[5], coefficientsSubtitleArray[5], coefficientsTextArray[5], coefficientsButtonArray[5]);
			} else {
				coefficientsSetter(coefficientsGraphicArray[6], coefficientsTitleArray[6], coefficientsSubtitleArray[6], coefficientsTextArray[6], coefficientsButtonArray[6]);
			}

			// letter-spacing decrease quicker
			sliderTitleCoefficientLetterSpacing = sliderTitleCoefficient;
			sliderSubtitleCoefficientLetterSpacing = sliderSubtitleCoefficient;
			sliderTextCoefficientLetterSpacing = sliderTextCoefficient;
			if(eltd.windowWidth <= responsiveBreakpointSet[0]) {
				sliderTitleCoefficientLetterSpacing = sliderTitleCoefficient / 2;
				sliderSubtitleCoefficientLetterSpacing = sliderSubtitleCoefficient / 2;
				sliderTextCoefficientLetterSpacing = sliderTextCoefficient / 2;
			}
		};

		/**
		 * Set sizes for slider elements
		 * @param slideItem, each slide
		 * @param index, index od slide item
		 * @param reset, boolean for reseting sizes
		 */
		var setSliderElementsSize = function(slideItem, index, reset) {

			if(reset) {
				sliderGraphicCoefficient = sliderTitleCoefficient = sliderSubtitleCoefficient = sliderTextCoefficient = sliderButtonCoefficient = sliderTitleCoefficientLetterSpacing = sliderSubtitleCoefficientLetterSpacing = sliderTextCoefficientLetterSpacing = 1;
			}

			slideItem.find('.eltd-thumb').css({
				"width": Math.round(window["slider_graphic_width_" + index][0] * sliderGraphicCoefficient) + 'px',
				"height": Math.round(window["slider_graphic_height_" + index][0] * sliderGraphicCoefficient) + 'px'
			});

			slideItem.find('.eltd-slide-title').css({
				"font-size": Math.round(window["slider_title_" + index][0] * sliderTitleCoefficient) + 'px',
				"line-height": Math.round(window["slider_title_" + index][1] * sliderTitleCoefficient) + 'px',
				"letter-spacing": Math.round(window["slider_title_" + index][2] * sliderTitleCoefficient) + 'px',
				"margin-bottom": Math.round(window["slider_title_" + index][3] * sliderTitleCoefficient) + 'px'
			});

			slideItem.find('.eltd-slide-subtitle').css({
				"font-size": Math.round(window["slider_subtitle_" + index][0] * sliderSubtitleCoefficient) + 'px',
				"line-height": Math.round(window["slider_subtitle_" + index][1] * sliderSubtitleCoefficient) + 'px',
				"margin-bottom": Math.round(window["slider_subtitle_" + index][3] * sliderSubtitleCoefficient) + 'px',
				"letter-spacing": Math.round(window["slider_subtitle_" + index][2] * sliderSubtitleCoefficientLetterSpacing) + 'px'
			});

			slideItem.find('.eltd-slide-text').css({
				"font-size": Math.round(window["slider_text_" + index][0] * sliderTextCoefficient) + 'px',
				"line-height": Math.round(window["slider_text_" + index][1] * sliderTextCoefficient) + 'px',
				"letter-spacing": Math.round(window["slider_text_" + index][2] * sliderTextCoefficientLetterSpacing) + 'px'
			});

			slideItem.find('.eltd-btn:eq(0)').css({
				"font-size": Math.round(window["slider_button1_" + index][0] * sliderButtonCoefficient) + 'px',
				"line-height": Math.round(window["slider_button1_" + index][1] * sliderButtonCoefficient) + 'px',
				"letter-spacing": Math.round(window["slider_button1_" + index][2] * sliderButtonCoefficient) + 'px',
				"padding-top": Math.round(window["slider_button1_" + index][3] * sliderButtonCoefficient) + 'px',
				"padding-bottom": Math.round(window["slider_button1_" + index][3] * sliderButtonCoefficient) + 'px',
				"padding-left": Math.round(window["slider_button1_" + index][4] * sliderButtonCoefficient) + 'px',
				"padding-right": Math.round(window["slider_button1_" + index][4] * sliderButtonCoefficient) + 'px'
			});
			slideItem.find('.eltd-btn:eq(1)').css({
				"font-size": Math.round(window["slider_button2_" + index][0] * sliderButtonCoefficient) + 'px',
				"line-height": Math.round(window["slider_button2_" + index][1] * sliderButtonCoefficient) + 'px',
				"letter-spacing": Math.round(window["slider_button2_" + index][2] * sliderButtonCoefficient) + 'px',
				"padding-top": Math.round(window["slider_button2_" + index][3] * sliderButtonCoefficient) + 'px',
				"padding-bottom": Math.round(window["slider_button2_" + index][3] * sliderButtonCoefficient) + 'px',
				"padding-left": Math.round(window["slider_button2_" + index][4] * sliderButtonCoefficient) + 'px',
				"padding-right": Math.round(window["slider_button2_" + index][4] * sliderButtonCoefficient) + 'px'
			});
		};

		/**
		 * Set heights for slider and elemnts depending on slider settings (full height, responsive height od set height)
		 * @param slider, current slider
		 */
		var setHeights = function(slider) {

			slider.find('.item').each(function(i) {
				setSizeGlobalVariablesForSlideElements($(this), i);
				setSliderElementsSize($(this), i, false);
			});

			if(slider.hasClass('eltd-full-screen')) {

				setSliderFullHeight(slider);

				$(window).resize(function() {
					setSliderElementsResponsiveCoeffeicients(responsiveBreakpointSet, coefficientsGraphicArray, coefficientsTitleArray, coefficientsSubtitleArray, coefficientsTextArray, coefficientsButtonArray);
					setSliderFullHeight(slider);
					slider.find('.item').each(function(i) {
						setSliderElementsSize($(this), i, false);
					});
				});

			} else if(slider.hasClass('eltd-responsive-height')) {

				var defaultHeight = slider.data('height');
				setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, false);

				$(window).resize(function() {
					setSliderElementsResponsiveCoeffeicients(responsiveBreakpointSet, coefficientsGraphicArray, coefficientsTitleArray, coefficientsSubtitleArray, coefficientsTextArray, coefficientsButtonArray);
					setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, false);
					slider.find('.item').each(function(i) {
						setSliderElementsSize($(this), i, false);
					});
				});

			} else {
				var defaultHeight = slider.data('height');

				slider.find('.eltd-slider-preloader').css({'height': (slider.height()) + 'px'});
				slider.find('.eltd-slider-preloader .eltd-ajax-loader').css({'display': 'block'});

				eltd.windowWidth < 1000 ? setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, false) : setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, true);

				$(window).resize(function() {
					setSliderElementsResponsiveCoeffeicients(responsiveBreakpointSet, coefficientsGraphicArray, coefficientsTitleArray, coefficientsSubtitleArray, coefficientsTextArray, coefficientsButtonArray);
					if(eltd.windowWidth < 1000) {
						setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, false);
						slider.find('.item').each(function(i) {
							setSliderElementsSize($(this), i, false);
						});
					} else {
						setSliderHeight(slider, defaultHeight, responsiveBreakpointSet, true);
						slider.find('.item').each(function(i) {
							setSliderElementsSize($(this), i, true);
						});
					}
				});
			}
		};

		/**
		 * Set prev/next numbers on navigation arrows
		 * @param slider, current slider
		 * @param currentItem, current slide item index
		 * @param totalItemCount, total number of slide items
		 */
		var setPrevNextNumbers = function(slider, currentItem, totalItemCount) {
			if(currentItem == 1) {
				slider.find('.left.carousel-control .prev').html(totalItemCount);
				slider.find('.right.carousel-control .next').html(currentItem + 1);
			} else if(currentItem == totalItemCount) {
				slider.find('.left.carousel-control .prev').html(currentItem - 1);
				slider.find('.right.carousel-control .next').html(1);
			} else {
				slider.find('.left.carousel-control .prev').html(currentItem - 1);
				slider.find('.right.carousel-control .next').html(currentItem + 1);
			}
		};

		/**
		 * Set video background size
		 * @param slider, current slider
		 */
		var initVideoBackgroundSize = function(slider) {
			var min_w = 1500; // minimum video width allowed
			var video_width_original = 1920;  // original video dimensions
			var video_height_original = 1080;
			var vid_ratio = 1920 / 1080;

			slider.find('.item .eltd-video .eltd-video-wrap').each(function() {

				var slideWidth = eltd.windowWidth;
				var slideHeight = $(this).closest('.carousel').height();

				$(this).width(slideWidth);

				min_w = vid_ratio * (slideHeight + 20);
				$(this).height(slideHeight);

				var scale_h = slideWidth / video_width_original;
				var scale_v = (slideHeight - eltdGlobalVars.vars.eltdMenuAreaHeight) / video_height_original;
				var scale = scale_v;
				if(scale_h > scale_v)
					scale = scale_h;
				if(scale * video_width_original < min_w) {
					scale = min_w / video_width_original;
				}

				$(this).find('video, .mejs-overlay, .mejs-poster').width(Math.ceil(scale * video_width_original + 2));
				$(this).find('video, .mejs-overlay, .mejs-poster').height(Math.ceil(scale * video_height_original + 2));
				$(this).scrollLeft(($(this).find('video').width() - slideWidth) / 2);
				$(this).find('.mejs-overlay, .mejs-poster').scrollTop(($(this).find('video').height() - slideHeight) / 2);
				$(this).scrollTop(($(this).find('video').height() - slideHeight) / 2);
			});
		};

		/**
		 * Init video background
		 * @param slider, current slider
		 */
		var initVideoBackground = function(slider) {
			$('.item .eltd-video-wrap .video').mediaelementplayer({
				enableKeyboard: false,
				iPadUseNativeControls: false,
				pauseOtherPlayers: false,
				// force iPhone's native controls
				iPhoneUseNativeControls: false,
				// force Android's native controls
				AndroidUseNativeControls: false
			});

			$(window).resize(function() {
				initVideoBackgroundSize(slider);
			});

			//mobile check
			if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|IEMobile|Opera Mini)/)) {
				$('.eltd-slider .eltd-mobile-video-image').show();
				$('.eltd-slider .eltd-video-wrap').remove();
			}
		};

		/**
		 * initiate slider
		 * @param slider, current slider
		 * @param currentItem, current slide item index
		 * @param totalItemCount, total number of slide items
		 * @param slideAnimationTimeout, timeout for slide change
		 */
		var initiateSlider = function(slider, totalItemCount, slideAnimationTimeout) {

			//set active class on first item
			slider.find('.carousel-inner .item:first-child').addClass('active');
			//check for header style
			eltdCheckSliderForHeaderStyle($('.carousel .active'), slider.hasClass('eltd-header-effect'));
			// setting numbers on carousel controls
			if(slider.hasClass('eltd-slider-numbers')) {
				setPrevNextNumbers(slider, 1, totalItemCount);
			}
			// set video background if there is video slide
			if(slider.find('.item video').length) {
				initVideoBackgroundSize(slider);
				initVideoBackground(slider);
			}

			//init slider
			if(slider.hasClass('eltd-auto-start')) {
				slider.carousel({
					interval: slideAnimationTimeout,
					pause: false
				});

				//pause slider when hover slider button
				slider.find('.slide_buttons_holder .qbutton')
					.mouseenter(function() {
						slider.carousel('pause');
					})
					.mouseleave(function() {
						slider.carousel('cycle');
					});
			} else {
				slider.carousel({
					interval: 0,
					pause: false
				});
			}


			//initiate image animation
			if($('.carousel-inner .item:first-child').hasClass('eltd-animate-image') && eltd.windowWidth > 1000) {
				slider.find('.carousel-inner .item.eltd-animate-image:first-child .eltd-image').transformAnimate({
					transform: "matrix(" + matrixArray[$('.carousel-inner .item:first-child').data('eltd_animate_image')] + ")",
					duration: 30000
				});
			}
		};

		return {
			init: function() {
				if(sliders.length) {
					sliders.each(function() {
						var $this = $(this);
						var slideAnimationTimeout = $this.data('slide_animation_timeout');
						var totalItemCount = $this.find('.item').length;
						if($this.data('eltd_responsive_breakpoints')) {
							if($this.data('eltd_responsive_breakpoints') == 'set2') {
								responsiveBreakpointSet = [1600, 1300, 1000, 768, 567, 320];
							}
						}
						coefficientsGraphicArray = $this.data('eltd_responsive_graphic_coefficients').split(',');
						coefficientsTitleArray = $this.data('eltd_responsive_title_coefficients').split(',');
						coefficientsSubtitleArray = $this.data('eltd_responsive_subtitle_coefficients').split(',');
						coefficientsTextArray = $this.data('eltd_responsive_text_coefficients').split(',');
						coefficientsButtonArray = $this.data('eltd_responsive_button_coefficients').split(',');

						setSliderElementsResponsiveCoeffeicients(responsiveBreakpointSet, coefficientsGraphicArray, coefficientsTitleArray, coefficientsSubtitleArray, coefficientsTextArray, coefficientsButtonArray);

						setHeights($this);

						/*** wait until first video or image is loaded and than initiate slider - start ***/
						if(eltd.htmlEl.hasClass('touch')) {
							if($this.find('.item:first-child .eltd-mobile-video-image').length > 0) {
								var src = imageRegex.exec($this.find('.item:first-child .eltd-mobile-video-image').attr('style'));
							} else {
								var src = imageRegex.exec($this.find('.item:first-child .eltd-image').attr('style'));
							}
							if(src) {
								var backImg = new Image();
								backImg.src = src[1];
								$(backImg).load(function() {
									$('.eltd-slider-preloader').fadeOut(500);
									initiateSlider($this, totalItemCount, slideAnimationTimeout);
								});
							}
						} else {
							if($this.find('.item:first-child video').length > 0) {
								$this.find('.item:first-child video').get(0).addEventListener('loadeddata', function() {
									$('.eltd-slider-preloader').fadeOut(500);
									initiateSlider($this, totalItemCount, slideAnimationTimeout);
								});
							} else {
								var src = imageRegex.exec($this.find('.item:first-child .eltd-image').attr('style'));
								if(src) {
									var backImg = new Image();
									backImg.src = src[1];
									$(backImg).load(function() {
										$('.eltd-slider-preloader').fadeOut(500);
										initiateSlider($this, totalItemCount, slideAnimationTimeout);
									});
								}
							}
						}
						/*** wait until first video or image is loaded and than initiate slider - end ***/

						/* before slide transition - start */
						$this.on('slide.bs.carousel', function() {
							$this.addClass('eltd-in-progress');
							$this.find('.active .eltd-slider-content-outer').fadeTo(250, 0);
						});
						/* before slide transition - end */

						/* after slide transition - start */
						$this.on('slid.bs.carousel', function() {
							$this.removeClass('eltd-in-progress');
							$this.find('.active .eltd-slider-content-outer').fadeTo(0, 1);

							// setting numbers on carousel controls
							if($this.hasClass('eltd-slider-numbers')) {
								var currentItem = $('.item').index($('.item.active')[0]) + 1;
								setPrevNextNumbers($this, currentItem, totalItemCount);
							}

							// initiate image animation on active slide and reset all others
							$('.item.eltd-animate-image .eltd-image').stop().css({
								'transform': '',
								'-webkit-transform': ''
							});
							if($('.item.active').hasClass('eltd-animate-image') && eltd.windowWidth > 1000) {
								$('.item.eltd-animate-image.active .eltd-image').transformAnimate({
									transform: "matrix(" + matrixArray[$('.item.eltd-animate-image.active').data('eltd_animate_image')] + ")",
									duration: 30000
								});
							}
						});
						/* after slide transition - end */

						/* swipe functionality - start */
						$this.swipe({
							swipeLeft: function() {
								$this.carousel('next');
							},
							swipeRight: function() {
								$this.carousel('prev');
							},
							threshold: 20
						});
						/* swipe functionality - end */

					});

					//adding parallax functionality on slider
					if($('.no-touch .carousel').length) {
						var skrollr_slider = skrollr.init({
							smoothScrolling: false,
							forceHeight: false
						});
						skrollr_slider.refresh();
					}

					$(window).scroll(function() {
						//set control class for slider in order to change header style
						if($('.eltd-slider .carousel').height() < eltd.scroll) {
							$('.eltd-slider .carousel').addClass('eltd-disable-slider-header-style-changing');
						} else {
							$('.eltd-slider .carousel').removeClass('eltd-disable-slider-header-style-changing');
							eltdCheckSliderForHeaderStyle($('.eltd-slider .carousel .active'), $('.eltd-slider .carousel').hasClass('eltd-header-effect'));
						}

						//hide slider when it is out of viewport
						if($('.eltd-slider .carousel').hasClass('eltd-full-screen') && eltd.scroll > eltd.windowHeight && eltd.windowWidth > 1000) {
							$('.eltd-slider .carousel').find('.carousel-inner, .carousel-indicators').hide();
						} else if(!$('.eltd-slider .carousel').hasClass('eltd-full-screen') && eltd.scroll > $('.eltd-slider .carousel').height() && eltd.windowWidth > 1000) {
							$('.eltd-slider .carousel').find('.carousel-inner, .carousel-indicators').hide();
						} else {
							$('.eltd-slider .carousel').find('.carousel-inner, .carousel-indicators').show();
						}
					});
				}
			}
		};
	};

	/**
	 * Check if slide effect on header style changing
	 * @param slide, current slide
	 * @param headerEffect, flag if slide
	 */

	function eltdCheckSliderForHeaderStyle(slide, headerEffect) {

		if($('.eltd-slider .carousel').not('.eltd-disable-slider-header-style-changing').length > 0) {

			var slideHeaderStyle = "";
			if(slide.hasClass('light')) {
				slideHeaderStyle = 'eltd-light-header';
			}
			if(slide.hasClass('dark')) {
				slideHeaderStyle = 'eltd-dark-header';
			}

			if(slideHeaderStyle !== "") {
				if(headerEffect) {
					eltd.body.removeClass('eltd-dark-header eltd-light-header').addClass(slideHeaderStyle);
				}
			} else {
				if(headerEffect) {
					eltd.body.removeClass('eltd-dark-header eltd-light-header').addClass(eltd.defaultHeaderStyle);
				}

			}
		}
	}

	function eltdInfoBox() {
		var infoBoxes = $('.eltd-info-box-holder');

		var getBottomHeight = function(bottomHolder) {
			if(bottomHolder.length) {
				return bottomHolder.height();
			}

			return false;
		}

		var infoBoxesHeight = function() {
			if(infoBoxes.length) {
				var maxHeight = 0;
				var heightestBox;

				infoBoxes.each(function() {
					var bottomHolder = $(this).find('.eltd-ib-bottom-holder');
					var topHolder = $(this).find('.eltd-ib-top-holder')

					var currentHeight = getBottomHeight(bottomHolder) + topHolder.height();

					maxHeight = Math.max(maxHeight, currentHeight);

					if(maxHeight <= currentHeight) {
						heightestBox = $(this);
						maxHeight = currentHeight;
					}
				});

				infoBoxes.height(maxHeight);
			}
		}

		var initHover = function(infoBox) {
			var timeline = new TimelineLite({paused: true}),
				topHolder = infoBox.find('.eltd-ib-top-holder'),
				bottomHolder = infoBox.find('.eltd-ib-bottom-holder'),
				bottomHeight = getBottomHeight(bottomHolder);

			timeline.to(topHolder, 0.6, {y: -(bottomHeight / 2), ease: Back.easeInOut.config(2)});
			timeline.to(bottomHolder, 0.4, {y: -(bottomHeight / 2), opacity: 1, ease: Back.easeOut}, '-=0.3');

			infoBox.on('mouseenter', function() {
				timeline.restart();
			});

			infoBox.on('mouseleave', function() {
				timeline.reverse();
			});
		}

		if(infoBoxes.length) {
			infoBoxesHeight();

			$(eltd.window).resize(function() {
				infoBoxesHeight();
			});

			infoBoxes.each(function() {
				var thisInfoBox = $(this);
				initHover(thisInfoBox);

				$(eltd.window).resize(function() {
					initHover(thisInfoBox);
				});
			});
		}
	}

	function eltdProcess() {
		var processes = $('.eltd-process-holder');

		var setProcessItemsPosition = function(process) {
			var items = process.find('.eltd-process-item-holder');
			var highlighted = items.filter('.eltd-pi-highlighted');

			if(highlighted.length) {
				if(highlighted.length === 1) {
					var afterHighlighed = highlighted.nextAll();

					if(afterHighlighed.length) {
						afterHighlighed.addClass('eltd-pi-push-right');
					}
				} else {
					process.addClass('eltd-process-multiple-highlights');
				}
			}
		}

		var processAnimation = function(process) {
			if(!eltd.body.hasClass('eltd-no-animations-on-touch')) {
				var items = process.find('.eltd-process-item-holder');
				var background = process.find('.eltd-process-bg-holder');

				process.appear(function() {

					items.each(function(i){

						var thisItem = $(this);
						setTimeout(function(){
							thisItem.addClass("eltd-process-item-appeared");
						}, 100+i*400);

						$(".eltd-process-bg-holder").addClass("eltd-process-bg-holder-loaded");

					});

				}, {accX: 0, accY: eltdGlobalVars.vars.eltdElementAppearAmount});
			}
		}

		return {
			init: function() {
				if(processes.length) {
					processes.each(function() {
						setProcessItemsPosition($(this));
						processAnimation($(this));
					});
				}
			}
		}
	};

	function eltdComparisonPricingTables() {
		var pricingTablesHolder = $('.eltd-comparision-pricing-tables-holder');

		var alterPricingTableColumn = function(holder) {
			var featuresHolder = holder.find('.eltd-cpt-features-item');
			var pricingTables = holder.find('.eltd-comparision-table-holder');

			if(pricingTables.length) {
				pricingTables.each(function() {
					var currentPricingTable = $(this);
					var pricingItems = currentPricingTable.find('.eltd-cpt-table-content li');

					if(pricingItems.length) {
						pricingItems.each(function(i) {
							var pricingItemFeature = featuresHolder[i];
							var pricingItem = this;
							var pricingItemContent = pricingItem.innerHTML;

							if(typeof pricingItemFeature !== 'undefined') {
								pricingItem.innerHTML = '<span class="eltd-cpt-table-item-feature">' + $(pricingItemFeature).text() + ': </span>' + pricingItemContent;
							}
						});
					}
				});
			}
		};

		return {
			init: function() {
				if(pricingTablesHolder.length) {
					pricingTablesHolder.each(function() {
						alterPricingTableColumn($(this));
					});
				}
			}
		}
	}

	function eltdBlogSlider() {
		var blogSliders = $('.eltd-blog-slider-holder');

		if(blogSliders.length) {
			blogSliders.each(function() {

				$(this).on('init', function() {
					$(this).css({'opacity': '1'});
					// change default opacity on init
					
					//blink fix
					var slide = $(this).find('.eltd-blog-slider-item'),
					    navBtn = $(this).find('button'),
					    slideTransitionInterval = 700,
					    toggleTimeout,
					    buttonTriggered = false,
					    cycle = slide.not('.slick-cloned').length,
					    delay = 0;

					var toggleActive = function() {
					    slide.each(function(){
					        var currentSlide = $(this),
					            currentSlideOffsetLeft = currentSlide.offset().left,
					            currentSlideWidth = currentSlide.outerWidth();

					        if(currentSlideOffsetLeft < 0 || currentSlideOffsetLeft + currentSlideWidth > eltd.windowWidth) {
					            currentSlide.addClass('eltd-darken');
					            currentSlide.removeClass('eltd-active');
					        } 
					        else {
					        	currentSlide.addClass('eltd-active');
					        }
					    });
					}

					toggleActive();

					navBtn.on('click',function(){
					    buttonTriggered = true;
					    slide.removeClass('eltd-darken');
					    clearTimeout(toggleTimeout);
					    toggleTimeout = setTimeout(function(){
					        toggleActive();
					        buttonTriggered = false;
					    }, slideTransitionInterval);
					});

					$(this).on('beforeChange', function(e, slick, currentSlide) {
					    setTimeout(function(){
					        if (!buttonTriggered) {
					            if( currentSlide == cycle - 1) {
					                delay = 400;
					            } else {
					                delay = 0;
					            }

					            slide.removeClass('eltd-darken');
					            setTimeout(function(){
					                toggleActive();
					            }, slideTransitionInterval + delay); 
					        }
					    },100); //check for buttonTriggered flag
					});

				}).slick({
					centerMode: true,
					slidesToShow: 1,
					arrows: true,
					prevArrow: '<span class="eltd-prev-icon"><i class="lnr lnr-chevron-left"></i></span>',
					nextArrow: '<span class="eltd-next-icon"><i class="lnr lnr-chevron-right"></i></span>',
					centerPadding: '0px',
					dots: true,
					speed: 700,
					cssEase: 'ease-out',
					swipeToSlide: true
				});
			});
		}
	}

	function eltdScrollDownIcon() {
		var icons = $('.eltd-row-scroll-icon');

		icons.on('click',function() {
			var row = $(this).closest('.eltd-section.eltd-row-with-scroll-icon');
			eltd.html.animate({scrollTop: (row.offset().top + row.outerHeight()) + 'px'}, 500);
		});
	}

	function emptySpaceResponsive() {
		var emptySpaces = $('.vc_empty_space');

		var sizes = {
			'large_laptop': 1559,
			'laptop': 1279,
			'tablet_landscape': 1023,
			'tablet_portrait': 767,
			'phone_landscape': 599,
			'phone_portrait': 479
		}

		var sizeValues = function() {
			var values = [];
			for(var size in sizes) {
				values.push(sizes[size]);
			}
			;

			return values;
		}();

		var getHeights = function(emptySpace) {
			var heights = {};

			for(var size in sizes) {
				var dataValue = emptySpace.data(size);
				if(typeof dataValue !== 'undefined' && dataValue !== '') {
					heights[size] = dataValue;
				}
			}

			return heights;
		};

		var usedSizes = function(emptySpace) {
			var usedSizes = [];

			for(var size in sizes) {
				var dataValue = emptySpace.data(size);
				if(typeof dataValue !== 'undefined' && dataValue !== '') {
					usedSizes.push(sizes[size]);
				}
			}

			return usedSizes;
		};

		var resizeEmptySpace = function(heights, emptySpace) {
			if(typeof heights !== 'undefined') {
				var originalHeight = emptySpace.data('original-height');
				var sizeValues = usedSizes(emptySpace);
				var heightestSize = Math.max.apply(null, sizeValues);

				for(var size in sizes) {
					if(eltd.windowWidth <= sizes[size]) {
						emptySpace.height(heights[size]);
					} else if(eltd.windowWidth > heightestSize) {
						emptySpace.height(originalHeight);
					}
				}
			}
		};

		return {
			init: function() {
				if(emptySpaces.length) {
					emptySpaces.each(function() {
						var heights = getHeights($(this));

						resizeEmptySpace(heights, $(this));

						var thisEmptySpace = $(this);

						$(window).resize(function() {
							resizeEmptySpace(heights, thisEmptySpace);
						});
					});
				}
			}
		}
	}

	/*
	 **	Vertical Split Slider
	 */

	function eltdInitVerticalSplitSlider() {

		if (eltd.body.hasClass('eltd-vertical-split-screen-initialized')) {
			eltd.body.removeClass('eltd-vertical-split-screen-initialized');
			$.fn.multiscroll.destroy();
		}

		var defaultHeaderStyle = '';
		if (eltd.body.hasClass('eltd-light-header')) {
			defaultHeaderStyle = 'light';
		} else if (eltd.body.hasClass('eltd-dark-header')) {
			defaultHeaderStyle = 'dark';
		}

		if ($('.eltd-vertical-split-slider').length) {

			var slider = $('.eltd-vertical-split-slider');

			slider.height(eltd.windowHeight).animate({opacity: 1}, 300);
			slider.multiscroll({
				scrollingSpeed: 700,
				easing: 'easeInOutQuart',
				navigation: true,
				useAnchorsOnLoad: false,
				sectionSelector: '.eltd-vss-ms-section',
				leftSelector: '.eltd-vss-ms-left',
				rightSelector: '.eltd-vss-ms-right',
				afterRender: function () {
					eltdCheckVerticalSplitSectionsForHeaderStyle($('.eltd-vss-ms-right .eltd-vss-ms-section:last-child').data('header-style'), defaultHeaderStyle);
					eltd.body.addClass('eltd-vertical-split-screen-initialized');
                    var contactForm7 = $('div.wpcf7 > form');
                    if(contactForm7.length) {
                      contactForm7.each(function(){
                       var thisForm = $(this);
                       var thisForm = $(this);

                       thisForm.find('.wpcf7-submit').off().on('click', function(e){
                        e.preventDefault();
                        wpcf7.submit(thisForm);
                       });
                      });
                    } // this function need to be initialized after initVerticalSplitSlide

					//prepare html for smaller screens - start //
					var verticalSplitSliderResponsive = $("<div class='eltd-vertical-split-slider-responsive' />");
					slider.after(verticalSplitSliderResponsive);
					var leftSide = $('.eltd-vertical-split-slider .eltd-vss-ms-left > div');
					var rightSide = $('.eltd-vertical-split-slider .eltd-vss-ms-right > div');

					for (var i = 0; i < leftSide.length; i++) {
						verticalSplitSliderResponsive.append($(leftSide[i]).clone(true));
						verticalSplitSliderResponsive.append($(rightSide[leftSide.length - 1 - i]).clone(true));
					}

					//prepare google maps clones
					if ($('.eltd-vertical-split-slider-responsive .eltd-google-map').length) {
						$('.eltd-vertical-split-slider-responsive .eltd-google-map').each(function () {
							var map = $(this);
							map.empty();
							var num = Math.floor((Math.random() * 100000) + 1);
							map.attr('id', 'eltd-map-' + num);
							map.data('unique-id', num);
						});
					}

					eltdButton().init();
                    eltdInitTestimonials();
					eltdInitPortfolioListMasonry();
					eltdInitPortfolioListPinterest();
					eltdInitPortfolioListJustifiedGallery();
					eltdInitPortfolio();
					eltdShowGoogleMap();
					eltdInitProgressBars();
					eltdIcon().init();
				},
				onLeave: function (index, nextIndex, direction) {

					$("#multiscroll-nav").removeClass("direction-up direction-down").addClass("direction-"+direction);
					eltdInitProgressBars();
					eltdCheckVerticalSplitSectionsForHeaderStyle($($('.eltd-vss-ms-right .eltd-vss-ms-section')[$(".eltd-vss-ms-right .eltd-vss-ms-section").length - nextIndex]).data('header-style'), defaultHeaderStyle);
				}
			});


			if (eltd.windowWidth <= 1024) {
				$.fn.multiscroll.destroy();
			} else {
				$.fn.multiscroll.build();
			}

			$(window).resize(function () {
				if (eltd.windowWidth <= 1024) {
					$.fn.multiscroll.destroy();
				} else {
					$.fn.multiscroll.build();
				}

			});
		}
	}

	/*
	 **	Check slides on load and slide change for header style changing
	 */
	function eltdCheckVerticalSplitSectionsForHeaderStyle(section_header_style, default_header_style) {

		if (section_header_style != undefined && section_header_style != '') {
			eltd.body.removeClass('eltd-light-header eltd-dark-header').addClass('eltd-' + section_header_style + '-header');
		} else if (default_header_style != '') {
			eltd.body.removeClass('eltd-light-header eltd-dark-header').addClass('eltd-' + default_header_style + '-header');
		} else {
			eltd.body.removeClass('eltd-light-header eltd-dark-header');
		}
	}

	function eltdHidingImages() {
		var containers = $('.eltd-hiding-images');

		if (containers.length) {
			var threshold = 0;

			var scatter = function(container, images) {
				var contTop = container.offset().top;
				var contHeight = container.height();
				var contWidth = container.width();
				var ratio = contHeight / contWidth;
				var scroll = $(window).scrollTop();
				if (contTop - threshold < scroll && contTop + contHeight + threshold > scroll) {
					var progress = (scroll - contTop + threshold) / (contHeight + 2*threshold);
					images.each(function() {
						var w = $(this).width();
						var h = $(this).height();
						$(this).css({
							'transform': 'translate3d('+(progress*(50 - $(this).data('initial-left')) * contWidth / w - 50)+'%,'+(progress*(50 - $(this).data('initial-top')) * contHeight / h - 50)+'%,0)'
							//'margin-left': progress * (50 - $(this).data('initial-left')) + '%',
							//'margin-top': progress * (50 - $(this).data('initial-top')) * ratio + '%',
						});
					});
				}
				else if (container.offset().top + container.height() + threshold <= scroll) {
					images.each(function() {
						var w = $(this).width();
						var h = $(this).height();
						$(this).css({
							'transform': 'translate3d('+((50 - $(this).data('initial-left')) * contWidth / w - 50)+'%,'+((50 - $(this).data('initial-top')) * contHeight / h - 50)+'%,0)'
							//'margin-left': (50 - $(this).data('initial-left')) + '%',
							//'margin-top': (50 - $(this).data('initial-top')) * ratio + '%'
						});
					});
				}
				else {
					images.css({
						'transform': 'translate3d(-50%,-50%,0)'
					});
				}
			}

			containers.each(function() {
                var container = $(this);
                var containerWidth = $(this).width();
                var containerHeight = $(this).height();
                var otherImages = container.find('.eltd-hi-other-image');
                otherImages.each(function () {
                    var w = $(this).width();
                    var h = $(this).height();
                    $(this).data('initial-left', parseFloat($(this).css('left')) / containerWidth * 100);
                    $(this).data('initial-top', parseFloat($(this).css('top')) / containerHeight * 100);
                    $(this).css({
                        'transform': 'translate3d(' + ((50 - $(this).data('initial-left')) * containerWidth / w - 50) + '%,' + ((50 - $(this).data('initial-top')) * containerHeight / h - 50) + '%,0)',
                        'transition': 'none'
                    });
                    $(window).load(function () {
                        otherImages.css({
                            'opacity': 1,
                            'transform': '',
                            'transition': ''
                        });
                    });
                    $(window).on('scroll resize', function () {
                        scatter(container, otherImages);
                    });
                });
            });
        }
	}

})(jQuery);
(function($) {
	'use strict';

	var woocommerce = {};
	eltd.modules.woocommerce = woocommerce;

	woocommerce.eltdInitQuantityButtons = eltdInitQuantityButtons;
	woocommerce.eltdfInitButtonLoading = eltdfInitButtonLoading;
	woocommerce.eltdInitSelect2 = eltdInitSelect2;

	woocommerce.eltdOnDocumentReady = eltdOnDocumentReady;
	woocommerce.eltdOnWindowLoad = eltdOnWindowLoad;
	woocommerce.eltdOnWindowResize = eltdOnWindowResize;
	woocommerce.eltdOnWindowScroll = eltdOnWindowScroll;

	$(document).ready(eltdOnDocumentReady);
	$(window).load(eltdOnWindowLoad);
	$(window).resize(eltdOnWindowResize);
	$(window).scroll(eltdOnWindowScroll);

	/* 
	 All functions to be called on $(document).ready() should be in this function
	 */
	function eltdOnDocumentReady() {
		eltdInitQuantityButtons();
		eltdfInitButtonLoading();
		eltdInitSelect2();
	}

	/* 
	 All functions to be called on $(window).load() should be in this function
	 */
	function eltdOnWindowLoad() {
		eltdInitProductListMasonryShortcode();
	}

	/* 
	 All functions to be called on $(window).resize() should be in this function
	 */
	function eltdOnWindowResize() {
		eltdInitProductListMasonryShortcode();
	}

	/* 
	 All functions to be called on $(window).scroll() should be in this function
	 */
	function eltdOnWindowScroll() {

	}


	function eltdInitQuantityButtons() {

		$(document).on('click', '.eltd-quantity-minus, .eltd-quantity-plus', function(e) {
			e.stopPropagation();

			var button = $(this),
				inputField = button.siblings('.eltd-quantity-input'),
				step = parseFloat(inputField.attr('step')),
				max = parseFloat(inputField.attr('max')),
				minus = false,
				inputValue = parseFloat(inputField.val()),
				newInputValue;

			if(button.hasClass('eltd-quantity-minus')) {
				minus = true;
			}

			if(minus) {
				newInputValue = inputValue - step;
				if(newInputValue >= 1) {
					inputField.val(newInputValue);
				} else {
					inputField.val(1);
				}
			} else {
				newInputValue = inputValue + step;
				if(max === undefined) {
					inputField.val(newInputValue);
				} else {
					if(newInputValue >= max) {
						inputField.val(max);
					} else {
						inputField.val(newInputValue);
					}
				}
			}
			inputField.trigger('change');
		});

	}


    function eltdfInitButtonLoading() {

        $(".add_to_cart_button").on('click',function(){
        	console.log(eltdGlobalVars.vars.eltdAddingToCart);
            $(this).text(eltdGlobalVars.vars.eltdAddingToCart);
        });

    }

	function eltdInitSelect2() {

		if($('.woocommerce-ordering .orderby').length || $('#calc_shipping_country').length) {

			$('.woocommerce-ordering .orderby').select2({
				minimumResultsForSearch: Infinity
			});

			$('#calc_shipping_country').select2();
		}

		$('.variations_form.cart select').select2({
			minimumResultsForSearch: Infinity
		});

	}

	/*
	 ** Init Product List Masonry Shortcode Layout
	 */
	function eltdInitProductListMasonryShortcode() {

		var container = $('.eltd-pl-holder.eltd-masonry-layout .eltd-pl-outer');

		if(container.length) {
			container.each(function() {
				var thisContainer = $(this);

				thisContainer.waitForImages(function() {
					thisContainer.isotope({
						itemSelector: '.eltd-pli',
						resizable: false,
						masonry: {
							columnWidth: '.eltd-pl-sizer',
							gutter: '.eltd-pl-gutter'
						}
					});

					thisContainer.isotope('layout');

					thisContainer.css('opacity', 1);
				});
			});
		}
	}

})(jQuery);
(function($) {
	'use strict';

	var portfolio = {};
	eltd.modules.portfolio = portfolio;

	portfolio.eltdOnDocumentReady = eltdOnDocumentReady;
	portfolio.eltdOnWindowLoad = eltdOnWindowLoad;
	portfolio.eltdOnWindowResize = eltdOnWindowResize;
	portfolio.eltdOnWindowScroll = eltdOnWindowScroll;

	$(document).ready(eltdOnDocumentReady);
	$(window).load(eltdOnWindowLoad);
	$(window).resize(eltdOnWindowResize);
	$(window).scroll(eltdOnWindowScroll);

	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function eltdOnDocumentReady() {
		portfolio.eltdPortfolioSlider();
	}

	/*
	 All functions to be called on $(window).load() should be in this function
	 */
	function eltdOnWindowLoad() {
		eltdPortfolioSingleFollow().init();
	}

	/*
	 All functions to be called on $(window).resize() should be in this function
	 */
	function eltdOnWindowResize() {

	}

	/*
	 All functions to be called on $(window).scroll() should be in this function
	 */
	function eltdOnWindowScroll() {

	}

	portfolio.eltdPortfolioSlider = function() {
		var sliders = $('.eltd-portfolio-slider-holder');

		var setPortfolioResponsiveOptions = function(slider, options) {
			var columns = slider.data('columns');
			if(typeof columns === 'number') {
				switch(columns) {
					case 1:
						options.items = 1;
						break;
					default:
						options.items = columns;
						options.itemsDesktop = false;
						break;
				}
			}
		};

		if(sliders.length) {
			sliders.each(function() {
				var sliderHolder = $(this).find('.eltd-portfolio-slider-list');
				var options = {};

				options.pagination = typeof sliderHolder.data('enable-pagination') !== 'undefined' && sliderHolder.data('enable-pagination') === 'yes';

				setPortfolioResponsiveOptions(sliderHolder, options);

				sliderHolder.owlCarousel(options);
			});

		}
	};


	var eltdPortfolioSingleFollow = function() {

		var info = $('.eltd-follow-portfolio-info .small-images.eltd-portfolio-single-holder .eltd-portfolio-info-holder, ' +
			'.eltd-follow-portfolio-info .small-slider.eltd-portfolio-single-holder .eltd-portfolio-info-holder');

		if(info.length) {
			var infoHolder = info.parent(),
				infoHolderOffset = infoHolder.offset().top,
				infoHolderHeight = infoHolder.height(),
				mediaHolder = $('.eltd-portfolio-media'),
				mediaHolderHeight = mediaHolder.height(),
				header = $('.header-appear, .eltd-fixed-wrapper'),
				headerHeight = (header.length) ? header.height() : 0,
				col1 = $('.eltd-portfolio-single-holder .eltd-column1'),
				col2 = $('.eltd-portfolio-single-holder .eltd-column2');
		}

		var infoHolderPosition = function() {

			if(info.length) {

				if(mediaHolderHeight > infoHolderHeight) {
					if(eltd.scroll > infoHolderOffset) {
						info.animate({
							marginTop: (eltd.scroll - (infoHolderOffset) + eltdGlobalVars.vars.eltdAddForAdminBar + headerHeight + 20) //20 px is for styling, spacing between header and info holder
						});
					}
				}

			}
		};

		var recalculateInfoHolderPosition = function() {

			if(info.length) {
				if (col1.offset().top == col2.offset().top) {
					if(mediaHolderHeight > infoHolderHeight) {
						if(eltd.scroll > infoHolderOffset) {

							if(eltd.scroll + headerHeight + eltdGlobalVars.vars.eltdAddForAdminBar + infoHolderHeight + 20 < infoHolderOffset + mediaHolderHeight) {    //20 px is for styling, spacing between header and info holder

								//Calculate header height if header appears
								if($('.header-appear, .eltd-fixed-wrapper').length) {
									headerHeight = $('.header-appear, .eltd-fixed-wrapper').height();
								}
								info.stop().animate({
									marginTop: (eltd.scroll - (infoHolderOffset) + eltdGlobalVars.vars.eltdAddForAdminBar + headerHeight + 20) //20 px is for styling, spacing between header and info holder
								});
								//Reset header height
								headerHeight = 0;
							}
							else {
								info.stop().animate({
									marginTop: mediaHolderHeight - infoHolderHeight
								});
							}
						} else {
							info.stop().animate({
								marginTop: 0
							});
						}
					}
				}
				else {
					info.css('margin-top','');
				}
			}
		};

		return {

			init: function() {
				if(!eltd.modules.common.eltdIsTouchDevice()) {
					infoHolderPosition();
					$(window).scroll(function() {
						recalculateInfoHolderPosition();
					});
				}
			}

		};

	};

})(jQuery);