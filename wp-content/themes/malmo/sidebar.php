<?php
$malmo_sidebar = malmo_elated_get_sidebar();
?>
<div class="eltd-column-inner">
	<aside class="eltd-sidebar">
		<?php
		if(is_active_sidebar($malmo_sidebar)) {
			dynamic_sidebar($malmo_sidebar);
		}
		?>
	</aside>
</div>
