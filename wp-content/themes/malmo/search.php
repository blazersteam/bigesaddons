<?php
$malmo_sidebar        = malmo_elated_sidebar_layout();
$malmo_excerpt_length_array = malmo_elated_blog_lists_number_of_chars();

$excerpt_length = 0;
if(is_array($malmo_excerpt_length_array) && array_key_exists('standard', $malmo_excerpt_length_array)) {
	$excerpt_length = $malmo_excerpt_length_array['standard'];
}

?>

<?php get_header(); ?>
<?php
global $wp_query;

if(get_query_var('paged')) {
	$malmo_paged = get_query_var('paged');
} elseif(get_query_var('page')) {
	$malmo_paged = get_query_var('page');
} else {
	$malmo_paged = 1;
}

if(malmo_elated_options()->getOptionValue('blog_page_range') != "") {
	$malmo_blog_page_range = esc_attr(malmo_elated_options()->getOptionValue('blog_page_range'));
} else {
	$malmo_blog_page_range = $wp_query->max_num_pages;
}
?>
<?php malmo_elated_get_title(); ?>
	<div class="eltd-container">
		<?php do_action('malmo_elated_after_container_open'); ?>
		<div class="eltd-container-inner clearfix">
			<div class="eltd-container">
				<?php do_action('malmo_elated_after_container_open'); ?>
				<div class="eltd-container-inner">
					<div class="eltd-blog-holder eltd-blog-type-standard">
						<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<div class="eltd-post-content">
									<div class="eltd-post-text">
										<div class="eltd-post-text-inner">
											<div class="eltd-post-info">
												<?php malmo_elated_post_info(array(
													'date'     => 'yes',
													'category' => 'yes',
													'comments' => 'yes',
													'like'     => 'yes'
												)) ?>
											</div>
											<h2 class="eltd-post-title">
												<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
											</h2>
											<?php if(get_post_type() === 'post') : ?>
												<?php malmo_elated_excerpt($excerpt_length); ?>
											<?php endif; ?>
										</div>

										<div class="eltd-author-desc clearfix">
											<div class="eltd-image-name">
												<div class="eltd-author-image">
													<?php echo malmo_elated_kses_img(get_avatar(get_the_author_meta('ID'), 102)); ?>
												</div>
												<div class="eltd-author-name-holder">
													<h5 class="eltd-author-name">
														<?php
														if(get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "") {
															echo esc_attr(get_the_author_meta('first_name'))." ".esc_attr(get_the_author_meta('last_name'));
														} else {
															echo esc_attr(get_the_author_meta('display_name'));
														}
														?>
													</h5>
												</div>
											</div>
											<div class="eltd-share-icons">
												<?php $post_info_array['share'] = malmo_elated_options()->getOptionValue('enable_social_share') == 'yes'; ?>
												<?php if($post_info_array['share'] == 'yes'): ?>
													<span class="eltd-share-label"><?php esc_html_e('Share', 'malmo'); ?></span>
												<?php endif; ?>
												<?php echo malmo_elated_get_social_share_html(array(
													'type'      => 'list',
													'icon_type' => 'normal'
												)); ?>
											</div>
										</div>
									</div>
								</div>
							</article>
						<?php endwhile; ?>
							<?php
							if(malmo_elated_options()->getOptionValue('pagination') == 'yes') {
								malmo_elated_pagination($wp_query->max_num_pages, $malmo_blog_page_range, $malmo_paged);
							}
							?>
						<?php else: ?>
							<div class="entry">
								<p><?php esc_html_e('No posts were found.', 'malmo'); ?></p>
							</div>
						<?php endif; ?>
					</div>
					<?php do_action('malmo_elated_before_container_close'); ?>
				</div>
			</div>
		</div>
		<?php do_action('malmo_elated_before_container_close'); ?>
	</div>
<?php get_footer(); ?>