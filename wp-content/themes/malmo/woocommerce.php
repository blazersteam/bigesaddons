<?php
/*
Template Name: WooCommerce
*/
?>
<?php

global $woocommerce;

$malmo_id      = get_option('woocommerce_shop_page_id');
$malmo_shop    = get_post($malmo_id);
$malmo_sidebar = malmo_elated_sidebar_layout();

if(get_post_meta($malmo_id, 'eltd_page_background_color', true) != '') {
	$malmo_background_color = 'background-color: '.esc_attr(get_post_meta($malmo_id, 'eltd_page_background_color', true));
} else {
	$malmo_background_color = '';
}

$malmo_content_style = '';
if(get_post_meta($malmo_id, 'eltd_content-top-padding', true) != '') {
	if(get_post_meta($malmo_id, 'eltd_content-top-padding-mobile', true) == 'yes') {
		$malmo_content_style = 'padding-top:'.esc_attr(get_post_meta($malmo_id, 'eltd_content-top-padding', true)).'px !important';
	} else {
		$malmo_content_style = 'padding-top:'.esc_attr(get_post_meta($malmo_id, 'eltd_content-top-padding', true)).'px';
	}
}

if(get_query_var('paged')) {
	$malmo_paged = get_query_var('paged');
} elseif(get_query_var('page')) {
	$malmo_paged = get_query_var('page');
} else {
	$malmo_paged = 1;
}

get_header();

malmo_elated_get_title();
get_template_part('slider');

$full_width = false;

if(malmo_elated_options()->getOptionValue('eltd_woo_products_list_full_width') == 'yes' && !is_singular('product')) {
	$full_width = true;
}

if($full_width) { ?>
<div class="eltd-full-width" <?php malmo_elated_inline_style($malmo_background_color); ?>>
	<?php } else { ?>
	<div class="eltd-container" <?php malmo_elated_inline_style($malmo_background_color); ?>>
		<?php }
		if($full_width) { ?>
		<div class="eltd-full-width-inner" <?php malmo_elated_inline_style($malmo_content_style); ?>>
			<?php } else { ?>
			<div class="eltd-container-inner clearfix" <?php malmo_elated_inline_style($malmo_content_style); ?>>
				<?php }

				//Woocommerce content
				if(!is_singular('product')) {

					switch($malmo_sidebar) {

						case 'sidebar-33-right': ?>
							<div class="eltd-two-columns-66-33 grid2 eltd-woocommerce-with-sidebar clearfix">
								<div class="eltd-column1">
									<div class="eltd-column-inner">
										<?php malmo_elated_woocommerce_content(); ?>
									</div>
								</div>
								<div class="eltd-column2">
									<?php get_sidebar(); ?>
								</div>
							</div>
							<?php
							break;
						case 'sidebar-25-right': ?>
							<div class="eltd-two-columns-75-25 grid2 eltd-woocommerce-with-sidebar clearfix">
								<div class="eltd-column1 eltd-content-left-from-sidebar">
									<div class="eltd-column-inner">
										<?php malmo_elated_woocommerce_content(); ?>
									</div>
								</div>
								<div class="eltd-column2">
									<?php get_sidebar(); ?>
								</div>
							</div>
							<?php
							break;
						case 'sidebar-33-left': ?>
							<div class="eltd-two-columns-33-66 grid2 eltd-woocommerce-with-sidebar clearfix">
								<div class="eltd-column1">
									<?php get_sidebar(); ?>
								</div>
								<div class="eltd-column2">
									<div class="eltd-column-inner">
										<?php malmo_elated_woocommerce_content(); ?>
									</div>
								</div>
							</div>
							<?php
							break;
						case 'sidebar-25-left': ?>
							<div class="eltd-two-columns-25-75 grid2 eltd-woocommerce-with-sidebar clearfix">
								<div class="eltd-column1">
									<?php get_sidebar(); ?>
								</div>
								<div class="eltd-column2 eltd-content-right-from-sidebar">
									<div class="eltd-column-inner">
										<?php malmo_elated_woocommerce_content(); ?>
									</div>
								</div>
							</div>
							<?php
							break;
						default:
							malmo_elated_woocommerce_content();
					}

				} else {
					malmo_elated_woocommerce_content();
				} ?>

			</div>
		</div>
		<?php get_footer(); ?>
