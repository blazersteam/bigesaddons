<?php get_header(); ?>

	<div class="eltd-container">
	<?php do_action('malmo_elated_after_container_open'); ?>
		<div class="eltd-container-inner eltd-404-page">
			<div class="eltd-page-not-found">
				<div class="eltd-404-mark"><?php esc_html_e( '404', 'malmo' ); ?></div>
				<h2>
					<?php if(malmo_elated_options()->getOptionValue('404_title')){
						echo esc_html(malmo_elated_options()->getOptionValue('404_title'));
					}
					else{
						esc_html_e('Page not found', 'malmo');
					} ?>
				</h2>
				<p>
					<?php if(malmo_elated_options()->getOptionValue('404_text')){
						echo esc_html(malmo_elated_options()->getOptionValue('404_text'));
					}
					else{
						esc_html_e('The page you are looking for does not exist. It may have been moved, or removed altogether. Perhaps you can return back to the site\'s homepage and see if you can find what you are looking for.', 'malmo');
					} ?>
				</p>
				<?php
                if(malmo_elated_core_installed()) {
                    $params = array();
                    if (malmo_elated_options()->getOptionValue('404_back_to_home')) {
                        $params['text'] = malmo_elated_options()->getOptionValue('404_back_to_home');
                    } else {
                        $params['text'] = esc_html__('Back to Homepage', 'malmo');
                    }
                    $params['link'] = esc_url(home_url('/'));
                    $params['target'] = '_self';
                    echo malmo_elated_execute_shortcode('eltd_button', $params);
                }
                ?>

			</div>
		</div>
		<?php do_action('malmo_elated_before_container_close'); ?>
	</div>
<?php get_footer(); ?>