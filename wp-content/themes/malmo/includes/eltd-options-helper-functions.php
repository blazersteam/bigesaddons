<?php

if(!function_exists('malmo_elated_is_responsive_on')) {
	/**
	 * Checks whether responsive mode is enabled in theme options
	 * @return bool
	 */
	function malmo_elated_is_responsive_on() {
		return malmo_elated_options()->getOptionValue('responsiveness') !== 'no';
	}
}