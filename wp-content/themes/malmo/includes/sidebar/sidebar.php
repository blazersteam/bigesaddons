<?php

if(!function_exists('malmo_elated_register_sidebars')) {
    /**
     * Function that registers theme's sidebars
     */
    function malmo_elated_register_sidebars() {

        register_sidebar(array(
            'name'          => esc_html__('Sidebar', 'malmo'),
            'id'            => 'sidebar',
            'description'   => esc_html__('Default Sidebar', 'malmo'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3><span class="eltd-sidearea-title">',
            'after_title'   => '</span></h3>'
        ));

    }

    add_action('widgets_init', 'malmo_elated_register_sidebars');
}

if(!function_exists('malmo_elated_add_support_custom_sidebar')) {
    /**
     * Function that adds theme support for custom sidebars. It also creates MalmoElatedSidebar object
     */
    function malmo_elated_add_support_custom_sidebar() {
        add_theme_support('MalmoElatedSidebar');
        if(get_theme_support('MalmoElatedSidebar')) {
            new MalmoElatedSidebar();
        }
    }

    add_action('after_setup_theme', 'malmo_elated_add_support_custom_sidebar');
}
