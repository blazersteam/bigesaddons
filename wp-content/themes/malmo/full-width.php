<?php 
/*
Template Name: Full Width
*/ 
?>
<?php
$malmo_sidebar = malmo_elated_sidebar_layout(); ?>

<?php get_header(); ?>
<?php malmo_elated_get_title(); ?>
<?php get_template_part('slider'); ?>

<div class="eltd-full-width">
<div class="eltd-full-width-inner">
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
		<div class="eltd-grid-row">
			<div <?php echo malmo_elated_get_content_sidebar_class(); ?>>
				<?php the_content(); ?>
				<?php do_action('malmo_elated_page_after_content'); ?>
			</div>

			<?php if(!in_array($malmo_sidebar, array('default', ''))) : ?>
				<div <?php echo malmo_elated_get_sidebar_holder_class(); ?>>
					<?php get_sidebar(); ?>
				</div>
			<?php endif; ?>
		</div>
	<?php endwhile; endif; ?>
</div>
</div>
<?php get_footer(); ?>