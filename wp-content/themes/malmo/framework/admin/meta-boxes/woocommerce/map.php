<?php

//WooCommerce
if(malmo_elated_is_woocommerce_installed()) {

    $woo_single_layout      = malmo_elated_options()->getOptionValue('single_product_layout');
    $woo_single_layout_hide = '';
    $woo_single_layout_show = '#eltd_eltd_show_standard_layout_container';

    if($woo_single_layout !== 'standard') {
        $woo_single_layout_hide = '#eltd_eltd_show_standard_layout_container';
        $woo_single_layout_show = '';
    }

    $woocommerce_meta_box = malmo_elated_create_meta_box(
        array(
            'scope' => array('product'),
            'title' => esc_html__('Product Meta', 'malmo'),
            'name'  => 'woo_product_meta'
        )
    );

    malmo_elated_create_meta_box_field(array(
        'name'        => 'eltd_product_featured_image_size',
        'type'        => 'select',
        'label'       => esc_html__('Dimensions for Product List Shortcode', 'malmo'),
        'description' => esc_html__('Choose image layout when it appears in Elated Product List - Masonry layout shortcode', 'malmo'),
        'parent'      => $woocommerce_meta_box,
        'options'     => array(
            'eltd-woo-image-normal-width' => esc_html__('Default', 'malmo'),
            'eltd-woo-image-large-width'  => esc_html__('Large Width', 'malmo')
        )
    ));

}