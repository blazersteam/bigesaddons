<?php

$general_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post'),
        'title' => esc_html__('General', 'malmo'),
        'name'  => 'general_meta'
    )
);


malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_page_background_color_meta',
        'type'          => 'color',
        'default_value' => '',
        'label'         => esc_html__('Page Background Color', 'malmo'),
        'description'   => esc_html__('Choose background color for page content', 'malmo'),
        'parent'        => $general_meta_box
    )
);


malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_page_fixed_background_image_meta',
        'type'          => 'image',
        'default_value' => '',
        'label'         => esc_html__('Fixed Background Image', 'malmo'),
        'description'   => esc_html__('Choose an image that will be displayed in the page background as fixed', 'malmo'),
        'parent'        => $general_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_page_padding_meta',
        'type'          => 'text',
        'default_value' => '',
        'label'         => esc_html__('Page Padding', 'malmo'),
        'description'   => esc_html__('Insert padding in format 10px 10px 10px 10px', 'malmo'),
        'parent'        => $general_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_page_content_behind_header_meta',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Always put content behind header', 'malmo'),
        'description'   => esc_html__('Enabling this option will put page content behind page header', 'malmo'),
        'parent'        => $general_meta_box,
        'args'          => array(
            'suffix' => 'px'
        )
    )
);

malmo_elated_create_meta_box_field(array(
    'name'        => 'eltd_show_back_button_meta',
    'type'        => 'select',
    'label'       => esc_html__('Show "Back To Top Button"', 'malmo'),
    'description' => esc_html__('"Yes" will display a Back to Top button on this page.', 'malmo'),
    'parent'      => $general_meta_box,
    'options'     => array(
        '' => esc_html__('Default', 'malmo'),
        'yes' => esc_html__('Yes', 'malmo'),
        'no' => esc_html__('No', 'malmo')
    )
));

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_page_slider_meta',
        'type'          => 'text',
        'default_value' => '',
        'label'         => esc_html__('Slider Shortcode', 'malmo'),
        'description'   => esc_html__('Paste your slider shortcode here', 'malmo'),
        'parent'        => $general_meta_box
    )
);

if(malmo_elated_options()->getOptionValue('smooth_pt_true_ajax') === 'yes') {
    malmo_elated_create_meta_box_field(
        array(
            'name'          => 'eltd_page_transition_type',
            'type'          => 'selectblank',
            'label'         => esc_html__('Page Transition', 'malmo'),
            'description'   => esc_html__('Choose the type of transition to this page', 'malmo'),
            'parent'        => $general_meta_box,
            'default_value' => '',
            'options'       => array(
                'no-animation' => esc_html__('No animation', 'malmo'),
                'fade'         => esc_html__('Fade', 'malmo')
            )
        )
    );
}

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_page_comments_meta',
        'type'        => 'selectblank',
        'label'       => esc_html__('Show Comments', 'malmo'),
        'description' => esc_html__('Enabling this option will show comments on your page', 'malmo'),
        'parent'      => $general_meta_box,
        'options'     => array(
            'yes' => esc_html__('Yes', 'malmo'),
            'no'  => esc_html__('No', 'malmo'),
        )
    )
);