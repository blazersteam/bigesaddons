<?php

/*** Audio Post Format ***/

$audio_post_format_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('post'),
        'title' => esc_html__('Audio Post Format', 'malmo'),
        'name'  => 'post_format_audio_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_audio_link_meta',
        'type'        => 'text',
        'label'       => esc_html__('Link', 'malmo'),
        'description' => esc_html__('Enter audion link', 'malmo'),
        'parent'      => $audio_post_format_meta_box,

    )
);
