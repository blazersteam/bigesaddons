<?php

//Slider

$slider_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('slides'),
        'title' => esc_html__('Slide Background Type', 'malmo'),
        'name'  => 'slides_type'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_background_type',
        'type'          => 'imagevideo',
        'label'         => esc_html__('Slide Background Type', 'malmo'),
        'description'   => esc_html__('Do you want to upload an image or video?', 'malmo'),
        'default_value' => 'image',
        'parent'        => $slider_meta_box,
        'args'          => array(
            "dependence"             => true,
            "dependence_hide_on_yes" => "#eltd-meta-box-eltd_slides_video_settings",
            "dependence_show_on_yes" => "#eltd-meta-box-eltd_slides_image_settings"
        )
    )
);


//Slide Image

$slider_meta_box = malmo_elated_create_meta_box(
    array(
        'scope'           => array('slides'),
        'title'           => esc_html__('Slide Background Image', 'malmo'),
        'name'            => 'eltd_slides_image_settings',
        'hidden_property' => 'eltd_slide_background_type',
        'hidden_values'   => array('video')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_image',
        'type'        => 'image',
        'label'       => esc_html__('Slide Image', 'malmo'),
        'description' => esc_html__('Choose background image', 'malmo'),
        'parent'      => $slider_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_overlay_image',
        'type'        => 'image',
        'label'       => esc_html__('Overlay Image', 'malmo'),
        'description' => esc_html__('Choose overlay image (pattern) for background image', 'malmo'),
        'parent'      => $slider_meta_box
    )
);


//Slide Video

$video_meta_box = malmo_elated_create_meta_box(
    array(
        'scope'           => array('slides'),
        'title'           => esc_html__('Slide Background Video', 'malmo'),
        'name'            => 'eltd_slides_video_settings',
        'hidden_property' => 'eltd_slide_background_type',
        'hidden_values'   => array('image')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_video_webm',
        'type'        => 'text',
        'label'       => esc_html__('Video - webm', 'malmo'),
        'description' => esc_html__('Path to the webm file that you have previously uploaded in Media Section', 'malmo'),
        'parent'      => $video_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_video_mp4',
        'type'        => 'text',
        'label'       => esc_html__('Video - mp4', 'malmo'),
        'description' => esc_html__('Path to the mp4 file that you have previously uploaded in Media Section', 'malmo'),
        'parent'      => $video_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_video_ogv',
        'type'        => 'text',
        'label'       => esc_html__('Video - ogv', 'malmo'),
        'description' => esc_html__('Path to the ogv file that you have previously uploaded in Media Section', 'malmo'),
        'parent'      => $video_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_video_image',
        'type'        => 'image',
        'label'       => esc_html__('Video Preview Image', 'malmo'),
        'description' => esc_html__('Choose background image that will be visible until video is loaded. This image will be shown on touch devices too.', 'malmo'),
        'parent'      => $video_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_video_overlay',
        'type'          => 'yesempty',
        'default_value' => '',
        'label'         => esc_html__('Video Overlay Image', 'malmo'),
        'description'   => esc_html__('Do you want to have a overlay image on video?', 'malmo'),
        'parent'        => $video_meta_box,
        'args'          => array(
            "dependence"             => true,
            "dependence_hide_on_yes" => "",
            "dependence_show_on_yes" => "#eltd_eltd_slide_video_overlay_container"
        )
    )
);

$slide_video_overlay_container = malmo_elated_add_admin_container(array(
    'name'            => 'eltd_slide_video_overlay_container',
    'parent'          => $video_meta_box,
    'hidden_property' => 'eltd_slide_video_overlay',
    'hidden_values'   => array('', 'no')
));

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_video_overlay_image',
        'type'        => 'image',
        'label'       => esc_html__('Overlay Image', 'malmo'),
        'description' => esc_html__('Choose overlay image (pattern) for background video.', 'malmo'),
        'parent'      => $slide_video_overlay_container
    )
);


//Slide General

$general_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('slides'),
        'title' => esc_html__('Slide General', 'malmo'),
        'name'  => 'eltd_slides_general_settings'
    )
);

malmo_elated_add_admin_section_title(
    array(
        'parent' => $general_meta_box,
        'name'   => 'eltd_text_content_title',
        'title'  => esc_html__('Slide Text Content', 'malmo')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_hide_title',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Hide Slide Title', 'malmo'),
        'description'   => esc_html__('Do you want to hide slide title?', 'malmo'),
        'parent'        => $general_meta_box,
        'args'          => array(
            "dependence"             => true,
            "dependence_hide_on_yes" => "#eltd_eltd_slide_hide_title_container, #eltd-meta-box-eltd_slides_title",
            "dependence_show_on_yes" => ""
        )
    )
);

$slide_hide_title_container = malmo_elated_add_admin_container(array(
    'name'            => 'eltd_slide_hide_title_container',
    'parent'          => $general_meta_box,
    'hidden_property' => 'eltd_slide_hide_title',
    'hidden_value'    => 'yes'
));

$group_title_link = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Title Link', 'malmo'),
    'name'        => 'group_title_link',
    'description' => esc_html__('Define styles for title', 'malmo'),
    'parent'      => $slide_hide_title_container
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $group_title_link
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_link',
        'type'   => 'textsimple',
        'label'  => esc_html__('Link', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $row1,
        'type'          => 'selectsimple',
        'name'          => 'eltd_slide_title_target',
        'default_value' => '_self',
        'label'         => esc_html__('Target', 'malmo'),
        'options'       => array(
            "_self"  => esc_html__('Self', 'malmo'),
            "_blank" => esc_html__('Blank', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_subtitle',
        'type'        => 'text',
        'label'       => esc_html__('Subtitle Text', 'malmo'),
        'description' => esc_html__('Enter text for subtitle', 'malmo'),
        'parent'      => $general_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_text',
        'type'        => 'text',
        'label'       => esc_html__('Body Text', 'malmo'),
        'description' => esc_html__('Enter slide body text', 'malmo'),
        'parent'      => $general_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_button_label',
        'type'        => 'text',
        'label'       => esc_html__('Button 1 Text', 'malmo'),
        'description' => esc_html__('Enter text to be displayed on button 1', 'malmo'),
        'parent'      => $general_meta_box
    )
);

$group_button1 = malmo_elated_add_admin_group(array(
    'title'  => esc_html__('Button 1 Link', 'malmo'),
    'name'   => 'group_button1',
    'parent' => $general_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $group_button1
));

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_button_link',
        'type'          => 'textsimple',
        'label'         => esc_html__('Link', 'malmo'),
        'default_value' => '',
        'parent'        => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $row1,
        'type'          => 'selectsimple',
        'name'          => 'eltd_slide_button_target',
        'default_value' => '_self',
        'label'         => esc_html__('Target', 'malmo'),
        'options'       => array(
            "_self"  => esc_html__('Self', 'malmo'),
            "_blank" => esc_html__('Blank', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_button_label2',
        'type'        => 'text',
        'label'       => esc_html__('Button 2 Text', 'malmo'),
        'description' => esc_html__('Enter text to be displayed on button 2', 'malmo'),
        'parent'      => $general_meta_box
    )
);

$group_button2 = malmo_elated_add_admin_group(array(
    'title'  => esc_html__('Button 2 Link', 'malmo'),
    'name'   => 'group_button2',
    'parent' => $general_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $group_button2
));

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_button_link2',
        'type'          => 'textsimple',
        'default_value' => '',
        'label'         => esc_html__('Link', 'malmo'),
        'parent'        => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $row1,
        'type'          => 'selectsimple',
        'name'          => 'eltd_slide_button_target2',
        'default_value' => '_self',
        'label'         => esc_html__('Target', 'malmo'),
        'options'       => array(
            "_self"  => esc_html__('Self', 'malmo'),
            "_blank" => esc_html__('Blank', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_text_content_top_margin',
        'type'        => 'text',
        'label'       => esc_html__('Text Content Top Margin', 'malmo'),
        'description' => esc_html__('Enter top margin for text content', 'malmo'),
        'parent'      => $general_meta_box,
        'args'        => array(
            'col_width' => 2,
            'suffix'    => 'px'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_text_content_bottom_margin',
        'type'        => 'text',
        'label'       => esc_html__('Text Content Bottom Margin', 'malmo'),
        'description' => esc_html__('Enter bottom margin for text content', 'malmo'),
        'parent'      => $general_meta_box,
        'args'        => array(
            'col_width' => 2,
            'suffix'    => 'px'
        )
    )
);

malmo_elated_add_admin_section_title(
    array(
        'parent' => $general_meta_box,
        'name'   => 'eltd_graphic_title',
        'title'  => esc_html__('Slide Graphic', 'malmo')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_thumbnail',
        'type'        => 'image',
        'label'       => esc_html__('Slide Graphic', 'malmo'),
        'description' => esc_html__('Choose slide graphic', 'malmo'),
        'parent'      => $general_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_thumbnail_link',
        'type'        => 'text',
        'label'       => esc_html__('Graphic Link', 'malmo'),
        'description' => esc_html__('Enter URL to link slide graphic', 'malmo'),
        'parent'      => $general_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_graphic_top_padding',
        'type'        => 'text',
        'label'       => esc_html__('Graphic Top Padding', 'malmo'),
        'description' => esc_html__('Enter top padding for slide graphic', 'malmo'),
        'parent'      => $general_meta_box,
        'args'        => array(
            'col_width' => 2,
            'suffix'    => 'px'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_graphic_bottom_padding',
        'type'        => 'text',
        'label'       => esc_html__('Graphic Bottom Padding', 'malmo'),
        'description' => esc_html__('Enter bottom padding for slide graphic', 'malmo'),
        'parent'      => $general_meta_box,
        'args'        => array(
            'col_width' => 2,
            'suffix'    => 'px'
        )
    )
);

malmo_elated_add_admin_section_title(
    array(
        'parent' => $general_meta_box,
        'name'   => 'eltd_general_styling_title',
        'title'  => esc_html__('General Styling', 'malmo')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $general_meta_box,
        'type'          => 'selectblank',
        'name'          => 'eltd_slide_header_style',
        'default_value' => '',
        'label'         => esc_html__('Header Style', 'malmo'),
        'description'   => esc_html__('Header style will be applied when this slide is in focus', 'malmo'),
        'options'       => array(
            "light" => esc_html__('Light', 'malmo'),
            "dark"  => esc_html__('Dark', 'malmo')
        )
    )
);

//Slide Behaviour

$behaviours_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('slides'),
        'title' => esc_html__('Slide Behaviours', 'malmo'),
        'name'  => 'eltd_slides_behaviour_settings'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_scroll_to_section',
        'type'        => 'text',
        'label'       => esc_html__('Scroll to Section', 'malmo'),
        'description' => esc_html__('An arrow will appear to take viewers to the next section of the page. Enter the section anchor here, for example, \'#contact\'', 'malmo'),
        'parent'      => $behaviours_meta_box
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_scroll_to_section_position',
        'type'        => 'select',
        'label'       => esc_html__('Scroll to Section Icon Position', 'malmo'),
        'description' => esc_html__('Choose position for anchor icon - scroll to section', 'malmo'),
        'parent'      => $behaviours_meta_box,
        'options'     => array(
            "in_content"       => esc_html__('In Text Content', 'malmo'),
            "bottom_of_slider" => esc_html__('Bottom of the slide', 'malmo')
        )
    )
);

malmo_elated_add_admin_section_title(
    array(
        'parent' => $behaviours_meta_box,
        'name'   => 'eltd_image_animation_title',
        'title'  => esc_html__('Slide Image Animation', 'malmo')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_enable_image_animation',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Enable Image Animation', 'malmo'),
        'description'   => esc_html__('Enabling this option will turn on a motion animation on the slide image', 'malmo'),
        'parent'        => $behaviours_meta_box,
        'args'          => array(
            "dependence"             => true,
            "dependence_hide_on_yes" => "",
            "dependence_show_on_yes" => "#eltd_eltd_enable_image_animation_container"
        )
    )
);

$enable_image_animation_container = malmo_elated_add_admin_container(array(
    'name'            => 'eltd_enable_image_animation_container',
    'parent'          => $behaviours_meta_box,
    'hidden_property' => 'eltd_enable_image_animation',
    'hidden_value'    => 'no'
));

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $enable_image_animation_container,
        'type'          => 'select',
        'name'          => 'eltd_enable_image_animation_type',
        'default_value' => esc_html__('zoom_center', 'malmo'),
        'label'         => esc_html__('Animation Type', 'malmo'),
        'options'       => array(
            "zoom_center"       => esc_html__('Zoom In Center', 'malmo'),
            "zoom_top_left"     => esc_html__('Zoom In to Top Left', 'malmo'),
            "zoom_top_right"    => esc_html__('Zoom In to Top Right', 'malmo'),
            "zoom_bottom_left"  => esc_html__('Zoom In to Bottom Left', 'malmo'),
            "zoom_bottom_right" => esc_html__('Zoom In to Bottom Right', 'malmo')
        )
    )
);

malmo_elated_add_admin_section_title(
    array(
        'parent' => $behaviours_meta_box,
        'name'   => 'eltd_content_animation_title',
        'title'  => esc_html__('Slide Content Entry Animations', 'malmo')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $behaviours_meta_box,
        'type'          => 'select',
        'name'          => 'eltd_slide_thumbnail_animation',
        'default_value' => 'flip',
        'label'         => esc_html__('Graphic Entry Animation', 'malmo'),
        'description'   => esc_html__('Choose entry animation for graphic', 'malmo'),
        'options'       => array(
            "flip"              => esc_html__('Flip', 'malmo'),
            "fade"              => esc_html__('Fade In', 'malmo'),
            "from_bottom"       => esc_html__('From Bottom', 'malmo'),
            "from_top"          => esc_html__('From Top', 'malmo'),
            "from_left"         => esc_html__('From Left', 'malmo'),
            "from_right"        => esc_html__('From Right', 'malmo'),
            "clip_anim_hor"     => esc_html__('Clip Animation Horizontal', 'malmo'),
            "clip_anim_ver"     => esc_html__('Clip Animation Vertical', 'malmo'),
            "clip_anim_puzzle"  => esc_html__('Clip Animation Puzzle', 'malmo'),
            "without_animation" => esc_html__('No Animation', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $behaviours_meta_box,
        'type'          => 'select',
        'name'          => 'eltd_slide_content_animation',
        'default_value' => 'all_at_once',
        'label'         => esc_html__('Content Entry Animation', 'malmo'),
        'description'   => esc_html__('Choose entry animation for whole slide content group (title, subtitle, text, button)', 'malmo'),
        'options'       => array(
            "all_at_once"       => esc_html__('All At Once', 'malmo'),
            "one_by_one"        => esc_html__('One By One', 'malmo'),
            "without_animation" => esc_html__('No Animation', 'malmo')
        ),
        'args'          => array(
            "dependence" => true,
            "hide"       => array(
                "all_at_once"       => "",
                "one_by_one"        => "",
                "without_animation" => "#eltd_eltd_slide_content_animation_container"
            ),
            "show"       => array(
                "all_at_once"       => "#eltd_eltd_slide_content_animation_container",
                "one_by_one"        => "#eltd_eltd_slide_content_animation_container",
                "without_animation" => ""
            )
        )
    )
);

$slide_content_animation_container = malmo_elated_add_admin_container(array(
    'name'            => 'eltd_slide_content_animation_container',
    'parent'          => $behaviours_meta_box,
    'hidden_property' => 'eltd_slide_content_animation',
    'hidden_value'    => 'without_animation'
));

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $slide_content_animation_container,
        'type'          => 'select',
        'name'          => 'eltd_slide_content_animation_direction',
        'default_value' => 'from_bottom',
        'label'         => esc_html__('Animation Direction', 'malmo'),
        'options'       => array(
            "from_bottom" => esc_html__('From Bottom', 'malmo'),
            "from_top"    => esc_html__('From Top', 'malmo'),
            "from_left"   => esc_html__('From Left', 'malmo'),
            "from_right"  => esc_html__('From Right', 'malmo'),
            "fade"        => esc_html__('Fade In', 'malmo')
        )
    )
);

//Slide Title Styles

$title_style_meta_box = malmo_elated_create_meta_box(
    array(
        'scope'           => array('slides'),
        'title'           => esc_html__('Slide Title Style', 'malmo'),
        'name'            => 'eltd_slides_title',
        'hidden_property' => 'eltd_slide_hide_title',
        'hidden_values'   => array('yes')
    )
);

$title_text_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Title Text Style', 'malmo'),
    'description' => esc_html__('Define styles for title text', 'malmo'),
    'name'        => 'eltd_title_text_group',
    'parent'      => $title_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $title_text_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Font Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_font_size',
        'type'   => 'textsimple',
        'label'  => esc_html__('Font Size (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_line_height',
        'type'   => 'textsimple',
        'label'  => esc_html__('Line Height (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_letter_spacing',
        'type'   => 'textsimple',
        'label'  => esc_html__('Letter Spacing (px)', 'malmo'),
        'parent' => $row1
    )
);

$row2 = malmo_elated_add_admin_row(array(
    'name'   => 'row2',
    'parent' => $title_text_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_font_family',
        'type'   => 'fontsimple',
        'label'  => esc_html__('Font Family', 'malmo'),
        'parent' => $row2
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_title_font_style',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Font Style', 'malmo'),
        'parent'  => $row2,
        'options' => $malmo_options_fontstyle
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_title_font_weight',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Font Weight', 'malmo'),
        'parent'  => $row2,
        'options' => $malmo_options_fontweight
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_title_text_transform',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Text Transform', 'malmo'),
        'parent'  => $row2,
        'options' => $malmo_options_texttransform
    )
);

$title_background_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Background', 'malmo'),
    'description' => esc_html__('Define background for title', 'malmo'),
    'name'        => 'eltd_title_background_group',
    'parent'      => $title_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $title_background_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_background_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Background Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_bg_color_transparency',
        'type'   => 'textsimple',
        'label'  => esc_html__('Background Color Transparency (values 0-1)', 'malmo'),
        'parent' => $row1
    )
);

$title_margin_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Margin Bottom (px)', 'malmo'),
    'description' => esc_html__('Enter value for title bottom margin (default value is 14)', 'malmo'),
    'name'        => 'eltd_title_margin_group',
    'parent'      => $title_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $title_margin_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_margin_bottom',
        'type'   => 'textsimple',
        'label'  => '',
        'parent' => $row1
    )
);

$title_padding_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Padding', 'malmo'),
    'description' => esc_html__('Define padding for title', 'malmo'),
    'name'        => 'eltd_title_padding_group',
    'parent'      => $title_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $title_padding_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_padding_top',
        'type'   => 'textsimple',
        'label'  => esc_html__('Top Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_padding_right',
        'type'   => 'textsimple',
        'label'  => esc_html__('Right Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_padding_bottom',
        'type'   => 'textsimple',
        'label'  => esc_html__('Bottom Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_padding_left',
        'type'   => 'textsimple',
        'label'  => esc_html__('Left Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

$slide_title_border = malmo_elated_create_meta_box_field(array(
    'label'         => esc_html__('Border', 'malmo'),
    'description'   => esc_html__('Do you want to have a title border?', 'malmo'),
    'name'          => 'eltd_slide_title_border',
    'type'          => 'yesno',
    'default_value' => 'no',
    'parent'        => $title_style_meta_box,
    'args'          => array(
        'dependence'             => true,
        'dependence_hide_on_yes' => '',
        'dependence_show_on_yes' => '#eltd_eltd_title_border_container'
    )
));

$title_border_container = malmo_elated_add_admin_container(array(
    'name'            => 'eltd_title_border_container',
    'parent'          => $title_style_meta_box,
    'hidden_property' => 'eltd_slide_title_border',
    'hidden_value'    => 'no'
));

$title_border_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Title Border', 'malmo'),
    'description' => esc_html__('Define border for title', 'malmo'),
    'name'        => 'eltd_title_border_group',
    'parent'      => $title_border_container
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $title_border_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_title_border_thickness',
        'type'   => 'textsimple',
        'label'  => esc_html__('Thickness (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_title_border_style',
        'type'    => 'selectsimple',
        'label'   => esc_html__('Style', 'malmo'),
        'parent'  => $row1,
        'options' => array(
            "solid"  => esc_html__('solid', 'malmo'),
            "dashed" => esc_html__('dashed', 'malmo'),
            "dotted" => esc_html__('dotted', 'malmo'),
            "double" => esc_html__('double', 'malmo'),
            "groove" => esc_html__('groove', 'malmo'),
            "ridge"  => esc_html__('ridge', 'malmo'),
            "inset"  => esc_html__('inset', 'malmo'),
            "outset" => esc_html__('outset', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slider_title_border_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Color', 'malmo'),
        'parent' => $row1
    )
);

//Slide Subtitle Styles

$subtitle_style_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('slides'),
        'title' => esc_html__('Slide Subtitle Style', 'malmo'),
        'name'  => 'eltd_slides_subtitle'
    )
);

$subtitle_text_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Subtitle Text Style', 'malmo'),
    'description' => esc_html__('Define styles for subtitle text', 'malmo'),
    'name'        => 'eltd_subtitle_text_group',
    'parent'      => $subtitle_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $subtitle_text_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Font Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_font_size',
        'type'   => 'textsimple',
        'label'  => esc_html__('Font Size (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_line_height',
        'type'   => 'textsimple',
        'label'  => esc_html__('Line Height (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_letter_spacing',
        'type'   => 'textsimple',
        'label'  => esc_html__('Letter Spacing (px)', 'malmo'),
        'parent' => $row1
    )
);

$row2 = malmo_elated_add_admin_row(array(
    'name'   => 'row2',
    'parent' => $subtitle_text_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_font_family',
        'type'   => 'fontsimple',
        'label'  => esc_html__('Font Family', 'malmo'),
        'parent' => $row2
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_subtitle_font_style',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Font Style', 'malmo'),
        'parent'  => $row2,
        'options' => $malmo_options_fontstyle
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_subtitle_font_weight',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Font Weight', 'malmo'),
        'parent'  => $row2,
        'options' => $malmo_options_fontweight
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_subtitle_text_transform',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Text Transform', 'malmo'),
        'parent'  => $row2,
        'options' => $malmo_options_texttransform
    )
);

$subtitle_background_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Background', 'malmo'),
    'description' => esc_html__('Define background for subtitle', 'malmo'),
    'name'        => 'eltd_subtitle_background_group',
    'parent'      => $subtitle_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $subtitle_background_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_background_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Background Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_bg_color_transparency',
        'type'   => 'textsimple',
        'label'  => esc_html__('Background Color Transparency (values 0-1)', 'malmo'),
        'parent' => $row1
    )
);

$subtitle_margin_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Margin Bottom (px)', 'malmo'),
    'description' => esc_html__('Enter value for subtitle bottom margin (default value is 14)', 'malmo'),
    'name'        => 'eltd_subtitle_margin_group',
    'parent'      => $subtitle_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $subtitle_margin_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_margin_bottom',
        'type'   => 'textsimple',
        'label'  => '',
        'parent' => $row1
    )
);

$subtitle_padding_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Padding', 'malmo'),
    'description' => esc_html__('Define padding for subtitle', 'malmo'),
    'name'        => 'eltd_subtitle_padding_group',
    'parent'      => $subtitle_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $subtitle_padding_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_padding_top',
        'type'   => 'textsimple',
        'label'  => esc_html__('Top Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_padding_right',
        'type'   => 'textsimple',
        'label'  => esc_html__('Right Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_padding_bottom',
        'type'   => 'textsimple',
        'label'  => esc_html__('Bottom Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_subtitle_padding_left',
        'type'   => 'textsimple',
        'label'  => esc_html__('Left Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

//Slide Text Styles

$text_style_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('slides'),
        'title' => esc_html__('Slide Text Style', 'malmo'),
        'name'  => 'eltd_slides_text'
    )
);

$text_common_text_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Text Color and Size', 'malmo'),
    'description' => esc_html__('Define text color and size', 'malmo'),
    'name'        => 'eltd_text_common_text_group',
    'parent'      => $text_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $text_common_text_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Font Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_font_size',
        'type'   => 'textsimple',
        'label'  => esc_html__('Font Size (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_line_height',
        'type'   => 'textsimple',
        'label'  => esc_html__('Line Height (px)', 'malmo'),
        'parent' => $row1
    )
);

$text_without_separator_padding_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Padding', 'malmo'),
    'description' => esc_html__('Define padding for text', 'malmo'),
    'name'        => 'eltd_text_without_separator_padding_group',
    'parent'      => $text_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $text_without_separator_padding_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_padding_top',
        'type'   => 'textsimple',
        'label'  => esc_html__('Top Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_padding_right',
        'type'   => 'textsimple',
        'label'  => esc_html__('Right Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_padding_bottom',
        'type'   => 'textsimple',
        'label'  => esc_html__('Bottom Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_padding_left',
        'type'   => 'textsimple',
        'label'  => esc_html__('Left Padding (px)', 'malmo'),
        'parent' => $row1
    )
);

$text_without_separator_text_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Text Style', 'malmo'),
    'description' => esc_html__('Define styles for slide text', 'malmo'),
    'name'        => 'eltd_text_without_separator_text_group',
    'parent'      => $text_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $text_without_separator_text_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_letter_spacing',
        'type'   => 'textsimple',
        'label'  => esc_html__('Letter Spacing (px)', 'malmo'),
        'parent' => $row1
    )
);

$row2 = malmo_elated_add_admin_row(array(
    'name'   => 'row2',
    'parent' => $text_without_separator_text_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_font_family',
        'type'   => 'fontsimple',
        'label'  => esc_html__('Font Family', 'malmo'),
        'parent' => $row2
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_text_font_style',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Font Style', 'malmo'),
        'parent'  => $row2,
        'options' => $malmo_options_fontstyle
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_text_font_weight',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Font Weight', 'malmo'),
        'parent'  => $row2,
        'options' => $malmo_options_fontweight
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_text_text_transform',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Text Transform', 'malmo'),
        'parent'  => $row2,
        'options' => $malmo_options_texttransform
    )
);

$text_without_separator_background_group = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Background', 'malmo'),
    'description' => esc_html__('Define background for text', 'malmo'),
    'name'        => 'eltd_text_without_separator_background_group',
    'parent'      => $text_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $text_without_separator_background_group
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_background_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Background Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_bg_color_transparency',
        'type'   => 'textsimple',
        'label'  => esc_html__('Background Color Transparency (values 0-1)', 'malmo'),
        'parent' => $row1
    )
);

//Slide Buttons Styles

$buttons_style_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('slides'),
        'title' => esc_html__('Slide Buttons Style', 'malmo'),
        'name'  => 'eltd_slides_buttons'
    )
);

malmo_elated_add_admin_section_title(
    array(
        'parent' => $buttons_style_meta_box,
        'name'   => 'eltd_button_1_styling_title',
        'title'  => esc_html__('Button 1', 'malmo')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_button_size',
        'type'          => 'selectblank',
        'parent'        => $buttons_style_meta_box,
        'label'         => esc_html__('Size', 'malmo'),
        'description'   => esc_html__('Choose button size', 'malmo'),
        'default_value' => '',
        'options'       => array(
            ""                => esc_html__('Default', 'malmo'),
            "small"           => esc_html__('Small', 'malmo'),
            "medium"          => esc_html__('Medium', 'malmo'),
            "large"           => esc_html__('Large', 'malmo'),
            "huge"            => esc_html__('Extra Large', 'malmo'),
            "huge-full-width" => esc_html__('Extra Large Full Width', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_button_type',
        'type'          => 'selectblank',
        'parent'        => $buttons_style_meta_box,
        'label'         => esc_html__('Type', 'malmo'),
        'description'   => esc_html__('Choose button type', 'malmo'),
        'default_value' => '',
        'options'       => array(
            ""        => esc_html__('Default', 'malmo'),
            "outline" => esc_html__('Outline', 'malmo'),
            "solid"   => esc_html__('Solid', 'malmo')
        )
    )
);

$buttons_style_group_1 = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Text Style', 'malmo'),
    'description' => esc_html__('Define text style', 'malmo'),
    'name'        => 'eltd_buttons_style_group_1',
    'parent'      => $buttons_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $buttons_style_group_1
));

/*
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'eltd_slide_button_font_family',
		'type'        => 'fontsimple',
		'label'       => 'Font Family',
		'parent'      => $row1
	)
);
*/

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_font_size',
        'type'   => 'textsimple',
        'label'  => esc_html__('Text Size(px)', 'malmo'),
        'parent' => $row1
    )
);

/*
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'eltd_slide_button_font_style',
		'type'        => 'selectblanksimple',
		'label'       => 'Font Style',
		'parent'      => $row1,
		'options'     => $malmo_options_fontstyle
	)
);
*/

malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_button_font_weight',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Font Weight', 'malmo'),
        'parent'  => $row1,
        'options' => $malmo_options_fontweight
    )
);
/*
$row2 = malmo_elated_add_admin_row(array(
	'name' => 'row2',
	'parent' => $buttons_style_group_1
));


	malmo_elated_create_meta_box_field(
		array(
			'name'        => 'eltd_slide_button_letter_spacing',
			'type'        => 'textsimple',
			'label'       => 'Letter Spacing(px)',
			'parent'      => $row2
		)
	);

	malmo_elated_create_meta_box_field(
		array(
			'name'        => 'eltd_slide_button_line_height',
			'type'        => 'textsimple',
			'label'       => 'Line Height (px)',
			'parent'      => $row2
		)
	);
	*/

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_text_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Text Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_text_hover_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Text Hover Color', 'malmo'),
        'parent' => $row1
    )
);

/*
$row3 = malmo_elated_add_admin_row(array(
	'name' => 'row3',
	'parent' => $buttons_style_group_1
));

	malmo_elated_create_meta_box_field(
		array(
			'name'        => 'eltd_slide_button_text_align',
			'type'        => 'selectblanksimple',
			'label'       => 'Text Align',
			'parent'      => $row3,
			'options'     => array(
				"left" => "Left",
				"center" => "Center",
				"right" => "Right"
			)
		)
	);

	malmo_elated_create_meta_box_field(
		array(
			'name'        => 'eltd_slide_button_text_transform',
			'type'        => 'selectblanksimple',
			'label'       => 'Text Transform',
			'parent'      => $row3,
			'options'     => $malmo_options_texttransform
		)
	);
*/

$buttons_style_group_2 = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Background', 'malmo'),
    'description' => esc_html__('Define background', 'malmo'),
    'name'        => 'eltd_buttons_style_group_2',
    'parent'      => $buttons_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $buttons_style_group_2
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_background_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Background Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_background_hover_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Background Hover Color', 'malmo'),
        'parent' => $row1
    )
);

/*
$buttons_style_group_3 = malmo_elated_add_admin_group(array(
	'title' => 'Size',
	'description' => 'Define button size',
	'name' => 'eltd_buttons_style_group_3',
	'parent' => $buttons_style_meta_box
));
	$row1 = malmo_elated_add_admin_row(array(
		'name' => 'row1',
		'parent' => $buttons_style_group_3
	));

		malmo_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_slide_button_width',
				'type'        => 'textsimple',
				'label'       => 'Width (px)',
				'parent'      => $row1
			)
		);

		malmo_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_slide_button_height',
				'type'        => 'textsimple',
				'label'       => 'Height (px)',
				'parent'      => $row1
			)
		);
		*/

$buttons_style_group_4 = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Border', 'malmo'),
    'description' => esc_html__('Define border style', 'malmo'),
    'name'        => 'eltd_buttons_style_group_4',
    'parent'      => $buttons_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $buttons_style_group_4
));

/*
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'eltd_slide_button_border_width',
		'type'        => 'textsimple',
		'label'       => 'Border Width (px)',
		'parent'      => $row1
	)
);
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'eltd_slide_button_border_radius',
		'type'        => 'textsimple',
		'label'       => 'Border Radius (px)',
		'parent'      => $row1
	)
);
*/
malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_border_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Border Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_border_hover_color',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Border Hover Color', 'malmo'),
        'parent' => $row1
    )
);

$buttons_style_group_5 = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Margin (px)', 'malmo'),
    'description' => esc_html__('Please insert margin in format (top right bottom left) i.e. 5px 5px 5px 5px', 'malmo'),
    'name'        => 'eltd_buttons_style_group_5',
    'parent'      => $buttons_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $buttons_style_group_5
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_margin1',
        'type'   => 'textsimple',
        'label'  => '',
        'parent' => $row1
    )
);
/*
$buttons_style_group_6 = malmo_elated_add_admin_group(array(
	'title' => 'Padding (px)',
	'description' => 'Please insert padding in format (top right bottom left) i.e. 5px 5px 5px 5px',
	'name' => 'eltd_buttons_style_group_6',
	'parent' => $buttons_style_meta_box
));

	$row1 = malmo_elated_add_admin_row(array(
		'name' => 'row1',
		'parent' => $buttons_style_group_6
	));

		malmo_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_slide_button_padding',
				'type'        => 'textsimple',
				'label'       => '',
				'parent'      => $row1
			)
		);
*/

/*
$buttons_style_group_7 = malmo_elated_add_admin_group(array(
	'title' => 'Button Hover Animation',
	'description' => 'Define hover animation for button',
	'name' => 'eltd_buttons_style_group_7',
	'parent' => $buttons_style_meta_box
));

	$row1 = malmo_elated_add_admin_row(array(
		'name' => 'row1',
		'parent' => $buttons_style_group_7
	));

		malmo_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_slide_button1_hover_button_animation',
				'type'        => 'selectblanksimple',
				'label'       => 'Hover Animation',
				'parent'      => $row1,
				'options'     => array(
					"fill_from_top" => "Fill From Top",
					"fill_from_left" => "Fill From Left",
					"fill_from_bottom" => "Fill From Bottom"
				)
			)
		);
*/
//init icon pack hide and show array. It will be populated dinamically from collections array
$button1_icon_pack_hide_array = array();
$button1_icon_pack_show_array = array();

//do we have some collection added in collections array?
if(is_array($malmo_IconCollections->iconCollections) && count($malmo_IconCollections->iconCollections)) {
    //get collections params array. It will contain values of 'param' property for each collection
    $button1_icon_collections_params = $malmo_IconCollections->getIconCollectionsParams();

    //foreach collection generate hide and show array
    foreach($malmo_IconCollections->iconCollections as $dep_collection_key => $dep_collection_object) {
        $button1_icon_pack_hide_array[$dep_collection_key] = '';
        $button1_icon_pack_hide_array["no_icon"]           = "";

        //button1_icon_size is input that is always shown when some icon pack is activated and hidden if 'no_icon' is selected
        $button1_icon_pack_hide_array["no_icon"] .= "#eltd_slider_button1_icon_size,";

        //we need to include only current collection in show string as it is the only one that needs to show
        $button1_icon_pack_show_array[$dep_collection_key] = '#eltd_slider_button1_icon_size, #eltd_button1_icon_'.$dep_collection_object->param.'_container';

        //for all collections param generate hide string
        foreach($button1_icon_collections_params as $button1_icon_collections_param) {
            //we don't need to include current one, because it needs to be shown, not hidden
            if($button1_icon_collections_param !== $dep_collection_object->param) {
                $button1_icon_pack_hide_array[$dep_collection_key] .= '#eltd_button1_icon_'.$button1_icon_collections_param.'_container,';
            }

            $button1_icon_pack_hide_array["no_icon"] .= '#eltd_button1_icon_'.$button1_icon_collections_param.'_container,';
        }

        //remove remaining ',' character
        $button1_icon_pack_hide_array[$dep_collection_key] = rtrim($button1_icon_pack_hide_array[$dep_collection_key], ',');
        $button1_icon_pack_hide_array["no_icon"]           = rtrim($button1_icon_pack_hide_array["no_icon"], ',');
    }

}

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_button1_icon_pack',
        'type'          => 'select',
        'label'         => esc_html__('Button 1 Icon Pack', 'malmo'),
        'description'   => esc_html__('Choose icon pack for the first button', 'malmo'),
        'default_value' => 'no_icon',
        'parent'        => $buttons_style_meta_box,
        'options'       => $malmo_IconCollections->getIconCollectionsEmpty("no_icon"),
        'args'          => array(
            "dependence" => true,
            "hide"       => $button1_icon_pack_hide_array,
            "show"       => $button1_icon_pack_show_array
        )
    )
);

//echo var_dump($button1_icon_pack_hide_array); die();

if(is_array($malmo_IconCollections->iconCollections) && count($malmo_IconCollections->iconCollections)) {
    //foreach icon collection we need to generate separate container that will have dependency set
    //it will have one field inside with icons dropdown
    foreach($malmo_IconCollections->iconCollections as $collection_key => $collection_object) {
        $icons_array = $collection_object->getIconsArray();

        //get icon collection keys (keys from collections array, e.g 'font_awesome', 'font_elegant' etc.)
        $icon_collections_keys = $malmo_IconCollections->getIconCollectionsKeys();

        //unset current one, because it doesn't have to be included in dependency that hides icon container
        unset($icon_collections_keys[array_search($collection_key, $icon_collections_keys)]);

        $button1_icon_hide_values   = $icon_collections_keys;
        $button1_icon_hide_values[] = "no_icon";
        $button1_icon_container     = malmo_elated_add_admin_container(array(
            'name'            => "button1_icon_".$collection_object->param."_container",
            'parent'          => $buttons_style_meta_box,
            'hidden_property' => 'eltd_button1_icon_pack',
            'hidden_value'    => '',
            'hidden_values'   => $button1_icon_hide_values
        ));

        malmo_elated_create_meta_box_field(
            array(
                'name'    => "button1_icon_".$collection_object->param,
                'type'    => 'select',
                'label'   => esc_html__('Button 1 Icon', 'malmo'),
                'parent'  => $button1_icon_container,
                'options' => $icons_array
            )
        );
    }

}
/*
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'slider_button1_icon_size',
		'type'        => 'text',
		'label'       => 'Icon Size (px)',
		'description' => 'Define size for icon in button',
		'parent'      => $buttons_style_meta_box,
		'hidden_property' => 'eltd_button1_icon_pack',
		'hidden_values' => array('no_icon')
	)
);
*/


malmo_elated_add_admin_section_title(
    array(
        'parent' => $buttons_style_meta_box,
        'name'   => 'eltd_button_2_styling_title',
        'title'  => esc_html__('Button 2', 'malmo')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_button_size2',
        'type'          => 'selectblank',
        'parent'        => $buttons_style_meta_box,
        'label'         => esc_html__('Size', 'malmo'),
        'description'   => esc_html__('Choose button size', 'malmo'),
        'default_value' => '',
        'options'       => array(
            ""                => esc_html__('Default', 'malmo'),
            "small"           => esc_html__('Small', 'malmo'),
            "medium"          => esc_html__('Medium', 'malmo'),
            "large"           => esc_html__('Large', 'malmo'),
            "huge"            => esc_html__('Extra Large', 'malmo'),
            "huge-full-width" => esc_html__('Extra Large Full Width', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_button_type2',
        'type'          => 'selectblank',
        'parent'        => $buttons_style_meta_box,
        'label'         => esc_html__('Type', 'malmo'),
        'description'   => esc_html__('Choose button type', 'malmo'),
        'default_value' => '',
        'options'       => array(
            ""        => esc_html__('Default', 'malmo'),
            "outline" => esc_html__('Outline', 'malmo'),
            "solid"   => esc_html__('Solid', 'malmo')
        )
    )
);

$buttons2_style_group_1 = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Text Style', 'malmo'),
    'description' => esc_html__('Define text style', 'malmo'),
    'name'        => 'eltd_buttons2_style_group_1',
    'parent'      => $buttons_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $buttons2_style_group_1
));
/*
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'eltd_slide_button_font_family2',
		'type'        => 'fontsimple',
		'label'       => 'Font Family',
		'parent'      => $row1
	)
);
*/
malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_font_size2',
        'type'   => 'textsimple',
        'label'  => esc_html__('Text Size(px)', 'malmo'),
        'parent' => $row1
    )
);
/*
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'eltd_slide_button_font_style2',
		'type'        => 'selectblanksimple',
		'label'       => 'Font Style',
		'parent'      => $row1,
		'options'     => $malmo_options_fontstyle
	)
);
*/
malmo_elated_create_meta_box_field(
    array(
        'name'    => 'eltd_slide_button_font_weight2',
        'type'    => 'selectblanksimple',
        'label'   => esc_html__('Font Weight', 'malmo'),
        'parent'  => $row1,
        'options' => $malmo_options_fontweight
    )
);
/*
$row2 = malmo_elated_add_admin_row(array(
	'name' => 'row2',
	'parent' => $buttons2_style_group_1
));

	malmo_elated_create_meta_box_field(
		array(
			'name'        => 'eltd_slide_button_letter_spacing2',
			'type'        => 'textsimple',
			'label'       => 'Letter Spacing(px)',
			'parent'      => $row2
		)
	);

	malmo_elated_create_meta_box_field(
		array(
			'name'        => 'eltd_slide_button_line_height2',
			'type'        => 'textsimple',
			'label'       => 'Line Height (px)',
			'parent'      => $row2
		)
	);
	*/
malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_text_color2',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Text Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_text_hover_color2',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Text Hover Color', 'malmo'),
        'parent' => $row1
    )
);
/*
$row3 = malmo_elated_add_admin_row(array(
	'name' => 'row3',
	'parent' => $buttons2_style_group_1
));

	malmo_elated_create_meta_box_field(
		array(
			'name'        => 'eltd_slide_button_text_align2',
			'type'        => 'selectblanksimple',
			'label'       => 'Text Align',
			'parent'      => $row3,
			'options'     => array(
				"left" => "Left",
				"center" => "Center",
				"right" => "Right"
			)
		)
	);

	malmo_elated_create_meta_box_field(
		array(
			'name'        => 'eltd_slide_button_text_transform2',
			'type'        => 'selectblanksimple',
			'label'       => 'Text Transform',
			'parent'      => $row3,
			'options'     => $malmo_options_texttransform
		)
	);
	*/
$buttons2_style_group_2 = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Background', 'malmo'),
    'description' => esc_html__('Define background', 'malmo'),
    'name'        => 'eltd_buttons2_style_group_2',
    'parent'      => $buttons_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $buttons2_style_group_2
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_background_color2',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Background Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_background_hover_color2',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Background Hover Color', 'malmo'),
        'parent' => $row1
    )
);
/*
$buttons2_style_group_3 = malmo_elated_add_admin_group(array(
	'title' => 'Size',
	'description' => 'Define button size',
	'name' => 'eltd_buttons2_style_group_3',
	'parent' => $buttons_style_meta_box
));

	$row1 = malmo_elated_add_admin_row(array(
		'name' => 'row1',
		'parent' => $buttons2_style_group_3
	));

		malmo_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_slide_button_width2',
				'type'        => 'textsimple',
				'label'       => 'Width (px)',
				'parent'      => $row1
			)
		);

		malmo_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_slide_button_height2',
				'type'        => 'textsimple',
				'label'       => 'Height (px)',
				'parent'      => $row1
			)
		);
		*/

$buttons2_style_group_4 = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Border', 'malmo'),
    'description' => esc_html__('Define border style', 'malmo'),
    'name'        => 'eltd_buttons2_style_group_4',
    'parent'      => $buttons_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $buttons2_style_group_4
));

/*
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'eltd_slide_button_border_width2',
		'type'        => 'textsimple',
		'label'       => 'Border Width (px)',
		'parent'      => $row1
	)
);
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'eltd_slide_button_border_radius2',
		'type'        => 'textsimple',
		'label'       => 'Border Radius (px)',
		'parent'      => $row1
	)
);
*/

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_border_color2',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Border Color', 'malmo'),
        'parent' => $row1
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_border_hover_color2',
        'type'   => 'colorsimple',
        'label'  => esc_html__('Border Hover Color', 'malmo'),
        'parent' => $row1
    )
);

$buttons2_style_group_5 = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Margin (px)', 'malmo'),
    'description' => esc_html__('Please insert margin in format (top right bottom left) i.e. 5px 5px 5px 5px', 'malmo'),
    'name'        => 'eltd_buttons2_style_group_5',
    'parent'      => $buttons_style_meta_box
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $buttons2_style_group_5
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_button_margin2',
        'type'   => 'textsimple',
        'label'  => '',
        'parent' => $row1
    )
);

/*
$buttons2_style_group_6 = malmo_elated_add_admin_group(array(
	'title' => 'Padding (px)',
	'description' => 'Please insert padding in format (top right bottom left) i.e. 5px 5px 5px 5px',
	'name' => 'eltd_buttons2_style_group_6',
	'parent' => $buttons_style_meta_box
));

	$row1 = malmo_elated_add_admin_row(array(
		'name' => 'row1',
		'parent' => $buttons2_style_group_6
	));

		malmo_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_slide_button_padding2',
				'type'        => 'textsimple',
				'label'       => '',
				'parent'      => $row1
			)
		);

$buttons2_style_group_7 = malmo_elated_add_admin_group(array(
	'title' => 'Button Hover Animation',
	'description' => 'Define hover animation for button',
	'name' => 'eltd_buttons2_style_group_7',
	'parent' => $buttons_style_meta_box
));

	$row1 = malmo_elated_add_admin_row(array(
		'name' => 'row1',
		'parent' => $buttons2_style_group_7
	));

		malmo_elated_create_meta_box_field(
			array(
				'name'        => 'eltd_slide_button2_hover_button_animation',
				'type'        => 'selectblanksimple',
				'label'       => 'Hover Animation',
				'parent'      => $row1,
				'options'     => array(
					"fill_from_top" => "Fill From Top",
					"fill_from_left" => "Fill From Left",
					"fill_from_bottom" => "Fill From Bottom"
				)
			)
		);
		*/

//init icon pack hide and show array. It will be populated dinamically from collections array
$button2_icon_pack_hide_array = array();
$button2_icon_pack_show_array = array();

//do we have some collection added in collections array?
if(is_array($malmo_IconCollections->iconCollections) && count($malmo_IconCollections->iconCollections)) {
    //get collections params array. It will contain values of 'param' property for each collection
    $button2_icon_collections_params = $malmo_IconCollections->getIconCollectionsParams();

    //foreach collection generate hide and show array
    foreach($malmo_IconCollections->iconCollections as $dep_collection_key => $dep_collection_object) {
        $button2_icon_pack_hide_array[$dep_collection_key] = '';
        $button2_icon_pack_hide_array["no_icon"]           = "";

        //button2_icon_size is input that is always shown when some icon pack is activated and hidden if 'no_icon' is selected
        $button2_icon_pack_hide_array["no_icon"] .= "#eltd_slider_button2_icon_size,";

        //we need to include only current collection in show string as it is the only one that needs to show
        $button2_icon_pack_show_array[$dep_collection_key] = '#eltd_slider_button2_icon_size, #eltd_button2_icon_'.$dep_collection_object->param.'_container';

        //for all collections param generate hide string
        foreach($button2_icon_collections_params as $button2_icon_collections_param) {
            //we don't need to include current one, because it needs to be shown, not hidden
            if($button2_icon_collections_param !== $dep_collection_object->param) {
                $button2_icon_pack_hide_array[$dep_collection_key] .= '#eltd_button2_icon_'.$button2_icon_collections_param.'_container,';
            }

            $button2_icon_pack_hide_array["no_icon"] .= '#eltd_button2_icon_'.$button2_icon_collections_param.'_container,';
        }

        //remove remaining ',' character
        $button2_icon_pack_hide_array[$dep_collection_key] = rtrim($button2_icon_pack_hide_array[$dep_collection_key], ',');
        $button2_icon_pack_hide_array["no_icon"]           = rtrim($button2_icon_pack_hide_array["no_icon"], ',');
    }

}

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_button2_icon_pack',
        'type'          => 'select',
        'label'         => esc_html__('Button 2 Icon Pack', 'malmo'),
        'description'   => esc_html__('Choose icon pack for the first button', 'malmo'),
        'default_value' => 'no_icon',
        'parent'        => $buttons_style_meta_box,
        'options'       => $malmo_IconCollections->getIconCollectionsEmpty("no_icon"),
        'args'          => array(
            "dependence" => true,
            "hide"       => $button2_icon_pack_hide_array,
            "show"       => $button2_icon_pack_show_array
        )
    )
);

//echo var_dump($button2_icon_pack_hide_array); die();

if(is_array($malmo_IconCollections->iconCollections) && count($malmo_IconCollections->iconCollections)) {
    //foreach icon collection we need to generate separate container that will have dependency set
    //it will have one field inside with icons dropdown
    foreach($malmo_IconCollections->iconCollections as $collection_key => $collection_object) {
        $icons_array = $collection_object->getIconsArray();

        //get icon collection keys (keys from collections array, e.g 'font_awesome', 'font_elegant' etc.)
        $icon_collections_keys = $malmo_IconCollections->getIconCollectionsKeys();

        //unset current one, because it doesn't have to be included in dependency that hides icon container
        unset($icon_collections_keys[array_search($collection_key, $icon_collections_keys)]);

        $button2_icon_hide_values   = $icon_collections_keys;
        $button2_icon_hide_values[] = "no_icon";
        $button2_icon_container     = malmo_elated_add_admin_container(array(
            'name'            => "button2_icon_".$collection_object->param."_container",
            'parent'          => $buttons_style_meta_box,
            'hidden_property' => 'eltd_button2_icon_pack',
            'hidden_value'    => '',
            'hidden_values'   => $button2_icon_hide_values
        ));

        malmo_elated_create_meta_box_field(
            array(
                'name'    => "button2_icon_".$collection_object->param,
                'type'    => 'select',
                'label'   => esc_html__('Button 2 Icon', 'malmo'),
                'parent'  => $button2_icon_container,
                'options' => $icons_array
            )
        );
    }

}
/*
malmo_elated_create_meta_box_field(
	array(
		'name'        => 'slider_button2_icon_size',
		'type'        => 'text',
		'label'       => 'Icon Size (px)',
		'description' => 'Define size for icon in button',
		'parent'      => $buttons_style_meta_box,
		'hidden_property' => 'eltd_button2_icon_pack',
		'hidden_values' => array('no_icon')
	)
);
*/


//Slide Content Positioning

$content_positioning_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('slides'),
        'title' => esc_html__('Slide Content Positioning', 'malmo'),
        'name'  => 'eltd_content_positioning_settings'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $content_positioning_meta_box,
        'type'          => 'selectblank',
        'name'          => 'eltd_slide_content_alignment',
        'default_value' => '',
        'label'         => esc_html__('Text Alignment', 'malmo'),
        'description'   => esc_html__('Choose an alignment for the slide text', 'malmo'),
        'options'       => array(
            "left"   => esc_html__('Left', 'malmo'),
            "center" => esc_html__('Center', 'malmo'),
            "right"  => esc_html__('Right', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $content_positioning_meta_box,
        'type'          => 'selectblank',
        'name'          => 'eltd_slide_separate_text_graphic',
        'default_value' => 'no',
        'label'         => esc_html__('Separate Graphic and Text Positioning', 'malmo'),
        'description'   => esc_html__('Do you want to separately position graphic and text?', 'malmo'),
        'options'       => array(
            "no"  => esc_html__('No', 'malmo'),
            "yes" => esc_html__('Yes', 'malmo')
        ),
        'args'          => array(
            "dependence" => true,
            "hide"       => array(
                ""   => "#eltd_eltd_slide_graphic_positioning_container",
                "no" => "#eltd_eltd_slide_graphic_positioning_container, #eltd_eltd_content_vertical_positioning_group_container"
            ),
            "show"       => array(
                "yes" => "#eltd_eltd_slide_graphic_positioning_container, #eltd_eltd_content_vertical_positioning_group_container"
            )
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_content_vertical_middle',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Vertically Align Content to Middle', 'malmo'),
        'parent'        => $content_positioning_meta_box,
        'args'          => array(
            "dependence"             => true,
            "dependence_hide_on_yes" => "#eltd_eltd_slide_content_vertical_middle_no_container",
            "dependence_show_on_yes" => "#eltd_eltd_slide_content_vertical_middle_yes_container"
        )
    )
);

$slide_content_vertical_middle_yes_container = malmo_elated_add_admin_container(array(
    'name'            => 'eltd_slide_content_vertical_middle_yes_container',
    'parent'          => $content_positioning_meta_box,
    'hidden_property' => 'eltd_slide_content_vertical_middle',
    'hidden_value'    => 'no'
));

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $slide_content_vertical_middle_yes_container,
        'type'          => 'selectblank',
        'name'          => 'eltd_slide_content_vertical_middle_type',
        'default_value' => '',
        'label'         => esc_html__('Align Content Vertically Relative to the Height Measured From', 'malmo'),
        'options'       => array(
            "bottom_of_header" => esc_html__('Bottom of Header', 'malmo'),
            "window_top"       => esc_html__('Window Top', 'malmo')
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_vertical_content_full_width',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Content Holder Full Width', 'malmo'),
        'description'   => esc_html__('Do you want to set slide content holder to full width?', 'malmo'),
        'parent'        => $slide_content_vertical_middle_yes_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_vertical_content_width',
        'type'        => 'text',
        'label'       => esc_html__('Content Width', 'malmo'),
        'description' => esc_html__('Enter Width for Content Area', 'malmo'),
        'parent'      => $slide_content_vertical_middle_yes_container,
        'args'        => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

$group_space_around_content = malmo_elated_add_admin_group(array(
    'title'  => esc_html__('Space Around Content in Slide', 'malmo'),
    'name'   => 'group_space_around_content',
    'parent' => $slide_content_vertical_middle_yes_container
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $group_space_around_content
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_vertical_content_left',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Left', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_vertical_content_right',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Right', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

$slide_content_vertical_middle_no_container = malmo_elated_add_admin_container(array(
    'name'            => 'eltd_slide_content_vertical_middle_no_container',
    'parent'          => $content_positioning_meta_box,
    'hidden_property' => 'eltd_slide_content_vertical_middle',
    'hidden_value'    => 'yes'
));

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_slide_content_full_width',
        'type'          => 'yesno',
        'default_value' => 'no',
        'label'         => esc_html__('Content Holder Full Width', 'malmo'),
        'description'   => esc_html__('Do you want to set slide content holder to full width?', 'malmo'),
        'parent'        => $slide_content_vertical_middle_no_container,
        'args'          => array(
            "dependence"             => true,
            "dependence_hide_on_yes" => "#eltd_eltd_slide_content_width_container",
            "dependence_show_on_yes" => ""
        )
    )
);

$slide_content_width_container = malmo_elated_add_admin_container(array(
    'name'            => 'eltd_slide_content_width_container',
    'parent'          => $slide_content_vertical_middle_no_container,
    'hidden_property' => 'eltd_slide_content_full_width',
    'hidden_value'    => 'yes'
));

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_slide_content_width',
        'type'        => 'text',
        'label'       => esc_html__('Content Holder Width', 'malmo'),
        'description' => esc_html__('Enter Width for Content Holder Area', 'malmo'),
        'parent'      => $slide_content_width_container,
        'args'        => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

$group_space_around_content = malmo_elated_add_admin_group(array(
    'title'  => esc_html__('Space Around Content in Slide', 'malmo'),
    'name'   => 'group_space_around_content',
    'parent' => $slide_content_vertical_middle_no_container
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $group_space_around_content
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_content_top',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Top', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_content_left',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Left', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_content_bottom',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Bottom', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_content_right',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Right', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

$row2 = malmo_elated_add_admin_row(array(
    'name'   => 'row2',
    'parent' => $group_space_around_content
));

$content_vertical_positioning_group_container = malmo_elated_add_admin_container_no_style(array(
    'name'            => 'eltd_content_vertical_positioning_group_container',
    'parent'          => $row2,
    'hidden_property' => 'eltd_slide_separate_text_graphic',
    'hidden_value'    => 'no'
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_text_width',
        'type'   => 'textsimple',
        'label'  => esc_html__('Text Holder Width', 'malmo'),
        'parent' => $content_vertical_positioning_group_container,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

$slide_graphic_positioning_container = malmo_elated_add_admin_container(array(
    'name'            => 'eltd_slide_graphic_positioning_container',
    'parent'          => $slide_content_vertical_middle_no_container,
    'hidden_property' => 'eltd_slide_separate_text_graphic',
    'hidden_value'    => 'no'
));

malmo_elated_create_meta_box_field(
    array(
        'parent'        => $slide_graphic_positioning_container,
        'type'          => 'selectblank',
        'name'          => 'eltd_slide_graphic_alignment',
        'default_value' => 'left',
        'label'         => esc_html__('Choose an alignment for the slide graphic', 'malmo'),
        'options'       => array(
            "left"   => esc_html__('Left', 'malmo'),
            "center" => esc_html__('Center', 'malmo'),
            "right"  => esc_html__('Right', 'malmo')
        )
    )
);

$group_graphic_positioning = malmo_elated_add_admin_group(array(
    'title'       => esc_html__('Graphic Positioning', 'malmo'),
    'description' => esc_html__('Positioning for slide graphic', 'malmo'),
    'name'        => 'group_graphic_positioning',
    'parent'      => $slide_graphic_positioning_container
));

$row1 = malmo_elated_add_admin_row(array(
    'name'   => 'row1',
    'parent' => $group_graphic_positioning
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_graphic_top',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Top', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_graphic_left',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Left', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_graphic_bottom',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Bottom', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_graphic_right',
        'type'   => 'textsimple',
        'label'  => esc_html__('From Right', 'malmo'),
        'parent' => $row1,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);

$row2 = malmo_elated_add_admin_row(array(
    'name'   => 'row2',
    'parent' => $group_graphic_positioning
));

malmo_elated_create_meta_box_field(
    array(
        'name'   => 'eltd_slide_graphic_width',
        'type'   => 'textsimple',
        'label'  => esc_html__('Graphic Holder Width', 'malmo'),
        'parent' => $row2,
        'args'   => array(
            'col_width' => 2,
            'suffix'    => '%'
        )
    )
);