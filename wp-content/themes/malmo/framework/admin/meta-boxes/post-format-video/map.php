<?php

/*** Video Post Format ***/

$video_post_format_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('post'),
        'title' => esc_html__('Video Post Format', 'malmo'),
        'name'  => 'post_format_video_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_video_type_meta',
        'type'          => 'select',
        'label'         => esc_html__('Video Type', 'malmo'),
        'description'   => esc_html__('Choose video type', 'malmo'),
        'parent'        => $video_post_format_meta_box,
        'default_value' => 'youtube',
        'options'       => array(
            'youtube' => esc_html__('Youtube', 'malmo'),
            'vimeo'   => esc_html__('Vimeo', 'malmo'),
            'self'    => esc_html__('Self Hosted', 'malmo')
        ),
        'args'          => array(
            'dependence' => true,
            'hide'       => array(
                'youtube' => '#eltd_eltd_video_self_hosted_container',
                'vimeo'   => '#eltd_eltd_video_self_hosted_container',
                'self'    => '#eltd_eltd_video_embedded_container'
            ),
            'show'       => array(
                'youtube' => '#eltd_eltd_video_embedded_container',
                'vimeo'   => '#eltd_eltd_video_embedded_container',
                'self'    => '#eltd_eltd_video_self_hosted_container'
            )
        )
    )
);

$eltd_video_embedded_container = malmo_elated_add_admin_container(
    array(
        'parent'          => $video_post_format_meta_box,
        'name'            => 'eltd_video_embedded_container',
        'hidden_property' => 'eltd_video_type_meta',
        'hidden_value'    => 'self'
    )
);

$eltd_video_self_hosted_container = malmo_elated_add_admin_container(
    array(
        'parent'          => $video_post_format_meta_box,
        'name'            => 'eltd_video_self_hosted_container',
        'hidden_property' => 'eltd_video_type_meta',
        'hidden_values'   => array('youtube', 'vimeo')
    )
);


malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_video_id_meta',
        'type'        => 'text',
        'label'       => esc_html__('Video ID', 'malmo'),
        'description' => esc_html__('Enter Video ID', 'malmo'),
        'parent'      => $eltd_video_embedded_container,

    )
);


malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_video_image_meta',
        'type'        => 'image',
        'label'       => esc_html__('Video Image', 'malmo'),
        'description' => esc_html__('Upload video image', 'malmo'),
        'parent'      => $eltd_video_self_hosted_container,

    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_video_webm_link_meta',
        'type'        => 'text',
        'label'       => esc_html__('Video WEBM', 'malmo'),
        'description' => esc_html__('Enter video URL for WEBM format', 'malmo'),
        'parent'      => $eltd_video_self_hosted_container,

    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_video_mp4_link_meta',
        'type'        => 'text',
        'label'       => esc_html__('Video MP4', 'malmo'),
        'description' => esc_html__('Enter video URL for MP4 format', 'malmo'),
        'parent'      => $eltd_video_self_hosted_container,

    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_post_video_ogv_link_meta',
        'type'        => 'text',
        'label'       => esc_html__('Video OGV', 'malmo'),
        'description' => esc_html__('Enter video URL for OGV format', 'malmo'),
        'parent'      => $eltd_video_self_hosted_container,

    )
);