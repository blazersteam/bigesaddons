<?php

/*** Gallery Post Format ***/

$gallery_post_format_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('post'),
        'title' => esc_html__('Gallery Post Format', 'malmo'),
        'name'  => 'post_format_gallery_meta'
    )
);

malmo_elated_add_multiple_images_field(
    array(
        'name'        => 'eltd_post_gallery_images_meta',
        'label'       => esc_html__('Gallery Images', 'malmo'),
        'description' => esc_html__('Choose your gallery images', 'malmo'),
        'parent'      => $gallery_post_format_meta_box,
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_post_gallery_type_meta',
        'type'          => 'select',
        'label'         => esc_html__('Gallery Type', 'malmo'),
        'parent'        => $gallery_post_format_meta_box,
        'default_value' => 'slider',
        'options'       => array(
            'slider' => esc_html__('Slider', 'malmo'),
            'grid'   => esc_html__('Grid', 'malmo')
        )
    )
);
