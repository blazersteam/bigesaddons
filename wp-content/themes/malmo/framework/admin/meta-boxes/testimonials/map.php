<?php

//Testimonials

$testimonial_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('testimonials'),
        'title' => esc_html__('Testimonial', 'malmo'),
        'name'  => 'testimonial_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_testimonial_title',
        'type'        => 'text',
        'label'       => esc_html__('Title', 'malmo'),
        'description' => esc_html__('Enter testimonial title', 'malmo'),
        'parent'      => $testimonial_meta_box,
    )
);


malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_testimonial_author',
        'type'        => 'text',
        'label'       => esc_html__('Author', 'malmo'),
        'description' => esc_html__('Enter author name', 'malmo'),
        'parent'      => $testimonial_meta_box,
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_testimonial_author_position',
        'type'        => 'text',
        'label'       => esc_html__('Job Position', 'malmo'),
        'description' => esc_html__('Enter job position', 'malmo'),
        'parent'      => $testimonial_meta_box,
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'        => 'eltd_testimonial_text',
        'type'        => 'text',
        'label'       => esc_html__('Text', 'malmo'),
        'description' => esc_html__('Enter testimonial text', 'malmo'),
        'parent'      => $testimonial_meta_box,
    )
);