<?php

$content_bottom_meta_box = malmo_elated_create_meta_box(
    array(
        'scope' => array('page', 'portfolio-item', 'post'),
        'title' => esc_html__('Content Bottom', 'malmo'),
        'name'  => 'content_bottom_meta'
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_enable_content_bottom_area_meta',
        'type'          => 'selectblank',
        'default_value' => '',
        'label'         => esc_html__('Enable Content Bottom Area', 'malmo'),
        'description'   => esc_html__('This option will enable Content Bottom area on pages', 'malmo'),
        'parent'        => $content_bottom_meta_box,
        'options'       => array(
            'no'  => esc_html__('No', 'malmo'),
            'yes' => esc_html__('Yes', 'malmo')
        ),
        'args'          => array(
            'dependence' => true,
            'hide'       => array(
                ''   => '#eltd_eltd_show_content_bottom_meta_container',
                'no' => '#eltd_eltd_show_content_bottom_meta_container'
            ),
            'show'       => array(
                'yes' => '#eltd_eltd_show_content_bottom_meta_container'
            )
        )
    )
);

$show_content_bottom_meta_container = malmo_elated_add_admin_container(
    array(
        'parent'          => $content_bottom_meta_box,
        'name'            => 'eltd_show_content_bottom_meta_container',
        'hidden_property' => 'eltd_enable_content_bottom_area_meta',
        'hidden_value'    => '',
        'hidden_values'   => array('', 'no')
    )
);

malmo_elated_create_meta_box_field(
    array(
        'name'          => 'eltd_content_bottom_sidebar_custom_display_meta',
        'type'          => 'selectblank',
        'default_value' => '',
        'label'         => esc_html__('Sidebar to Display', 'malmo'),
        'description'   => esc_html__('Choose a Content Bottom sidebar to display', 'malmo'),
        'options'       => malmo_elated_get_custom_sidebars(),
        'parent'        => $show_content_bottom_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'type'          => 'selectblank',
        'name'          => 'eltd_content_bottom_in_grid_meta',
        'default_value' => '',
        'label'         => esc_html__('Display in Grid', 'malmo'),
        'description'   => esc_html__('Enabling this option will place Content Bottom in grid', 'malmo'),
        'options'       => array(
            'no'  => esc_html__('No', 'malmo'),
            'yes' => esc_html__('Yes', 'malmo')
        ),
        'parent'        => $show_content_bottom_meta_container
    )
);

malmo_elated_create_meta_box_field(
    array(
        'type'          => 'color',
        'name'          => 'eltd_content_bottom_background_color_meta',
        'default_value' => '',
        'label'         => esc_html__('Background Color', 'malmo'),
        'description'   => esc_html__('Choose a background color for Content Bottom area', 'malmo'),
        'parent'        => $show_content_bottom_meta_container
    )
);