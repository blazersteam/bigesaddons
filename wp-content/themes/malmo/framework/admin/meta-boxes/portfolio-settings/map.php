<?php

if(!function_exists('malmo_elated_map_portfolio_settings')) {
    function malmo_elated_map_portfolio_settings() {
        $meta_box = malmo_elated_create_meta_box(array(
            'scope' => 'portfolio-item',
            'title' => esc_html__('Portfolio Settings', 'malmo'),
            'name'  => 'portfolio_settings_meta_box'
        ));

		malmo_elated_create_meta_box_field(array(
            'name'        => 'eltd_portfolio_single_template_meta',
            'type'        => 'select',
            'label'       => esc_html__('Portfolio Type', 'malmo'),
            'description' => esc_html__('Choose a default type for Single Project pages', 'malmo'),
            'parent'      => $meta_box,
            'options'     => array(
                ''                  => esc_html__('Default', 'malmo'),
                'small-images'      => esc_html__('Portfolio small images', 'malmo'),
                'small-slider'      => esc_html__('Portfolio small slider', 'malmo'),
                'big-images'        => esc_html__('Portfolio big images', 'malmo'),
                'big-slider'        => esc_html__('Portfolio big slider', 'malmo'),
                'custom'            => esc_html__('Portfolio custom', 'malmo'),
                'full-width-custom' => esc_html__('Portfolio full width custom', 'malmo'),
                'gallery'           => esc_html__('Portfolio gallery', 'malmo')
            )
        ));

        malmo_elated_create_meta_box_field(array(
            'name'        => 'eltd_portfolio_prominent_image_meta',
            'type'        => 'select',
            'label'       => esc_html__('Prominent First Image', 'malmo'),
            'description' => esc_html__('Choose whether to prominently display the first image in portfolio', 'malmo'),
            'parent'      => $meta_box,
            'options'     => array(
                '' => esc_html__('Default', 'malmo'),
                'no' => esc_html__('No', 'malmo'),
                'yes' => esc_html__('Yes', 'malmo')
            )
        ));

        malmo_elated_create_meta_box_field(array(
            'name'        => 'eltd_portfolio_wide_view_meta',
            'type'        => 'select',
            'label'       => esc_html__('Use Wide View', 'malmo'),
            'description' => esc_html__('If wide view is chosen, the content will stretch almost to the edges of the screen.', 'malmo'),
            'parent'      => $meta_box,
            'options'     => array(
                '' => esc_html__('Default', 'malmo'),
                'no' => esc_html__('No', 'malmo'),
                'yes' => esc_html__('Yes', 'malmo')
            )
        ));

        malmo_elated_create_meta_box_field(array(
            'name'        => 'eltd_portfolio_info_box_position_meta',
            'type'        => 'select',
            'label'       => esc_html__('Info Box Position', 'malmo'),
            'description' => esc_html__('Choose where to place basic project information', 'malmo'),
            'parent'      => $meta_box,
            'options'     => array(
                '' => esc_html__('Default', 'malmo'),
                'left' => esc_html__('Left', 'malmo'),
                'right' => esc_html__('Right', 'malmo')
            )
        ));

        $all_pages = array();
        $pages     = get_pages();
        foreach($pages as $page) {
            $all_pages[$page->ID] = $page->post_title;
        }

        malmo_elated_create_meta_box_field(array(
            'name'        => 'portfolio_single_back_to_link',
            'type'        => 'select',
            'label'       => esc_html__('"Back To" Link', 'malmo'),
            'description' => esc_html__('Choose "Back To" page to link from portfolio Single Project page', 'malmo'),
            'parent'      => $meta_box,
            'options'     => $all_pages
        ));

		$group_portfolio_external_link = malmo_elated_add_admin_group(array(
			'name'        => 'group_portfolio_external_link',
			'title'       => esc_html__('Portfolio External Link', 'malmo'),
			'description' => esc_html__('Enter URL to link from Portfolio List page', 'malmo'),
			'parent'      => $meta_box
		));

		$row_portfolio_external_link =  malmo_elated_add_admin_row(array(
			'name'   => 'row_gradient_overlay',
			'parent' => $group_portfolio_external_link
		));

		malmo_elated_create_meta_box_field(array(
			'name'        => 'portfolio_external_link',
			'type'        => 'textsimple',
			'label'       => esc_html__('Link', 'malmo'),
			'description' => '',
			'parent'      => $row_portfolio_external_link,
			'args'        => array(
				'col_width' => 3
			)
		));

		malmo_elated_create_meta_box_field(array(
			'name'        => 'portfolio_external_link_target',
			'type'        => 'selectsimple',
			'label'       => esc_html__('Target', 'malmo'),
			'description' => '',
			'parent'      => $row_portfolio_external_link,
			'options'     => array(
				'_self'  => esc_html__('Same Window', 'malmo'),
				'_blank' => esc_html__('New Window', 'malmo')
			)
		));

        malmo_elated_create_meta_box_field(array(
            'name'        => 'portfolio_masonry_dimenisions',
            'type'        => 'select',
            'label'       => esc_html__('Dimensions for Masonry', 'malmo'),
            'description' => esc_html__('Choose image layout when it appears in Masonry type portfolio lists', 'malmo'),
            'parent'      => $meta_box,
            'options'     => array(
                'default'            => esc_html__('Default', 'malmo'),
                'large_width'        => esc_html__('Large width', 'malmo'),
                'large_height'       => esc_html__('Large height', 'malmo'),
                'large_width_height' => esc_html__('Large width/height', 'malmo')
            )
        ));

        malmo_elated_create_meta_box_field(array(
            'name'        => 'portfolio_masonry_parallax_behavior',
            'type'        => 'select',
            'label'       => esc_html__('Behavior in Masonry Parallax', 'malmo'),
            'description' => esc_html__('Choose image behavior in Parallax mode of Masonry type portfolio lists', 'malmo'),
            'parent'      => $meta_box,
            'options'     => array(
                'fixed'            => esc_html__('Fixed', 'malmo'),
                'floating'         => esc_html__('Floating', 'malmo')
            )
        ));


        $meta_box = malmo_elated_create_meta_box(array(
            'scope' => 'portfolio-item',
            'title' => esc_html__('Portfolio Information', 'malmo'),
            'name'  => 'portfolio_information_meta_box'
        ));

        malmo_elated_create_meta_box_field(array(
            'name'        => 'portfolio_author_info',
            'type'        => 'text',
            'label'       => esc_html__('Author(s)', 'malmo'),
            'description' => esc_html__('Enter the name(s) of the author(s) for this portfolio or leave empty.', 'malmo'),
            'parent'      => $meta_box
        ));

        malmo_elated_create_meta_box_field(array(
            'name'        => 'portfolio_client_info',
            'type'        => 'text',
            'label'       => esc_html__('Client(s)', 'malmo'),
            'description' => esc_html__('Enter the name(s) of the client(s) for this portfolio or leave empty.', 'malmo'),
            'parent'      => $meta_box
        ));
    }

    add_action('malmo_elated_meta_boxes_map', 'malmo_elated_map_portfolio_settings');
}