<?php

$eltd_pages = array();
$pages      = get_pages();
foreach($pages as $page) {
    $eltd_pages[$page->ID] = $page->post_title;
}

global $malmo_IconCollections;

//Portfolio Images

$eltdPortfolioImages = new MalmoElatedMetaBox("portfolio-item", esc_html__('Portfolio Images (multiple upload)', 'malmo'), '', '', 'portfolio_images');
$malmo_Framework->eltdMetaBoxes->addMetaBox("portfolio_images", $eltdPortfolioImages);

$eltd_portfolio_image_gallery = new MalmoElatedMultipleImages("eltd_portfolio-image-gallery", esc_html__('Portfolio Images', 'malmo'), esc_html__('Choose your portfolio images', 'malmo'));
$eltdPortfolioImages->addChild("eltd_portfolio-image-gallery", $eltd_portfolio_image_gallery);

//Portfolio Images/Videos 2

$eltdPortfolioImagesVideos2 = new MalmoElatedMetaBox("portfolio-item", esc_html__('Portfolio Images/Videos (single upload)', 'malmo'));
$malmo_Framework->eltdMetaBoxes->addMetaBox("portfolio_images_videos2", $eltdPortfolioImagesVideos2);

$eltd_portfolio_images_videos2 = new MalmoElatedImagesVideosFramework(esc_html__('Portfolio Images/Videos 2', 'malmo'), "ThisIsDescription");
$eltdPortfolioImagesVideos2->addChild("eltd_portfolio_images_videos2", $eltd_portfolio_images_videos2);

//Portfolio Additional Sidebar Items

$eltdAdditionalSidebarItems = new MalmoElatedMetaBox("portfolio-item", esc_html__('Additional Portfolio Sidebar Items', 'malmo'));
$malmo_Framework->eltdMetaBoxes->addMetaBox("portfolio_properties", $eltdAdditionalSidebarItems);

$eltd_portfolio_properties = new MalmoElatedOptionsFramework(esc_html__('Portfolio Properties', 'malmo'), "ThisIsDescription");
$eltdAdditionalSidebarItems->addChild("eltd_portfolio_properties", $eltd_portfolio_properties);

