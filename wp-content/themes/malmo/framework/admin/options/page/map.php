<?php

if(!function_exists('malmo_elated_page_options_map')) {

    function malmo_elated_page_options_map() {

        malmo_elated_add_admin_page(
            array(
                'slug'  => '_page_page',
                'title' => esc_html__('Page', 'malmo'),
                'icon'  => 'fa fa-institution'
            )
        );

        $custom_sidebars = malmo_elated_get_custom_sidebars();

        $panel_sidebar = malmo_elated_add_admin_panel(
            array(
                'page'  => '_page_page',
                'name'  => 'panel_sidebar',
                'title' => esc_html__('Design Style', 'malmo')
            )
        );

        malmo_elated_add_admin_field(array(
            'name'          => 'page_sidebar_layout',
            'type'          => 'select',
            'label'         => esc_html__('Sidebar Layout', 'malmo'),
            'description'   => esc_html__('Choose a sidebar layout for pages', 'malmo'),
            'default_value' => 'default',
            'parent'        => $panel_sidebar,
            'options'       => array(
                'default'          => esc_html__('No Sidebar', 'malmo'),
                'sidebar-33-right' => esc_html__('Sidebar 1/3 Right', 'malmo'),
                'sidebar-25-right' => esc_html__('Sidebar 1/4 Right', 'malmo'),
                'sidebar-33-left'  => esc_html__('Sidebar 1/3 Left', 'malmo'),
                'sidebar-25-left'  => esc_html__('Sidebar 1/4 Left', 'malmo')
            )
        ));


        if(count($custom_sidebars) > 0) {
            malmo_elated_add_admin_field(array(
                'name'        => 'page_custom_sidebar',
                'type'        => 'selectblank',
                'label'       => esc_html__('Sidebar to Display', 'malmo'),
                'description' => esc_html__('Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'malmo'),
                'parent'      => $panel_sidebar,
                'options'     => $custom_sidebars
            ));
        }

        malmo_elated_add_admin_field(array(
            'name'          => 'page_show_comments',
            'type'          => 'yesno',
            'label'         => esc_html__('Show Comments', 'malmo'),
            'description'   => esc_html__('Enabling this option will show comments on your page', 'malmo'),
            'default_value' => 'no',
            'parent'        => $panel_sidebar
        ));

    }

    add_action('malmo_elated_options_map', 'malmo_elated_page_options_map', 5);

}