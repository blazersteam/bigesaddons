<?php

if(!function_exists('malmo_elated_reset_options_map')) {
    /**
     * Reset options panel
     */
    function malmo_elated_reset_options_map() {

        malmo_elated_add_admin_page(
            array(
                'slug'  => '_reset_page',
                'title' => esc_html__('Reset', 'malmo'),
                'icon'  => 'fa fa-retweet'
            )
        );

        $panel_reset = malmo_elated_add_admin_panel(
            array(
                'page'  => '_reset_page',
                'name'  => 'panel_reset',
                'title' => esc_html__('Reset', 'malmo')
            )
        );

        malmo_elated_add_admin_field(array(
            'type'          => 'yesno',
            'name'          => 'reset_to_defaults',
            'default_value' => 'no',
            'label'         => esc_html__('Reset to Defaults', 'malmo'),
            'description'   => esc_html__('This option will reset all Elated Options values to defaults', 'malmo'),
            'parent'        => $panel_reset
        ));

    }

    add_action('malmo_elated_options_map', 'malmo_elated_reset_options_map', 19);

}