<?php

if(!function_exists('malmo_elated_social_options_map')) {

	function malmo_elated_social_options_map() {

		malmo_elated_add_admin_page(
			array(
				'slug'  => '_social_page',
				'title' => esc_html__('Social Networks', 'malmo'),
				'icon'  => 'fa fa-share-alt'
			)
		);

		/**
		 * Enable Social Share
		 */
		$panel_social_share = malmo_elated_add_admin_panel(array(
			'page'  => '_social_page',
			'name'  => 'panel_social_share',
			'title' => esc_html__('Enable Social Share', 'malmo')
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_social_share',
			'default_value' => 'no',
			'label'         => esc_html__('Enable Social Share', 'malmo'),
			'description'   => esc_html__('Enabling this option will allow social share on networks of your choice', 'malmo'),
			'args'          => array(
				'dependence'             => true,
				'dependence_hide_on_yes' => '',
				'dependence_show_on_yes' => '#eltd_panel_social_networks, #eltd_panel_show_social_share_on'
			),
			'parent'        => $panel_social_share
		));

		$panel_show_social_share_on = malmo_elated_add_admin_panel(array(
			'page'            => '_social_page',
			'name'            => 'panel_show_social_share_on',
			'title'           => esc_html__('Show Social Share On', 'malmo'),
			'hidden_property' => 'enable_social_share',
			'hidden_value'    => 'no'
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_social_share_on_post',
			'default_value' => 'no',
			'label'         => esc_html__('Posts', 'malmo'),
			'description'   => esc_html__('Show Social Share on Blog Posts', 'malmo'),
			'parent'        => $panel_show_social_share_on
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_social_share_on_page',
			'default_value' => 'no',
			'label'         => esc_html__('Pages', 'malmo'),
			'description'   => esc_html__('Show Social Share on Pages', 'malmo'),
			'parent'        => $panel_show_social_share_on
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_social_share_on_attachment',
			'default_value' => 'no',
			'label'         => esc_html__('Media', 'malmo'),
			'description'   => esc_html__('Show Social Share for Images and Videos', 'malmo'),
			'parent'        => $panel_show_social_share_on
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_social_share_on_portfolio-item',
			'default_value' => 'no',
			'label'         => esc_html__('Portfolio Item', 'malmo'),
			'description'   => esc_html__('Show Social Share for Portfolio Items', 'malmo'),
			'parent'        => $panel_show_social_share_on
		));

		if(malmo_elated_is_woocommerce_installed()) {
			malmo_elated_add_admin_field(array(
				'type'          => 'yesno',
				'name'          => 'enable_social_share_on_product',
				'default_value' => 'no',
				'label'         => esc_html__('Product', 'malmo'),
				'description'   => esc_html__('Show Social Share for Product Items', 'malmo'),
				'parent'        => $panel_show_social_share_on
			));
		}

		/**
		 * Social Share Networks
		 */
		$panel_social_networks = malmo_elated_add_admin_panel(array(
			'page'            => '_social_page',
			'name'            => 'panel_social_networks',
			'title'           => esc_html__('Social Networks', 'malmo'),
			'hidden_property' => 'enable_social_share',
			'hidden_value'    => 'no'
		));

		/**
		 * Facebook
		 */
		malmo_elated_add_admin_section_title(array(
			'parent' => $panel_social_networks,
			'name'   => 'facebook_title',
			'title'  => esc_html__('Share on Facebook', 'malmo')
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_facebook_share',
			'default_value' => 'no',
			'label'         => esc_html__('Enable Share', 'malmo'),
			'description'   => esc_html__('Enabling this option will allow sharing via Facebook', 'malmo'),
			'args'          => array(
				'dependence'             => true,
				'dependence_hide_on_yes' => '',
				'dependence_show_on_yes' => '#eltd_enable_facebook_share_container'
			),
			'parent'        => $panel_social_networks
		));

		$enable_facebook_share_container = malmo_elated_add_admin_container(array(
			'name'            => 'enable_facebook_share_container',
			'hidden_property' => 'enable_facebook_share',
			'hidden_value'    => 'no',
			'parent'          => $panel_social_networks
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'image',
			'name'          => 'facebook_icon',
			'default_value' => '',
			'label'         => esc_html__('Upload Icon', 'malmo'),
			'parent'        => $enable_facebook_share_container
		));

		/**
		 * Twitter
		 */
		malmo_elated_add_admin_section_title(array(
			'parent' => $panel_social_networks,
			'name'   => 'twitter_title',
			'title'  => esc_html__('Share on Twitter', 'malmo')
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_twitter_share',
			'default_value' => 'no',
			'label'         => esc_html__('Enable Share', 'malmo'),
			'description'   => esc_html__('Enabling this option will allow sharing via Twitter', 'malmo'),
			'args'          => array(
				'dependence'             => true,
				'dependence_hide_on_yes' => '',
				'dependence_show_on_yes' => '#eltd_enable_twitter_share_container'
			),
			'parent'        => $panel_social_networks
		));

		$enable_twitter_share_container = malmo_elated_add_admin_container(array(
			'name'            => 'enable_twitter_share_container',
			'hidden_property' => 'enable_twitter_share',
			'hidden_value'    => 'no',
			'parent'          => $panel_social_networks
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'image',
			'name'          => 'twitter_icon',
			'default_value' => '',
			'label'         => esc_html__('Upload Icon', 'malmo'),
			'parent'        => $enable_twitter_share_container
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'text',
			'name'          => 'twitter_via',
			'default_value' => '',
			'label'         => esc_html__('Via', 'malmo'),
			'parent'        => $enable_twitter_share_container
		));

		/**
		 * Google Plus
		 */
		malmo_elated_add_admin_section_title(array(
			'parent' => $panel_social_networks,
			'name'   => 'google_plus_title',
			'title'  => esc_html__('Share on Google Plus', 'malmo')
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_google_plus_share',
			'default_value' => 'no',
			'label'         => esc_html__('Enable Share', 'malmo'),
			'description'   => esc_html__('Enabling this option will allow sharing via Google Plus', 'malmo'),
			'args'          => array(
				'dependence'             => true,
				'dependence_hide_on_yes' => '',
				'dependence_show_on_yes' => '#eltd_enable_google_plus_container'
			),
			'parent'        => $panel_social_networks
		));

		$enable_google_plus_container = malmo_elated_add_admin_container(array(
			'name'            => 'enable_google_plus_container',
			'hidden_property' => 'enable_google_plus_share',
			'hidden_value'    => 'no',
			'parent'          => $panel_social_networks
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'image',
			'name'          => 'google_plus_icon',
			'default_value' => '',
			'label'         => esc_html__('Upload Icon', 'malmo'),
			'parent'        => $enable_google_plus_container
		));

		/**
		 * Linked In
		 */
		malmo_elated_add_admin_section_title(array(
			'parent' => $panel_social_networks,
			'name'   => 'linkedin_title',
			'title'  => esc_html__('Share on LinkedIn', 'malmo')
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_linkedin_share',
			'default_value' => 'no',
			'label'         => esc_html__('Enable Share', 'malmo'),
			'description'   => esc_html__('Enabling this option will allow sharing via LinkedIn', 'malmo'),
			'args'          => array(
				'dependence'             => true,
				'dependence_hide_on_yes' => '',
				'dependence_show_on_yes' => '#eltd_enable_linkedin_container'
			),
			'parent'        => $panel_social_networks
		));

		$enable_linkedin_container = malmo_elated_add_admin_container(array(
			'name'            => 'enable_linkedin_container',
			'hidden_property' => 'enable_linkedin_share',
			'hidden_value'    => 'no',
			'parent'          => $panel_social_networks
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'image',
			'name'          => 'linkedin_icon',
			'default_value' => '',
			'label'         => esc_html__('Upload Icon', 'malmo'),
			'parent'        => $enable_linkedin_container
		));

		/**
		 * Tumblr
		 */
		malmo_elated_add_admin_section_title(array(
			'parent' => $panel_social_networks,
			'name'   => 'tumblr_title',
			'title'  => esc_html__('Share on Tumblr', 'malmo')
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_tumblr_share',
			'default_value' => 'no',
			'label'         => esc_html__('Enable Share', 'malmo'),
			'description'   => esc_html__('Enabling this option will allow sharing via Tumblr', 'malmo'),
			'args'          => array(
				'dependence'             => true,
				'dependence_hide_on_yes' => '',
				'dependence_show_on_yes' => '#eltd_enable_tumblr_container'
			),
			'parent'        => $panel_social_networks
		));

		$enable_tumblr_container = malmo_elated_add_admin_container(array(
			'name'            => 'enable_tumblr_container',
			'hidden_property' => 'enable_tumblr_share',
			'hidden_value'    => 'no',
			'parent'          => $panel_social_networks
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'image',
			'name'          => 'tumblr_icon',
			'default_value' => '',
			'label'         => esc_html__('Upload Icon', 'malmo'),
			'parent'        => $enable_tumblr_container
		));

		/**
		 * Pinterest
		 */
		malmo_elated_add_admin_section_title(array(
			'parent' => $panel_social_networks,
			'name'   => 'pinterest_title',
			'title'  => esc_html__('Share on Pinterest', 'malmo')
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_pinterest_share',
			'default_value' => 'no',
			'label'         => esc_html__('Enable Share', 'malmo'),
			'description'   => esc_html__('Enabling this option will allow sharing via Pinterest', 'malmo'),
			'args'          => array(
				'dependence'             => true,
				'dependence_hide_on_yes' => '',
				'dependence_show_on_yes' => '#eltd_enable_pinterest_container'
			),
			'parent'        => $panel_social_networks
		));

		$enable_pinterest_container = malmo_elated_add_admin_container(array(
			'name'            => 'enable_pinterest_container',
			'hidden_property' => 'enable_pinterest_share',
			'hidden_value'    => 'no',
			'parent'          => $panel_social_networks
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'image',
			'name'          => 'pinterest_icon',
			'default_value' => '',
			'label'         => esc_html__('Upload Icon', 'malmo'),
			'parent'        => $enable_pinterest_container
		));

		/**
		 * VK
		 */
		malmo_elated_add_admin_section_title(array(
			'parent' => $panel_social_networks,
			'name'   => 'vk_title',
			'title'  => esc_html__('Share on VK', 'malmo')
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'yesno',
			'name'          => 'enable_vk_share',
			'default_value' => 'no',
			'label'         => esc_html__('Enable Share', 'malmo'),
			'description'   => esc_html__('Enabling this option will allow sharing via VK', 'malmo'),
			'args'          => array(
				'dependence'             => true,
				'dependence_hide_on_yes' => '',
				'dependence_show_on_yes' => '#eltd_enable_vk_container'
			),
			'parent'        => $panel_social_networks
		));

		$enable_vk_container = malmo_elated_add_admin_container(array(
			'name'            => 'enable_vk_container',
			'hidden_property' => 'enable_vk_share',
			'hidden_value'    => 'no',
			'parent'          => $panel_social_networks
		));

		malmo_elated_add_admin_field(array(
			'type'          => 'image',
			'name'          => 'vk_icon',
			'default_value' => '',
			'label'         => esc_html__('Upload Icon', 'malmo'),
			'parent'        => $enable_vk_container
		));

		if(defined('ELATED_TWITTER_FEED_VERSION')) {
			$twitter_panel = malmo_elated_add_admin_panel(array(
				'title' => esc_html__('Twitter', 'malmo'),
				'name'  => 'panel_twitter',
				'page'  => '_social_page'
			));

			malmo_elated_add_admin_twitter_button(array(
				'name'   => 'twitter_button',
				'parent' => $twitter_panel
			));
		}

		if(defined('ELATED_INSTAGRAM_FEED_VERSION')) {
			$instagram_panel = malmo_elated_add_admin_panel(array(
				'title' => esc_html__('Instagram', 'malmo'),
				'name'  => 'panel_instagram',
				'page'  => '_social_page'
			));

			malmo_elated_add_admin_instagram_button(array(
				'name'   => 'instagram_button',
				'parent' => $instagram_panel
			));
		}
	}

	add_action('malmo_elated_options_map', 'malmo_elated_social_options_map', 14);
}