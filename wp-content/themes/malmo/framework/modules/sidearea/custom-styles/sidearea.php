<?php

if(!function_exists('malmo_elated_side_area_slide_from_right_type_style')) {

	function malmo_elated_side_area_slide_from_right_type_style() {

		if(malmo_elated_options()->getOptionValue('side_area_type') == 'side-menu-slide-from-right') {

			if(malmo_elated_options()->getOptionValue('side_area_width') !== '') {
				echo malmo_elated_dynamic_css('.eltd-side-menu-slide-from-right .eltd-side-menu', array(
					'right' => '-'.malmo_elated_options()->getOptionValue('side_area_width').'px',
					'width' => malmo_elated_options()->getOptionValue('side_area_width').'px'
				));
			}

			if(malmo_elated_options()->getOptionValue('side_area_content_overlay_color') !== '') {

				echo malmo_elated_dynamic_css('.eltd-side-menu-slide-from-right .eltd-wrapper .eltd-cover', array(
					'background-color' => malmo_elated_options()->getOptionValue('side_area_content_overlay_color')
				));

			}
			if(malmo_elated_options()->getOptionValue('side_area_content_overlay_opacity') !== '') {

				echo malmo_elated_dynamic_css('.eltd-side-menu-slide-from-right.eltd-right-side-menu-opened .eltd-wrapper .eltd-cover', array(
					'opacity' => malmo_elated_options()->getOptionValue('side_area_content_overlay_opacity')
				));

			}
		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_slide_from_right_type_style');

}

if(!function_exists('malmo_elated_side_area_icon_color_styles')) {

	function malmo_elated_side_area_icon_color_styles() {

		if(malmo_elated_options()->getOptionValue('side_area_icon_font_size') !== '') {

			echo malmo_elated_dynamic_css('a.eltd-side-menu-button-opener', array(
				'font-size' => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_icon_font_size')).'px'
			));

			if(malmo_elated_options()->getOptionValue('side_area_icon_font_size') > 30) {
				echo '@media only screen and (max-width: 480px) {
						a.eltd-side-menu-button-opener {
						font-size: 30px;
						}
					}';
			}

		}

		if(malmo_elated_options()->getOptionValue('side_area_icon_color') !== '') {

			echo malmo_elated_dynamic_css('a.eltd-side-menu-button-opener', array(
				'color' => malmo_elated_options()->getOptionValue('side_area_icon_color')
			));

		}
		if(malmo_elated_options()->getOptionValue('side_area_icon_hover_color') !== '') {

			echo malmo_elated_dynamic_css('a.eltd-side-menu-button-opener:hover', array(
				'color' => malmo_elated_options()->getOptionValue('side_area_icon_hover_color')
			));

		}
		if(malmo_elated_options()->getOptionValue('side_area_light_icon_color') !== '') {

			echo malmo_elated_dynamic_css('.eltd-light-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-side-menu-button-opener,
			.eltd-light-header.eltd-header-style-on-scroll .eltd-page-header .eltd-side-menu-button-opener,
			.eltd-light-header .eltd-top-bar .eltd-side-menu-button-opener', array(
				'color' => malmo_elated_options()->getOptionValue('side_area_light_icon_color').' !important'
			));

		}
		if(malmo_elated_options()->getOptionValue('side_area_light_icon_hover_color') !== '') {

			echo malmo_elated_dynamic_css('.eltd-light-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-side-menu-button-opener:hover,
			.eltd-light-header.eltd-header-style-on-scroll .eltd-page-header .eltd-side-menu-button-opener:hover,
			.eltd-light-header .eltd-top-bar .eltd-side-menu-button-opener:hover', array(
				'color' => malmo_elated_options()->getOptionValue('side_area_light_icon_hover_color').' !important'
			));

		}
		if(malmo_elated_options()->getOptionValue('side_area_dark_icon_color') !== '') {

			echo malmo_elated_dynamic_css('.eltd-dark-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-side-menu-button-opener,
			.eltd-dark-header.eltd-header-style-on-scroll .eltd-page-header .eltd-side-menu-button-opener,
			.eltd-dark-header .eltd-top-bar .eltd-side-menu-button-opener', array(
				'color' => malmo_elated_options()->getOptionValue('side_area_dark_icon_color').' !important'
			));

		}
		if(malmo_elated_options()->getOptionValue('side_area_dark_icon_hover_color') !== '') {

			echo malmo_elated_dynamic_css('.eltd-dark-header .eltd-page-header > div:not(.eltd-sticky-header) .eltd-side-menu-button-opener:hover,
			.eltd-dark-header.eltd-header-style-on-scroll .eltd-page-header .eltd-side-menu-button-opener:hover,
			.eltd-dark-header .eltd-top-bar .eltd-side-menu-button-opener:hover', array(
				'color' => malmo_elated_options()->getOptionValue('side_area_dark_icon_hover_color').' !important'
			));

		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_icon_color_styles');

}

if(!function_exists('malmo_elated_side_area_icon_spacing_styles')) {

	function malmo_elated_side_area_icon_spacing_styles() {
		$icon_spacing = array();

		if(malmo_elated_options()->getOptionValue('side_area_icon_padding_left') !== '') {
			$icon_spacing['padding-left'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_icon_padding_left')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_icon_padding_right') !== '') {
			$icon_spacing['padding-right'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_icon_padding_right')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_icon_margin_left') !== '') {
			$icon_spacing['margin-left'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_icon_margin_left')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_icon_margin_right') !== '') {
			$icon_spacing['margin-right'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_icon_margin_right')).'px';
		}

		if(!empty($icon_spacing)) {

			echo malmo_elated_dynamic_css('a.eltd-side-menu-button-opener', $icon_spacing);

		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_icon_spacing_styles');
}

if(!function_exists('malmo_elated_side_area_icon_border_styles')) {

	function malmo_elated_side_area_icon_border_styles() {
		if(malmo_elated_options()->getOptionValue('side_area_icon_border_yesno') == 'yes') {

			$side_area_icon_border = array();

			if(malmo_elated_options()->getOptionValue('side_area_icon_border_color') !== '') {
				$side_area_icon_border['border-color'] = malmo_elated_options()->getOptionValue('side_area_icon_border_color');
			}

			if(malmo_elated_options()->getOptionValue('side_area_icon_border_width') !== '') {
				$side_area_icon_border['border-width'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_icon_border_width')).'px';
			} else {
				$side_area_icon_border['border-width'] = '1px';
			}

			if(malmo_elated_options()->getOptionValue('side_area_icon_border_radius') !== '') {
				$side_area_icon_border['border-radius'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_icon_border_radius')).'px';
			}

			if(malmo_elated_options()->getOptionValue('side_area_icon_border_style') !== '') {
				$side_area_icon_border['border-style'] = malmo_elated_options()->getOptionValue('side_area_icon_border_style');
			} else {
				$side_area_icon_border['border-style'] = 'solid';
			}

			if(!empty($side_area_icon_border)) {
				$side_area_icon_border['-webkit-transition'] = 'all 0.15s ease-out';
				$side_area_icon_border['transition']         = 'all 0.15s ease-out';
				echo malmo_elated_dynamic_css('a.eltd-side-menu-button-opener', $side_area_icon_border);
			}

			if(malmo_elated_options()->getOptionValue('side_area_icon_border_hover_color') !== '') {
				$side_area_icon_border_hover['border-color'] = malmo_elated_options()->getOptionValue('side_area_icon_border_hover_color');
				echo malmo_elated_dynamic_css('a.eltd-side-menu-button-opener:hover', $side_area_icon_border_hover);
			}
		}
	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_icon_border_styles');

}

if(!function_exists('malmo_elated_side_area_alignment')) {

	function malmo_elated_side_area_alignment() {

		if(malmo_elated_options()->getOptionValue('side_area_aligment')) {

			echo malmo_elated_dynamic_css('.eltd-side-menu-slide-from-right .eltd-side-menu, .eltd-side-menu-slide-with-content .eltd-side-menu, .eltd-side-area-uncovered-from-content .eltd-side-menu', array(
				'text-align' => malmo_elated_options()->getOptionValue('side_area_aligment')
			));

		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_alignment');

}

if(!function_exists('malmo_elated_side_area_styles')) {

	function malmo_elated_side_area_styles() {

		$side_area_styles = array();

		if(malmo_elated_options()->getOptionValue('side_area_background_color') !== '') {
			$side_area_styles['background-color'] = malmo_elated_options()->getOptionValue('side_area_background_color');
		}

		if(malmo_elated_options()->getOptionValue('side_area_padding_top') !== '') {
			$side_area_styles['padding-top'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_padding_top')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_padding_right') !== '') {
			$side_area_styles['padding-right'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_padding_right')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_padding_bottom') !== '') {
			$side_area_styles['padding-bottom'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_padding_bottom')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_padding_left') !== '') {
			$side_area_styles['padding-left'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_padding_left')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_bakground_image') !== '') {
			$side_area_styles['background-image'] = 'url('.malmo_elated_options()->getOptionValue('side_area_bakground_image').')';
			$side_area_styles['background-size']  = 'cover';
			$side_area_styles['background-position']  = 'center center';
		}

		if(!empty($side_area_styles)) {
			echo malmo_elated_dynamic_css('.eltd-side-menu', $side_area_styles);
		}

		if(malmo_elated_options()->getOptionValue('side_area_close_icon') == 'dark') {
			echo malmo_elated_dynamic_css('.eltd-side-menu a.eltd-close-side-menu span, .eltd-side-menu a.eltd-close-side-menu i', array(
				'color' => '#000000'
			));
		}

		if(malmo_elated_options()->getOptionValue('side_area_close_icon_size') !== '') {
			echo malmo_elated_dynamic_css('.eltd-side-menu a.eltd-close-side-menu', array(
				'height'      => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_close_icon_size')).'px',
				'width'       => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_close_icon_size')).'px',
				'line-height' => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_close_icon_size')).'px',
				'padding'     => 0,
			));
			echo malmo_elated_dynamic_css('.eltd-side-menu a.eltd-close-side-menu span, .eltd-side-menu a.eltd-close-side-menu i', array(
				'font-size'   => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_close_icon_size')).'px',
				'height'      => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_close_icon_size')).'px',
				'width'       => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_close_icon_size')).'px',
				'line-height' => malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_close_icon_size')).'px',
			));
		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_styles');

}

if(!function_exists('malmo_elated_side_area_title_styles')) {

	function malmo_elated_side_area_title_styles() {

		$title_styles = array();

		if(malmo_elated_options()->getOptionValue('side_area_title_color') !== '') {
			$title_styles['color'] = malmo_elated_options()->getOptionValue('side_area_title_color');
		}

		if(malmo_elated_options()->getOptionValue('side_area_title_fontsize') !== '') {
			$title_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_title_fontsize')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_title_lineheight') !== '') {
			$title_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_title_lineheight')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_title_texttransform') !== '') {
			$title_styles['text-transform'] = malmo_elated_options()->getOptionValue('side_area_title_texttransform');
		}

		if(malmo_elated_options()->getOptionValue('side_area_title_google_fonts') !== '-1') {
			$title_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('side_area_title_google_fonts')).', sans-serif';
		}

		if(malmo_elated_options()->getOptionValue('side_area_title_fontstyle') !== '') {
			$title_styles['font-style'] = malmo_elated_options()->getOptionValue('side_area_title_fontstyle');
		}

		if(malmo_elated_options()->getOptionValue('side_area_title_fontweight') !== '') {
			$title_styles['font-weight'] = malmo_elated_options()->getOptionValue('side_area_title_fontweight');
		}

		if(malmo_elated_options()->getOptionValue('side_area_title_letterspacing') !== '') {
			$title_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_title_letterspacing')).'px';
		}

		if(!empty($title_styles)) {

			echo malmo_elated_dynamic_css('.eltd-side-menu-title h4, .eltd-side-menu-title h5', $title_styles);

		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_title_styles');

}

if(!function_exists('malmo_elated_side_area_text_styles')) {

	function malmo_elated_side_area_text_styles() {
		$text_styles = array();

		if(malmo_elated_options()->getOptionValue('side_area_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('side_area_text_google_fonts')).', sans-serif';
		}

		if(malmo_elated_options()->getOptionValue('side_area_text_fontsize') !== '') {
			$text_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_text_fontsize')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_text_lineheight') !== '') {
			$text_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_text_lineheight')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('side_area_text_letterspacing')).'px';
		}

		if(malmo_elated_options()->getOptionValue('side_area_text_fontweight') !== '') {
			$text_styles['font-weight'] = malmo_elated_options()->getOptionValue('side_area_text_fontweight');
		}

		if(malmo_elated_options()->getOptionValue('side_area_text_fontstyle') !== '') {
			$text_styles['font-style'] = malmo_elated_options()->getOptionValue('side_area_text_fontstyle');
		}

		if(malmo_elated_options()->getOptionValue('side_area_text_texttransform') !== '') {
			$text_styles['text-transform'] = malmo_elated_options()->getOptionValue('side_area_text_texttransform');
		}

		if(malmo_elated_options()->getOptionValue('side_area_text_color') !== '') {
			$text_styles['color'] = malmo_elated_options()->getOptionValue('side_area_text_color');
		}

		if(!empty($text_styles)) {

			echo malmo_elated_dynamic_css('.eltd-side-menu .widget, .eltd-side-menu .widget.widget_search form, .eltd-side-menu .widget.widget_search form input[type="text"], .eltd-side-menu .widget.widget_search form input[type="submit"], .eltd-side-menu .widget h6, .eltd-side-menu .widget h6 a, .eltd-side-menu .widget p, .eltd-side-menu .widget li a, .eltd-side-menu .widget.widget_rss li a.rsswidget, .eltd-side-menu #wp-calendar caption,.eltd-side-menu .widget li, .eltd-side-menu h3, .eltd-side-menu .widget.widget_archive select, .eltd-side-menu .widget.widget_categories select, .eltd-side-menu .widget.widget_text select, .eltd-side-menu .widget.widget_search form input[type="submit"], .eltd-side-menu #wp-calendar th, .eltd-side-menu #wp-calendar td, .eltd-side-menu .eltd_social_icon_holder i.simple_social', $text_styles);

		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_text_styles');

}

if(!function_exists('malmo_elated_side_area_link_styles')) {

	function malmo_elated_side_area_link_styles() {
		$link_styles = array();

		if(malmo_elated_options()->getOptionValue('sidearea_link_font_family') !== '-1') {
			$link_styles['font-family'] = malmo_elated_get_formatted_font_family(malmo_elated_options()->getOptionValue('sidearea_link_font_family')).',sans-serif';
		}

		if(malmo_elated_options()->getOptionValue('sidearea_link_font_size') !== '') {
			$link_styles['font-size'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('sidearea_link_font_size')).'px';
		}

		if(malmo_elated_options()->getOptionValue('sidearea_link_line_height') !== '') {
			$link_styles['line-height'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('sidearea_link_line_height')).'px';
		}

		if(malmo_elated_options()->getOptionValue('sidearea_link_letter_spacing') !== '') {
			$link_styles['letter-spacing'] = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('sidearea_link_letter_spacing')).'px';
		}

		if(malmo_elated_options()->getOptionValue('sidearea_link_font_weight') !== '') {
			$link_styles['font-weight'] = malmo_elated_options()->getOptionValue('sidearea_link_font_weight');
		}

		if(malmo_elated_options()->getOptionValue('sidearea_link_font_style') !== '') {
			$link_styles['font-style'] = malmo_elated_options()->getOptionValue('sidearea_link_font_style');
		}

		if(malmo_elated_options()->getOptionValue('sidearea_link_text_transform') !== '') {
			$link_styles['text-transform'] = malmo_elated_options()->getOptionValue('sidearea_link_text_transform');
		}

		if(malmo_elated_options()->getOptionValue('sidearea_link_color') !== '') {
			$link_styles['color'] = malmo_elated_options()->getOptionValue('sidearea_link_color');
		}

		if(!empty($link_styles)) {

			echo malmo_elated_dynamic_css('.eltd-side-menu .widget li a, .eltd-side-menu .widget a:not(.qbutton)', $link_styles);

		}

		if(malmo_elated_options()->getOptionValue('sidearea_link_hover_color') !== '') {
			echo malmo_elated_dynamic_css('.eltd-side-menu .widget a:hover, .eltd-side-menu .widget li:hover, .eltd-side-menu .widget li:hover>a', array(
				'color' => malmo_elated_options()->getOptionValue('sidearea_link_hover_color')
			));
		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_link_styles');

}

if(!function_exists('malmo_elated_side_area_border_styles')) {

	function malmo_elated_side_area_border_styles() {

		if(malmo_elated_options()->getOptionValue('side_area_enable_bottom_border') == 'yes') {

			if(malmo_elated_options()->getOptionValue('side_area_bottom_border_color') !== '') {

				echo malmo_elated_dynamic_css('.eltd-side-menu .widget', array(
					'border-bottom:'  => '1px solid '.malmo_elated_options()->getOptionValue('side_area_bottom_border_color'),
					'margin-bottom:'  => '10px',
					'padding-bottom:' => '10px',
				));

			}

		}

	}

	add_action('malmo_elated_style_dynamic', 'malmo_elated_side_area_border_styles');

}