<?php

if(!function_exists('malmo_elated_portfolio_options_map')) {

	function malmo_elated_portfolio_options_map() {

		malmo_elated_add_admin_page(array(
			'slug'  => '_portfolio',
			'title' => esc_html__('Portfolio', 'malmo'),
			'icon'  => 'fa fa-camera-retro'
		));

		$panel = malmo_elated_add_admin_panel(array(
			'title' => esc_html__('Portfolio Single', 'malmo'),
			'name'  => 'panel_portfolio_single',
			'page'  => '_portfolio'
		));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_template',
			'type'          => 'select',
			'label'         => esc_html__('Portfolio Type', 'malmo'),
			'default_value' => 'small-images',
			'description'   => esc_html__('Choose a default type for Single Project pages', 'malmo'),
			'parent'        => $panel,
			'options'       => array(
				'small-images'      => esc_html__('Portfolio small images', 'malmo'),
				'small-slider'      => esc_html__('Portfolio small slider', 'malmo'),
				'big-images'        => esc_html__('Portfolio big images', 'malmo'),
				'big-slider'        => esc_html__('Portfolio big slider', 'malmo'),
				'custom'            => esc_html__('Portfolio custom', 'malmo'),
				'full-width-custom' => esc_html__('Portfolio full width custom', 'malmo'),
				'gallery'           => esc_html__('Portfolio gallery', 'malmo'),
			)
		));

        malmo_elated_add_admin_field(array(
            'name'          => 'portfolio_prominent_image',
            'type'          => 'select',
            'label'         => esc_html__('Prominent First Image', 'malmo'),
            'default_value' => 'no',
            'description'   => esc_html__('Choose whether to prominently display the first image in portfolio', 'malmo'),
            'parent'        => $panel,
            'options'       => array(
                'no'      => esc_html__('No', 'malmo'),
                'yes'      => esc_html__('Yes', 'malmo'),
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'portfolio_wide_view',
            'type'          => 'select',
            'label'         => esc_html__('Use Wide View', 'malmo'),
            'default_value' => 'no',
            'description'   => esc_html__('If wide view is chosen, the content will stretch almost to the edges of the screen.', 'malmo'),
            'parent'        => $panel,
            'options'       => array(
                'no'      => esc_html__('No', 'malmo'),
                'yes'      => esc_html__('Yes', 'malmo'),
            )
        ));

        malmo_elated_add_admin_field(array(
            'name'          => 'portfolio_info_box_position',
            'type'          => 'select',
            'label'         => esc_html__('Info Box Position', 'malmo'),
            'default_value' => 'left',
            'description'   => esc_html__('Choose where to place basic project information', 'malmo'),
            'parent'        => $panel,
            'options'       => array(
                'left'      => esc_html__('Left', 'malmo'),
                'right'      => esc_html__('Right', 'malmo'),
            )
        ));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_images',
			'type'          => 'yesno',
			'label'         => esc_html__('Lightbox for Images', 'malmo'),
			'description'   => esc_html__('Enabling this option will turn on lightbox functionality for projects with images.', 'malmo'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_lightbox_videos',
			'type'          => 'yesno',
			'label'         => esc_html__('Lightbox for Videos', 'malmo'),
			'description'   => esc_html__('Enabling this option will turn on lightbox functionality for YouTube/Vimeo projects.', 'malmo'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_hide_categories',
			'type'          => 'yesno',
			'label'         => esc_html__('Hide Categories', 'malmo'),
			'description'   => esc_html__('Enabling this option will disable category meta description on Single Projects.', 'malmo'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_hide_date',
			'type'          => 'yesno',
			'label'         => esc_html__('Hide Date', 'malmo'),
			'description'   => esc_html__('Enabling this option will disable date meta on Single Projects.', 'malmo'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_comments',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Comments', 'malmo'),
			'description'   => esc_html__('Enabling this option will show comments on your page.', 'malmo'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_sticky_sidebar',
			'type'          => 'yesno',
			'label'         => esc_html__('Sticky Side Text', 'malmo'),
			'description'   => esc_html__('Enabling this option will make side text sticky on Single Project pages', 'malmo'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_hide_pagination',
			'type'          => 'yesno',
			'label'         => esc_html__('Hide Pagination', 'malmo'),
			'description'   => esc_html__('Enabling this option will turn off portfolio pagination functionality.', 'malmo'),
			'parent'        => $panel,
			'default_value' => 'no',
			'args'          => array(
				'dependence'             => true,
				'dependence_hide_on_yes' => '#eltd_navigate_same_category_container'
			)
		));

		$container_navigate_category = malmo_elated_add_admin_container(array(
			'name'            => 'navigate_same_category_container',
			'parent'          => $panel,
			'hidden_property' => 'portfolio_single_hide_pagination',
			'hidden_value'    => 'yes'
		));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_nav_same_category',
			'type'          => 'yesno',
			'label'         => esc_html__('Enable Pagination Through Same Category', 'malmo'),
			'description'   => esc_html__('Enabling this option will make portfolio pagination sort through current category.', 'malmo'),
			'parent'        => $container_navigate_category,
			'default_value' => 'no'
		));

		malmo_elated_add_admin_field(array(
			'name'          => 'portfolio_single_numb_columns',
			'type'          => 'select',
			'label'         => esc_html__('Number of Columns', 'malmo'),
			'default_value' => 'three-columns',
			'description'   => esc_html__('Enter the number of columns for Portfolio Gallery type', 'malmo'),
			'parent'        => $panel,
			'options'       => array(
				'two-columns'   => esc_html__('2 columns', 'malmo'),
				'three-columns' => esc_html__('3 columns', 'malmo'),
				'four-columns'  => esc_html__('4 columns', 'malmo'),
			)
		));

		malmo_elated_add_admin_field(array(
			'name'        => 'portfolio_single_slug',
			'type'        => 'text',
			'label'       => esc_html__('Portfolio Single Slug', 'malmo'),
			'description' => esc_html__('Enter if you wish to use a different Single Project slug (Note: After entering slug, navigate to Settings -> Permalinks and click "Save" in order for changes to take effect)', 'malmo'),
			'parent'      => $panel,
			'args'        => array(
				'col_width' => 3
			)
		));

	}

	add_action('malmo_elated_options_map', 'malmo_elated_portfolio_options_map', 13);

}