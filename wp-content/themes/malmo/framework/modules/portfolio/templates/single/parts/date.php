<?php if(malmo_elated_options()->getOptionValue('portfolio_single_hide_date') !== 'yes') : ?>

    <div class="eltd-portfolio-date eltd-portfolio-info-item">
        <h3 class="eltd-portfolio-info-label"><?php esc_html_e('Date', 'malmo'); ?></h3>

        <h5 class="eltd-portfolio-info-value"><?php the_time(get_option('date_format')); ?></h5>
    </div>

<?php endif; ?>