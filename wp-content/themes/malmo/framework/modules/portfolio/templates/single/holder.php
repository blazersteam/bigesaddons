<?php if($fullwidth) : ?>
<div class="eltd-full-width">
	<div class="eltd-full-width-inner">
		<?php else: ?>
		<div class="eltd-container">
			<div class="eltd-container-inner clearfix">
				<?php endif; ?>
				<div <?php malmo_elated_class_attribute($holder_class); ?>>
					<?php if(post_password_required()) {
						echo get_the_password_form();
					} else {
						//load proper portfolio template
						malmo_elated_get_module_template_part('templates/single/single', 'portfolio', $portfolio_template, $params);

						//load portfolio navigation
						malmo_elated_portfolio_get_single_navigation();

						//load portfolio comments
						malmo_elated_get_module_template_part('templates/single/parts/comments', 'portfolio');

					} ?>
				</div>
			</div>
		</div>