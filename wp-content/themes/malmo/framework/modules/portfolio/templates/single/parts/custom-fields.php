<?php
$custom_fields = get_post_meta(get_the_ID(), 'eltd_portfolios', true);

if(is_array($custom_fields) && count($custom_fields)) :
    usort($custom_fields, 'malmo_elated_compare_portfolio_options');

    foreach($custom_fields as $custom_field) : ?>
        <div class="eltd-portfolio-info-item eltd-portfolio-custom-field">
            <?php if(!empty($custom_field['optionLabel'])) : ?>
                <h3 class="eltd-portfolio-info-label"><?php echo esc_html($custom_field['optionLabel']); ?></h3>
            <?php endif; ?>
            <h5 class="eltd-portfolio-info-value">
                <?php if(!empty($custom_field['optionUrl'])) : ?>
                <a href="<?php echo esc_url($custom_field['optionUrl']); ?>">
                    <?php endif; ?>
                    <?php echo esc_html($custom_field['optionValue']); ?>
                    <?php if(!empty($custom_field['optionUrl'])) : ?>
                </a>
            <?php endif; ?>
            </h5>
        </div>
    <?php endforeach; ?>

<?php endif; ?>
