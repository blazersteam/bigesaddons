<?php

$portfolio_author = get_post_meta(get_the_ID(), 'portfolio_author_info', true);
if($portfolio_author != '') :
    ?>
    <div class="eltd-portfolio-author-info">
        <h5><?php echo esc_html($portfolio_author); ?></h5>
    </div>
<?php endif; ?>