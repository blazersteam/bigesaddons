<?php if(malmo_elated_options()->getOptionValue('portfolio_single_hide_pagination') !== 'yes') : ?>

    <?php
    $back_to_link      = get_post_meta(get_the_ID(), 'portfolio_single_back_to_link', true);
    $nav_same_category = malmo_elated_options()->getOptionValue('portfolio_single_nav_same_category') == 'yes';
    ?>
    <div class="eltd-portfolio-single-navigation clearfix">
        <div class="eltd-portfolio-single-navigation-inner clearfix">
            <?php if($has_prev_post) : ?>
                <div class="eltd-portfolio-single-prev clearfix">
                    <?php if($prev_post_has_image) : ?>
                        <div class="eltd-single-nav-image-holder">
                            <a href="<?php echo esc_url($prev_post['link']); ?>">
                            </a>
                        </div>
                    <?php endif; ?>

                    <div class="eltd-single-nav-content-holder clearfix">
                        <div class="eltd-single-nav-text">
                            <h3 class="eltd-single-nav-post-title">
                                <a href="<?php echo esc_url($prev_post['link']); ?>">
                                    <?php echo esc_html($prev_post['title']); ?>
                                </a>
                            </h3>
                            <a href="<?php echo esc_url($prev_post['link']) ?>">
                                <span class="eltd-single-nav-post-sub"><?php esc_html_e('Previous post', 'malmo') ?></span>
                            </a>
                            <div class="eltd-single-nav-arrow">
                                <a href="<?php echo esc_url($prev_post['link']) ?>"><?php echo malmo_elated_icon_collections()->renderIcon('lnr-chevron-left', 'linear_icons') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if($has_next_post) : ?>
                <div class="eltd-portfolio-single-next clearfix">
                    <?php if($next_post_has_image) : ?>
                        <div class="eltd-single-nav-image-holder clearfix">
                            <a href="<?php echo esc_url($next_post['link']); ?>">
                            </a>
                        </div>
                    <?php endif; ?>

                    <div class="eltd-single-nav-content-holder clearfix">
                        <div class="eltd-single-nav-text">
                            <h3 class="eltd-single-nav-post-title">
                                <a href="<?php echo esc_url($next_post['link']); ?>">
                                    <?php echo esc_html($next_post['title']); ?>
                                </a>
                            </h3>
                            <a href="<?php echo esc_url($next_post['link']) ?>">
                                <span class="eltd-single-nav-post-sub"><?php esc_html_e('Next post', 'malmo') ?></span>
                            </a>
                            <div class="eltd-single-nav-arrow">
                                <a href="<?php echo esc_url($next_post['link']) ?>"><?php echo malmo_elated_icon_collections()->renderIcon('lnr-chevron-right', 'linear_icons') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if($back_to_link !== '') : ?>
                <div class="eltd-portfolio-back-btn">
                    <a href="<?php echo esc_url(get_permalink($back_to_link)); ?>">
                        <span class="eltd-portfolio-back-btn-square"></span>
                        <span class="eltd-portfolio-back-btn-square"></span>
                        <span class="eltd-portfolio-back-btn-square"></span>
                        <span class="eltd-portfolio-back-btn-square"></span>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>