<div class="eltd-portfolio-info-item eltd-portfolio-content-holder">
    <h2 class="eltd-portfolio-item-title"><?php the_title(); ?></h2>

    <?php malmo_elated_portfolio_get_info_part('portfolio-author'); ?>

    <div class="eltd-portfolio-item-content">
        <?php the_content(); ?>
    </div>

    <?php
    $link = get_post_meta(get_the_ID(), 'portfolio_external_link', true);
    if($link != '') :
        ?>
        <div class="eltd-portfolio-item-button">
            <?php
            echo malmo_elated_get_button_html(array(
                'text' => esc_html__('Visit project', 'malmo'),
                'link' => esc_url($link),
                'type' => 'black'
            ));
            ?>
        </div>
        <?php
    endif;
    ?>
</div>