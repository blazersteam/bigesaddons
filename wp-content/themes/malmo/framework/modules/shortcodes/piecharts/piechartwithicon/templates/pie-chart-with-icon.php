<div class="eltd-pie-chart-with-icon-holder" <?php echo malmo_elated_get_inline_attrs($data_attr); ?>>
	<div class="eltd-percentage-with-icon-holder">
		<div class="eltd-percentage-with-icon" <?php echo malmo_elated_get_inline_attrs($pie_chart_data); ?>>
			<?php echo malmo_elated_get_module_part( $icon ); ?>
		</div>
	</div>
	<div class="eltd-pie-chart-text" <?php malmo_elated_inline_style($pie_chart_style)?>>
		<<?php echo esc_html($title_tag)?> class="eltd-pie-title">
			<?php echo esc_html($title); ?>
		</<?php echo esc_html($title_tag)?>>
		<p><?php echo esc_html($text); ?></p>
	</div>
</div>