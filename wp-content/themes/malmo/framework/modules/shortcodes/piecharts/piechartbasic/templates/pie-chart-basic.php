<?php
/**
 * Pie Chart Basic Shortcode Template
 */
?>
<div <?php eltd_core_class_attribute($holder_classes); ?> <?php echo malmo_elated_get_inline_attrs($data_attr); ?>>

	<div class="eltd-percentage-holder">
		<div class="eltd-percentage" <?php echo malmo_elated_get_inline_attrs($pie_chart_data); ?>>
			<?php if ($type_of_central_text == "title") { ?>
				<<?php echo esc_attr($title_tag); ?> class="eltd-pie-title">
					<?php echo esc_html($title); ?>
				</<?php echo esc_attr($title_tag); ?>>
			<?php } else { ?>
				<span class="eltd-to-counter" <?php if (isset($perc_color) && $perc_color != '') echo 'style="color:'.esc_attr($perc_color).';"'; ?>>
					<?php echo esc_html($percent); ?>
				</span>
			<?php } ?>
		</div>
	</div>
	<div class="eltd-pie-chart-text" <?php malmo_elated_inline_style($pie_chart_style); ?>>
		<?php if ($type_of_central_text == "title") { ?>
			<span class="eltd-to-counter" <?php if (isset($perc_color) && $perc_color != '') echo 'style="color:'.esc_attr($perc_color).';"'; ?>>
				<?php echo esc_html($percent); ?>
			</span>
		<?php } else { ?>
			<<?php echo esc_attr($title_tag); ?> class="eltd-pie-title">
				<?php echo esc_html($title); ?>
			</<?php echo esc_attr($title_tag); ?>>
		<?php } ?>
		<<?php echo esc_attr($text_tag); ?>><?php echo esc_html($text); ?></<?php echo esc_attr($text_tag); ?>>
	</div>
</div>