<div class="eltd-vss-ms-section" <?php malmo_elated_inline_style($content_style);?> <?php echo malmo_elated_get_inline_attrs($content_data); ?>>
	<?php echo do_shortcode($content); ?>
</div>