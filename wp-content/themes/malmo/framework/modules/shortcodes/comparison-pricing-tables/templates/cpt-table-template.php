<div class="eltd-comparision-table-holder eltd-cpt-table">
	<div class="eltd-cpt-table-holder-inner">
		<?php if($display_border) : ?>
			<div class="eltd-cpt-table-border-top" <?php malmo_elated_inline_style($border_style); ?>></div>
		<?php endif; ?>

		<div class="eltd-cpt-table-head-holder">
			<div class="eltd-cpt-table-head-holder-inner">
				<?php if($title !== '') : ?>
					<h4 class="eltd-cpt-table-title"><?php echo esc_html($title); ?></h4>
				<?php endif; ?>

				<?php if($price !== '') : ?>
					<div class="eltd-cpt-table-price-holder">
						<?php if($currency !== '') : ?>
						<span class="eltd-cpt-table-currency"><?php echo esc_html($currency); ?></span><!--
						<?php else: ?>
							<!--
						<?php endif; ?>

						 --><span class="eltd-cpt-table-price"><?php echo esc_html($price); ?></span>

						<?php if($price_period !== '') : ?>
							<span class="eltd-cpt-table-period">
								/ <?php echo esc_html($price_period); ?>
							</span>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>

		<div class="eltd-cpt-table-content">
			<?php echo do_shortcode(preg_replace('#^<\/p>|<p>$#', '', $content)); ?>
		</div>

		<div class="eltd-cpt-table-footer">
			<div class="eltd-cpt-table-btn">
				<a <?php malmo_elated_inline_style($btn_styles); ?> href="<?php echo esc_url($link); ?>">
					<span><?php echo esc_html($button_text); ?></span>
				</a>
			</div>
		</div>
	</div>
</div>