<div class="eltd-progress-bar">
	<h6 class="eltd-progress-title-holder clearfix">
		<span class="eltd-progress-title" <?php malmo_elated_inline_style($title_color); ?>><?php echo esc_attr($title) ?></span>
		<span class="eltd-progress-number-wrapper <?php echo esc_attr($percentage_classes) ?> ">
			<span class="eltd-progress-number">
				<span class="eltd-percent" <?php malmo_elated_inline_style($percentage_color); ?>>0</span>
			</span>
		</span>
	</h6>

	<div class="eltd-progress-content-outer" <?php malmo_elated_inline_style($inactive_bar_style); ?>>
		<div data-percentage=<?php echo esc_attr($percent) ?> class="eltd-progress-content" <?php malmo_elated_inline_style($bar_styles); ?>></div>
	</div>
</div>	