<div <?php malmo_elated_class_attribute($holder_classes); ?>>
	<div class="eltd-process-inner">
		<?php echo do_shortcode($content); ?>
	</div>
</div>