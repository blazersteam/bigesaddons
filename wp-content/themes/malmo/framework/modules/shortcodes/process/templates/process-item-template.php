<div <?php malmo_elated_class_attribute($item_classes); ?>>
	<div class="eltd-pi-holder-inner">
		<?php if(!empty($image) || isset($icon) && $icon) : ?>
			<div class="eltd-pi-image-wrapper <?php if ($icon_only == 'yes') echo 'eltd-pi-icon-only'; ?>">
				<?php if ($icon_only == 'no' && !empty($image)) : ?>
				<div class="eltd-pi-image-holder">
				<?php echo wp_get_attachment_image($image, 'full'); ?>
				</div>
				<?php endif; ?>
				<?php if(isset($icon) && $icon) : ?>
				<div class="eltd-pi-icon-holder <?php if ($icon_only == 'yes') echo 'eltd-pi-icon-only'; ?>"><div class="eltd-pi-icon"><?php echo malmo_elated_icon_collections()->renderIcon($icon, $icon_pack); ?></div></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<div class="eltd-pi-content-holder">
			<?php if(!empty($title)) : ?>
				<div class="eltd-pi-title-holder">
					<h5 class="eltd-pi-title"><?php echo esc_html($title); ?></h5>
				</div>
			<?php endif; ?>

			<?php if(!empty($text)) : ?>
				<div class="eltd-pi-text-holder">
					<p><?php echo esc_html($text); ?></p>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>