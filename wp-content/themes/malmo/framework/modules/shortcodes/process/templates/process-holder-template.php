<div <?php malmo_elated_class_attribute($holder_classes); ?>>
	<div class="eltd-process-bg-holder"><img alt="<?php esc_attr_e( 'process-bg-holder', 'malmo' ); ?>" src="<?php echo ELATED_ASSETS_ROOT?>/css/img/process_bgnd.png"></div>
	<div class="eltd-process-inner">
		<?php echo do_shortcode($content); ?>
	</div>
</div>