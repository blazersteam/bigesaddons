<?php
namespace Malmo\Modules\Shortcodes\Process;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

class ProcessItem implements ShortcodeInterface {
    private $base;

    public function __construct() {
        $this->base = 'eltd_process_item';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name'                    => esc_html__('Process Item', 'malmo'),
            'base'                    => $this->getBase(),
            'as_child'                => array('only' => 'eltd_process_holder'),
            'category'                => esc_html__('by ELATED', 'malmo'),
            'icon'                    => 'icon-wpb-process-item extended-custom-icon',
            'show_settings_on_create' => true,
            'params'                  => array_merge(
                array(
                    array(
                        'type'       => 'attach_image',
                        'heading'    => esc_html__('Image', 'malmo'),
                        'param_name' => 'image'
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Title', 'malmo'),
                        'param_name'  => 'title',
                        'save_always' => true,
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'textarea',
                        'heading'     => esc_html__('Text', 'malmo'),
                        'param_name'  => 'text',
                        'save_always' => true,
                        'admin_label' => true
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Highlight Item?', 'malmo'),
                        'param_name'  => 'highlighted',
                        'value'       => array(
                            esc_html__('No', 'malmo')  => 'no',
                            esc_html__('Yes', 'malmo') => 'yes'
                        ),
                        'save_always' => true,
                        'admin_label' => true
                    )
                ),
                malmo_elated_icon_collections()->getVCParamsArray(array(), '', true),
                array(
                    array(
                        'type'        => 'dropdown',
                        'heading'     => esc_html__('Use Icon Only', 'malmo'),
                        'description' => esc_html__('If Yes, the icon will fill the entire process item.', 'malmo'),
                        'param_name'  => 'icon_only',
                        'value'       => array(
                            esc_html__('No', 'malmo')  => 'no',
                            esc_html__('Yes', 'malmo') => 'yes'
                        ),
                        'save_always' => true,
                        'admin_label' => true
                    )
                )
            )
        ));
    }

    public function render($atts, $content = null) {
        $default_atts = array(
            'image'       => '',
            'title'       => '',
            'text'        => '',
            'highlighted' => '',
            'icon_only'   => ''
        );

        $default_atts = array_merge($default_atts, malmo_elated_icon_collections()->getShortcodeParams());
        $params       = shortcode_atts($default_atts, $atts);

        $iconPackName   = malmo_elated_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);
        if ($params['icon_pack'] != '') {
            $params['icon'] = $params[$iconPackName];
        }

        $params['item_classes'] = array(
            'eltd-process-item-holder'
        );

        if($params['highlighted'] === 'yes') {
            $params['item_classes'][] = 'eltd-pi-highlighted';
        }

        return malmo_elated_get_shortcode_module_template_part('templates/process-item-template', 'process', '', $params);
    }

}