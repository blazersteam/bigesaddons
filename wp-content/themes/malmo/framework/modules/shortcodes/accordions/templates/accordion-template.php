<h6 class="clearfix eltd-title-holder">
<span class="eltd-accordion-mark eltd-left-mark">
		<span class="eltd-accordion-mark-icon">
			<span class="eltd-acc-plus">+</span>
			<span class="eltd-acc-minus">-</span>
		</span>
</span>
<span class="eltd-tab-title">
		<span class="eltd-tab-title-inner">
			<?php if($params['icon']) : ?>
				<span class="eltd-icon-accordion-holder">
				 <?php echo malmo_elated_icon_collections()->renderIcon($icon, $icon_pack); ?>
			 </span>
			<?php endif; ?>
			<?php echo esc_attr($title) ?>
		</span>
</span>
</h6>
<div class="eltd-accordion-content">
	<div class="eltd-accordion-content-inner">
		<?php echo do_shortcode($content) ?>

		<?php if(is_array($link_params) && count($link_params)) : ?>
			<a class="eltd-arrow-link" target="<?php echo esc_attr($link_params['link_target']); ?>" href="<?php echo esc_url($link_params['link']); ?>">
				<span class="eltd-al-icon">
					<span class="icon-arrow-right-circle"></span>
				</span>
				<span class="eltd-al-text"><?php echo esc_html($link_params['link_text']); ?></span>
			</a>
		<?php endif; ?>
	</div>
</div>
