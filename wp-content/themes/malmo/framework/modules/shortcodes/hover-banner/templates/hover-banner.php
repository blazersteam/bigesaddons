<?php
$pane_style = '';
if ($hover_sheet_color != '') {
	$pane_style .= 'background-color:'.esc_attr($hover_sheet_color).';';
	if ($hover_sheet_opacity != '') {
		$pane_style .= 'opacity:'.esc_attr($hover_sheet_opacity).';';
	}
}
?>
<div class="eltd-hover-banner">
	<div class="eltd-hb-inner">
	<?php
	if ($image != '') {
		echo wp_get_attachment_image($image, 'full');
		?>
		<div class="eltd-hb-overlay">

			<div class="eltd-hb-overlay-pane" <?php if ($pane_style != '') echo 'style="'.esc_attr($pane_style).'"'; ?>></div>

			<?php if ($text != '') : ?>
			<div class="eltd-hb-overlay-text">
				<<?php echo esc_attr($text_tag); if ($text_color != '') echo ' style="color:'.esc_attr($text_color).';"'; ?>><?php echo esc_html($text); ?></<?php echo esc_attr($text_tag); ?>>
			</div>
			<?php endif; ?>

			<?php if ($link !== '') : ?>
			<a class="eltd-hb-link" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($link_target); ?>"></a>
			<?php endif; ?>

		</div>
		<?php
	}
	?>
	</div>
</div>