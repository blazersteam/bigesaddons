<?php

if(!function_exists('malmo_elated_get_button_html')) {
    /**
     * Calls button shortcode with given parameters and returns it's output
     *
     * @param $params
     *
     * @return mixed|string
     */
    function malmo_elated_get_button_html($params) {
        if(malmo_elated_core_installed()) {
            $button_html = malmo_elated_execute_shortcode('eltd_button', $params);
            $button_html = str_replace("\n", '', $button_html);

            return $button_html;
        }
    }
}

if(!function_exists('malmo_elated_get_btn_hover_animation_types')) {
    /**
     * @param bool $empty_val
     *
     * @return array
     */
    function malmo_elated_get_btn_hover_animation_types($empty_val = false) {
        $types = array(
            'disable'         => esc_html__('Disable Animation', 'malmo'),
            'fill-from-top'   => esc_html__('Fill From Top', 'malmo'),
            'fill-from-left'  => esc_html__('Fill From Left', 'malmo'),
            'fill-from-right' => esc_html__('Fill From Right', 'malmo')
        );

        if($empty_val) {
            $types = array_merge(array(
                '' => 'Default'
            ), $types);
        }

        return $types;
    }
}

if(!function_exists('eltd_get_btn_types')) {
    function malmo_elated_get_btn_types($empty_val = false) {
        $types = array(
            'outline'       => esc_html__('Outline', 'malmo'),
            'solid'         => esc_html__('Solid', 'malmo'),
            'white'         => esc_html__('White', 'malmo'),
            'white-outline' => esc_html__('White Outline', 'malmo'),
            'transparent'   => esc_html__('Transparent', 'malmo'),
            'black'         => esc_html__('Black', 'malmo')
        );

        if($empty_val) {
            $types = array_merge(array(
                '' => 'Default'
            ), $types);
        }

        return $types;
    }
}