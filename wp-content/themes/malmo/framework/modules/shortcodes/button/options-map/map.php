<?php

if(!function_exists('malmo_elated_button_map')) {
    function malmo_elated_button_map() {
        $panel = malmo_elated_add_admin_panel(array(
            'title' => esc_html__('Button', 'malmo'),
            'name'  => 'panel_button',
            'page'  => '_elements_page'
        ));

        malmo_elated_add_admin_field(array(
            'name'        => 'button_hover_animation',
            'type'        => 'select',
            'label'       => esc_html__('Hover Animation', 'malmo'),
            'description' => esc_html__('Choose default hover animation type', 'malmo'),
            'parent'      => $panel,
            'options'     => malmo_elated_get_btn_hover_animation_types()
        ));

        //Typography options
        malmo_elated_add_admin_section_title(array(
            'name'   => 'typography_section_title',
            'title'  => esc_html__('Typography', 'malmo'),
            'parent' => $panel
        ));

        $typography_group = malmo_elated_add_admin_group(array(
            'name'        => 'typography_group',
            'title'       => esc_html__('Typography', 'malmo'),
            'description' => esc_html__('Setup typography for all button types', 'malmo'),
            'parent'      => $panel
        ));

        $typography_row = malmo_elated_add_admin_row(array(
            'name'   => 'typography_row',
            'next'   => true,
            'parent' => $typography_group
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'fontsimple',
            'name'          => 'button_font_family',
            'default_value' => '',
            'label'         => esc_html__('Font Family', 'malmo'),
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_text_transform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'malmo'),
            'options'       => malmo_elated_get_text_transform_array()
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_font_style',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'malmo'),
            'options'       => malmo_elated_get_font_style_array()
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'textsimple',
            'name'          => 'button_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'malmo'),
            'args'          => array(
                'suffix' => 'px'
            )
        ));

        $typography_row2 = malmo_elated_add_admin_row(array(
            'name'   => 'typography_row2',
            'next'   => true,
            'parent' => $typography_group
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $typography_row2,
            'type'          => 'selectsimple',
            'name'          => 'button_font_weight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight', 'malmo'),
            'options'       => malmo_elated_get_font_weight_array()
        ));

        //Outline type options
        malmo_elated_add_admin_section_title(array(
            'name'   => 'type_section_title',
            'title'  => esc_html__('Types', 'malmo'),
            'parent' => $panel
        ));

        $outline_group = malmo_elated_add_admin_group(array(
            'name'        => 'outline_group',
            'title'       => esc_html__('Outline Type', 'malmo'),
            'description' => esc_html__('Setup outline button type', 'malmo'),
            'parent'      => $panel
        ));

        $outline_row = malmo_elated_add_admin_row(array(
            'name'   => 'outline_row',
            'next'   => true,
            'parent' => $outline_group
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'malmo'),
            'description'   => ''
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Hover Color', 'malmo'),
            'description'   => ''
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Background Color', 'malmo'),
            'description'   => ''
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color', 'malmo'),
            'description'   => ''
        ));

        $outline_row2 = malmo_elated_add_admin_row(array(
            'name'   => 'outline_row2',
            'next'   => true,
            'parent' => $outline_group
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $outline_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_border_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Border Color', 'malmo'),
            'description'   => ''
        ));

        //Solid type options
        $solid_group = malmo_elated_add_admin_group(array(
            'name'        => 'solid_group',
            'title'       => esc_html__('Solid Type', 'malmo'),
            'description' => esc_html__('Setup solid button type', 'malmo'),
            'parent'      => $panel
        ));

        $solid_row = malmo_elated_add_admin_row(array(
            'name'   => 'solid_row',
            'next'   => true,
            'parent' => $solid_group
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'malmo'),
            'description'   => ''
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Hover Color', 'malmo'),
            'description'   => ''
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Background Color', 'malmo'),
            'description'   => ''
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Background Color', 'malmo'),
            'description'   => ''
        ));

        $solid_row2 = malmo_elated_add_admin_row(array(
            'name'   => 'solid_row2',
            'next'   => true,
            'parent' => $solid_group
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color', 'malmo'),
            'description'   => ''
        ));

        malmo_elated_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_border_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Border Color', 'malmo'),
            'description'   => ''
        ));
    }

    add_action('malmo_elated_options_elements_map', 'malmo_elated_button_map');
}