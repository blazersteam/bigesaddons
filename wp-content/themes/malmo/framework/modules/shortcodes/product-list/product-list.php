<?php

namespace Malmo\Modules\Shortcodes\ProductList;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;


/**
 * Class ProductList
 */
class ProductList implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    function __construct() {
        $this->base = 'eltd_product_list';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {

        vc_map(array(
            'name'                      => esc_html__('Elated Product List', 'malmo'),
            'base'                      => $this->base,
            'icon'                      => 'icon-wpb-product-list extended-custom-icon',
            'category'                  => esc_html__('by ELATED', 'malmo'),
            'allowed_container_element' => 'vc_row',
            'params'                    => array(
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Type', 'malmo'),
                    'param_name'  => 'type',
                    'value'       => array(
                        esc_html__('Standard', 'malmo') => 'standard',
                        esc_html__('Masonry', 'malmo')  => 'masonry'
                    ),
                    'save_always' => true
                ),
                array(
                    'type'       => 'textfield',
                    'holder'     => 'div',
                    'heading'    => esc_html__('Number of Products', 'malmo'),
                    'param_name' => 'number_of_posts'
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Number of Columns', 'malmo'),
                    'param_name'  => 'number_of_columns',
                    'value'       => array(
                        esc_html__('One', 'malmo')   => '1',
                        esc_html__('Two', 'malmo')   => '2',
                        esc_html__('Three', 'malmo') => '3',
                        esc_html__('Four', 'malmo')  => '4',
                        esc_html__('Five', 'malmo')  => '5',
                        esc_html__('Six', 'malmo')   => '6'
                    ),
                    'save_always' => true
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Space Between Items', 'malmo'),
                    'param_name'  => 'space_between_items',
                    'value'       => array(
                        esc_html__('Normal', 'malmo')   => 'normal',
                        esc_html__('Small', 'malmo')    => 'small',
                        esc_html__('Tiny', 'malmo')     => 'tiny',
                        esc_html__('No Space', 'malmo') => 'no'
                    ),
                    'save_always' => true
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Order By', 'malmo'),
                    'param_name'  => 'order_by',
                    'value'       => array(
                        esc_html__('Title', 'malmo')      => 'title',
                        esc_html__('Date', 'malmo')       => 'date',
                        esc_html__('Random', 'malmo')     => 'rand',
                        esc_html__('Post Name', 'malmo')  => 'name',
                        esc_html__('ID', 'malmo')         => 'id',
                        esc_html__('Menu Order', 'malmo') => 'menu_order'
                    ),
                    'save_always' => true
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Order', 'malmo'),
                    'param_name'  => 'order',
                    'value'       => array(
                        esc_html__('ASC', 'malmo')  => 'ASC',
                        esc_html__('DESC', 'malmo') => 'DESC'
                    ),
                    'save_always' => true
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Choose Sorting Taxonomy', 'malmo'),
                    'param_name'  => 'taxonomy_to_display',
                    'value'       => array(
                        esc_html__('Category', 'malmo') => 'category',
                        esc_html__('Tag', 'malmo')      => 'tag',
                        esc_html__('Id', 'malmo')       => 'id'
                    ),
                    'save_always' => true,
                    'description' => esc_html__('If you would like to display only certain products, this is where you can select the criteria by which you would like to choose which products to display', 'malmo')
                ),
                array(
                    'type'        => 'textfield',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Enter Taxonomy Values', 'malmo'),
                    'param_name'  => 'taxonomy_values',
                    'description' => esc_html__('Separate values (category slugs, tags, or post IDs) with a comma', 'malmo')
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Image Proportions', 'malmo'),
                    'param_name'  => 'image_size',
                    'value'       => array(
                        esc_html__('Default', 'malmo')   => '',
                        esc_html__('Original', 'malmo')  => 'full',
                        esc_html__('Square', 'malmo')    => 'square',
                        esc_html__('Landscape', 'malmo') => 'landscape',
                        esc_html__('Portrait', 'malmo')  => 'portrait',
                        esc_html__('Medium', 'malmo')    => 'medium',
                        esc_html__('Large', 'malmo')     => 'large'
                    ),
                    'save_always' => true
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Product Info Skin', 'malmo'),
                    'param_name'  => 'product_info_skin',
                    'value'       => array(
                        esc_html__('Default', 'malmo') => 'default',
                        esc_html__('Light', 'malmo')   => 'light',
                        esc_html__('Dark', 'malmo')    => 'dark'
                    ),
                    'save_always' => true,
                    'group'       => 'Product Info Style'
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Display Quick View', 'malmo'),
                    'param_name'  => 'display_quick_view',
                    'value'       => array(
                        esc_html__('No', 'malmo')  => 'no',
                        esc_html__('Yes', 'malmo') => 'yes'
                    ),
                    'save_always' => true,
                    'description' => esc_html__('This option works only if YITH WooCommerce Quick View plugin is installed', 'malmo'),
                    'group'       => 'YITH Product Info'
                ),
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'heading'     => esc_html__('Display Wishlist', 'malmo'),
                    'param_name'  => 'display_wishlist',
                    'value'       => array(
                        esc_html__('No', 'malmo')  => 'no',
                        esc_html__('Yes', 'malmo') => 'yes'
                    ),
                    'save_always' => true,
                    'description' => esc_html__('This option works only if YITH WooCommerce Wishlist plugin is installed', 'malmo'),
                    'group'       => 'YITH Product Info'
                )
            )
        ));
    }

    public function render($atts, $content = null) {

        $default_atts = array(
            'type'                => 'standard',
            'number_of_posts'     => '8',
            'number_of_columns'   => '4',
            'space_between_items' => 'normal',
            'order_by'            => '',
            'order'               => '',
            'taxonomy_to_display' => 'category',
            'taxonomy_values'     => '',
            'image_size'          => '',
            'product_info_skin'   => '',
            'display_quick_view'  => 'no',
            'display_wishlist'    => 'no'
        );

        $params = shortcode_atts($default_atts, $atts);
        extract($params);

        $params['holder_classes'] = $this->getHolderClasses($params);

        $queryArray             = $this->generateProductQueryArray($params);
        $query_result           = new \WP_Query($queryArray);
        $params['query_result'] = $query_result;

        $html = malmo_elated_get_shortcode_module_template_part('templates/product-list-template', 'product-list', '', $params);

        return $html;
    }

    /**
     * Generates holder classes
     *
     * @param $params
     *
     * @return string
     */
    private function getHolderClasses($params) {
        $holderClasses = '';

        $productListType = $this->getProductListTypeClass($params);

        $columnsSpace = $this->getColumnsSpaceClass($params);

        $columnNumber = $this->getColumnNumberClass($params);

        $productInfoClasses = $this->getProductInfoSkinClass($params);

        $holderClasses .= $productListType.' '.$columnsSpace.' '.$columnNumber.' '.$productInfoClasses;

        return $holderClasses;
    }

    /**
     * Generates product list type classes for product list holder
     *
     * @param $params
     *
     * @return string
     */
    private function getProductListTypeClass($params) {

        $type            = '';
        $productListType = $params['type'];

        switch($productListType) {
            case 'standard':
                $type = 'eltd-standard-layout';
                break;
            case 'masonry':
                $type = 'eltd-masonry-layout';
                break;
            default:
                $type = 'eltd-standard-layout';
                break;
        }

        return $type;
    }

    /**
     * Generates space between columns classes for product list holder
     *
     * @param $params
     *
     * @return string
     */
    private function getColumnsSpaceClass($params) {

        $columnsSpace      = '';
        $spaceBetweenItems = $params['space_between_items'];

        switch($spaceBetweenItems) {
            case 'normal':
                $columnsSpace = 'eltd-normal-space';
                break;
            case 'small':
                $columnsSpace = 'eltd-small-space';
                break;
            case 'tiny':
                $columnsSpace = 'eltd-tiny-space';
                break;
            case 'no':
                $columnsSpace = 'eltd-no-space';
                break;
            default:
                $columnsSpace = 'eltd-normal-space';
                break;
        }

        return $columnsSpace;
    }

    /**
     * Generates columns number classes for product list holder
     *
     * @param $params
     *
     * @return string
     */
    private function getColumnNumberClass($params) {

        $columnsNumber = '';
        $columns       = $params['number_of_columns'];

        switch($columns) {
            case 1:
                $columnsNumber = 'eltd-one-column';
                break;
            case 2:
                $columnsNumber = 'eltd-two-columns';
                break;
            case 3:
                $columnsNumber = 'eltd-three-columns';
                break;
            case 4:
                $columnsNumber = 'eltd-four-columns';
                break;
            case 5:
                $columnsNumber = 'eltd-five-columns';
                break;
            case 6:
                $columnsNumber = 'eltd-six-columns';
                break;
            default:
                $columnsNumber = 'eltd-four-columns';
                break;
        }

        return $columnsNumber;
    }

    /**
     * Generates product info skin class for product list holder
     *
     * @param $params
     *
     * @return string
     */
    private function getProductInfoSkinClass($params) {

        $classes         = '';
        $productInfoSkin = $params['product_info_skin'];

        switch($productInfoSkin) {
            case 'light':
                $classes = 'eltd-product-info-light';
                break;
            case 'dark':
                $classes = 'eltd-product-info-dark';
                break;
            default:
                $classes = '';
                break;
        }


        return $classes;
    }

    /**
     * Generates query array
     *
     * @param $params
     *
     * @return array
     */
    public function generateProductQueryArray($params) {

        $queryArray = array(
            'post_type'           => 'product',
            'post_status'         => 'publish',
            'ignore_sticky_posts' => 1,
            'posts_per_page'      => $params['number_of_posts'],
            'orderby'             => $params['order_by'],
            'order'               => $params['order'],
            'meta_query'          => WC()->query->get_meta_query()
        );

        if($params['taxonomy_to_display'] !== '' && $params['taxonomy_to_display'] === 'category') {
            $queryArray['product_cat'] = $params['taxonomy_values'];
        }

        if($params['taxonomy_to_display'] !== '' && $params['taxonomy_to_display'] === 'tag') {
            $queryArray['product_tag'] = $params['taxonomy_values'];
        }

        if($params['taxonomy_to_display'] !== '' && $params['taxonomy_to_display'] === 'id') {
            $idArray                = $params['taxonomy_values'];
            $ids                    = explode(',', $idArray);
            $queryArray['post__in'] = $ids;
        }

        return $queryArray;
    }

    /**
     * Return Style for Title
     *
     * @param $params
     *
     * @return string
     */
    private function getTitleStyles($params) {
        $item_styles = array();

        if($params['title_transform'] !== '') {
            $item_styles[] = 'text-transform: '.$params['title_transform'];
        }

        return implode(';', $item_styles);
    }

}