<?php
namespace Malmo\Modules\Clients;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class Clients
 */
class Clients implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    function __construct() {
        $this->base = 'eltd_clients';
        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    public function vcMap() {

        vc_map(array(
            'name'                    => esc_html__('Clients', 'malmo'),
            'base'                    => $this->getBase(),
            'as_parent'               => array('only' => 'eltd_client'),
            'content_element'         => true,
            'show_settings_on_create' => true,
            'category'                => esc_html__('by ELATED', 'malmo'),
            'icon'                    => 'icon-wpb-clients extended-custom-icon',
            'js_view'                 => 'VcColumnView',
            'params'                  => array(
                array(
                    'heading'     => esc_html__('Number of columns', 'malmo'),
                    'type'        => 'dropdown',
                    'admin-label' => true,
                    'param_name'  => 'columns',
                    'value'       => array(
                        esc_html__('Two', 'malmo') => 'two',
                        esc_html__('Three', 'malmo') => 'three',
                        esc_html__('Four', 'malmo')  => 'four'
                    ),
                    'save_always' => true,
                    'description' => ''
                ),
                array(
                    'heading'     => esc_html__('Hover Animation', 'malmo'),
                    'type'        => 'dropdown',
                    'admin-label' => true,
                    'param_name'  => 'hover_animation',
                    'value'       => array(
                        esc_html__('Fade', 'malmo') => 'fade',
                        esc_html__('Flip', 'malmo') => 'flip'
                    ),
                    'save_always' => true,
                    'description' => ''
                )
            )
        ));

    }

    public function render($atts, $content = null) {
        $args = array(
            'columns'           => '',
            'hover_animation'    => ''
        );

        $params = shortcode_atts($args, $atts);

        extract($params);

        $params['content'] = $content;

        return malmo_elated_get_shortcode_module_template_part('templates/clients', 'clients', '', $params);
    }


}