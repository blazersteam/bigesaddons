<div class="eltd-clients eltd-clients-<?php echo esc_attr($columns); ?>-columns eltd-clients-animation-<?php echo esc_attr($hover_animation); ?>">
	<div class="eltd-clients-inner clearfix">
		<?php echo do_shortcode($content); ?>
	</div>
</div>