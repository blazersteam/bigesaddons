<?php
/**
 * Team info on hover shortcode template
 */

global $malmo_IconCollections;
$number_of_social_icons = 5;
?>

<div class="eltd-team <?php echo esc_attr($team_type) ?>">
	<div class="eltd-team-inner">
		<?php if($team_image !== '') { ?>
		<div class="eltd-team-image">
			<img src="<?php echo esc_url($team_image_src); ?>" alt="<?php esc_attr_e( 'team-image', 'malmo' ); ?>"/>

			<div class="eltd-team-social-holder <?php echo esc_attr($light_class);?>">
				<div class="eltd-team-social">
					<div class="eltd-team-social-inner">
						<div class="eltd-team-title-holder">
							<?php if($team_name !== '') { ?>
							<<?php echo esc_attr($team_name_tag); ?> class="eltd-team-name <?php echo esc_attr($light_class);?>">
							<?php echo esc_attr($team_name); ?>
						</<?php echo esc_attr($team_name_tag); ?>>
						<?php }
						if($team_position !== '') { ?>
							<h5 class="eltd-team-position <?php echo esc_attr($light_class);?>">
								<?php echo esc_attr($team_position); ?>
							</h5>
						<?php } ?>
					</div>
					<?php if (is_array($team_social_icons) && count($team_social_icons)) : ?>
					<div class="eltd-team-social-wrapp <?php echo esc_attr($light_class);?>">

						<?php foreach($team_social_icons as $team_social_icon) { ?>
							<span class="eltd-team-social-icon <?php echo esc_attr($light_class);?>"><?php echo malmo_elated_get_module_part($team_social_icon); ?></span>
						<?php } ?>

					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php }

	if($team_description !== '') { ?>
		<div class="eltd-team-text">
			<div class="eltd-team-text-inner">
				<div class="eltd-team-description <?php echo esc_attr($light_class);?>">
					<p><?php echo esc_attr($team_description); ?></p>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
</div>