<?php
/**
 * Team info on hover shortcode template
 */
global $malmo_IconCollections;
$number_of_social_icons = 5;
?>

<div class="eltd-team <?php echo esc_attr($team_type) ?>">
	<div class="eltd-team-inner">
		<?php if($team_image !== '') { ?>
			<div class="eltd-team-image">
				<img src="<?php echo malmo_elated_get_module_part($team_image_src); ?>" alt="<?php esc_attr_e( 'eltd-team-image', 'malmo' ); ?>"/>
			</div>
		<?php } ?>

		<?php if ($team_name !== '' || $team_position !== '' || $team_description != "" || $show_skills == 'yes') { ?>
			<div class="eltd-team-info eltd-team-align-<?php echo esc_attr($team_text_align); ?>">
				<?php if ($team_name !== '' || $team_position !== '') { ?>
					<div class="eltd-team-title-holder <?php echo esc_attr($team_social_icon_type) ?>">
						<?php if ($team_name !== '') { ?>
							<<?php echo esc_attr($team_name_tag); ?> class="eltd-team-name <?php echo esc_attr($light_class);?>">
								<?php echo esc_attr($team_name); ?>
							</<?php echo esc_attr($team_name_tag); ?>>
						<?php } ?>
						<?php if ($team_position !== "") { ?>
							<h5 class="eltd-team-position <?php echo esc_attr($light_class);?>"><?php echo esc_attr($team_position) ?></h5>
						<?php } ?>
					</div>
				<?php } ?>

				<?php if ($team_description != "") { ?>
					<div class='eltd-team-text eltd-team-align-<?php echo esc_attr($team_text_align); ?>'>
						<div class='eltd-team-text-inner'>
							<div class='eltd-team-description <?php echo esc_attr($light_class);?>'>
								<p><?php echo esc_attr($team_description) ?></p>
							</div>
						</div>
					</div>
				<?php }
			} ?>

		<?php if($team_social_icons){ ?>
		<div class="eltd-team-social-holder-between">
			<div class="eltd-team-social <?php echo esc_attr($team_social_icon_type) ?>">
				<div class="eltd-team-social-inner eltd-team-align-<?php echo esc_attr($team_text_align); ?>">
					<div class="eltd-team-social-wrapp">

						<?php foreach($team_social_icons as $team_social_icon) { ?>
							<span class="eltd-team-social-icon"><?php echo malmo_elated_get_module_part($team_social_icon); ?></span>
						<?php } ?>

					</div>
				</div>
			</div>
		</div>
		<?php } ?>

	</div>
</div>
</div>