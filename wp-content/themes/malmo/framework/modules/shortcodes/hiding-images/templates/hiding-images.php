<?php
$main_image = wp_get_attachment_image_src($main_image, 'full');
?>
<div class="eltd-hiding-images">
    <div class="eltd-hi-inner">
        <div class="eltd-hi-other-images">
            <?php echo do_shortcode($content); ?>
        </div>
        <div class="eltd-hi-main-image">
            <?php if(!empty($link)) : ?>
            <a class="eltd-hiding-image-link" href="<?php echo esc_url($link); ?>" <?php malmo_elated_inline_attr($target, 'target'); ?>></a>
            <div class="eltd-hi-main-image-holder" style="background-image: url('<?php echo esc_url($main_image[0]); ?>')">
                <?php else: ?>
                <div class="eltd-hi-main-image-holder" style="background-image: url('<?php echo esc_url($main_image[0]); ?>')">
                    <?php endif; ?>
                </div>
                <img class="eltd-hi-laptop" src="<?php echo esc_url(ELATED_ASSETS_ROOT); ?>/css/img/hidden-images-laptop.png" alt="<?php esc_attr_e( 'hidden-images-laptop', 'malmo' ); ?>">

            </div>
        </div>
    </div>