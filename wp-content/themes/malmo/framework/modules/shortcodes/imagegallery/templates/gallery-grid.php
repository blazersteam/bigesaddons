<div class="eltd-image-gallery">
	<div class="eltd-image-gallery-grid <?php echo esc_html($columns); ?><?php echo esc_attr($space); ?> <?php echo esc_html($gallery_classes); ?>">
		<?php foreach($images as $image) { ?>
			<div class="eltd-gallery-image <?php echo esc_attr($hover_overlay); ?>">
				<div class="eltd-image-gallery-holder">
					<?php if($pretty_photo) { ?>
					<a href="<?php echo esc_url($image['url']) ?>" data-rel="prettyPhoto[single_pretty_photo]" title="<?php echo esc_attr($image['title']); ?>">
						<div class="eltd-icon-holder"><?php echo malmo_elated_icon_collections()->renderIcon('icon_plus', 'font_elegant'); ?></div>
						<?php } ?>
						<?php if(is_array($image_size) && count($image_size)) : ?>
							<?php echo malmo_elated_generate_thumbnail($image['image_id'], null, $image_size[0], $image_size[1]); ?>
						<?php else: ?>
							<?php echo wp_get_attachment_image($image['image_id'], $image_size); ?>
						<?php endif; ?>
						<span class="eltd-image-gallery-hover"></span>
						<?php if($pretty_photo) { ?>
					</a>
				<?php } ?>
				</div>
			</div>
		<?php } ?>
	</div>
</div>