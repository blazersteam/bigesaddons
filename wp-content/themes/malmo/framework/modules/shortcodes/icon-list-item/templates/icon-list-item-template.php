<div <?php malmo_elated_inline_style($holder_styles); ?> <?php malmo_elated_class_attribute($holder_classes); ?>>
	<div class="eltd-icon-list-icon-holder">
		<div class="eltd-icon-list-icon-holder-inner clearfix">
			<?php echo malmo_elated_icon_collections()->renderIcon($icon, $icon_pack, $params);
			?>
		</div>
	</div>
	<p class="eltd-icon-list-text" <?php echo malmo_elated_get_inline_style($title_style) ?> > <?php echo esc_attr($title) ?></p>
</div>