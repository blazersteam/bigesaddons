<?php
namespace Malmo\Modules\PricingTables;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingTables implements ShortcodeInterface {
    private $base;

    function __construct() {
        $this->base = 'eltd_pricing_tables';
        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {

        vc_map(array(
            'name'                    => esc_html__('Pricing Tables', 'malmo'),
            'base'                    => $this->base,
            'as_parent'               => array('only' => 'eltd_pricing_table'),
            'content_element'         => true,
            'category'                => esc_html__('by ELATED', 'malmo'),
            'icon'                    => 'icon-wpb-pricing-tables extended-custom-icon',
            'show_settings_on_create' => true,
            'params'                  => array(
                array(
                    'type'        => 'dropdown',
                    'holder'      => 'div',
                    'class'       => '',
                    'heading'     => esc_html__('Columns', 'malmo'),
                    'param_name'  => 'columns',
                    'value'       => array(
                        'Two'   => 'eltd-two-columns',
                        'Three' => 'eltd-three-columns',
                        'Four'  => 'eltd-four-columns',
                    ),
                    'save_always' => true,
                    'description' => ''
                )
            ),
            'js_view'                 => 'VcColumnView'
        ));

    }

    public function render($atts, $content = null) {
        $args = array(
            'columns' => 'eltd-two-columns'
        );

        $params = shortcode_atts($args, $atts);
        extract($params);

        $html = '';

        $html .= '<div class="eltd-pricing-tables clearfix '.$columns.'">';
        $html .= '<div class="eltd-pricing-tables-inner clearfix">';
        $html .= malmo_elated_remove_auto_ptag($content);
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

}
