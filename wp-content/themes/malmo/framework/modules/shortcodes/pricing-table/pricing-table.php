<?php
namespace Malmo\Modules\PricingTable;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingTable implements ShortcodeInterface {
    private $base;

    function __construct() {
        $this->base = 'eltd_pricing_table';
        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        vc_map(array(
            'name'                      => esc_html__('Pricing Table', 'malmo'),
            'base'                      => $this->base,
            'icon'                      => 'icon-wpb-pricing-table extended-custom-icon',
            'category'                  => esc_html__('by ELATED', 'malmo'),
            'allowed_container_element' => 'vc_row',
            'as_child'                  => array('only' => 'eltd_pricing_tables'),
            'params'                    => array_merge(
                \MalmoElatedIconCollections::get_instance()->getVCParamsArray(),
                array(
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Title', 'malmo'),
                        'param_name'  => 'title',
                        'value'       => esc_html__('Basic Plan', 'malmo'),
                        'description' => ''
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Title Size (px)', 'malmo'),
                        'param_name'  => 'title_size',
                        'value'       => '',
                        'description' => '',
                        'dependency'  => array('element' => 'title', 'not_empty' => true)
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Price', 'malmo'),
                        'param_name'  => 'price'
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Currency', 'malmo'),
                        'param_name'  => 'currency'
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Price Period', 'malmo'),
                        'param_name'  => 'price_period'
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Label', 'malmo'),
                        'param_name'  => 'label',
                        'save_always' => ''
                    ),
                    array(
                        'type'        => 'dropdown',
                        'admin_label' => true,
                        'heading'     => esc_html__('Show Button', 'malmo'),
                        'param_name'  => 'show_button',
                        'value'       => array(
                            esc_html__('Default', 'malmo') => '',
                            esc_html__('Yes', 'malmo')     => 'yes',
                            esc_html__('No', 'malmo')      => 'no'
                        ),
                        'description' => ''
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Button Text', 'malmo'),
                        'param_name'  => 'button_text',
                        'dependency'  => array('element' => 'show_button', 'value' => 'yes')
                    ),
                    array(
                        'type'        => 'textfield',
                        'admin_label' => true,
                        'heading'     => esc_html__('Button Link', 'malmo'),
                        'param_name'  => 'link',
                        'dependency'  => array('element' => 'show_button', 'value' => 'yes')
                    ),
                    array(
                        'type'        => 'dropdown',
                        'admin_label' => true,
                        'heading'     => esc_html__('Active', 'malmo'),
                        'param_name'  => 'active',
                        'value'       => array(
                            esc_html__('No', 'malmo')  => 'no',
                            esc_html__('Yes', 'malmo') => 'yes'
                        ),
                        'save_always' => true,
                        'description' => ''
                    ),
                    array(
                        'type'        => 'textarea_html',
                        'holder'      => 'div',
                        'class'       => '',
                        'heading'     => esc_html__('Content', 'malmo'),
                        'param_name'  => 'content',
                        'value'       => '<li>content content content</li><li>content content content</li><li>content content content</li>',
                        'description' => ''
                    )
                )
            )
        ));
    }

    public function render($atts, $content = null) {

        $args = array(
            'title'        => 'Basic Plan',
            'title_size'   => '',
            'price'        => '100',
            'currency'     => '',
            'price_period' => '',
            'label'        => '',
            'active'       => 'no',
            'show_button'  => 'yes',
            'link'         => '',
            'button_text'  => 'button'
        );

        $default_atts = array_merge($args, malmo_elated_icon_collections()->getShortcodeParams());
        $params       = shortcode_atts($default_atts, $atts);

        $iconPackName = malmo_elated_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);

        extract($params);

        $params['icon'] = $params[$iconPackName];


        $html                  = '';
        $pricing_table_clasess = 'eltd-price-table';

        if($active == 'yes') {
            $pricing_table_clasess .= ' eltd-pt-active';
        }

        $params['pricing_table_classes'] = $pricing_table_clasess;
        $params['content']               = $content;
        $params['button_params']         = $this->getButtonParams($params);

        $params['title_styles'] = array();

        if(!empty($params['title_size'])) {
            $params['title_styles'][] = 'font-size: '.malmo_elated_filter_px($params['title_size']).'px';
        }

        $html .= malmo_elated_get_shortcode_module_template_part('templates/pricing-table-template', 'pricing-table', '', $params);

        return $html;

    }

    private function getButtonParams($params) {
        $buttonParams = array();

        if($params['show_button'] === 'yes' && $params['button_text'] !== '') {
            $buttonParams = array(
                'link' => $params['link'],
                'text' => $params['button_text'],
                'size' => 'medium'
            );

            $buttonParams['type']       = $params['active'] === 'yes' ? 'white' : 'solid';
            $buttonParams['hover_type'] = $params['active'] === 'yes' ? 'white' : 'outline';
        }

        return $buttonParams;
    }

}
