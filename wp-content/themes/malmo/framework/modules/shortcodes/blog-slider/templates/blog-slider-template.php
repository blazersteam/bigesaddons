<div class="eltd-blog-slider-holder <?php if (isset($faded_posts) && $faded_posts == 'yes') echo 'eltd-faded-posts eltd-grid'; ?>">
    <?php if($query->have_posts()) : ?>
        <?php while($query->have_posts()) : $query->the_post(); ?>
            <div class="eltd-blog-slider-item">
                <div class="eltd-bs-item-image-holder">
                    <?php if(has_post_thumbnail()) : ?>
                        <div class="eltd-bs-item-image" style="background-image: url('<?php echo the_post_thumbnail_url(); ?>')">
                            <a href="<?php the_permalink(); ?>"></a>
                        </div>
                    <?php endif; ?>
                    <div class="eltd-bs-item-content">
                        <div class="eltd-bs-item-text">
                            <h2 class="eltd-bs-item-title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <div class="eltd-post-info">
                                <?php malmo_elated_post_info(array(
                                    'date'     => 'yes',
                                    'author'   => 'yes',
                                    'comments' => 'yes',
                                    'like'     => 'yes'
                                )) ?>
                            </div>
                            <div class="eltd-post-info small">
                                <?php malmo_elated_post_info(array(
                                    'date'     => 'yes'
                                )) ?>
                            </div>
                            <?php if(strlen(get_the_excerpt()) && $text_length != '0') {
                                $excerpt = ($text_length > 0) ? substr(get_the_excerpt(), 0, intval($text_length)) : get_the_excerpt(); ?>
                                <p class="eltd-bs-item-excerpt"><?php echo esc_html($excerpt) ?></p>
                            <?php } ?>
                        </div>
                        <div class="eltd-bs-item-button">
                            <?php
                            echo malmo_elated_get_button_html(array(
                                'text' => esc_html__('Read More', 'malmo'),
                                'type' => 'solid',
                                'link' => esc_html(get_the_permalink())
                            ));
                            ?>
                        </div>
                        <div class="eltd-bs-item-button small">
                            <?php
                            echo malmo_elated_get_button_html(array(
                                'text' => esc_html__('Read More', 'malmo'),
                                'type' => 'transparent',
                                'hover_type' => 'transparent',
                                'size' => 'small',
                                'link' => esc_html(get_the_permalink())
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
    <?php else: ?>
        <p><?php esc_html_e('No posts were found.', 'malmo'); ?></p>
    <?php endif; ?>
</div>