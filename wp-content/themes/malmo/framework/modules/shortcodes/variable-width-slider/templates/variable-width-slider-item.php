<div class="eltd-vws-item">
    <div class="eltd-vws-item-inner">
        <div class="eltd-vws-image">
            <?php if($link != '') : ?>
            <a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
            <?php endif; ?>
                <?php echo wp_get_attachment_image($image, 'full', '', array("class" => "eltd-vws-img")); ?>
                <div class="eltd-vws-overlay"></div>
            <?php if($link != '') : ?>
            </a>
            <?php endif; ?>
        </div>
    </div>
</div>
