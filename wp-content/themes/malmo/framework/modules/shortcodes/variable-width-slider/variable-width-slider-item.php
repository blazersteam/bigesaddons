<?php
namespace Malmo\Modules\VariableWidthSliderItem;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class VariableWidthSliderItem
 */
class VariableWidthSliderItem implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    function __construct() {
        $this->base = 'eltd_variable_width_slider_item';
        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    public function vcMap() {

        vc_map(array(
            'name'                    => esc_html__('Variable Width Slider Item', 'malmo'),
            'base'                    => $this->getBase(),
            'as_child'                => array('only' => 'eltd_variable_width_slider'),
            'category'                => esc_html__('by ELATED', 'malmo'),
            'icon'                    => 'icon-wpb-variable-width-slider-item extended-custom-icon',
            'show_settings_on_create' => true,
            'params'                  => array(
                array(
                    'type'        => 'attach_image',
                    'admin_label' => true,
                    'heading'     => esc_html__('Image', 'malmo'),
                    'param_name'  => 'image',
                    'description' => ''
                ),
                array(
                    'type'        => 'textfield',
                    'admin_label' => true,
                    'heading'     => esc_html__('Link', 'malmo'),
                    'value'       => '',
                    'param_name'  => 'link',
                    'dependency'  => array('element' => 'image', 'not_empty' => true)
                ),
                array(
                    'type'        => 'dropdown',
                    'admin_label' => true,
                    'heading'     => esc_html__('Target', 'malmo'),
                    'param_name'  => 'target',
                    'value'       => array(
                        esc_html__('Blank', 'malmo') => '_blank',
                        esc_html__('Self', 'malmo')  => '_self'
                    ),
                    'save_always' => true,
                    'dependency'  => array('element' => 'image', 'not_empty' => true)
                )
            )
        ));

    }

    public function render($atts, $content = null) {

        $default_atts = array(
            'image'       => '',
            'link'        => '',
            'target'      => ''
        );

        $params = shortcode_atts($default_atts, $atts);
        extract($params);

        return malmo_elated_get_shortcode_module_template_part('templates/variable-width-slider-item', 'variable-width-slider', '', $params);

    }
}