<?php
namespace Malmo\Modules\VariableWidthSlider;

use Malmo\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class VariableWidthSlider
 */
class VariableWidthSlider implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    function __construct() {
        $this->base = 'eltd_variable_width_slider';
        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base for shortcode
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    public function vcMap() {

        vc_map(array(
            'name'                    => esc_html__('Variable Width Slider', 'malmo'),
            'base'                    => $this->getBase(),
            'as_parent'               => array('only' => 'eltd_variable_width_slider_item'),
            'content_element'         => true,
            'show_settings_on_create' => false,
            'category'                => esc_html__('by ELATED', 'malmo'),
            'icon'                    => 'icon-wpb-variable-width-slider extended-custom-icon',
            'js_view'                 => 'VcColumnView',
            'params'                  => array(
            )
        ));

    }

    public function render($atts, $content = null) {
        $args = array(
            'columns' => ''
        );

        $params = shortcode_atts($args, $atts);

        extract($params);

        $params['content'] = $content;

        return malmo_elated_get_shortcode_module_template_part('templates/variable-width-slider', 'variable-width-slider', '', $params);
    }
}