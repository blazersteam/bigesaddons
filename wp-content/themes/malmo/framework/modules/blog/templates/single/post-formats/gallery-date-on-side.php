<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <?php malmo_elated_get_module_template_part('templates/lists/parts/gallery', 'blog'); ?>
        <div class="eltd-date-format">
            <?php if(!malmo_elated_post_has_title()) : ?>
            <a href="<?php esc_url(the_permalink()); ?>">
                <?php endif; ?>

                <span class="eltd-day"><?php the_time($eltd_j) ?></span>
                <span class="eltd-month"><?php the_time($eltd_M) ?></span>

                <?php if(!malmo_elated_post_has_title()) : ?>
            </a>
        <?php endif; ?>
        </div>
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner">
                <div class="eltd-post-info">
                    <?php malmo_elated_post_info(array(
                        'category' => 'yes',
                        'comments' => 'yes',
                        'like'     => 'yes'
                    )) ?>
                </div>
                <?php malmo_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
                <?php
                the_content();
                $args_pages = array(
                    'before'      => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
                    'after'       => '</div></div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                    'pagelink'    => '%'
                );

                wp_link_pages($args_pages);
                ?>
            </div>
            <div class="eltd-author-desc clearfix">

                <?php do_action('malmo_elated_before_blog_article_closed_tag'); ?>

                <div class="eltd-share-icons">
                    <?php $post_info_array['share'] = malmo_elated_options()->getOptionValue('enable_social_share') == 'yes'; ?>
                    <?php if($post_info_array['share'] == 'yes'): ?>
                        <span class="eltd-share"><?php esc_html_e('Share', 'malmo'); ?></span>
                    <?php endif; ?>
                    <?php echo malmo_elated_get_social_share_html(array(
                        'type'      => 'list',
                        'icon_type' => 'circle'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</article>