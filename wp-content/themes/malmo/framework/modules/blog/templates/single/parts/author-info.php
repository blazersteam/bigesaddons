<?php
$author_info_box   = esc_attr(malmo_elated_options()->getOptionValue('blog_author_info'));
$author_info_email = esc_attr(malmo_elated_options()->getOptionValue('blog_author_info_email'));
$social_networks   = malmo_elated_get_user_custom_fields();

?>
<?php if($author_info_box === 'yes') { ?>
    <div class="eltd-author-description">
        <div class="eltd-author-description-inner clearfix">
            <div class="eltd-author-description-image">
                <?php echo malmo_elated_kses_img(get_avatar(get_the_author_meta('ID'), 78)); ?>
            </div>
            <div class="eltd-author-description-text-holder">
                <?php if(get_the_author_meta('description') != "") { ?>
                    <div class="eltd-author-text">
                        <h5><?php echo esc_attr(get_the_author_meta('description')); ?></h5>
                    </div>
                <?php } ?>


                <div class="eltd-post-author">
                    <h5 class="eltd-post-author-name-position">
                    <span class="eltd-post-author-name"><?php
                    if(get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "") {
                        echo esc_attr(get_the_author_meta('first_name'))." ".esc_attr(get_the_author_meta('last_name'));
                    } else {
                        echo esc_attr(get_the_author_meta('display_name'));
                    }
                    ?></span>
                    <?php if(get_the_author_meta('position') !== '') { ?>
                        <span class="eltd-post-author-position"><?php echo esc_html(get_the_author_meta('position')) ?></span>
                    <?php } ?>
                    </h5>
                    <?php if($author_info_email === 'yes' && is_email(get_the_author_meta('email'))) { ?>
                        <p class="eltd-author-email"><?php echo sanitize_email(get_the_author_meta('email')); ?></p>
                    <?php } ?>
                </div>

                <?php if(is_array($social_networks) && count($social_networks)) { ?>

                    <div class="eltd-author-social-holder clearfix">
                        <?php foreach($social_networks as $network) { ?>
                            <a href="<?php echo esc_url($network['link']) ?>" target="blank">
                                <?php echo malmo_elated_icon_collections()->renderIcon($network['class'], 'font_elegant'); ?>
                            </a>
                        <?php } ?>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>