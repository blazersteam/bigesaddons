<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <?php malmo_elated_get_module_template_part('templates/lists/parts/gallery', 'blog'); ?>
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner clearfix">
                <?php malmo_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
                <div class="eltd-post-info">
                    <?php malmo_elated_post_info(array('author' => 'yes', 'like' => 'yes', 'comments' => 'yes', 'category' => 'yes')) ?>
                </div>
                <?php
                malmo_elated_excerpt($excerpt_length);
                $args_pages = array(
                    'before'      => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
                    'after'       => '</div></div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                    'pagelink'    => '%'
                );

                wp_link_pages($args_pages);
                ?>
            </div>
        </div>
    </div>
</article>