<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner">
                <div class="eltd-post-mark">
                    <span aria-hidden="true" class="icon_quotations"></span>
                </div>
                <div class="eltd-post-info">
                    <?php malmo_elated_post_info(array('date' => 'yes')) ?>
                </div>
                <div class="eltd-post-title">
                    <h3>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo esc_attr(get_post_meta(get_the_ID(), "eltd_post_quote_text_meta", true)); ?></a>
                    </h3>
                    <span class="quote_author">&mdash; <?php the_title(); ?></span>
                </div>
            </div>
            <div class="eltd-author-desc clearfix">
                <div class="eltd-image-name">
                    <div class="eltd-author-image">
                        <?php echo malmo_elated_kses_img(get_avatar(get_the_author_meta('ID'), 102)); ?>
                    </div>
                    <div class="eltd-author-name-holder">
                        <h5 class="eltd-author-name">
                            <?php
                            if(get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "") {
                                echo esc_attr(get_the_author_meta('first_name'))." ".esc_attr(get_the_author_meta('last_name'));
                            } else {
                                echo esc_attr(get_the_author_meta('display_name'));
                            }
                            ?>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>