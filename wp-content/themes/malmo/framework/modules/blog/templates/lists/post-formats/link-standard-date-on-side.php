<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <a href="<?php the_permalink(); ?>">
        <div class="eltd-post-content clearfix">
            <div class="eltd-post-text">
                <div class="eltd-post-text-inner">
                    <div class="eltd-post-mark">
                        <?php echo malmo_elated_icon_collections()->renderIcon('icon_link', 'font_elegant'); ?>
                    </div>
                    <h4 class="eltd-post-title"><?php the_title(); ?></h4>
                </div>
            </div>
        </div>
    </a>
</article>