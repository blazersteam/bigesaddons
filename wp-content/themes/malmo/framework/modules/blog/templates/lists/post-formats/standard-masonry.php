<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="eltd-post-content">
        <?php malmo_elated_get_module_template_part('templates/lists/parts/image', 'blog'); ?>
        <div class="eltd-post-text">
            <div class="eltd-post-text-inner">
                <div class="eltd-post-info">
                    <?php malmo_elated_post_info(array('date' => 'yes')) ?>
                </div>
                <?php malmo_elated_get_module_template_part('templates/lists/parts/title', 'blog'); ?>
                <?php
                malmo_elated_excerpt($excerpt_length);
                $args_pages = array(
                    'before'      => '<div class="eltd-single-links-pages"><div class="eltd-single-links-pages-inner">',
                    'after'       => '</div></div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                    'pagelink'    => '%'
                );

                wp_link_pages($args_pages);
                ?>
            </div>
            <div class="eltd-author-desc clearfix">
                <div class="eltd-image-name">
                    <div class="eltd-author-image">
                        <?php echo malmo_elated_kses_img(get_avatar(get_the_author_meta('ID'), 102)); ?>
                    </div>
                    <div class="eltd-author-name-holder">
                        <h5 class="eltd-author-name">
                            <?php
                            if(get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "") {
                                echo esc_attr(get_the_author_meta('first_name'))." ".esc_attr(get_the_author_meta('last_name'));
                            } else {
                                echo esc_attr(get_the_author_meta('display_name'));
                            }
                            ?>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>