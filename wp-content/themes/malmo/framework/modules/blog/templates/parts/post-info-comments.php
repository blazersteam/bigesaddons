<div class="eltd-post-info-comments-holder eltd-post-info-item">
    <a class="eltd-post-info-comments" href="<?php comments_link(); ?>">
        <?php comments_number(esc_html__('No comments', 'malmo'), esc_html__('1 comment', 'malmo'), esc_html__('% comments', 'malmo')); ?>
    </a>
</div>