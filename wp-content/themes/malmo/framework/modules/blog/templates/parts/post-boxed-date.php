<div class="eltd-post-info-date">
    <?php if(!malmo_elated_post_has_title()) { ?>
    <a href="<?php the_permalink() ?>">
        <?php } ?>
        <span class="eltd-post-date-wrapper"><?php echo sprintf('<span class="eltd-post-date-day">%1$s</span><span class="eltd-post-date-month">%2$s</span>', get_the_date('d'), get_the_date('F')); ?></span>
        <?php if(!malmo_elated_post_has_title()) { ?>
    </a>
<?php } ?>
</div>