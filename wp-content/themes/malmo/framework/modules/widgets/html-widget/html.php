<?php

class MalmoElatedHtmlWidget extends MalmoElatedWidget {
    public function __construct() {
        parent::__construct(
            'eltd_html_widget', // Base ID
            esc_html__('Elated Raw HTML', 'malmo') // Name
        );

        $this->setParams();
    }

    protected function setParams() {
        $this->params = array(
            array(
                'name'  => 'html',
                'type'  => 'textarea',
                'title' => esc_html__('Raw HTML', 'malmo')
            )
        );
    }

    public function widget($args, $instance) {
		echo malmo_elated_get_module_part($args['before_widget']); ?>
        <div class="eltd-html-widget">
            <?php echo malmo_elated_get_module_part($instance['html']); ?>
        </div>
        <?php echo malmo_elated_get_module_part($args['after_widget']);
    }

}