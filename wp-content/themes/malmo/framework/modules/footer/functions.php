<?php

if(!function_exists('malmo_elated_get_footer_classes')) {
    /**
     * Return all footer classes
     *
     * @param $page_id
     *
     * @return string|void
     */
    function malmo_elated_get_footer_classes($page_id) {

        $footer_classes       = '';
        $footer_classes_array = array('eltd-page-footer');

        $footer_style = malmo_elated_get_meta_field_intersect('footer_style', $page_id);

        //is uncovering footer option set in theme options?
        if(malmo_elated_options()->getOptionValue('uncovering_footer') == 'yes') {
            $footer_classes_array[] = 'eltd-footer-uncover';
        }

        if(get_post_meta($page_id, 'eltd_disable_footer_meta', true) == 'yes') {
            $footer_classes_array[] = 'eltd-disable-footer';
        }

        if($footer_style !== '') {
            $footer_classes_array[] = 'eltd-'.$footer_style;
        }

        //is some class added to footer classes array?
        if(is_array($footer_classes_array) && count($footer_classes_array)) {
            //concat all classes and prefix it with class attribute
            $footer_classes = esc_attr(implode(' ', $footer_classes_array));
        }

        return $footer_classes;

    }

}

if(!function_exists('malmo_elated_get_footer_top_border')) {
    /**
     * Return HTML for footer top border
     *
     * @return string
     */
    function malmo_elated_get_footer_top_border() {

        $footer_top_border = '';

        if(malmo_elated_options()->getOptionValue('footer_top_border_color')) {
            if(malmo_elated_options()->getOptionValue('footer_top_border_width') !== '') {
                $footer_border_height = malmo_elated_options()->getOptionValue('footer_top_border_width');
            } else {
                $footer_border_height = '1';
            }

            $footer_top_border = 'height: '.esc_attr($footer_border_height).'px; background-color: '.esc_attr(malmo_elated_options()->getOptionValue('footer_top_border_color')).';';
        }

        return $footer_top_border;
    }
}

if(!function_exists('malmo_elated_get_footer_bottom_border')) {
    /**
     * Return HTML for footer bottom border top
     *
     * @return string
     */
    function malmo_elated_get_footer_bottom_border() {

        $footer_bottom_border = '';

        if(malmo_elated_options()->getOptionValue('footer_bottom_border_color')) {
            if(malmo_elated_options()->getOptionValue('footer_bottom_border_width') !== '') {
                $footer_border_height = malmo_elated_options()->getOptionValue('footer_bottom_border_width');
            } else {
                $footer_border_height = '1';
            }

            $footer_bottom_border = 'height: '.esc_attr($footer_border_height).'px; background-color: '.esc_attr(malmo_elated_options()->getOptionValue('footer_bottom_border_color')).';';
        }

        return $footer_bottom_border;
    }
}


if(!function_exists('malmo_elated_get_footer_bottom_bottom_border')) {
    /**
     * Return HTML for footer bottom border bottom
     *
     * @return string
     */
    function malmo_elated_get_footer_bottom_bottom_border() {

        $footer_bottom_border = '';

        if(malmo_elated_options()->getOptionValue('footer_bottom_border_bottom_color')) {
            if(malmo_elated_options()->getOptionValue('footer_bottom_border_bottom_width') !== '') {
                $footer_border_height = malmo_elated_options()->getOptionValue('footer_bottom_border_bottom_width');
            } else {
                $footer_border_height = '1';
            }

            $footer_bottom_border = 'height: '.esc_attr($footer_border_height).'px; background-color: '.esc_attr(malmo_elated_options()->getOptionValue('footer_bottom_border_bottom_color')).';';
        }

        return $footer_bottom_border;
    }
}

if(!function_exists('malmo_elated_footer_top_classes')) {
    /**
     * Return classes for footer top
     *
     * @return string
     */
    function malmo_elated_footer_top_classes() {

        $footer_top_classes = array();

        if(malmo_elated_options()->getOptionValue('footer_in_grid') != 'yes') {
            $footer_top_classes[] = 'eltd-footer-top-full';
        }

        //footer aligment
        if(malmo_elated_options()->getOptionValue('footer_top_columns_alignment') != '') {
            $footer_top_classes[] = 'eltd-footer-top-aligment-'.malmo_elated_options()->getOptionValue('footer_top_columns_alignment');
        }


        return implode(' ', $footer_top_classes);
    }

}

if(!function_exists('malmo_elated_footer_body_classes')) {
    /**
     * @param $classes
     *
     * @return array
     */
    function malmo_elated_footer_body_classes($classes) {
        $background_image     = malmo_elated_get_meta_field_intersect('footer_background_image', malmo_elated_get_page_id());
        $enable_image_on_page = get_post_meta(malmo_elated_get_page_id(), 'eltd_enable_footer_image_meta', true);
        $is_footer_full_width = malmo_elated_options()->getOptionValue('footer_in_grid') !== 'yes';

        if($background_image !== '' && $enable_image_on_page !== 'yes') {
            $classes[] = 'eltd-footer-with-bg-image';
        }

        if($is_footer_full_width) {
            $classes[] = 'eltd-fullwidth-footer';
        }

        return $classes;
    }

    add_filter('body_class', 'malmo_elated_footer_body_classes');
}


if(!function_exists('malmo_elated_footer_page_styles')) {
    /**
     * @param $style
     *
     * @return array
     */
    function malmo_elated_footer_page_styles($style) {
        $background_image = get_post_meta(malmo_elated_get_page_id(), 'eltd_footer_background_image_meta', true);
        $page_prefix      = malmo_elated_get_unique_page_class();

        if($background_image !== '') {
            $footer_bg_image_style_array['background-image'] = 'url('.$background_image.')';

            $style[] = malmo_elated_dynamic_css('body.eltd-footer-with-bg-image'.$page_prefix.' .eltd-page-footer', $footer_bg_image_style_array);
        }

        return $style;
    }

    add_filter('malmo_elated_add_page_custom_style', 'malmo_elated_footer_page_styles');
}

if(!function_exists('malmo_elated_fixed_background_image')) {
    /**
     * @param $classes
     *
     * @return array
     */
    function malmo_elated_fixed_background_image($classes) {
        $image_url =  malmo_elated_get_fixed_background_image_url();

        if($image_url != '') {
            $classes[] = 'eltd-fixed-background';
        }

        return $classes;
    }

    add_filter('body_class', 'malmo_elated_fixed_background_image');
}

if(!function_exists('malmo_elated_get_fixed_background_image_url')) {
    /**
     * @param 
     *
     * @return string
     */
    function malmo_elated_get_fixed_background_image_url() {
        return malmo_elated_get_meta_field_intersect('page_fixed_background_image', malmo_elated_get_page_id());
    }
}