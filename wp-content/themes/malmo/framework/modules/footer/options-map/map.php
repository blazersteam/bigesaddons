<?php

if(!function_exists('malmo_elated_footer_options_map')) {
    /**
     * Add footer options
     */
    function malmo_elated_footer_options_map() {

        malmo_elated_add_admin_page(
            array(
                'slug'  => '_footer_page',
                'title' => esc_html__('Footer', 'malmo'),
                'icon'  => 'fa fa-sort-amount-asc'
            )
        );

        $footer_panel = malmo_elated_add_admin_panel(
            array(
                'title' => esc_html__('Footer', 'malmo'),
                'name'  => 'footer',
                'page'  => '_footer_page'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'uncovering_footer',
                'default_value' => 'no',
                'label'         => esc_html__('Uncovering Footer', 'malmo'),
                'description'   => esc_html__('Enabling this option will make Footer gradually appear on scroll', 'malmo'),
                'parent'        => $footer_panel,
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $footer_panel,
                'type'          => 'select',
                'name'          => 'footer_style',
                'default_value' => '',
                'label'         => esc_html__('Footer Skin', 'malmo'),
                'description'   => esc_html__('Choose Footer Skin for Footer Area', 'malmo'),
                'options'       => array(
                    ''             => '',
                    'dark-footer'  => esc_html__('Dark', 'malmo'),
                    'light-footer' => esc_html__('Light', 'malmo')
                )
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'        => 'footer_background_image',
                'type'        => 'image',
                'label'       => esc_html__('Background Image', 'malmo'),
                'description' => esc_html__('Choose Background Image for Footer Area', 'malmo'),
                'parent'      => $footer_panel
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'footer_in_grid',
                'default_value' => 'yes',
                'label'         => esc_html__('Footer in Grid', 'malmo'),
                'description'   => esc_html__('Enabling this option will place Footer content in grid', 'malmo'),
                'parent'        => $footer_panel,
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'show_footer_top',
                'default_value' => 'yes',
                'label'         => esc_html__('Show Footer Top', 'malmo'),
                'description'   => esc_html__('Enabling this option will show Footer Top area', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_show_footer_top_container'
                ),
                'parent'        => $footer_panel,
            )
        );

        $show_footer_top_container = malmo_elated_add_admin_container(
            array(
                'name'            => 'show_footer_top_container',
                'hidden_property' => 'show_footer_top',
                'hidden_value'    => 'no',
                'parent'          => $footer_panel
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'select',
                'name'          => 'footer_top_columns',
                'default_value' => '4',
                'label'         => esc_html__('Footer Top Columns', 'malmo'),
                'description'   => esc_html__('Choose number of columns for Footer Top area', 'malmo'),
                'options'       => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '5' => '3(25%+25%+50%)',
                    '6' => '3(50%+25%+25%)',
                    '4' => '4'
                ),
                'parent'        => $show_footer_top_container,
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'select',
                'name'          => 'footer_top_columns_alignment',
                'default_value' => '',
                'label'         => esc_html__('Footer Top Columns Alignment', 'malmo'),
                'description'   => esc_html__('Text Alignment in Footer Columns', 'malmo'),
                'options'       => array(
                    'left'   => esc_html__('Left', 'malmo'),
                    'center' => esc_html__('Center', 'malmo'),
                    'right'  => esc_html__('Right', 'malmo')
                ),
                'parent'        => $show_footer_top_container,
            )
        );

        malmo_elated_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'show_footer_bottom',
                'default_value' => 'yes',
                'label'         => esc_html__('Show Footer Bottom', 'malmo'),
                'description'   => esc_html__('Enabling this option will show Footer Bottom area', 'malmo'),
                'args'          => array(
                    'dependence'             => true,
                    'dependence_hide_on_yes' => '',
                    'dependence_show_on_yes' => '#eltd_show_footer_bottom_container'
                ),
                'parent'        => $footer_panel,
            )
        );

        $show_footer_bottom_container = malmo_elated_add_admin_container(
            array(
                'name'            => 'show_footer_bottom_container',
                'hidden_property' => 'show_footer_bottom',
                'hidden_value'    => 'no',
                'parent'          => $footer_panel
            )
        );


        malmo_elated_add_admin_field(
            array(
                'type'          => 'select',
                'name'          => 'footer_bottom_columns',
                'default_value' => '3',
                'label'         => esc_html__('Footer Bottom Columns', 'malmo'),
                'description'   => esc_html__('Choose number of columns for Footer Bottom area', 'malmo'),
                'options'       => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3'
                ),
                'parent'        => $show_footer_bottom_container,
            )
        );

    }

    add_action('malmo_elated_options_map', 'malmo_elated_footer_options_map',9);

}