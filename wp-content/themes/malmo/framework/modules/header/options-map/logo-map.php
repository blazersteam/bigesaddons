<?php

if ( ! function_exists('malmo_elated_logo_options_map') ) {

	function malmo_elated_logo_options_map() {

		malmo_elated_add_admin_page(
			array(
				'slug' => '_logo_page',
				'title' => esc_html__( 'Logo','malmo'),
				'icon' => 'fa fa-coffee'
			)
		);

        $panel_logo = malmo_elated_add_admin_panel(
            array(
                'page'  => '_logo_page',
                'name'  => 'panel_logo',
                'title' => esc_html__('Logo', 'malmo')
            )
        );

        malmo_elated_add_admin_field(
            array(
                'parent'        => $panel_logo,
                'type'          => 'yesno',
                'name'          => 'hide_logo',
                'default_value' => 'no',
                'label'         => esc_html__('Hide Logo', 'malmo'),
                'description'   => esc_html__('Enabling this option will hide logo image', 'malmo'),
                'args'          => array(
                    "dependence"             => true,
                    "dependence_hide_on_yes" => "#eltd_hide_logo_container",
                    "dependence_show_on_yes" => ""
                )
            )
        );

        $hide_logo_container = malmo_elated_add_admin_container(
            array(
                'parent'          => $panel_logo,
                'name'            => 'hide_logo_container',
                'hidden_property' => 'hide_logo',
                'hidden_value'    => 'yes'
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'logo_image',
                'type'          => 'image',
                'default_value' => ELATED_ASSETS_ROOT."/img/logo.png",
                'label'         => esc_html__('Logo Image - Default', 'malmo'),
                'description'   => esc_html__('Choose a default logo image to display ', 'malmo'),
                'parent'        => $hide_logo_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'logo_image_dark',
                'type'          => 'image',
                'default_value' => ELATED_ASSETS_ROOT."/img/logo_black.png",
                'label'         => esc_html__('Logo Image - Dark', 'malmo'),
                'description'   => esc_html__('Choose a default logo image to display ', 'malmo'),
                'parent'        => $hide_logo_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'logo_image_light',
                'type'          => 'image',
                'default_value' => ELATED_ASSETS_ROOT."/img/logo_white.png",
                'label'         => esc_html__('Logo Image - Light', 'malmo'),
                'description'   => esc_html__('Choose a default logo image to display ', 'malmo'),
                'parent'        => $hide_logo_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'logo_image_sticky',
                'type'          => 'image',
                'default_value' => ELATED_ASSETS_ROOT."/img/logo_white.png",
                'label'         => esc_html__('Logo Image - Sticky', 'malmo'),
                'description'   => esc_html__('Choose a default logo image to display ', 'malmo'),
                'parent'        => $hide_logo_container
            )
        );

        malmo_elated_add_admin_field(
            array(
                'name'          => 'logo_image_mobile',
                'type'          => 'image',
                'default_value' => ELATED_ASSETS_ROOT."/img/logo.png",
                'label'         => esc_html__('Logo Image - Mobile', 'malmo'),
                'description'   => esc_html__('Choose a default logo image to display ', 'malmo'),
                'parent'        => $hide_logo_container
            )
        );

	}

	add_action( 'malmo_elated_options_map', 'malmo_elated_logo_options_map', 2);

}