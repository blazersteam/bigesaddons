<?php
namespace Malmo\Modules\Header\Types;

use Malmo\Modules\Header\Lib\HeaderType;

/**
 * Class that represents Header Minimal layout and option
 *
 * Class HeaderMinimal
 */
class HeaderMinimal extends HeaderType {
    protected $heightOfTransparency;
    protected $heightOfCompleteTransparency;
    protected $headerHeight;
    protected $mobileHeaderHeight;

    /**
     * Sets slug property which is the same as value of option in DB
     */
    public function __construct() {
        $this->slug = 'header-minimal';

        if(!is_admin()) {

            $menuAreaHeight       = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('menu_area_height_header_minimal'));
            $this->menuAreaHeight = $menuAreaHeight !== '' ? $menuAreaHeight : 100;

            $mobileHeaderHeight       = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('mobile_header_height'));
            $this->mobileHeaderHeight = $mobileHeaderHeight !== '' ? $mobileHeaderHeight : 100;

            add_action('wp', array($this, 'setHeaderHeightProps'));

            add_filter('malmo_elated_js_global_variables', array($this, 'getGlobalJSVariables'));
            add_filter('malmo_elated_per_page_js_vars', array($this, 'getPerPageJSVariables'));
            add_filter('malmo_elated_add_page_custom_style', array($this, 'headerPerPageStyles'));
        }
    }

    public function headerPerPageStyles($style) {
        $id                     = malmo_elated_get_page_id();
        $class_prefix           = malmo_elated_get_unique_page_class();
        $main_menu_style        = array();
        $main_menu_grid_style   = array();
        $disable_grid_shadow = malmo_elated_get_meta_field_intersect('menu_area_in_grid_shadow_header_minimal',$id) == 'no';

        $main_menu_selector = array($class_prefix.'.eltd-header-minimal .eltd-menu-area');
        $main_menu_grid_selector = array($class_prefix.'.eltd-header-minimal .eltd-page-header .eltd-menu-area .eltd-grid .eltd-vertical-align-containers');

        /* header style - start */

        $menu_area_background_color        = get_post_meta($id, 'eltd_menu_area_background_color_header_minimal_meta', true);
        $menu_area_background_transparency = get_post_meta($id, 'eltd_menu_area_background_transparency_header_minimal_meta', true);

        if($menu_area_background_transparency === '') {
            $menu_area_background_transparency = 1;
        }

        $menu_area_background_color_rgba = malmo_elated_rgba_color($menu_area_background_color, $menu_area_background_transparency);

        if($menu_area_background_color_rgba !== null) {
            $main_menu_style['background-color'] = $menu_area_background_color_rgba;
        }
        /* header style - end */

        /* header in grid style - start */

        if(!$disable_grid_shadow) {
            $main_menu_grid_style['box-shadow'] = '0px 1px 3px rgba(0,0,0,0.15)';
        }

        $menu_area_grid_background_color        = get_post_meta($id, 'eltd_menu_area_grid_background_color_header_minimal_meta', true);
        $menu_area_grid_background_transparency = get_post_meta($id, 'eltd_menu_area_grid_background_transparency_header_minimal_meta', true);

        if($menu_area_grid_background_transparency === '') {
            $menu_area_grid_background_transparency = 1;
        }

        $menu_area_grid_background_color_rgba = malmo_elated_rgba_color($menu_area_grid_background_color, $menu_area_grid_background_transparency);

        if($menu_area_grid_background_color_rgba !== null) {
            $main_menu_grid_style['background-color'] = $menu_area_grid_background_color_rgba;
        }

        /* header in grid style - end */

        $style[] = malmo_elated_dynamic_css($main_menu_selector, $main_menu_style);
        $style[] = malmo_elated_dynamic_css($main_menu_grid_selector, $main_menu_grid_style);

        return $style;
    }

    /**
     * Loads template file for this header type
     *
     * @param array $parameters associative array of variables that needs to passed to template
     */
    public function loadTemplate($parameters = array()) {
        $id  = malmo_elated_get_page_id();

        $parameters['menu_area_in_grid'] = malmo_elated_get_meta_field_intersect('menu_area_in_grid_header_minimal',$id) == 'yes' ? true : false;

        $parameters = apply_filters('malmo_elated_header_minimal_parameters', $parameters);

        malmo_elated_get_module_template_part('templates/types/'.$this->slug, $this->moduleName, '', $parameters);
    }

    /**
     * Sets header height properties after WP object is set up
     */
    public function setHeaderHeightProps() {
        $this->heightOfTransparency         = $this->calculateHeightOfTransparency();
        $this->heightOfCompleteTransparency = $this->calculateHeightOfCompleteTransparency();
        $this->headerHeight                 = $this->calculateHeaderHeight();
        $this->mobileHeaderHeight           = $this->calculateMobileHeaderHeight();
    }

    /**
     * Returns total height of transparent parts of header
     *
     * @return int
     */
    public function calculateHeightOfTransparency() {
        $id                 = malmo_elated_get_page_id();
        $transparencyHeight = 0;

        if(get_post_meta($id, 'eltd_menu_area_background_color_header_minimal_meta', true) !== '') {
            $menuAreaTransparent = get_post_meta($id, 'eltd_menu_area_background_color_header_minimal_meta', true) !== '' &&
                                   get_post_meta($id, 'eltd_menu_area_background_transparency_header_minimal_meta', true) !== '1';
        } elseif(malmo_elated_options()->getOptionValue('menu_area_background_color_header_minimal') == '') {
            $menuAreaTransparent = malmo_elated_options()->getOptionValue('menu_area_grid_background_color_header_minimal') !== '' &&
                                   malmo_elated_options()->getOptionValue('menu_area_grid_background_transparency_header_minimal') !== '1';
        } else {
            $menuAreaTransparent = malmo_elated_options()->getOptionValue('menu_area_background_color_header_minimal') !== '' &&
                                   malmo_elated_options()->getOptionValue('menu_area_background_transparency_header_minimal') !== '1';
        }


        $contentBehindHeader = get_post_meta($id, 'eltd_page_content_behind_header_meta', true) === 'yes';

        if($contentBehindHeader) {
            $menuAreaTransparent = true;
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->menuAreaHeight;

            if(malmo_elated_is_top_bar_enabled() && malmo_elated_is_top_bar_transparent()) {
                $transparencyHeight += malmo_elated_get_top_bar_height();
            }
        }

        return $transparencyHeight;
    }

    /**
     * Returns height of completely transparent header parts
     *
     * @return int
     */
    public function calculateHeightOfCompleteTransparency() {
        $id                 = malmo_elated_get_page_id();
        $transparencyHeight = 0;

        if(get_post_meta($id, 'eltd_menu_area_background_color_header_minimal_meta', true) !== '') {
            $menuAreaTransparent = get_post_meta($id, 'eltd_menu_area_background_color_header_minimal_meta', true) !== '' &&
                                   get_post_meta($id, 'eltd_menu_area_background_transparency_header_minimal_meta', true) === '0';
        } elseif(malmo_elated_options()->getOptionValue('menu_area_background_color_header_minimal') == '') {
            $menuAreaTransparent = malmo_elated_options()->getOptionValue('menu_area_grid_background_color_header_minimal') !== '' &&
                                   malmo_elated_options()->getOptionValue('menu_area_grid_background_transparency_header_minimal') === '0';
        } else {
            $menuAreaTransparent = malmo_elated_options()->getOptionValue('menu_area_background_color_header_minimal') !== '' &&
                                   malmo_elated_options()->getOptionValue('menu_area_background_transparency_header_minimal') === '0';
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->menuAreaHeight;
        }

        return $transparencyHeight;
    }


    /**
     * Returns total height of header
     *
     * @return int|string
     */
    public function calculateHeaderHeight() {
        $headerHeight = $this->menuAreaHeight;
        if(malmo_elated_is_top_bar_enabled()) {
            $headerHeight += malmo_elated_get_top_bar_height();
        }

        return $headerHeight;
    }

    /**
     * Returns total height of mobile header
     *
     * @return int|string
     */
    public function calculateMobileHeaderHeight() {
        $mobileHeaderHeight = $this->mobileHeaderHeight;

        return $mobileHeaderHeight;
    }

    /**
     * Returns global js variables of header
     *
     * @param $globalVariables
     *
     * @return int|string
     */
    public function getGlobalJSVariables($globalVariables) {
        $globalVariables['eltdLogoAreaHeight']     = 0;
        $globalVariables['eltdMenuAreaHeight']     = $this->headerHeight;
        $globalVariables['eltdMobileHeaderHeight'] = $this->mobileHeaderHeight;

        return $globalVariables;
    }

    /**
     * Returns per page js variables of header
     *
     * @param $perPageVars
     *
     * @return int|string
     */
    public function getPerPageJSVariables($perPageVars) {
        //calculate transparency height only if header has no sticky behaviour
        if(!in_array(malmo_elated_get_meta_field_intersect('header_behaviour'), array(
            'sticky-header-on-scroll-up',
            'sticky-header-on-scroll-down-up'
        ))
        ) {
            $perPageVars['eltdHeaderTransparencyHeight'] = $this->headerHeight - (malmo_elated_get_top_bar_height() + $this->heightOfCompleteTransparency);
        } else {
            $perPageVars['eltdHeaderTransparencyHeight'] = 0;
        }

        return $perPageVars;
    }
}