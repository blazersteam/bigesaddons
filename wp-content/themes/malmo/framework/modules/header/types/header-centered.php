<?php
namespace Malmo\Modules\Header\Types;

use Malmo\Modules\Header\Lib\HeaderType;

/**
 * Class that represents Header Centered layout and option
 *
 * Class HeaderCentered
 */
class HeaderCentered extends HeaderType {
    protected $heightOfTransparency;
    protected $heightOfCompleteTransparency;
    protected $headerHeight;
    protected $mobileHeaderHeight;

    /**
     * Sets slug property which is the same as value of option in DB
     */
    public function __construct() {
        $this->slug = 'header-centered';

        if(!is_admin()) {

            $logoAreaHeight       = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('logo_area_height_header_centered'));
            $this->logoAreaHeight = $logoAreaHeight !== '' ? intval($logoAreaHeight) : 80;

            $menuAreaHeight       = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('menu_area_height_header_centered'));
            $this->menuAreaHeight = $menuAreaHeight !== '' ? intval($menuAreaHeight) : 100;

            $mobileHeaderHeight       = malmo_elated_filter_px(malmo_elated_options()->getOptionValue('mobile_header_height'));
            $this->mobileHeaderHeight = $mobileHeaderHeight !== '' ? $mobileHeaderHeight : 100;

            add_action('wp', array($this, 'setHeaderHeightProps'));

            add_filter('malmo_elated_js_global_variables', array($this, 'getGlobalJSVariables'));
            add_filter('malmo_elated_per_page_js_vars', array($this, 'getPerPageJSVariables'));
            add_filter('malmo_elated_add_page_custom_style', array($this, 'headerPerPageStyles'));
        }
    }

    public function headerPerPageStyles($style) {
        $id                     = malmo_elated_get_page_id();
        $class_prefix           = malmo_elated_get_unique_page_class();
        $main_menu_style        = array();
        $main_menu_grid_style   = array();
        $logo_area_style        = array();
        $logo_area_grid_style   = array();

        $disable_logo_grid_border = malmo_elated_get_meta_field_intersect('logo_area_in_grid_border_header_centered',$id) == 'no';

        $disable_menu_grid_shadow = malmo_elated_get_meta_field_intersect('menu_area_in_grid_shadow_header_centered',$id) == 'no';

        $main_menu_selector = array($class_prefix.'.eltd-header-centered .eltd-menu-area');
        $main_menu_grid_selector = array($class_prefix.'.eltd-header-centered .eltd-page-header .eltd-menu-area .eltd-grid .eltd-vertical-align-containers');

        $logo_area_selector = array($class_prefix.'.eltd-header-centered .eltd-logo-area');
        $logo_area_grid_selector = array($class_prefix.'.eltd-header-centered .eltd-page-header .eltd-logo-area .eltd-grid .eltd-vertical-align-containers');

        /* logo area style - start */

        $logo_area_background_color        = get_post_meta($id, 'eltd_logo_area_background_color_header_centered_meta', true);
        $logo_area_background_transparency = get_post_meta($id, 'eltd_logo_area_background_transparency_header_centered_meta', true);

        if($logo_area_background_transparency === '') {
            $logo_area_background_transparency = 1;
        }

        $logo_area_background_color_rgba = malmo_elated_rgba_color($logo_area_background_color, $logo_area_background_transparency);

        if($logo_area_background_color_rgba !== null) {
            $logo_area_style['background-color'] = $logo_area_background_color_rgba;
        }
        /* logo area style - end */

        /* logo area in grid style - start */

        if(!$disable_logo_grid_border) {
            $logo_grid_border_color = get_post_meta($id, 'eltd_logo_area_in_grid_border_color_header_centered_meta', true);

            if($logo_grid_border_color !== '') {
                $logo_area_grid_style['border-bottom'] = '1px solid '.$logo_grid_border_color;
            }
        }

        $logo_area_grid_background_color        = get_post_meta($id, 'eltd_logo_area_grid_background_color_header_centered_meta', true);
        $logo_area_grid_background_transparency = get_post_meta($id, 'eltd_logo_area_grid_background_transparency_header_centered_meta', true);

        if($logo_area_grid_background_transparency === '') {
            $logo_area_grid_background_transparency = 1;
        }

        $logo_area_grid_background_color_rgba = malmo_elated_rgba_color($logo_area_grid_background_color, $logo_area_grid_background_transparency);

        if($logo_area_grid_background_color_rgba !== null) {
            $logo_area_grid_style['background-color'] = $logo_area_grid_background_color_rgba;
        }

        /* logo area in grid style - end */

        $style[] = malmo_elated_dynamic_css($logo_area_selector, $logo_area_style);
        $style[] = malmo_elated_dynamic_css($logo_area_grid_selector, $logo_area_grid_style);

        /* menu area style - start */

        $menu_area_background_color        = get_post_meta($id, 'eltd_menu_area_background_color_header_centered_meta', true);
        $menu_area_background_transparency = get_post_meta($id, 'eltd_menu_area_background_transparency_header_centered_meta', true);

        if($menu_area_background_transparency === '') {
            $menu_area_background_transparency = 1;
        }

        $menu_area_background_color_rgba = malmo_elated_rgba_color($menu_area_background_color, $menu_area_background_transparency);

        if($menu_area_background_color_rgba !== null) {
            $main_menu_style['background-color'] = $menu_area_background_color_rgba;
        }
        /* menu area style - end */

        /* menu area in grid style - start */

        if(!$disable_menu_grid_shadow) {
            $main_menu_grid_style['box-shadow'] = '0px 1px 3px rgba(0,0,0,0.15)';
        }

        $menu_area_grid_background_color        = get_post_meta($id, 'eltd_menu_area_grid_background_color_header_centered_meta', true);
        $menu_area_grid_background_transparency = get_post_meta($id, 'eltd_menu_area_grid_background_transparency_header_centered_meta', true);

        if($menu_area_grid_background_transparency === '') {
            $menu_area_grid_background_transparency = 1;
        }

        $menu_area_grid_background_color_rgba = malmo_elated_rgba_color($menu_area_grid_background_color, $menu_area_grid_background_transparency);

        if($menu_area_grid_background_color_rgba !== null) {
            $main_menu_grid_style['background-color'] = $menu_area_grid_background_color_rgba;
        }

        /* menu area in grid style - end */

        $style[] = malmo_elated_dynamic_css($main_menu_selector, $main_menu_style);
        $style[] = malmo_elated_dynamic_css($main_menu_grid_selector, $main_menu_grid_style);

        return $style;
    }

    /**
     * Loads template file for this header type
     *
     * @param array $parameters associative array of variables that needs to passed to template
     */
    public function loadTemplate($parameters = array()) {
        $id  = malmo_elated_get_page_id();

        $parameters['logo_area_in_grid'] = malmo_elated_get_meta_field_intersect('logo_area_in_grid_header_centered',$id) == 'yes' ? true : false;
        $parameters['menu_area_in_grid'] = malmo_elated_get_meta_field_intersect('menu_area_in_grid_header_centered',$id) == 'yes' ? true : false;

        $parameters = apply_filters('malmo_elated_header_centered_parameters', $parameters);

        malmo_elated_get_module_template_part('templates/types/'.$this->slug, $this->moduleName, '', $parameters);
    }

    /**
     * Sets header height properties after WP object is set up
     */
    public function setHeaderHeightProps() {
        $this->heightOfTransparency         = $this->calculateHeightOfTransparency();
        $this->heightOfCompleteTransparency = $this->calculateHeightOfCompleteTransparency();
        $this->headerHeight                 = $this->calculateHeaderHeight();
        $this->mobileHeaderHeight           = $this->calculateMobileHeaderHeight();
    }

    /**
     * Returns total height of transparent parts of header
     *
     * @return int
     */
    public function calculateHeightOfTransparency() {
        $id                 = malmo_elated_get_page_id();
        $transparencyHeight = 0;

        if(get_post_meta($id, 'eltd_logo_area_background_color_header_centered_meta', true) !== '') {
            $logoAreaTransparent = get_post_meta($id, 'eltd_logo_area_background_color_header_centered_meta', true) !== '' &&
                                   get_post_meta($id, 'eltd_logo_area_background_transparency_header_centered_meta', true) !== '1';
        } elseif(malmo_elated_options()->getOptionValue('logo_area_background_color_header_centered') == '') {
            $logoAreaTransparent = malmo_elated_options()->getOptionValue('logo_area_grid_background_color_header_centered') !== '' &&
                                   malmo_elated_options()->getOptionValue('logo_area_grid_background_transparency_header_centered') !== '1';
        } else {
            $logoAreaTransparent = malmo_elated_options()->getOptionValue('logo_area_background_color_header_centered') !== '' &&
                                   malmo_elated_options()->getOptionValue('logo_area_background_transparency_header_centered') !== '1';
        }

        if(get_post_meta($id, 'eltd_menu_area_background_color_header_centered_meta', true) !== '') {
            $menuAreaTransparent = get_post_meta($id, 'eltd_menu_area_background_color_header_centered_meta', true) !== '' &&
                                   get_post_meta($id, 'eltd_menu_area_background_transparency_header_centered_meta', true) !== '1';
        } elseif(malmo_elated_options()->getOptionValue('menu_area_background_color_header_centered') == '') {
            $menuAreaTransparent = malmo_elated_options()->getOptionValue('menu_area_grid_background_color_header_centered') !== '' &&
                                   malmo_elated_options()->getOptionValue('menu_area_grid_background_transparency_header_centered') !== '1';
        } else {
            $menuAreaTransparent = malmo_elated_options()->getOptionValue('menu_area_background_color_header_centered') !== '' &&
                                   malmo_elated_options()->getOptionValue('menu_area_background_transparency_header_centered') !== '1';
        }


        $contentBehindHeader = get_post_meta($id, 'eltd_page_content_behind_header_meta', true) === 'yes';

        if($contentBehindHeader) {
            $menuAreaTransparent = true;
            $logoAreaTransparent = true;
        }

        if($logoAreaTransparent || $menuAreaTransparent) {
            if($logoAreaTransparent) {
                $transparencyHeight = $this->logoAreaHeight + $this->menuAreaHeight;

                if(malmo_elated_is_top_bar_enabled() && malmo_elated_is_top_bar_transparent()) {
                    $transparencyHeight += malmo_elated_get_top_bar_height();
                }
            }

            if(!$logoAreaTransparent && $menuAreaTransparent) {
                $transparencyHeight = $this->menuAreaHeight;
            }
        }

        return $transparencyHeight;
    }

    /**
     * Returns height of completely transparent header parts
     *
     * @return int
     */
    public function calculateHeightOfCompleteTransparency() {
        $id                 = malmo_elated_get_page_id();
        $transparencyHeight = 0;

        if(get_post_meta($id, 'eltd_logo_area_background_color_header_centered_meta', true) !== '') {
            $logoAreaTransparent = get_post_meta($id, 'eltd_logo_area_background_color_header_centered_meta', true) !== '' &&
                                   get_post_meta($id, 'eltd_logo_area_background_transparency_header_centered_meta', true) === '0';
        } elseif(malmo_elated_options()->getOptionValue('logo_area_background_color_header_centered') == '') {
            $logoAreaTransparent = malmo_elated_options()->getOptionValue('logo_area_grid_background_color_header_centered') !== '' &&
                                   malmo_elated_options()->getOptionValue('logo_area_grid_background_transparency_header_centered') === '0';
        } else {
            $logoAreaTransparent = malmo_elated_options()->getOptionValue('logo_area_background_color_header_centered') !== '' &&
                                   malmo_elated_options()->getOptionValue('logo_area_background_transparency_header_centered') === '0';
        }


        if(get_post_meta($id, 'eltd_menu_area_background_color_header_centered_meta', true) !== '') {
            $menuAreaTransparent = get_post_meta($id, 'eltd_menu_area_background_color_header_centered_meta', true) !== '' &&
                                   get_post_meta($id, 'eltd_menu_area_background_transparency_header_centered_meta', true) === '0';
        } elseif(malmo_elated_options()->getOptionValue('menu_area_background_color_header_centered') == '') {
            $menuAreaTransparent = malmo_elated_options()->getOptionValue('menu_area_grid_background_color_header_centered') !== '' &&
                                   malmo_elated_options()->getOptionValue('menu_area_grid_background_transparency_header_centered') === '0';
        } else {
            $menuAreaTransparent = malmo_elated_options()->getOptionValue('menu_area_background_color_header_centered') !== '' &&
                                   malmo_elated_options()->getOptionValue('menu_area_background_transparency_header_centered') === '0';
        }


        if($logoAreaTransparent || $menuAreaTransparent) {
            if($logoAreaTransparent) {
                $transparencyHeight = $this->logoAreaHeight + $this->menuAreaHeight;

                if(malmo_elated_is_top_bar_enabled() && malmo_elated_is_top_bar_completely_transparent()) {
                    $transparencyHeight += malmo_elated_get_top_bar_height();
                }
            }

            if(!$logoAreaTransparent && $menuAreaTransparent) {
                $transparencyHeight = $this->menuAreaHeight;
            }
        }

        return $transparencyHeight;
    }


    /**
     * Returns total height of header
     *
     * @return int|string
     */
    public function calculateHeaderHeight() {
        $headerHeight = $this->logoAreaHeight + $this->menuAreaHeight;
        if(malmo_elated_is_top_bar_enabled()) {
            $headerHeight += malmo_elated_get_top_bar_height();
        }

        return $headerHeight;
    }

    /**
     * Returns total height of mobile header
     *
     * @return int|string
     */
    public function calculateMobileHeaderHeight() {
        $mobileHeaderHeight = $this->mobileHeaderHeight;

        return $mobileHeaderHeight;
    }

    /**
     * Returns global js variables of header
     *
     * @param $globalVariables
     *
     * @return int|string
     */
    public function getGlobalJSVariables($globalVariables) {
        $globalVariables['eltdLogoAreaHeight']     = $this->logoAreaHeight;
        $globalVariables['eltdMenuAreaHeight']     = $this->menuAreaHeight;
        $globalVariables['eltdMobileHeaderHeight'] = $this->mobileHeaderHeight;

        return $globalVariables;
    }

    /**
     * Returns per page js variables of header
     *
     * @param $perPageVars
     *
     * @return int|string
     */
    public function getPerPageJSVariables($perPageVars) {
        //calculate transparency height only if header has no sticky behaviour
        if(!in_array(malmo_elated_get_meta_field_intersect('header_behaviour'), array(
            'sticky-header-on-scroll-up',
            'sticky-header-on-scroll-down-up'
        ))
        ) {
            $perPageVars['eltdHeaderTransparencyHeight'] = $this->headerHeight - (malmo_elated_get_top_bar_height() + $this->heightOfCompleteTransparency);
        } else {
            $perPageVars['eltdHeaderTransparencyHeight'] = 0;
        }

        return $perPageVars;
    }
}