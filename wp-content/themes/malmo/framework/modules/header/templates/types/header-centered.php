<?php do_action('malmo_elated_before_page_header'); ?>

<header class="eltd-page-header">
    <div class="eltd-logo-area">
        <?php if($logo_area_in_grid) : ?>
        <div class="eltd-grid">
            <?php endif; ?>
            <?php do_action('malmo_elated_after_header_logo_area_html_open') ?>
            <div class="eltd-vertical-align-containers">
                <div class="eltd-position-center">
                    <div class="eltd-position-center-inner">
                        <?php if(!$hide_logo) {
                            malmo_elated_get_logo('centered');
                        } ?>
                    </div>
                </div>
            </div>
            <?php if($logo_area_in_grid) : ?>
        </div>
    <?php endif; ?>
    </div>
    <?php if($show_fixed_wrapper) : ?>
    <div class="eltd-fixed-wrapper">
        <?php endif; ?>
        <div class="eltd-menu-area">
            <?php if($menu_area_in_grid) : ?>
            <div class="eltd-grid">
                <?php endif; ?>
                <?php do_action('malmo_elated_after_header_menu_area_html_open') ?>
                <div class="eltd-vertical-align-containers">
                    <div class="eltd-position-center">
                        <div class="eltd-position-center-inner">
                            <?php malmo_elated_get_main_menu(); ?>
                        </div>
                    </div>

                </div>
                <?php if($menu_area_in_grid) : ?>
            </div>
        <?php endif; ?>
        </div>
        <?php if($show_fixed_wrapper) : ?>
    </div>
<?php endif; ?>
    <?php if($show_sticky) {
        malmo_elated_get_sticky_header('centered');
    } ?>
</header>

<?php do_action('malmo_elated_after_page_header'); ?>

