<?php

if(!function_exists('malmo_elated_header_class')) {
	/**
	 * Function that adds class to header based on theme options
	 *
	 * @param array array of classes from main filter
	 *
	 * @return array array of classes with added header class
	 */
	function malmo_elated_header_class($classes) {
		$header_type = malmo_elated_get_meta_field_intersect('header_type', malmo_elated_get_page_id());

		$classes[] = 'eltd-'.$header_type;

		return $classes;
	}

	add_filter('body_class', 'malmo_elated_header_class');
}

if(!function_exists('malmo_elated_header_behaviour_class')) {
	/**
	 * Function that adds behaviour class to header based on theme options
	 *
	 * @param array array of classes from main filter
	 *
	 * @return array array of classes with added behaviour class
	 */
	function malmo_elated_header_behaviour_class($classes) {

		$classes[] = 'eltd-'.malmo_elated_options()->getOptionValue('header_behaviour');

		return $classes;
	}

	add_filter('body_class', 'malmo_elated_header_behaviour_class');
}

if(!function_exists('malmo_elated_mobile_header_class')) {
	/**
	 * @param $classes
	 *
	 * @return array
	 */
	function malmo_elated_mobile_header_class($classes) {
		$classes[] = 'eltd-default-mobile-header';

		$classes[] = 'eltd-sticky-up-mobile-header';

		return $classes;
	}

	add_filter('body_class', 'malmo_elated_mobile_header_class');
}

if(!function_exists('malmo_elated_header_class_first_level_bg_color')) {
	/**
	 * Function that adds first level menu background color class to header tag
	 *
	 * @param array array of classes from main filter
	 *
	 * @return array array of classes with added first level menu background color class
	 */
	function malmo_elated_header_class_first_level_bg_color($classes) {

		//check if first level hover background color is set
		if(malmo_elated_options()->getOptionValue('menu_hover_background_color') !== '') {
			$classes[] = 'eltd-menu-item-first-level-bg-color';
		}

		return $classes;
	}

	add_filter('body_class', 'malmo_elated_header_class_first_level_bg_color');
}

if(!function_exists('malmo_elated_menu_dropdown_appearance')) {
	/**
	 * Function that adds menu dropdown appearance class to body tag
	 *
	 * @param array array of classes from main filter
	 *
	 * @return array array of classes with added menu dropdown appearance class
	 */
	function malmo_elated_menu_dropdown_appearance($classes) {

		if(malmo_elated_options()->getOptionValue('menu_dropdown_appearance') !== 'default') {
			$classes[] = 'eltd-'.malmo_elated_options()->getOptionValue('menu_dropdown_appearance');
		}

		return $classes;
	}

	add_filter('body_class', 'malmo_elated_menu_dropdown_appearance');
}

if(!function_exists('malmo_elated_header_skin_class')) {

	/**
	 * @param $classes
	 *
	 * @return array
	 */
	function malmo_elated_header_skin_class($classes) {

		$id = malmo_elated_get_page_id();

		if(($meta_temp = get_post_meta($id, 'eltd_header_style_meta', true)) !== '') {
			$classes[] = 'eltd-'.$meta_temp;
		} else if(malmo_elated_options()->getOptionValue('header_style') !== '') {
			$classes[] = 'eltd-'.malmo_elated_options()->getOptionValue('header_style');
		}

		return $classes;

	}

	add_filter('body_class', 'malmo_elated_header_skin_class');

}

if(!function_exists('malmo_elated_header_scroll_style_class')) {

	/**
	 * @param $classes
	 *
	 * @return array
	 */
	function malmo_elated_header_scroll_style_class($classes) {

		if(malmo_elated_get_meta_field_intersect('enable_header_style_on_scroll') == 'yes') {
			$classes[] = 'eltd-header-style-on-scroll';
		}

		return $classes;

	}

	add_filter('body_class', 'malmo_elated_header_scroll_style_class');

}

if(!function_exists('malmo_elated_header_global_js_var')) {
	/**
	 * @param $global_variables
	 *
	 * @return mixed
	 */
	function malmo_elated_header_global_js_var($global_variables) {

		$global_variables['eltdTopBarHeight']                   = malmo_elated_get_top_bar_height();
		$global_variables['eltdStickyHeaderHeight']             = malmo_elated_get_sticky_header_height();
		$global_variables['eltdStickyHeaderTransparencyHeight'] = malmo_elated_get_sticky_header_height_of_complete_transparency();

		return $global_variables;
	}

	add_filter('malmo_elated_js_global_variables', 'malmo_elated_header_global_js_var');
}

if(!function_exists('malmo_elated_header_per_page_js_var')) {
	/**
	 * @param $perPageVars
	 *
	 * @return mixed
	 */
	function malmo_elated_header_per_page_js_var($perPageVars) {
		$id = malmo_elated_get_page_id();

		$perPageVars['eltdStickyScrollAmount']           = malmo_elated_get_sticky_scroll_amount();
		$perPageVars['eltdStickyScrollAmountFullScreen'] = get_post_meta($id, 'eltd_scroll_amount_for_sticky_fullscreen_meta', true) === 'yes';

		return $perPageVars;
	}

	add_filter('malmo_elated_per_page_js_vars', 'malmo_elated_header_per_page_js_var');
}

if(!function_exists('malmo_elated_full_width_wide_menu_class')) {
	/**
	 * @param $classes
	 *
	 * @return array
	 */
	function malmo_elated_full_width_wide_menu_class($classes) {
		if(malmo_elated_options()->getOptionValue('enable_wide_menu_background') === 'yes') {
			$classes[] = 'eltd-full-width-wide-menu';
		}

		return $classes;
	}

	add_filter('body_class', 'malmo_elated_full_width_wide_menu_class');
}

if(!function_exists('malmo_elated_header_bottom_border_class')) {
	/**
	 * @param $classes
	 *
	 * @return array
	 */
	function malmo_elated_header_bottom_border_class($classes) {
		$id = malmo_elated_get_page_id();

		$disable_border = get_post_meta($id, 'eltd_menu_area_bottom_border_disable_header_standard_meta', true) == 'yes';
		if($disable_border) {
			$classes[] = 'eltd-header-standard-border-disable';
		}

		return $classes;
	}

	add_filter('body_class', 'malmo_elated_header_bottom_border_class');
}

if(!function_exists('malmo_elated_get_top_bar_styles')) {
	/**
	 * Sets per page styles for header top bar
	 *
	 * @param $styles
	 *
	 * @return array
	 */
	function malmo_elated_get_top_bar_styles($styles) {
		$id            = malmo_elated_get_page_id();
		$class_prefix  = malmo_elated_get_unique_page_class();
		$top_bar_style = array();

		$top_bar_bg_color = get_post_meta($id, 'eltd_top_bar_background_color_meta', true);

		$top_bar_selector = array(
			$class_prefix.' .eltd-top-bar'
		);

		if($top_bar_bg_color !== '') {
			$top_bar_transparency = get_post_meta($id, 'eltd_top_bar_background_transparency_meta', true);
			if($top_bar_transparency === '') {
				$top_bar_transparency = 1;
			}

			$top_bar_style['background-color'] = malmo_elated_rgba_color($top_bar_bg_color, $top_bar_transparency);
		}

		$styles[] = malmo_elated_dynamic_css($top_bar_selector, $top_bar_style);

		return $styles;
	}

	add_filter('malmo_elated_add_page_custom_style', 'malmo_elated_get_top_bar_styles');
}

if(!function_exists('malmo_elated_top_bar_skin_class')) {
	/**
	 * @param $classes
	 *
	 * @return array
	 */
	function malmo_elated_top_bar_skin_class($classes) {
		$id           = malmo_elated_get_page_id();
		$top_bar_skin = get_post_meta($id, 'eltd_top_bar_skin_meta', true);

		if($top_bar_skin !== '') {
			$classes[] = 'eltd-top-bar-'.$top_bar_skin;
		}

		return $classes;
	}

	add_filter('body_class', 'malmo_elated_top_bar_skin_class');
}