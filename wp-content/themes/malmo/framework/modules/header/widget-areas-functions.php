<?php

if(!function_exists('malmo_elated_register_top_header_areas')) {
	/**
	 * Registers widget areas for top header bar when it is enabled
	 */
	function malmo_elated_register_top_header_areas() {
		$top_bar_layout = malmo_elated_options()->getOptionValue('top_bar_layout');

		register_sidebar(array(
			'name'          => esc_html__('Top Bar Left', 'malmo'),
			'id'            => 'eltd-top-bar-left',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-top-bar-widget"><div class="eltd-top-bar-widget-inner">',
			'after_widget'  => '</div></div>',
			'description'   => esc_html__('Widgets added here will appear on the left side of top bar', 'malmo')
		));

		//register this widget area only if top bar layout is three columns
		if($top_bar_layout === 'three-columns') {
			register_sidebar(array(
				'name'          => esc_html__('Top Bar Center', 'malmo'),
				'id'            => 'eltd-top-bar-center',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-top-bar-widget"><div class="eltd-top-bar-widget-inner">',
				'after_widget'  => '</div></div>',
				'description'   => esc_html__('Widgets added here will appear on the center side of top bar', 'malmo')
			));
		}

		register_sidebar(array(
			'name'          => esc_html__('Top Bar Right', 'malmo'),
			'id'            => 'eltd-top-bar-right',
			'before_widget' => '<div id="%1$s" class="widget %2$s eltd-top-bar-widget"><div class="eltd-top-bar-widget-inner">',
			'after_widget'  => '</div></div>',
			'description'   => esc_html__('Widgets added here will appear on the right side of top bar', 'malmo')
		));
	}

	add_action('widgets_init', 'malmo_elated_register_top_header_areas');
}

if(!function_exists('malmo_elated_header_standard_widget_areas')) {
	/**
	 * Registers widget areas for standard header type
	 */
	function malmo_elated_header_standard_widget_areas() {
		if(malmo_elated_options()->getOptionValue('header_type') == 'header-standard' && malmo_elated_core_plugin_installed()) {
			register_sidebar(array(
				'name'          => esc_html__('Right From Main Menu', 'malmo'),
				'id'            => 'eltd-right-from-main-menu',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-right-from-main-menu-widget"><div class="eltd-right-from-main-menu-widget-inner">',
				'after_widget'  => '</div></div>',
				'description'   => esc_html__('Widgets added here will appear on the right hand side from the main menu', 'malmo')
			));
		}
	}

	add_action('widgets_init', 'malmo_elated_header_standard_widget_areas');
}

if(!function_exists('malmo_elated_header_vertical_widget_areas')) {
	/**
	 * Registers widget areas for vertical header
	 */
	function malmo_elated_header_vertical_widget_areas() {
			register_sidebar(array(
				'name'          => esc_html__('Vertical Area', 'malmo'),
				'id'            => 'eltd-vertical-area',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-vertical-area-widget">',
				'after_widget'  => '</div>',
				'description'   => esc_html__('Widgets added here will appear on the bottom of vertical menu', 'malmo')
			));
	}

	add_action('widgets_init', 'malmo_elated_header_vertical_widget_areas');
}

if(!function_exists('malmo_elated_register_mobile_header_areas')) {
	/**
	 * Registers widget areas for mobile header
	 */
	function malmo_elated_register_mobile_header_areas() {
		if(malmo_elated_is_responsive_on() && malmo_elated_core_installed() ) {
			register_sidebar(array(
				'name'          => esc_html__('Right From Mobile Logo', 'malmo'),
				'id'            => 'eltd-right-from-mobile-logo',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-right-from-mobile-logo">',
				'after_widget'  => '</div>',
				'description'   => esc_html__('Widgets added here will appear on the right hand side from the mobile logo', 'malmo')
			));
		}
	}

	add_action('widgets_init', 'malmo_elated_register_mobile_header_areas');
}

if(!function_exists('malmo_elated_register_sticky_header_areas')) {
	/**
	 * Registers widget area for sticky header
	 */
	function malmo_elated_register_sticky_header_areas() {
		if(in_array(malmo_elated_options()->getOptionValue('header_behaviour'), array(
			'sticky-header-on-scroll-up',
			'sticky-header-on-scroll-down-up'
		))) {
			register_sidebar(array(
				'name'          => esc_html__('Sticky Right', 'malmo'),
				'id'            => 'eltd-sticky-right',
				'before_widget' => '<div id="%1$s" class="widget %2$s eltd-sticky-right-widget"><div class="eltd-sticky-right-widget-inner">',
				'after_widget'  => '</div></div>',
				'description'   => esc_html__('Widgets added here will appear on the right hand side in sticky menu', 'malmo')
			));
		}
	}

	add_action('widgets_init', 'malmo_elated_register_sticky_header_areas');
}