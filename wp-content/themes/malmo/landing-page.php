<?php
/*
Template Name: Landing Page
*/
$malmo_sidebar = malmo_elated_sidebar_layout();

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php if(!malmo_elated_is_ajax_request()) {
		malmo_elated_wp_title();
	} ?>

	<?php
	/**
	 * malmo_elated_header_meta hook
	 *
	 * @see malmo_elated_header_meta() - hooked with 10
	 * @see eltd_user_scalable_meta() - hooked with 10
	 */
	if(!malmo_elated_is_ajax_request()) {
		do_action('malmo_elated_header_meta');
	}
	?>

	<?php if(!malmo_elated_is_ajax_request()) {
		wp_head();
	} ?>
</head>

<body <?php body_class(); ?>>

<?php
if((!malmo_elated_is_ajax_request()) && malmo_elated_options()->getOptionValue('smooth_page_transitions') == "yes") {
	$ajax_class = malmo_elated_options()->getOptionValue('smooth_pt_true_ajax') === 'yes' ? 'eltd-ajax' : 'eltd-mimic-ajax';
	?>
	<div class="eltd-smooth-transition-loader <?php echo esc_attr($ajax_class); ?>">
		<div class="eltd-st-loader">
			<div class="eltd-st-loader1">
				<?php malmo_elated_loading_spinners(); ?>
			</div>
		</div>
	</div>
<?php } ?>

<div class="eltd-wrapper">
	<div class="eltd-wrapper-inner">
		<div class="eltd-content">
			<?php if(malmo_elated_is_ajax_enabled()) { ?>
				<div class="eltd-meta">
					<?php do_action('malmo_elated_ajax_meta'); ?>
					<span id="eltd-page-id"><?php echo esc_html($wp_query->get_queried_object_id()); ?></span>

					<div class="eltd-body-classes"><?php echo esc_html(implode(',', get_body_class())); ?></div>
				</div>
			<?php } ?>
			<div class="eltd-content-inner">
				<?php malmo_elated_get_title(); ?>
				<?php get_template_part('slider'); ?>
				<div class="eltd-full-width">
					<div class="eltd-full-width-inner">
						<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
							<div class="eltd-grid-row">
								<div <?php echo malmo_elated_get_content_sidebar_class(); ?>>
									<?php the_content(); ?>
									<?php do_action('malmo_elated_page_after_content'); ?>
								</div>

								<?php if(!in_array($malmo_sidebar, array('default', ''))) : ?>
									<div <?php echo malmo_elated_get_sidebar_holder_class(); ?>>
										<?php get_sidebar(); ?>
									</div>
								<?php endif; ?>
							</div>
						<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>